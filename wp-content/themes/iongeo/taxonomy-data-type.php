<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Iongeo
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<?php
			$termslug = get_query_var( 'term' );
			$term = get_term_by( 'slug', $termslug, 'data-type');
			$fImg = get_field('featured_image', $term);
			?>

      <header id="page-header" class="full-width-image bg-centered" style="background-image:url(<?php echo $fImg['sizes']['large']; ?>);">
				<div id="page-header-title" class="content-inner">
					<div class="page-header-title-content">
						<h1><?php echo $term->name; ?></h1>
					</div>
				</div>
			</header>
			<section class="page-content-wrapper content-inner<?php echo is_active_sidebar( 'location-sidebar' ) ? ' has-sidebar' : ''; ?>">
				<div class="page-content-container flex">
					<div class="page-content col-8">
						<h3>Overview</h3>
						<?php echo term_description( $term->term_id, 'data-type' ) ?>
					</div>

					<div id="sidebar" class="col-4">
						<?php
						// Get technologies for sidebar
						if( $sidebar_image = get_field('sidebar_image', $term) ) { ?>
							<div id="sidebar-featured-image" style="background-image: url('<?php echo $sidebar_image['sizes']['medium']; ?>')">

							</div>
						<?php
						}
						$techArgs = [
							'post_type' => 'technology',
							'posts_per_page' => 5,
							'tax_query' => [
								[
									'taxonomy' => 'data-type',
									'field' => 'term_id',
									'terms' => $term->term_id
								]
							]
						];
						$techs = new WP_Query($techArgs);
						if($techs->have_posts()) : ?>
							<div class="sidebar-ion-related" style="margin-bottom: 40px;">
								<h4>Related Technologies</h4>
								<?php
						  	while($techs->have_posts()): $techs->the_post();
									$post_title = explode( ' – ', get_the_title() );
									$post_title = $post_title[0];
									// $post_title = get_the_title(); ?>
									<a class="post-link" href="<?php echo get_the_permalink(); ?>">
										<?php echo $post_title; ?>
									</a>
								<?php
								endwhile;
								wp_reset_postdata(); ?>
							</div>
						<?php
						endif;
						if ( is_active_sidebar( 'data-type-sidebar' ) ) :
							dynamic_sidebar( 'data-type-sidebar' );
						endif; ?>
					</div>
				</div>
				<?php
				$featuredPrograms = get_field('featured_locations', $term)
					? get_field('featured_locations', $term)
					: [];
				if(count($featuredPrograms) > 0) { ?>
					<div id="featured-programs-post-row" class="post-row">
		        <h2>Featured Programs</h2>
		        <div class="post-row-content column-count-3 flex row">
		          <?php
		          foreach($featuredPrograms as $program) { ?>
		            <div class="post-row-item col-4">
		              <?php
		              $image = has_post_thumbnail($program)
										? get_the_post_thumbnail_url($program)
										: get_template_directory_uri().'/images/post-placeholder.png'; ?>
		              <a href="<?php echo get_the_permalink($program); ?>" class="bg-centered post-row-image-container ratio-image-container ratio-3-1">
		                <img class="post-row-image" src="<?php echo $image; ?>"/>
		              </a>
		              <h6 class="post-row-title">
		                <a href="<?php echo get_the_permalink($program); ?>">
		                  <?php echo get_the_title($program); ?>
		                </a>
		              </h6>
		            </div>
		          <?php
		          } ?>
		        </div>
		      </div>
			  <?php
				}

				$loc_arr = [];
				$locArgs = [
					'post_type' => 'data-library',
					'posts_per_page' => -1,
					'tax_query' => [
						[
							'taxonomy' => 'data-type',
							'field' => 'term_id',
							'terms' => $term->term_id
						]
					]
				];
				$parentLocs = new WP_Query($locArgs);
				if($parentLocs->have_posts()) :
			  	while($parentLocs->have_posts()): $parentLocs->the_post();
						$loc_title = get_the_title();
						$loc_link = get_the_permalink();
						$ancestors = get_ancestors(get_the_ID(), 'page');
						$ancestor_id = $ancestors[count($ancestors) - 1];
				    $parent_post = get_post($ancestor_id);
				    $parent_post_title = $parent_post->post_title;
						if( array_key_exists($parent_post_title, $loc_arr) ) {
							array_push( $loc_arr[$parent_post_title], ['name' => $loc_title, 'link' => $loc_link] );
						} else {
							$loc_arr[$parent_post_title] = [
								['name' => $loc_title, 'link' => $loc_link]
							];
						}
			    endwhile;
			   	wp_reset_postdata();
				endif;

				foreach($loc_arr as $key=>$loc) { ?>
					<section class="related-offerings-container content-inner">
			      <div>
			  			<h2><?php echo $key; ?></h2>
			  			<div id="related-offerings">
			          <div class="flex row">
			    				<?php
			    				foreach($loc as $locItem) { ?>
			    					<a class="col-3" href="<?php echo $locItem['link']; ?>">
			    						<?php echo $locItem['name']; ?>
			    					</a>
			    				<?php
									} ?>
			    			</div>
			        </div>
			      </div>
					</section>
				<?php
				}
				if($insights = get_field('post_x_data-type', $term)) {
					echo get_related_insights($insights);
				} ?>
			</section>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
