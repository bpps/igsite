<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Iongeo
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

		<?php
		while ( have_posts() ) : the_post();
			$headerLayout = get_field('header_layout');
			$pageLayout = get_field('page_layout'); ?>
			<header id="page-header" class="<?php echo $headerLayout; ?>-header">
				<div id="page-header-title" class="content-inner">
					<div class="page-header-title-content">
						<h1><?php the_title(); ?></h1>
					</div>
				</div>
			</header>
			<?php echo get_share_link('ION Geo | '.get_the_title(), get_the_permalink()); ?>
			<section class="page-content-wrapper">
				<div class="page-content-container <?php echo $pageLayout; ?>-page has-sidebar content-inner">

						<div id="tech-landing">
							<?php
							if($technologies = get_terms([
								'taxonomy' => 'technology-type',
								'hide_empty' => false,
								'parent' => 0
							])) { ?>
								<?php side_scroll_nav('technologies'); ?>
							<?php
							$featuredTechs = get_field('featured_technologies', 'option');
							foreach($technologies as $tech) {
								$techParent = $tech->term_id;
								$techCount = 0; ?>
								<div class="tech-type side-scroll-item">
									<div class="tech-items grid">
										<h2><?php echo $tech->name; ?></h2>
										<?php
										$args = [
											'posts_per_page' => 1,
											'post_type' => 'technology',
											'post__in' => $featuredTechs,
											'tax_query' => [
												[
													'taxonomy' => 'technology-type',
													'field' => 'id',
													'terms' => $tech->term_id
												]
											]
										];
										// Get featured post if it exists
										$techPosts = new WP_Query($args);
									  if($techPosts->have_posts()) :
									    while($techPosts->have_posts()): $techPosts->the_post(); ?>
									      <div class="tech-item featured-post">
													<h5>
										        <a href="<?php echo get_the_permalink(); ?>">
										          Featured
										        </a>
													</h5>
													<?php
													if(has_post_thumbnail()) {
														$image = get_the_post_thumbnail_url(get_the_id(), 'small-medium'); ?>
														<a class="tech-image-container" href="<?php echo get_the_permalink(); ?>">
															<div class="tech-image bg-centered" style="background-image:url(<?php echo $image; ?>);">
															</div>
														</a>
													<?php
													} ?>
													<h6 class="featured-post-label">
														<a href="<?php echo get_the_permalink(); ?>">
															<?php the_title(); ?>
														</a>
													</h6>
									      </div>
									    <?php
											$techCount++;
									    endwhile;
									    wp_reset_postdata();
									  endif;
										// Get sub technologies
										if($subtechs = get_terms([
											'taxonomy' => 'technology-type',
											'hide_empty' => false,
											'parent' => $techParent,
											'number' => 3 - $techCount
										])) {
											foreach($subtechs as $tech) {
												$termLink = get_term_link($tech); ?>
												<div class="tech-item">
													<h5>
										        <a href="<?php echo $termLink; ?>">
										          <?php echo $tech->name; ?>
										        </a>
													</h5>
													<?php
													if($image = get_field('featured_image', $tech)) {
														$image = $image['sizes']['small-medium']; ?>
														<a class="tech-image-container" href="<?php echo $termLink; ?>">
															<div class="tech-image bg-centered" style="background-image:url(<?php echo $image; ?>);">
															</div>
														</a>
													<?php
													}
													if($description = $tech->description) { ?>
														<div class="tech-description">
															<?php echo $description; ?>
														</div>
													<?php
													} ?>
									      </div>
											<?php
											}
										} ?>
									</div>
								</div>
							<?php
							} ?>
						</div>
					<?php
					} ?>

				</div>
			</section>
		<?php
		endwhile; // End of the loop.
		?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
