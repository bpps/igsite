<?php
/**
 * The template for displaying resource taxonomy pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Iongeo
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<?php
			$term = get_queried_object();
			?>

			<?php
			if($investorHeader = get_field('global_investors_header_image', 'option')) {
				$headerImage = ['large' => $investorHeader['sizes']['large'], 'small' => $investorHeader['sizes']['small']];
			}
			$header = ion_page_header([ 'fimg' => $headerImage, 'frompage' => 'page', 'title' => $term->name]);
			echo $header->header;
			echo get_share_link('ION Geo Resources | '.$term->name, get_term_link($term)); ?>
			<section class="page-content-wrapper content-inner">
				<div class="page-content-container has-header-image">
					<?php
					$paged = 1;
					if(isset($_GET['page'])) {
						$paged = $_GET['page'];
					}
					$assoc_post_type = wpse_172645_get_post_types_by_taxonomy( $term->taxonomy );
					$taxes = get_object_taxonomies( $assoc_post_type[0] );
					$filter = [];
					foreach($taxes as $tax) {
						if(isset($_GET[$tax])) {
							array_push($filter, ['taxonomy' => $tax, 'field' => 'slug', 'terms' => explode(",",$_GET[$tax])]);
						}
					}
					$filter_props = [
						'type' => $term->taxonomy,
						'tType' => $term,
						'pType' => $assoc_post_type[0],
						'filters' => $filter,
						'paged' => $paged
					];
					echo create_list_by_term($filter_props); ?>
				</div>
			</section>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
