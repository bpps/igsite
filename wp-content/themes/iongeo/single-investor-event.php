<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Iongeo
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

		<?php
		while ( have_posts() ) : the_post();
			$eventdate = strtr(get_field('event_date'), '/', '-');
			$eventID = get_the_ID();
			$content = get_the_content(); ?>
			<header id="page-header" class="standard-header">
				<div id="page-header-title" class="content-inner">
					<div class="event-header">
						<div class="event-date">
							<?php echo date("Y", strtotime($eventdate)).'</br>'.date("M", strtotime($eventdate)).' '.date("j", strtotime($eventdate)); ?>
						</div>
						<div class="event-title">
							<h1><?php the_title(); ?></h1>
							<?php
							if($location = get_field('event_location')) { ?>
								<p><?php echo $location; ?></p>
							<?php
							} ?>
						</div>
					</div>
				</div>
				<div class="page-header-content">
				</div>
			</header>
			<?php
			$featured_image = has_post_thumbnail() ? get_the_post_thumbnail_url(get_the_ID(), 'medium') : false;
			echo get_share_link(get_the_title(), get_the_permalink()); ?>
			<section class="page-content-wrapper content-inner<?php echo is_active_sidebar( 'insights-sidebar' ) ? ' has-sidebar' : ''; ?>">
				<div class="page-content-container flex">
					<?php
					custom_sidebar('insights-sidebar', $featured_image); ?>
					<div class="page-content col-8">
						<?php echo apply_filters('the_content', $content); ?>
						<?php
						if($talks = get_field('featured_talks')) { ?>
							<div id="featured-talks">
								<h3>Featured Talk</h3>
								<?php
								foreach($talks as $talk) {
									$talkImage = $talk['image'] ? $talk['image']['sizes']['small'] : get_template_directory_uri().'/images/post-placeholder.png'; ?>
									<div class="featured-talk">
										<div class="talk-image ratio-1-1 bg-centered" style="background-image:url(<?php echo $talkImage; ?>)">
										</div>
										<div class="talk-date-container">
											<div class="talk-date">
												<?php echo date("D", strtotime($talk['date'])).'</br>'.date("M j", strtotime($talk['date'])); ?>
											</div>
											<?php
												if($talk['time']) { ?>
													<div class="talk-time">
														<?php echo $talk['time']; ?>
													</div>
												<?php
												} ?>
										</div>
										<div class="talk-content">
											<h4><?php echo $talk['title']; ?></h4>
											<?php
											if($talk['speaker']) { ?>
												<p><?php echo $talk['speaker']; ?></p>
											<?php
											} ?>
										</div>
									</div>
								<?php
								} ?>
							</div>
						<?php
						} ?>
					</div>
				</div>
				<?php
				if ( !has_shortcode( $content, 'content-tabs' ) ) {
					echo content_tabs();
				}
				$date = date('Y-m-d H:i:s',strtotime("today"));
				$args = [
					'post_type' => 'investor-event',
					'posts_per_page' => 3,
					'post__not_in' => [$eventID],
					'meta_query' => [
						'relation' => 'OR',
						[
							'key'       => 'event_end_date',
							'value'     => $date,
							'compare'   => '>=',
							'type'      => 'DATETIME'
						],
						[
							'key'       => 'event_date',
							'value'     => $date,
							'compare'   => '>=',
							'type'      => 'DATETIME'
						]
					]
				];
				$events = new WP_Query($args);
			  if($events->have_posts()) : ?>
					<section class="post-row">
						<h2>More Events</h2>
						<div class="post-row-content column-count-3 flex row">
			        <?php
			        while($events->have_posts()): $events->the_post();
								$event_link = get_field('external_link', $eventID) ? get_field('external_link', $eventID) : get_the_permalink();
								$event_target = get_field('external_link', $eventID) ? '_blank' : '_self'; ?>
				        <div class="post-row-item col-4">
				          <?php
									$date = get_field('event_date');
				          $image = has_post_thumbnail() ? get_the_post_thumbnail_url(get_the_id(), 'small-medium') : get_template_directory_uri().'/images/post-placeholder.png'; ?>
									<div class="event-header">
						        <div class="event-date">
						          <?php
						          $month = date("M", strtotime($date));
						          $day = date("d", strtotime($date));
						          echo $month; ?></br>
						          <?php echo $day; ?>
						        </div>
										<div class="event-header-content">
							        <h4 class="event-title">
							          <a
													target="<?php echo $event_target; ?>"
													href="<?php echo $event_link; ?>">
							            <?php the_title() ?>
							          </a>
							        </h4>
										</div>
						      </div>
									<a href="<?php echo $event_link; ?>" target="<?php echo $event_target; ?>" class="post-row-image-container ratio-image-container ratio-3-2">
				            <div class="post-row-image bg-centered animated-image" style="background-image:url('<?php echo $image; ?>');">

				            </div>
				          </a>
									<?php
						      if(get_field('event_location') || has_excerpt()) { ?>
						        <div class="event-description">
						          <?php
						          if($location) { ?>
						            <span><?php echo get_field('event_location'); ?></span>
						          <?php
						          }
						          if(has_excerpt()) {
						            echo get_the_excerpt();
						          } ?>
						        </div>
						      <?php
						      } ?>
				        </div>
						      <?php
								endwhile;
								wp_reset_postdata(); ?>
						</div>
					</section>
				<?php
				endif; ?>
			</section>
			<?php
			if( strtotime($eventdate) > strtotime('now') ) { // Future events
				echo get_footer_cta('events');
			} else {
				echo get_footer_cta('past_events');
			}
		endwhile; // End of the loop.
		?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
