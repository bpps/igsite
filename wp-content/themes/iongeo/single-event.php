<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Iongeo
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

		<?php
		while ( have_posts() ) : the_post();
			$event_date = get_field('event_date');
			$event_id = get_the_ID();
			$content = get_the_content(); ?>
			<header id="page-header" class="standard-header">
				<div id="page-header-title" class="content-inner">
					<div class="event-header">
						<div class="event-date">
							<?php echo date("Y", strtotime($event_date)).'</br>'.date("M", strtotime($event_date)).' '.date("j", strtotime($event_date)); ?>
						</div>
						<div class="event-title">
							<h1><?php the_title(); ?></h1>
							<?php
							if($location = get_field('event_location')) { ?>
								<p><?php echo $location; ?></p>
							<?php
							} ?>
						</div>
					</div>
				</div>
				<div class="page-header-content">
				</div>
			</header>
			<?php
			$featured_image = has_post_thumbnail() ? get_the_post_thumbnail_url(get_the_ID(), 'large') : false;
			echo get_share_link(get_the_title(), get_the_permalink()); ?>
			<section class="page-content-wrapper content-inner<?php echo is_active_sidebar( 'event-sidebar' ) ? ' has-sidebar' : ''; ?>">
				<div class="page-content-container reverse-container">
					<?php
					custom_sidebar('event-sidebar', $featured_image); ?>
					<div class="page-content col-8">
						<?php echo apply_filters('the_content', $content); ?>
						<?php
						if($talks = get_field('featured_talks')) { ?>
							<div id="featured-talks">
								<h3>Featured Talks</h3>
								<?php
								foreach($talks as $talk) {
									$talk_image = $talk['image'] ? $talk['image']['sizes']['small'] : get_template_directory_uri().'/images/post-placeholder.png';
									if ( $talk['external_link'] ) { ?>
										<a target="_blank" href="<?php echo $talk['external_link']; ?>">
									<?php
									} ?>
									<div class="featured-talk flex row">
										<div class="talk-image-container col-3">
											<div class="talk-image ratio-1-1 bg-centered" style="background-image:url(<?php echo $talk_image; ?>)">
											</div>
										</div>
										<div class="talk-date-container col-1">
											<div class="talk-date">
												<?php echo date("Y", strtotime($talk['date'])).'</br>'.date("M j", strtotime($talk['date'])); ?>
											</div>
											<?php
												if($talk['time']) { ?>
													<div class="talk-time">
														<?php echo $talk['time']; ?>
													</div>
												<?php
												} ?>
										</div>
										<div class="talk-content col-8">
											<h4><?php echo $talk['title']; ?></h4>
											<?php
											if($talk['speaker']) { ?>
												<p><?php echo $talk['speaker']; ?></p>
											<?php
											} ?>
										</div>
										<?php
										if ( $talk['external_link'] ) { ?>
											<div class="external-link-icon">
												<svg width="512px" height="512px" viewBox="0 0 512 512" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
											    <g id="linkout-icon" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
										        <g id="linkout-icon-content" fill="#000000" fill-rule="nonzero">
									            <path d="M488.727,0 L302.545,0 C289.692,0 279.272,10.42 279.272,23.273 C279.272,36.126 289.692,46.546 302.545,46.546 L432.542,46.546 L192.999,286.09 C183.91,295.179 183.91,309.913 192.999,319.002 C197.542,323.546 203.498,325.818 209.454,325.818 C215.41,325.818 221.367,323.547 225.911,319.001 L465.455,79.458 L465.455,209.455 C465.455,222.308 475.875,232.728 488.728,232.728 C501.581,232.728 512.001,222.308 512.001,209.455 L512.001,23.273 C512.001,10.42 501.58,0 488.727,0 Z" id="linkout-path"></path>
									            <path d="M395.636,232.727 C382.783,232.727 372.363,243.147 372.363,256 L372.363,465.455 L46.545,465.455 L46.545,139.636 L256,139.636 C268.853,139.636 279.273,129.216 279.273,116.363 C279.273,103.51 268.853,93.091 256,93.091 L23.273,93.091 C10.42,93.091 0,103.511 0,116.364 L0,488.728 C0,501.58 10.42,512 23.273,512 L395.637,512 C408.49,512 418.91,501.58 418.91,488.727 L418.91,256 C418.91,243.147 408.489,232.727 395.636,232.727 Z" id="linkout-path"></path>
										        </g>
											    </g>
												</svg>
											</div>
										<?php
										} ?>
									</div>
									<?php
									if ( $talk['external_link'] ) { ?>
										</a>
									<?php
									}
								} ?>
							</div>
						<?php
						} ?>
					</div>
				</div>
				<?php
				if ( !has_shortcode( $content, 'content-tabs' ) ) {
					echo content_tabs();
				}
				// Related technologies
				// Related Technologies
				// If manual override of related technologies ?>
			</section>
			<?php
			if ( isset($term) ) {
				if( $related_techs = get_field('event_x_technology', $term) ) {
					$tech_args = [
						'post_type' => 'technology',
						'posts_per_page' => 3,
						'post__in' => $related_techs
					];
					echo get_related_technologies($tech_args);
				}
			}

			$date = date('Y-m-d H:i:s',strtotime("today"));
			$args = [
				'post_type' => 'event',
				'posts_per_page' => 3,
				'post__not_in' => [$event_id],
				'orderby' => 'meta_value',
				'meta_key' => 'event_date',
				'order' => 'ASC',
				'meta_query' => [
					[
						'key'       => 'event_date',
						'value'     => $date,
						'compare'   => '>=',
						'type'      => 'DATETIME',
					],
				]
			];
			$events = new WP_Query($args);
			if($events->have_posts()) : ?>
				<section class="post-row content-inner">
					<h2>More Events</h2>
					<div class="post-row-content flex row">
						<?php
						while($events->have_posts()): $events->the_post(); ?>
							<div class="post-row-item col-3">
								<?php
								$date = get_field('event_date');
								$image = has_post_thumbnail() ? get_the_post_thumbnail_url(get_the_id(), 'small-medium') : get_template_directory_uri().'/images/post-placeholder.png'; ?>
								<div class="event-header">
									<div class="event-date">
										<?php
										$month = date("M", strtotime($date));
										$day = date("d", strtotime($date));
										echo $month; ?></br>
										<?php echo $day; ?>
									</div>
									<div class="event-header-content">
										<h4 class="event-title">
											<a
												target="<?php get_field('external_link') ? '_target' : '_self'; ?>"
												href="<?php echo get_field('external_link') ? get_field('external_link') : get_permalink(); ?>">
												<?php the_title() ?>
											</a>
										</h4>
									</div>
								</div>
								<a
									target="<?php get_field('external_link') ? '_target' : '_self'; ?>"
									href="<?php echo get_field('external_link') ? get_field('external_link') : get_permalink(); ?>"
									class="post-row-image-container ratio-image-container ratio-3-2">
									<div class="post-row-image bg-centered animated-image" style="background-image:url('<?php echo $image; ?>');">

									</div>
									<?php
									if ( get_field('external_link') ) { ?>
										<div class="external-link-icon">
											<svg width="512px" height="512px" viewBox="0 0 512 512" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
												<g id="linkout-icon" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
													<g id="linkout-icon-content" fill="#000000" fill-rule="nonzero">
														<path d="M488.727,0 L302.545,0 C289.692,0 279.272,10.42 279.272,23.273 C279.272,36.126 289.692,46.546 302.545,46.546 L432.542,46.546 L192.999,286.09 C183.91,295.179 183.91,309.913 192.999,319.002 C197.542,323.546 203.498,325.818 209.454,325.818 C215.41,325.818 221.367,323.547 225.911,319.001 L465.455,79.458 L465.455,209.455 C465.455,222.308 475.875,232.728 488.728,232.728 C501.581,232.728 512.001,222.308 512.001,209.455 L512.001,23.273 C512.001,10.42 501.58,0 488.727,0 Z" id="linkout-path"></path>
														<path d="M395.636,232.727 C382.783,232.727 372.363,243.147 372.363,256 L372.363,465.455 L46.545,465.455 L46.545,139.636 L256,139.636 C268.853,139.636 279.273,129.216 279.273,116.363 C279.273,103.51 268.853,93.091 256,93.091 L23.273,93.091 C10.42,93.091 0,103.511 0,116.364 L0,488.728 C0,501.58 10.42,512 23.273,512 L395.637,512 C408.49,512 418.91,501.58 418.91,488.727 L418.91,256 C418.91,243.147 408.489,232.727 395.636,232.727 Z" id="linkout-path"></path>
													</g>
												</g>
											</svg>
										</div>
									<?php
									} ?>
								</a>
								<?php
								if(get_field('event_location') || has_excerpt()) { ?>
									<div class="event-description">
										<?php
										if($location) { ?>
											<span><?php echo get_field('event_location'); ?></span>
										<?php
										}
										if(has_excerpt()) { ?>
											<span class="event-description-excerpt">
												<?php echo get_the_excerpt(); ?>
											</span>
										<?php
										} ?>
									</div>
								<?php
								} ?>
							</div>
								<?php
							endwhile;
							wp_reset_postdata(); ?>
					</div>
				</section>
			<?php
			endif;
			if( strtotime($event_date) > strtotime('now') ) { // Future events
				echo get_footer_cta('events');
			} else {
				echo get_footer_cta('past_events');
			}
		endwhile; // End of the loop.
		?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
