<?php
/**
 * The template for displaying offering taxonomy pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Iongeo
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<?php
			$term_slug = get_query_var( 'term' );
			$term = get_term_by( 'slug', $term_slug, $taxonomy);
			$f_img = get_field('featured_image', $term);
			$taxonomy = $term->taxonomy;
			$term_content = get_field('content', $term);
			$sidebar_pos = get_field('sidebar_position', $term);
			$sidebar_image = get_field('sidebar_image', $term) ? get_field('sidebar_image', $term) : false;
			?>

      <header id="page-header" class="full-width-image bg-centered" style="background-image:url(<?php echo $f_img['sizes']['large']; ?>);">
				<div id="page-header-title" class="content-inner">
					<div class="page-header-title-content">
						<h1><?php echo $term->name; ?></h1>
					</div>
					<?php
					if(!get_field('content', $term) && $desc = term_description( $term->term_id, 'data-type' )) { ?>
						<div class="header-description">
							<?php echo $desc; ?>
						</div>
					<?php
					} ?>
				</div>
			</header>
			<?php
      echo get_share_link('ION Geo Offerings | '.$term->name, get_term_link($term)); ?>
			<section class="page-content-wrapper content-inner<?php echo $term_content && is_active_sidebar( 'offering-type-sidebar' ) ? ' has-sidebar' : ''; ?>">
				<?php
				if($term_content) { ?>
					<div class="page-content-container flex <?php echo $sidebar_pos; ?>">
						<div class="page-content col-8">
							<?php echo $term_content; ?>
						</div>
						<?php
						// custom_sidebar( 'offering-type-sidebar', $sidebar_image['sizes']['medium']);
						if ( $modules = get_field('modules') ) :
							echo custom_sidebar_modules($modules);
						elseif ( is_active_sidebar( 'offering-type-sidebar' ) ) :
							custom_sidebar( 'offering-type-sidebar', $sidebar_image['sizes']['medium']);
						endif; ?>
					</div>
				<?php
				}
				echo by_the_numbers($term);
				$sub_sections_arr = [];
				// If sub sections field has sections
				if ($sub_sections = get_field('offering_subsections', $term) ) {
					foreach( $sub_sections as $sub_section ) {
						$sub_section_obj = [];
						//print_r($sub_section);
						switch ($sub_section['type']) {
							case 'offering-type':
								$sub_section_obj['image'] = $sub_section['image']
									? $sub_section['image']['sizes']['small-medium']
									: get_field('featured_image', $sub_section['offering_type'])['sizes']['small-medium'];
								$sub_section_obj['title'] = $sub_section['title']
									? $sub_section['title']
									: $sub_section['offering_type']['name'];
								$sub_section_obj['text'] = $sub_section['text']
									? $sub_section['text']
									: $sub_section['offering_type']['description'];
								if ( $sub_section['show_link'] ) {
									$sub_section_obj['link'] = $sub_section['link']
										? $sub_section['link']
										: [
											'url' => get_term_link($s_term, $taxonomy),
											'title' => 'read more',
											'target' => '_self'
										];
								}
							break;
							case 'offering':
								$offering = $sub_section['offering'];
								//print_r($offering);
								$sub_section_obj['image'] = $sub_section['image']
									? $sub_section['image']['sizes']['small-medium']
									: get_the_post_thumbnail_url($offering->ID, 'small-medium');
								$sub_section_obj['title'] = $sub_section['title']
									? $sub_section['title']
									: $offering->post_title;
								$sub_section_obj['text'] = $sub_section['text']
									? $sub_section['text']
									: $offering->post_excerpt;
								if ( $sub_section['show_link'] ) {
									$sub_section_obj['link'] = $sub_section['link']
										? $sub_section['link']
										: [
											'url' => get_the_permalink($offering->ID),
											'title' => 'read more',
											'target' => '_self'
										];
								}
							break;
							case 'info':
								$sub_section_obj['image'] = $sub_section['image']['sizes']['small-medium'];
								$sub_section_obj['title'] = $sub_section['title'];
								$sub_section_obj['text'] = $sub_section['text'];
								if( $link = $sub_section['link'] ) {
									$sub_section_obj['link'] = $link;
								}
							break;
						}
						array_push($sub_sections_arr, $sub_section_obj);
					}
				} else { // If subsection field has no sections
					if($subTerms = get_terms(['taxonomy' => $taxonomy, 'parent' => $term->term_id])) {
						foreach($subTerms as $s_term) {
							$sub_section_obj = [
								'title' => $s_term->name,
								'link' => [
									'url' => get_term_link($s_term, $taxonomy),
									'title' => 'read more',
									'target' => '_self'
								]
							];
							if($image = get_field('featured_image', $s_term)) {
								$sub_section_obj['image'] = $image['sizes']['small-medium'];
							}
							if($desc = term_description( $term->term_id, 'data-type' )) {
								$sub_section_obj['text'] = $desc;
							}
							array_push($sub_sections_arr, $sub_section_obj);
						}
					} else {
						// If no child taxonomy, look for pages with the current taxonomy
						$args = array(
					    'post_type' => 'offering',
					    'posts_per_page' => -1,
							'orderby' => 'menu_order',
							'order' => 'ASC',
					    'tax_query' => [
					      [
					        'taxonomy' => $taxonomy,
					        'field' => 'slug',
					        'terms' => $term_slug
					      ]
					    ]
					  );
					  $taxPosts = new WP_Query($args);
					  if($taxPosts->have_posts()) :
							while($taxPosts->have_posts()): $taxPosts->the_post();
								$sub_section_obj = [
									'title' => get_the_title(),
									'link' => [
										'url' => get_the_permalink(),
										'title' => 'read more',
										'target' => '_self'
									]
								];
								if(has_post_thumbnail()) {
									$sub_section_obj['image'] = get_the_post_thumbnail_url(get_the_id(), 'small-medium');
								}
								if(has_excerpt()) {
									$sub_section_obj['text'] = get_the_excerpt();
								}
								array_push($sub_sections_arr, $sub_section_obj);
					    endwhile;
					    wp_reset_postdata();
					  endif;
					}
				} // End if subsections
				if ( count($sub_sections_arr) > 0 ) { ?>
					<div id="offering-type-children">
						<?php
						side_scroll_nav('offering-types');
						foreach($sub_sections_arr as $sub_sec) {
							echo create_child_offering($sub_sec);
						} ?>
					</div>
				<?php
				}
				// Gets related offerings by offering pages that are
				// related to this offering type or its parent if it's a child offering type
				$parentID = $term->ID;
				if($term->parent != 0) {
					$ancestors = get_ancestors($term->term_id, $taxonomy, 'taxonomy');
					$parentID = $ancestors[0];
				}
				$oArgs = [
					'post_type' => 'offering',
					'posts_per_page' => 8,
					'orderby' => 'rand',
					'tax_query' => [
						'relation' => 'AND',
						[
							'taxonomy' => $taxonomy,
							'field' => 'term_id',
							'terms' => $parentID,
						]
					],
				]; ?>
			</section>
			<?php echo get_related_offerings($oArgs); ?>
			<?php
			// Related Technologies
			// If manual override of related technologies
			$techArgs = [
				'post_type' => 'technology',
				'posts_per_page' => 3,

			];
			if($manualTechs = get_field('related_technologies', $term)) {
				$techArray = [];
				foreach($manualTechs as $tech) {
					array_push($techArray, $tech->ID);
				}
				$techArgs['post__in'] = $techArray;
				echo get_related_technologies($techArgs);
			}
			?>
			<?php echo get_footer_cta('default'); ?>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
