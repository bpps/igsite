<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Iongeo
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<link href="https://fonts.googleapis.com/css?family=IBM+Plex+Sans:200,300,400,500,600,700" rel="stylesheet">
	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_template_directory_uri(); ?>/images/favicon/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_template_directory_uri(); ?>/images/favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_template_directory_uri(); ?>/images/favicon/favicon-16x16.png">
	<link rel="manifest" href="<?php echo get_template_directory_uri(); ?>/images/favicon/site.webmanifest">
	<link rel="mask-icon" href="<?php echo get_template_directory_uri(); ?>/images/favicon/safari-pinned-tab.svg" color="#f5f5f5">
	<meta name="msapplication-TileColor" content="#f5f5f5">
	<meta name="theme-color" content="#ff0000">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'iongeo' ); ?></a>
	<div class="search-overlay-background">
	</div>
	<header id="masthead" class="site-header">
		<div class="content-inner">
			<div class="site-header-content">
				<div class="site-branding">
					<?php
					the_custom_logo();
					$iongeo_description = get_bloginfo( 'description', 'display' );
					if ( $iongeo_description || is_customize_preview() ) :
					endif; ?>
				</div><!-- .site-branding -->
				<div id="header-right">
					<button class="hamburger hamburger--3dxy" type="button">
					  <span class="hamburger-box">
					    <span class="hamburger-inner"></span>
					  </span>
					</button>
					<?php
					$rows = get_field('profile', 'option');
					if($rows) { ?>
						<ul id="header-social-links">
							<?php foreach($rows as $row) { ?>
								<li>
									<a target="_blank" id="social-<?php echo slugify($row['platform']); ?>" href="<?php echo $row['profile_url']; ?>">
										<img alt="<?php echo slugify($row['platform']); ?>" src="<?php echo $row['icon']['sizes']['small']; ?>"/>
									</a>
								</li>
							<?php
							} ?>
						</ul>
					<?php
					} ?>
					<a href="#" class="search-icon">
						<img alt="search" src="<?php echo get_template_directory_uri(); ?>/images/icons/search.png">
					</a>
				</div>
			</div>
			<div id="search-overlay">
				<div class="wrapper">
					<!-- <div id="predictive-search"></div> -->
					<div class="content-inner search-bar">
						<div>
							<!--<img src="<?php echo get_template_directory_uri(); ?>/images/icons/search.png">-->
							<?php get_search_form(true); ?>
						</div>
						<a href="#" class="search-close">
							<img alt="close search" src="<?php echo get_template_directory_uri(); ?>/images/icons/x.png"/>
						</a>
					</div>
				</div>
				<div class="content-inner search-results">
					<div id="search-container" class="search-results-container"></div>
				</div>
			</div>
			<?php
			// Show breadcrumbs
			if(!is_404()) {
				echo get_smart_breadcrumbs();
			} ?>
		</div>
			<?php
			global $post;
			// End Breadcrumbs
			$menuObjs = [];
			$navItems = wp_get_nav_menu_items( 'main-menu' );
			$navSecondary = wp_get_nav_menu_items( 'secondary-menu' );
			$currentURL = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>
			<style>
				<?php
				foreach($navItems as $navItem) {
					$menuID = $navItem->object_id;
					$color = '#8DC73F';
					$menu_class = basename($navItem->url);
					if(basename($navItem->url) == basename(home_url())) {
						$menu_class = 'ion';
					}
					if($menuCol = get_field('menu_color', $navItem)) {
						$color = $menuCol;
					} ?>
					#navigation-container #menu-item-<?php echo $menu_class; ?>::after {
						border-color: transparent <?php echo $color; ?> transparent transparent;
					}
					#navigation-container #menu-item-<?php echo $menu_class; ?>::before {
						background-color: <?php echo $color; ?>;
					}
				<?php
				} ?>
		</style>
		<div id="navigation-container" class='shadow'>
			<?php get_loader(true, true); ?>
			<nav id="site-navigation" class="main-navigation">
				<div id="primary-menu">
					<?php
					foreach($navItems as $navItem) {
						if($navItem->menu_item_parent == 0) {
							$pageID = $navItem->object_id;
							$menuModules = check_menu_modules($navItem, $navItems);
							$menu_class = $currentURL == $navItem->url ? ' current-page' : 'page-link';
							$menuColor = get_field('menu_color', $navItem);

							$menuObj = (object)[];
							$menuObj->id = $pageID;
							$menuObj->classes = $menuModules ? 'has-modules' : 'no-modules';
							$menuObj->navitem = $navItem->ID;
							$menuObj->url = $navItem->url;
							$menuObj->menu = 'main-menu';
							$menuObj->title = $navItem->title;
							$menuObj->color = $menuColor;
							$menuObj->status = $menu_class;
							if($menuIcon = get_field('menu_icon', $navItem)) {
								$menuObj->image = $menuIcon['sizes']['small'];
							}
							$menu_class = basename($navItem->url);
							if(basename($navItem->url) == basename(home_url())) {
								$menu_class = 'ion';
							}
							array_push($menuObjs, $menuObj);
							//print_r($navItem);
							//print $post->ID; ?>
							<a
								id="menu-item-<?php echo $menu_class; ?>"
								class="menu-item<?php echo $menuModules ? ' has-modules' : ''; echo ' '.$menu_class; ?>"
								<?php echo $menuColor ? 'data-menucol="'.$menuColor.'" ' : ''; ?>
								data-menuid="<?php echo $pageID; ?>"
								href="<?php echo $navItem->url; ?>"
							>
								<?php
								if($menuIcon = get_field('menu_icon', $navItem)) { ?>
									<img alt="<?php echo $navItem->title; ?> menu icon" src="<?php echo $menuIcon['sizes']['small']; ?>">
								<?php
								} ?>
								<span><?php echo $navItem->title; ?></span>
							</a>
						<?php
						}
					} ?>
				</div>
				<div class="mobile more-menu-items">
					More
				</div>
				<div id="secondary-menu">
					<?php
					foreach($navSecondary as $navItem) {
						$menu_class = $currentURL == $navItem->url ? ' current-page' : '';
						// print $navItem->title;
						// print $navItem->post_parent;
						if($navItem->menu_item_parent == 0) {
							// if($navItem->post_parent != 0) {
							// 	continue;
							// }
							$menuModules = check_menu_modules($navItem, $navSecondary);
							$pageID = $navItem->object_id;
							$menuObj = (object)[];
							$menuObj->url = $navItem->url;
							$menuObj->title = $navItem->title;
							$menuObj->id = $pageID;
							$menuObj->navitem = $navItem->ID;
							$menuObj->status = $menu_class;
							$menuObj->classes = $menuModules ? 'has-modules' : 'no-modules';
							$menuObj->menu = 'secondary-menu';
							if($menuModules) {
								$menuObj->content = $menuModules;
							}
							array_push($menuObjs, $menuObj); ?>
							<a
								class="menu-item<?php echo $menuModules ? ' has-modules' : ''; echo ' ' . $menu_class; ?>"
								data-menuid="<?php echo $pageID; ?>"
								href="<?php echo $navItem->url; ?>"
							>
								<span><?php echo $navItem->title; ?></span>
							</a>
						<?php
						}
					}
					?>
				</div>
				<?php
				wp_localize_script('iongeo-js', 'menuItems', $menuObjs ); ?>
				<div id="menu-color-wrapper">
					<div id="menu-color">
					</div>
				</div>
			</nav><!-- #site-navigation -->
			<div id="navigation-slide">
			</div>
			<div id="main-menu-button">
				main menu
			</div>
				</menu>
		</div>
	</header><!-- #masthead -->

	<div id="content" class="site-content">
