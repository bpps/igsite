<?php
/**
 * The template for displaying offerings page
 *
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Iongeo
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

		<?php
		while ( have_posts() ) : the_post();
			$headerLayout = get_field('header_layout');
			$pageLayout = get_field('page_layout'); ?>
			<header id="page-header" class="<?php echo $headerLayout; ?>-header">
				<div id="page-header-title" class="content-inner">
					<div class="page-header-title-content">
						<h1><?php the_title(); ?></h1>
					</div>
				</div>
			</header>
			<?php echo get_share_link('ION Geo | '.get_the_title(), get_the_permalink()); ?>
			<section class="page-content-wrapper">
				<div class="page-content-container <?php echo $pageLayout; ?>-page has-sidebar full-width-page">
 					<div class="page-content content-inner">
 						<div id="offerings-landing" class="flex has-side-scroll">
							<?php
							side_scroll_nav('offerings');
							$oTypes = get_terms(['taxonomy' => 'offering-type', 'parent' => 0, 'hide_empty' => false]);
							foreach($oTypes as $oType) {
								$typeLink = get_term_link($oType, 'offering-type'); ?>
								<div class="landing-offering side-scroll-item">
									<h3>
										<a href="<?php echo $typeLink; ?>">
											<?php echo $oType->name; ?>
										</a>
									</h3>
									<?php
									if($image = get_field('featured_image', $oType)) { ?>
										<a class="offering-image" href="<?php echo $typeLink; ?>">
											<img alt="<?php echo $oType->slug; ?>" src="<?php echo $image['sizes']['small-medium']; ?>"/>
										</a>
									<?php
									}
									if($desc = term_description( $oType->term_id, 'offering-type' )) { ?>
										<div class="offering-desc">
											<?php echo $desc; ?>
										</div>
									<?php
									}
									if($childTypes = get_terms(['taxonomy' => 'offering-type', 'parent' => $oType->term_id, 'hide_empty' => false])) { ?>
										<div class="child-offerings">
											<h5>More</h5>
											<ul>
												<?php
												foreach($childTypes as $cType) { ?>
													<li>
														<a href="<?php echo get_term_link($cType, 'offering-type'); ?>">
															<?php echo $cType->name; ?>
														</a>
													</li>
												<?php
												} ?>
											</ul>
										</div>
									<?php
									} ?>
								</div>
							<?php
							} ?>
					 </div>
					</div>
				</div>
			</section>
		<?php
		endwhile; // End of the loop.
		?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
