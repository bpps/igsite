<?php
/**
 * Template Name:  Investors Landing
 *
 * The template for displaying the investors landing page
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Ion Geo
 */
get_header(); ?>
<div id="primary" class="content-area">
	<main id="main" class="site-main">

	<?php
	while ( have_posts() ) : the_post();
		$pageLayout = get_field('page_layout');
		$headerImage = false;
		if($investorHeader = get_field('global_investors_header_image', 'option')) {
			$headerImage = ['large' => $investorHeader['sizes']['large'], 'small' => $investorHeader['sizes']['small']];
		}
		$header = ion_page_header([ 'fimg' => $headerImage, 'frompage' => 'investors' ]);
		$content = get_the_content();
		echo $header->header;
		echo get_share_link('ION Geo | '.get_the_title(), get_the_permalink()); ?>
		<section class="page-content-wrapper">
			<div class="page-content-container full-width-page has-header-image">
				<div class="content-inner">
					<div class="page-content-container flex has-side-scroll">
						<div class="page-content">
							<?php echo do_shortcode('[investors-page]'); ?>
						</div>
					</div>
				</div>
			</div>
		</section>
		<?php
		if($footerCTA = get_field('footer_cta')) {
			echo get_footer_cta($footerCTA);
		}
	endwhile; // End of the loop.
	?>

	</main><!-- #main -->
</div><!-- #primary -->

<?php
get_footer();
