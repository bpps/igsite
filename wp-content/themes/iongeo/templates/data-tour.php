<?php
/**
 * Template Name:  Data Tour
 *
 * The template for displaying the investors landing page
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Ion Geo
 */
get_header(); ?>
<div id="primary" class="content-area">
	<main id="main" class="site-main">
		<section class="page-content-wrapper">
			<div class="page-content-container full-width-page has-header-image">
				<div class="content-inner">
					<div class="page-content-container-inner">
						<?php
						while ( have_posts() ) : the_post();
							the_content();
						endwhile; // End of the loop.
						?>
					</div>
				</div>
			</div>
		</section>
	</main><!-- #main -->
</div><!-- #primary -->

<?php
get_footer();
