<?php
/**
 * Template Name:  Collateral
 *
 * The template for displaying the Collateral On Demand page
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Ion Geo
 */
get_header();
	while ( have_posts() ) : the_post();
		$fImg = get_the_post_thumbnail_url(get_the_id(), 'large');
		$pw = post_password_required(); ?>
		<div id="primary" class="content-area">
			<main id="main" class="site-main">
				<header id="page-header" class="full-width-image bg-centered" style="background-image:url(<?php echo $fImg; ?>);">
					<div id="page-header-title" class="content-inner">
						<div class="page-header-title-content">
							<h1><?php the_title(); ?></h1>
						</div>
						<div id="page-header-search">
							<input data-tax-type="investor-material-type" placeholder="Search Resources"/>
						</div>
					</div>
				</header>
				<section class="page-content-wrapper<?php echo $pw ? ' password-protected' : ''; ?>">
					<div class="page-content-container full-width-page">
						<div class="content-inner">
							<div class="page-content-container">
								<?php
								$args = [
									'post_type' => 'resource',
									'posts_per_page' => -1
								];
								$resourcePosts = new WP_Query($args);
							  if($resourcePosts->have_posts()) : ?>
									<div class="col-resources">
										<?php
										$attachments = get_posts($args);
							    	while($resourcePosts->have_posts()): $resourcePosts->the_post();
											$pdf = get_field('upload_pdf');
											$file = get_attached_file($pdf['ID']);
											$filename = explode('/', $file);
											$filename = $filename[count($filename)-1];
											$thumb = $pdf ? wp_get_attachment_image_src($pdf['ID'], 'small-medium')[0] : get_template_directory_uri().'/images/post-placeholder.png';
											if($pdf) { ?>
												<a href="#" data-url="<?php echo $file; ?>" data-colid="<?php echo get_the_id(); ?>" class="col-resource">
													<div class="col-resource-content">
														<div class="col-resource-image-container">
															<div class="col-resource-icon-container">
																<div class="col-resource-icon">

																</div>
															</div>
															<img src="<?php echo $thumb; ?>"/>
														</div>
													</div>
													<?php the_title(); ?>
													<p class="add-remove-col">+ Add to my list</p>
												</a>
											<?php
											} ?>
										<?php
										endwhile;
										wp_reset_postdata(); ?>
									</div>
									<div class="col-resources-popup">
										<div class="col-resources-popup-wrapper">
											<div class="col-resources-popup-content">

											</div>
										</div>
									</div>
									<div class="col-resources-email-popup">
										<div class="col-resources-email-popup-wrapper">
											<div class="col-resources-email-popup-content">
												<input type="text" class="col-resources-name" placeholder="Name" />
												<input type="email" class="col-resources-email" placeholder="Email" />
												<input type="submit" class="col-resources-submit" value="Send">
											</div>
										</div>
									</div>
								<?php
								endif; ?>
							</div>
						</div>
					</div>
				</section>
			</main><!-- #main -->
		</div><!-- #primary -->
	<?php
	endwhile;
get_footer();
