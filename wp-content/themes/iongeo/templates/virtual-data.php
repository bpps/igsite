<?php
/**
 * Template Name:  Virtual Data
 *
 * The template for displaying the investors landing page
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Ion Geo
 */
get_header(); ?>
<div id="primary" class="content-area">
	<main id="main" class="site-main">
		<?php $pw = post_password_required(); ?>
		<a class="virtual-logo" href="<?php echo home_url(); ?>" alt="Ion Geo Home">
			<img src="<?php echo get_template_directory_uri(); ?>/images/Logo_White.png"/>
		</a>
		<?php
		while ( have_posts() ) : the_post();
			if ( $slides = get_field('slides') ) { ?>
				<div id="full-width-slider-wrapper" class="<?php echo $pw ? ' password-protected' : ' public-page'; ?>">
					<div id="full-width-slider">
						<?php
						foreach ( $slides as $slide ) { ?>
							<div class="full-width-slide">
								<div class="slide-background bg-centered" style="background-image:url('<?php echo $slide['background_image']['sizes']['large']; ?>');">
								</div>
								<div class="slide-content">
									<div class="content-inner">
										<?php
										if ( $slide['title'] ) { ?>
											<h2><?php echo $slide['title']; ?></h2>
											<?php
										}
										if ( $slide['subtitle'] ) { ?>
											<p><?php echo $slide['subtitle']; ?></p>
											<?php
										}
										if ( $slide['link'] ) { ?>
											<div class="ion-cta-container" style="margin-top: 1.5rem;">
												<a class="ion-cta" target="<?php echo $slide['link']['target']; ?>" href="<?php echo $slide['link']['url']; ?>">
													<?php echo $slide['link']['title'] ? $slide['link']['title'] : 'learn more'; ?>
												</a>
											</div>
											<?php
										} ?>
									</div>
								</div>
							</div>
						<?php
						} ?>
					</div>
					<div class="slider-overlay">
						<?php
						$slider_title = get_field('slider_title');
						$slider_subtitle = get_field('slider_subtitle');
						$slider_link = get_field('slider_link');
						$slider_logo = get_field('slider_logo'); ?>
						<div class="slider-overlay-content">
							<div class="content-inner">
								<?php
								if ( $slider_logo ) { ?>
									<div class="slider-overlay-image">
										<div class="slider-logo">
											<img src="<?php echo $slider_logo['sizes']['medium']; ?>"/>
										</div>
									</div>
								<?php
								} ?>
							</div>
						</div>
					</div>
					<div class="slider-arrow arrow-prev circle-arrow">
					</div>
					<div class="slider-arrow arrow-next circle-arrow">
					</div>
				</div>
			<?php
			} ?>
			<section class="page-content-wrapper">
				<div class="page-content-container full-width-page has-header-image">
					<div class="content-inner">
						<div class="page-content-container<?php echo $pw ? ' password-protected' : ''; ?>">
							<div class="page-content">
								<?php
								if ( $page_links_sections = get_field('page_links_sections') ) {
									foreach ( $page_links_sections as $section ) { ?>
										<div class="page-links-section">
											<?php
											if ( $section['section_title'] ) { ?>
												<h2><?php echo $section['section_title']; ?></h2>
											<?php
											}
											if ( $page_links = $section['page_links'] ) { ?>
												<div class="post-row">
													<div class="post-row-content flex row">
														<?php
														foreach ( $page_links as $page_link ) {
															$term_ex = explode('|', $page_link['taxonomy']);
															$term = $page_link['taxonomy'] != null ? get_term_by('term_id', $term_ex[1], $term_ex[0]) : false;
															$link = $term && $page_link['choose_link'] == 'taxonomy' ? get_term_link($term) : get_the_permalink($page_link['page']);
															$target = "_self";
															if ( $page_link['external_link'] && $page_link['choose_link'] == 'external' ) {
																$link = $page_link['external_link']['url'];
																$target = $page_link['external_link']['target'];
															}
															$image = $title = '';
															// print_r($page_link);
															if ( $term ) {
																$image = $page_link['image']
																? $page_link['image']['sizes']['medium']
																: get_field('featured_image', $term)['sizes']['medium'];
																$title = $page_link['title'] ? $page_link['title'] : $term->name;
															}
															if ( $page_link['page'] ) {
																$image = $page_link['image']
																? $page_link['image']['sizes']['medium']
																: get_the_post_thumbnail_url($page_link['page'], 'medium');
																$title = $page_link['title'] ? $page_link['title'] : get_the_title($page_link['page']);
															} ?>
															<div class="post-row-item col-4">
											          <a target="<?php echo $target; ?>" href="<?php echo $link; ?>" class="post-row-image-container ratio-image-container ratio-3-2">
											            <div class="post-row-image bg-centered animated-image" style="background-image:url('<?php echo $image; ?>');">
											            </div>
																	<?php
																	$overlay_var = 'overlay_title';
																	// $overlay_var = 'title';
																	if ( $page_link[$overlay_var] ) { ?>
																		<div class="overlay-title">
																			<?php
																			if ( $page_link['include_ion_logo'] ) { ?>
																				<img src="<?php echo get_template_directory_uri(); ?>/images/ion_logo_all_white.png"/>
																			<?php
																			} ?>
																			<p><?php echo $page_link[$overlay_var]; ?></p>
																		</div>
																	<?php
																	}
																	if ( $page_link['caption'] ) { ?>
																		<sub><?php echo $page_link['caption']; ?></sub>
																	<?php
																	} ?>
											          </a>
																<?php
																if ( !$page_link['hide_title'] ) { ?>
																	<h5><a target="<?php echo $target; ?>" href="<?php echo $link; ?>"><?php echo $title; ?></a></h5>
																<?php
																}
																if ( $page_link['subtitle'] ) { ?>
																	<p><?php echo $page_link['subtitle']; ?></p>
																<?php
																} ?>
															</div>
														<?php
														} ?>
													</div>
												</div>
											<?php
											} ?>
										</div>
									<?php
									}
								} ?>
							</div>
						</div>
					</div>
				</div>
			</section>
		<?php
		endwhile; ?>
		<footer id="virtual-data-footer">
			<div class="footer-content flex">
				<p>Powering data-driven decisions</p>
				<a class="virtual-logo" href="<?php echo home_url(); ?>" alt="Ion Geo Home">
					<img src="<?php echo get_template_directory_uri(); ?>/images/Logo_White.png"/>
				</a>
			</div>
		</footer>
	</main><!-- #main -->
</div><!-- #primary -->

<?php
get_footer();
