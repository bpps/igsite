<?php
/**
 * Template Name:  Investor Resources
 *
 * The template for displaying the investors landing page
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Ion Geo
 */
get_header();
	while ( have_posts() ) : the_post();
		$fImg = get_the_post_thumbnail_url(get_the_id(), 'large'); ?>
		<div id="primary" class="content-area">
			<main id="main" class="site-main">
				<header id="page-header" class="full-width-image bg-centered" style="background-image:url(<?php echo $fImg; ?>);">
					<div id="page-header-title" class="content-inner">
						<div class="page-header-title-content">
							<h1><?php the_title(); ?></h1>
						</div>
						<div id="page-header-search">
							<input data-tax-type="investor-material-type" placeholder="SEARCH IN INVESTOR RESOURCES"/>
						</div>
					</div>
				</header>
				<?php

				?>
			</main><!-- #main -->
		</div><!-- #primary -->
	<?php
	endwhile;
get_footer();
