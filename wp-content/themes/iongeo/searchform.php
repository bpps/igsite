<?php
/**
 * Template for displaying search forms in Ion Geo
 *
 * @package WordPress
 * @subpackage Ion Geo
 * @since Ion Geo 1.0
 */
?>
	<form method="get" autocomplete="off" id="searchform" action="">
    <input autocomplete="false" name="hidden" type="text" style="display:none;">
		<input type="text" class="search-field" name="s" id="s" placeholder="<?php esc_attr_e( 'Search...', 'iongeo' ); ?>" />
    <?php get_loader(); ?>
	</form>
