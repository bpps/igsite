<?php
/**
 * The template for displaying all single dataset posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Iongeo
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

		<?php
		while ( have_posts() ) :
			the_post();
			$posttype = get_field('post_type');
			$sidebar_image = get_field('sidebar_image') ? get_field('sidebar_image') : false;
			$title = get_the_title();
			$token = get_field('token');
			$slug = basename(get_permalink());
			$dataset = false;
			$content = get_the_content();
			if(isset($_GET['data-program'])) {
				$args = array(
				  'name'        => $_GET['data-program'],
				  'post_type'   => 'data-library',
				  'numberposts' => 1
				);
				$dataset = get_posts($args);
				$title = $dataset[0]->post_title;
				$token = get_field('token', $dataset[0]->ID);
				$slug = $_GET['data-program'];
			}
			$header = ion_page_header(['frompage' => 'data library single']);
			echo $header->header;
			echo get_share_link(get_the_title(), get_the_permalink(), true); ?>
			<section class="full-width-page<?php echo $posttype == 'region' ? ' region-grey-background' : ''; ?>">
				<div class="page-content-wrapper content-inner<?php if( is_active_sidebar( 'data-library-sidebar' ) && $posttype == 'data-program' ) { echo ' has-sidebar'; } ?>">
					<div class="page-content-container reverse-container<?php echo $header->image ? ' has-header-image' : ''; echo $posttype == 'region' ? '' : ' flex'; ?>">
						<?php
						if ( is_active_sidebar( 'data-library-sidebar' ) && $posttype == 'data-program' ) :
							custom_sidebar('data-library-sidebar', $sidebar_image['sizes']['medium']);
						endif; ?>
						<div class="page-content<?php echo $posttype == 'region' ? ' data-library-content' : ' col-8'; ?>">
							<?php
							switch($posttype) {
								case 'region':
									echo get_data_library_map(['type' => 'region', 'title' => $title, 'token' => $token, 'slug' => $slug]);
								break;
								case 'data-program':
									the_content();
									if ( !has_shortcode( $content, 'content-tabs' ) ) {
										echo content_tabs();
									}
								break;
							} ?>
						</div>
					</div>

					<?php
					if($insights = get_field('post_x_data-library')) {
						echo get_related_insights($insights);
					}
					// Multi client section
					if($mcps = get_the_terms(get_the_id(), 'data-type')) { ?>
						<div id="mcps" class="post-row">
					    <h2>Multi-Client Products</h2>
					    <div class="post-row-content flex row">
					      <?php
					      foreach($mcps as $mcp) { ?>
					        <div class="post-row-item col-4">
					          <?php
					          $image = '';
					          if($taximage = get_field('featured_image', $mcp)) {
					            $image = $taximage['sizes']['small-medium'];
					          } else {
					            $image = get_template_directory_uri().'/images/post-placeholder.png';
					          } ?>
					          <a href="<?php echo get_term_link($mcp); ?>" class="post-row-image-container ratio-image-container ratio-3-2">
					            <div class="post-row-image bg-centered animated-image" style="background-image:url('<?php echo $image; ?>');">

					            </div>
					          </a>
					          <h6 class="post-row-title">
					            <a href="<?php echo get_term_link($mcp); ?>">
					              <?php echo $mcp->name; ?>
					            </a>
					          </h6>
					          <?php
					          if($mcp->description) { ?>
					            <div class="post-row-item-content">
					              <?php echo character_limit($mcp->description, 100); ?>
					            </div>
					          <?php
					          } ?>
					        </div>
					      <?php
					      } ?>
					    </div>
						</div>
					<?php
					} ?>
				</div>
			</section>
			<?php
			echo get_footer_cta('default');
		endwhile; // End of the loop.
		?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
