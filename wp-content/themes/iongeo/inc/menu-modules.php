<?php
function check_menu_modules($navItem) {
	$menuModules = get_field('menu_modules', $navItem);
	if($menuModules) {
		return true;
	}
	return;
}
function get_menu_modules($navItem, $navItems) {
	$menuID = $navItem->ID;
	$id = $navItem->object_id;
	$hasModules = false;
	ob_start(); ?>
	<section class="menu-slider-menu">
		<div class="menu-slider-header">
			<a href="<?php echo $navItem->url; ?>"><h1><?php echo $navItem->title; ?></h1></a>
			<?php
			if(basename($navItem->post_title) == 'Investors') { ?>
				<div class="menu-investor-quote">
					<?php
					$stockprice = get_stock_price();
					$change = 'positive';
					if($stockprice['changePercent'] == 0) {
						$change = 'neutral';
					}
					if($stockprice['changePercent'] < 0) {
						$change = 'negative';
					} ?>
					<span class="stock-symbol">
						<?php echo $stockprice['symbol']; ?>
					</span>
					<div class="stock-change <?php echo $change; ?>">
					</div>
					<span class="stock-bid">
						<?php echo '$'. number_format($stockprice['bid'], 2); ?>
					</span>
				</div>
			<?php
			} ?>
		</div>
		<?php
		$menuModules = get_field('menu_modules', $navItem);
		if($menuModules) {
			$hasModules = true;
			foreach($menuModules as $menuMod) { ?>
				<div class="module-section <?php echo $menuMod['acf_fc_layout']; ?>">
					<?php
					switch($menuMod['acf_fc_layout']) {
						case 'page_link':
							$image = $menuMod['image'];
							$pagelink = (isset($menuMod['page_link']))
								? $menuMod['page_link']
								: get_permalink($id);
							?>
							<a href="<?php echo $pagelink; ?>" class="page-link-image bg-centered" style="background-image:url(<?php echo $image['sizes']['small-medium']; ?>);">
							</a>
							<?php
							if(isset($menuMod['description'])) { ?>
								<p><?php echo $menuMod['description']; ?></p>
							<?php
							}
						break;
						case 'taxonomy_list':
							switch($menuMod['taxonomy']) {
								case 'offering-type':
									$parentTerms = get_terms([
										'hide_empty' => false,
										'orderby' => 'term_order',
										'parent' => 0,
										'taxonomy' => $menuMod['taxonomy']
									]);
									foreach($parentTerms as $term) {
										$termLink = get_term_link($term); ?>
										<div class="menu-tax-list-container">
											<h3><a href="<?php echo $termLink; ?>"><?php echo $term->name; ?></a></h3>
											<?php
											if($childTerms = get_terms(['parent' => $term->term_id, 'taxonomy' => $menuMod['taxonomy']])) { ?>
												<div class="menu-tax-children">
													<?php
													foreach($childTerms as $term) {
														$termLink = get_term_link($term); ?>
														<strong><a href="<?php echo $termLink; ?>"><?php echo $term->name; ?></a></strong>
														<?php
														$args = [
															'post_type' => 'offering',
															'orderby' => 'menu_order',
															'order' => 'ASC',
															'tax_query' => [
																[
																	'taxonomy' => $menuMod['taxonomy'],
																	'field' => 'slug',
																	'terms' => $term->slug
																]
															]
														];
														$offerings = new WP_Query($args);
													  if($offerings->have_posts()) : ?>
															<div class="menu-tax-posts">
														    <?php
																while($offerings->have_posts()): $offerings->the_post(); ?>
																	<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
														    <?php
																endwhile;
														    wp_reset_postdata(); ?>
															</div>
														<?php
													  endif;
													} ?>
												</div>
											<?php
											} ?>
										</div>
									<?php
									}
								break;
								case 'technology-type':
									$parentTerms = get_terms([
										'parent' => 0,
										'orderby' => 'term_order',
										'taxonomy' => $menuMod['taxonomy']
									]);
									//$featuredTechs = get_field('featured_technologies', 'option');
									$featuredTechs = [];
									foreach($parentTerms as $term) {
										$termLink = get_term_link($term); ?>
										<div class="menu-tax-list-container">
											<h3><?php echo $term->name; ?></h3>
											<div class="menu-tech-item">
												<?php
												if ( count($featuredTechs) ) {
													$args = [
														'posts_per_page' => 1,
														'post_type' => 'technology',
														'post__in' => $featuredTechs,
														'tax_query' => [
															[
																'taxonomy' => 'technology-type',
																'field' => 'id',
																'terms' => $term->term_id
															]
														]
													];
													// Get featured post if it exists
													$techPosts = new WP_Query($args);
													if($techPosts->have_posts()) : ?>
														<div class="menu-tech-featured">
															<h5>Featured</h5>
															<?php
															while($techPosts->have_posts()): $techPosts->the_post();
																$image = (has_post_thumbnail()) ? get_the_post_thumbnail_url(get_the_id(), 'small') : get_template_directory_uri().'/images/post-placeholder.png'; ?>
																<a href="<?php the_permalink(); ?>" class="menu-tech-image bg-centered ratio-3-2">
																	<img src="<?php echo $image; ?>"/>
																</a>
																<a href="<?php the_permalink(); ?>" class="menu-tech-title">
																	<?php the_title(); ?>
																</a>
															<?php
															endwhile; ?>
														</div>
													<?php
													wp_reset_postdata();
													endif;
												}
												if($techTypes = get_terms(['parent' => $term->term_id, 'taxonomy' => $menuMod['taxonomy']])) { ?>
													<div class="menu-tech-types">
														<?php
														foreach($techTypes as $techType) { ?>
															<a href="<?php echo get_term_link($techType); ?>"><?php echo $techType->name; ?></a>
														<?php
														} ?>
													</div>
												<?php
												} ?>
											</div>
										</div>
									<?php
									}
								break;
							}
						break;
						case 'template':
							switch($menuMod['template_type']) {
								case 'events':
									$date = date('Y-m-d H:i:s',strtotime("today"));
									$args = [
										'post_type' => 'event',
										'posts_per_page' => 1,
										'order_by' => 'meta_value_num',
										'meta_key' => 'event_date',
										'order' => 'ASC',
										'meta_query' => [
											'relation' => 'OR',
											[
												'key'       => 'event_end_date',
												'value'     => $date,
												'compare'   => '>=',
												'type'      => 'DATETIME'
											],
											[
												'key'       => 'event_date',
												'value'     => $date,
												'compare'   => '>=',
												'type'      => 'DATETIME'
											]
										]
									];
									$nextID = '';
									$eventPosts = new WP_Query($args);
								  if($eventPosts->have_posts()) : ?>
										<div class="menu-next-event">
											<h6>NEXT EVENT</h6>
									    <?php
											while($eventPosts->have_posts()): $eventPosts->the_post();
												$nextID = get_the_id();
												$image = (has_post_thumbnail()) ? get_the_post_thumbnail_url(get_the_id(), 'small-medium') : get_template_directory_uri().'/images/post-placeholder.png';
												$event_link = get_field('external_link') ? get_field('external_link') : get_permalink();
												$event_target = get_field('external_link') ? '_blank' : '_self'; ?>
												<a target="<?php echo $event_target; ?>" href="<?php echo $event_link; ?>" class="page-link-image bg-centered" style="background-image:url(<?php echo $image; ?>);">
													<?php
													if ( get_field('external_link') ) { ?>
														<div class="external-link-icon">
															<svg width="512px" height="512px" viewBox="0 0 512 512" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
																<g id="linkout-icon" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																	<g id="linkout-icon-content" fill="#000000" fill-rule="nonzero">
																		<path d="M488.727,0 L302.545,0 C289.692,0 279.272,10.42 279.272,23.273 C279.272,36.126 289.692,46.546 302.545,46.546 L432.542,46.546 L192.999,286.09 C183.91,295.179 183.91,309.913 192.999,319.002 C197.542,323.546 203.498,325.818 209.454,325.818 C215.41,325.818 221.367,323.547 225.911,319.001 L465.455,79.458 L465.455,209.455 C465.455,222.308 475.875,232.728 488.728,232.728 C501.581,232.728 512.001,222.308 512.001,209.455 L512.001,23.273 C512.001,10.42 501.58,0 488.727,0 Z" id="linkout-path"></path>
																		<path d="M395.636,232.727 C382.783,232.727 372.363,243.147 372.363,256 L372.363,465.455 L46.545,465.455 L46.545,139.636 L256,139.636 C268.853,139.636 279.273,129.216 279.273,116.363 C279.273,103.51 268.853,93.091 256,93.091 L23.273,93.091 C10.42,93.091 0,103.511 0,116.364 L0,488.728 C0,501.58 10.42,512 23.273,512 L395.637,512 C408.49,512 418.91,501.58 418.91,488.727 L418.91,256 C418.91,243.147 408.489,232.727 395.636,232.727 Z" id="linkout-path"></path>
																	</g>
																</g>
															</svg>
														</div>
													<?php
													} ?>
												</a>
												<p><a class="menu-event-date" target="<?php echo $event_target; ?>" href="<?php echo $event_link; ?>">
													<span class="event-date">
									          <?php
														$date = get_field('event_date');
									          $month = date("M", strtotime($date));
									          $day = date("d", strtotime($date));
									          echo $month.' '.$day; ?>
								        	</span><?php the_title(); ?></a></p>
											<?php
											endwhile; ?>
										</div>
										<?php
										wp_reset_postdata();
									endif;
									// Get customer events
									create_menu_events_module($nextID);
								break;
								case 'data-library':
									$listTypes = [
										['title' => 'Regions', 'slug' => 'region'],
										['title' => 'Multi-Client Products', 'slug' => 'data-type'],
									];
									foreach($listTypes as $list) { ?>
										<div class="menu-tax-list-container">
											<div class="menu-tax-children">
												<h5><?php echo $list['title']; ?></h5>
												<?php
												switch($list['slug']) {
													case 'data-type':
														if($childTerms = get_terms(['taxonomy' => 'data-type'])) { ?>
															<div class="menu-tax-posts">
																<?php
																foreach($childTerms as $term) {
																	$termLink = get_term_link($term); ?>
																	<a href="<?php echo $termLink; ?>"><?php echo $term->name; ?></a>
																<?php
																} ?>
															</div>
														<?php
														}
													break;
													case 'application':
													case 'region':
														$args = [
															'post_type' => 'data-library',
															'posts_per_page' => -1,
															'meta_key' => 'post_type',
															'meta_value' => 'data-program',
															'orderby' => 'title',
															'order' => 'ASC'
														];
														if($list['slug'] == 'region') {
															$args['meta_value'] = 'region';
															$args['post_parent'] = 0;
														}

														if($regions = get_posts($args)) { ?>
															<div class="menu-tax-posts">
																<?php
																foreach($regions as $region) { ?>
																	<a href="<?php echo get_the_permalink($region->ID); ?>"><?php echo $region->post_title; ?></a>
																<?php
																} ?>
															</div>
														<?php
														}
													break;
												} ?>
											</div>
										</div>
									<?php
									}
								break;
							}
						break;
						case 'recent_news':
							$label = (isset($menuMod['label'])) ? $menuMod['label'] : false;
							$posttype = $menuMod['news_type'];
							$seeMore = (isset($menuMod['see_more_page']['url'])) ? $menuMod['see_more_page'] : false;
							$seeMoreText = (isset($menuMod['see_more_page']['title'])) ? $menuMod['see_more_page']['title'] : 'see more';
							$numberOfPosts = (isset($menuMod['number_of_posts'])) ? $menuMod['number_of_posts'] : 6;
							$seeMore = (isset($menuMod['see_more_page']['url'])) ? $menuMod['see_more_page'] : false;
							$seeMoreText = (isset($menuMod['see_more_page']['title'])) ? $menuMod['see_more_page']['title'] : 'see more';
							if($label) { ?>
								<h4 class="grey-header"><?php echo $label; ?></h4>
							<?php
							}
							if ( $posttype == 'investor-news' ) {
								$ir_news_posts = get_recent_investor_news();
								if ( count($ir_news_posts->data) > 0 ) { ?>
									<div class="menu-news-posts">
										<?php
										foreach( $ir_news_posts->data as $news_post ) { ?>
											<a class="arrow-before" href="<?php echo $news_post->link->hostedUrl; ?>"><?php echo $news_post->title; ?></a>
										<?php
										} ?>
									</div>
								<?php
								}
								if($seeMore) { ?>
									<a href="<?php echo $seeMore['url']; ?>" class="menu-see-more"><?php echo $seeMoreText; ?></a>
								<?php
								}
								break;
							}
							$args = [
								'post_type' => $posttype,
								'posts_per_page' => $numberOfPosts,
							];
							$newsPosts = new WP_Query($args);
						  if($newsPosts->have_posts()) : ?>
								<div class="menu-news-posts">
								    <?php
										while($newsPosts->have_posts()): $newsPosts->the_post(); ?>
											<a class="arrow-before" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
										<?php
									endwhile; ?>
								</div>
								<?php
								wp_reset_postdata();
							endif;
							if($seeMore) { ?>
								<a href="<?php echo $seeMore['url']; ?>" class="menu-see-more"><?php echo $seeMoreText; ?></a>
							<?php
							}
							if($menuMod['display_featured_topics']) {
								$featuredTopics = []; ?>
								<div class="featured-topics-container">
									<h4 class="grey-header">Featured Topics</h4>
									<div class="featured-topics">
										<?php
										$featuredTopics = (get_field('featured_topics','option'))
											? get_field('featured_topics','option')
											: get_tags([
												'orderby' => 'count',
												'number' => 6,
											]);
										foreach($featuredTopics as $topic) { ?>
											<a class="featured-topic" href="<?php echo get_the_permalink(get_option('page_for_posts')).'?topic='.$topic->slug; ?>"><?php echo $topic->name; ?></a>
										<?php
										} ?>
									</div>
								</div>
							<?php
							}
						break;
						case 'regions':
							$regions = [];
							if($menuMod['select_regions']) {
								$regions = $menuMod['select_regions'];
							} else {
								$regions = get_posts([
									'post_type' => 'data-library',
									'post_parent' => 0,
									'posts_per_page' => -1
								]);
							} ?>
							<h5>Regions</h5>
							<ul class="menu-region-list">
								<?php
								foreach($regions as $region) { ?>
									<li><a href="<?php echo get_the_permalink($region->ID); ?>"><?php echo $region->post_title; ?></a></li>
								<?php
								} ?>
							</ul>
						<?php
						break;
					} ?>
				</div>
			<?php
			}
		}
		$count = 0;
		$currentMenu = false;
		$parentsKeyArray = [];
		$parentsIDArray = [];
    foreach( $navItems as $key=>$item ):
			// This is a new parent Item. Continue until the loop reaches the current nav item.
			if($currentMenu && $item->menu_item_parent == 0) {
				// The current menu has already been processed. Stop loop.
				break;
			}
    	if($item->menu_item_parent == 0 && $item->ID != $navItem->ID) {
				continue;
			}
			if($item->ID == $navItem->ID) {
				// !!This is the current menu item. Start looping.
				if($item->children) { // If it has children, set the parent id and get the children (open the container)
					$currentMenu = true; ?>
					<div class="sub-menu-container module-section">
				<?php
				} else { // Else move on to the next item which should be on the same level as the last item
					break;
				}
			}
			if(!$currentMenu) { // Keep passing until the current menu is active
				continue;
			}
			// $parentChildren = $navItems[end($parentsArray)]->children;
			// if(is_array($parentChildren)) { // If there are children of the current parent
			// 	if(!in_array($item->menu_item_parent, $parentChildren)) { // if this menu item is not a child of the current parent, it's a new level
			// 		$currentParent = $currentItem;
			// 	}
			// 	// $lastElement = $parentChildren[count($parentChildren) - 1];
			// }

			if($item->ID != $navItem->ID) { ?>
				<a href="<?php echo $item->url; ?>" class="<?php echo $item->url == '#' ? 'no-link' : ''; ?>"><?php echo $item->title; ?></a>
				<?php
			}
			if(isset($item->children)) { // if this item has children, open the child container and continue looping ?>
				<div class="children<?php echo $item->ID == $navItem->ID ? ' first-children':''; ?>">
			<?php
			}

			$currentItemKey = $key;
			$currentItemID = $item->ID;
			//print $currentItemID;
			if(!empty($parentsKeyArray) && !isset($item->children)) {
				$tempParentsArray = $parentsKeyArray;
				foreach($tempParentsArray as $parent) {
					// if current item = the last child of the parent item ($parent) close the div
					//echo $navItems[$parent]->title."'s children:";
					//print_r($navItems[$parent]->children);
					$lastChild = $navItems[$parent]->children[count($navItems[$parent]->children) - 1];
					//echo $lastChild.' vs. '.$currentItemID.' </br>';
					if($currentItemID == $lastChild) { ?>
						</div>
						<?php
						$currentItemID = $navItems[$currentItemKey]->menu_item_parent;
						$currentItemKey = $parentsKeyArray[array_search($navItems[$currentItemKey]->menu_item_parent, $parentsIDArray)];
						array_shift($parentsIDArray);
						array_shift($parentsKeyArray); // Remove this item from the array now.
					}
				}
			}

			if(isset($item->children)) {
				//print_r($item->children);
				array_unshift($parentsKeyArray, $key);
				array_unshift($parentsIDArray, $item->ID);
			}
		endforeach;
		 ?>
	</section>
	<?php
	if($hasModules) {
		return ob_get_clean();
	}
	$clean = ob_get_clean();
	return false;
}

function get_recent_investor_news () {
	$fetchPosts = wp_remote_request('https://clientapi.gcs-web.com/data/8cc48279-c570-4075-aa09-5c796c645901/News?size=3', array(
    'method'     => 'GET'
  ));
  // pagination: links->next, links->prev, links->first, links->last
  $fetchJSON = wp_remote_retrieve_body($fetchPosts);
  $fetchJSON = json_decode($fetchJSON);
	return $fetchJSON;
}

function create_menu_events_module($exclude) {
	$postType = 'event';
	$title = 'Events';

	$date = date('Y-m-d H:i:s',strtotime("today"));
	$eventsPage = get_page_by_title($title);
	$args = [
		'post_type' => $postType,
		'posts_per_page' => 3,
		'post__not_in' => [$exclude],
		'order' => 'ASC',
		'meta_query' => [
			'relation' => 'OR',
			[
				'key'       => 'event_end_date',
				'value'     => $date,
				'compare'   => '>=',
				'type'      => 'DATETIME'
			],
			[
				'key'       => 'event_date',
				'value'     => $date,
				'compare'   => '>=',
				'type'      => 'DATETIME'
			]
		]
	];
	$eventPosts = new WP_Query($args);
	if($eventPosts->have_posts()) : ?>
		<div class="menu-events-module-container">
			<div class="menu-events-module">
				<h4><?php echo 'Upcoming Events'; ?></h4>
				<?php
				while($eventPosts->have_posts()): $eventPosts->the_post();
					$event_link = get_field('external_link') ? get_field('external_link') : get_permalink();
					$event_target = get_field('external_link') ? '_blank' : '_self'; ?>
					<p>
						<a alt="<?php the_title(); ?>" target="<?php echo $event_target; ?>" class="menu-event-date <?php echo get_field('external_link') ? ' external-link' : ''; ?>" href="<?php echo $event_link; ?>">
							<span class="event-date">
								<?php
								$date = get_field('event_date');
								$month = date("M", strtotime($date));
								$day = date("d", strtotime($date));
								echo $month.'</br>'.$day; ?>
							</span>
							<span><?php the_title(); ?></span>
							<?php
							if ( get_field('external_link') ) { ?>
								<svg class="external-link-icon black-icon" width="512px" height="512px" viewBox="0 0 512 512" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
									<g id="linkout-icon" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
										<g id="linkout-icon-content" fill="#000000" fill-rule="nonzero">
											<path d="M488.727,0 L302.545,0 C289.692,0 279.272,10.42 279.272,23.273 C279.272,36.126 289.692,46.546 302.545,46.546 L432.542,46.546 L192.999,286.09 C183.91,295.179 183.91,309.913 192.999,319.002 C197.542,323.546 203.498,325.818 209.454,325.818 C215.41,325.818 221.367,323.547 225.911,319.001 L465.455,79.458 L465.455,209.455 C465.455,222.308 475.875,232.728 488.728,232.728 C501.581,232.728 512.001,222.308 512.001,209.455 L512.001,23.273 C512.001,10.42 501.58,0 488.727,0 Z" id="linkout-path"></path>
											<path d="M395.636,232.727 C382.783,232.727 372.363,243.147 372.363,256 L372.363,465.455 L46.545,465.455 L46.545,139.636 L256,139.636 C268.853,139.636 279.273,129.216 279.273,116.363 C279.273,103.51 268.853,93.091 256,93.091 L23.273,93.091 C10.42,93.091 0,103.511 0,116.364 L0,488.728 C0,501.58 10.42,512 23.273,512 L395.637,512 C408.49,512 418.91,501.58 418.91,488.727 L418.91,256 C418.91,243.147 408.489,232.727 395.636,232.727 Z" id="linkout-path"></path>
										</g>
									</g>
								</svg>
							<?php
							} ?>
						</a>
					</p>
				<?php
				endwhile; ?>
			</div>
			<a class="more-posts" href="<?php echo get_permalink($eventsPage->ID); ?>">See All</a>
		</div>
		<?php
		wp_reset_postdata();
	endif;
}

function add_menu_children($items, $args) {
	$lastItem = 0;
	$parents = (object)[];
	$parentMenuItems = [];
	foreach($items as $key=>$item) {
		//print_r($item);
		$itemID = $item->ID;
		$parents->$itemID = $key;
		$itemParent = $item->menu_item_parent;
		if(isset($parents->$itemParent)) {
			$parentItem = $parents->$itemParent;
			if(!array_key_exists('children', $items[$parentItem])) {
				$items[$parentItem]->children = [];
			}
			array_push($items[$parentItem]->children, $item->ID);
		}
	}
return $items;
}
add_filter('wp_get_nav_menu_items', 'add_menu_children', 10, 2);
