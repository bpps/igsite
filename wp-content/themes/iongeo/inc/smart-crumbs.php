<?php
function get_smart_breadcrumbs() {
	if(!is_front_page() && !is_home()) {
		ob_start(); ?>
		<div id="ion-smart-crumbs">
			<?php
			global $post;
			$postType = get_post_type($post);
			$postTypeObj = get_post_type_object( $postType );
			if(is_tax()) {
				$term = get_queried_object();
				get_tax_crumbs($term);
				return;
			}

			$term = false;
			// get taxonomies if this post type doesn't specify not to.
			if(!isset($postTypeObj->hide_tax_crumbs)) {
				$taxes = get_object_taxonomies( $post );
				if($taxes) {
					$primary_tax = isset($postTypeObj->primary_tax_type) ? $postTypeObj->primary_tax_type : $taxes[0];
					$terms = get_the_terms($post->ID, $primary_tax);
					$term = $terms ? $terms[0] : false;
				}
			}
			switch($postType) {
				case 'post':
					$crumbObj = (object)[
						'currentid' => 0,
						'pageid' => get_option('page_for_posts'),
						'link' => get_post_type_archive_link( 'post' ),
						'name' => get_the_title(get_option('page_for_posts'))
					];
					echo create_smart_crumb($crumbObj);
				break;
				case 'investor-news':
					$crumbPost = get_page_by_title('investor news');
					echo get_page_crumbs($crumbPost);
				break;
				default:
				break;
			}
			echo get_page_crumbs($post, $term); ?>
		</div>
		<?php
		return ob_get_clean();
	} else {
		return false;
	}
}

function get_page_crumbs($post, $term = false) {
	if(is_archive()) {
		// If archive page, show the page and get the terms with parent 0
		$archive = get_queried_object();
		// Don't show breadcrumb for technology landing
		if($archive->name != 'data-library') {
			return;
		}
		$crumbObj = (object)[
			'currentid' => 0,
			'pageid' => 0,
			'link' => home_url($archive->has_archive),
			'name' => $archive->label
		];
		if($archive->name == 'data-library') {
			$children = get_data_library_parent_regions();
			$objchildren = [];
			foreach($children as $cPost) {
				array_push($objchildren, (object)[
					'link' => get_the_permalink($cPost->ID),
					'name' => $cPost->post_title
				]);
			}
			$crumbObj->children = $objchildren;
			echo create_smart_crumb($crumbObj);
			return;
		}
		$taxes = get_object_taxonomies($archive->name);
		if($taxes) {
			$terms = get_terms(['taxonomy' => $taxes[0], 'parent' => 0]);
			$objchildren = [];
			foreach($terms as $term) {
				array_push($objchildren, (object)[
					'link' => get_term_link($term),
					'name' => $term->name
				]);
			}
			$crumbObj->children = $objchildren;
		}

		echo create_smart_crumb($crumbObj);
		return;
	}
	$ancestors = get_ancestors($post->ID, 'page');
	$ancestors = array_reverse($ancestors);
	array_push($ancestors, $post->ID);
	if(isset($_GET['data-program'])) {
		echo $_GET['data-program'];
		$args = array(
			'name'        => $_GET['data-program'],
			'post_type'   => 'data-library',
			'numberposts' => 1
		);
		$dataset = get_posts($args);
		array_push($ancestors, $dataset[0]->ID);
	}
	if($term == false) { // If it has a term, the archive bread crumb is made, but if it doesn't, make it here instead.
		$postType = get_post_type_object( $post->post_type );
		if($postType->has_archive && $postType->name != 'page') {
			$crumbObj = (object)[
				'currentid' => 0,
				'pageid' => 1,
				'link' => home_url($postType->has_archive),
				'name' => $postType->label
			];
			echo create_smart_crumb($crumbObj);
		}
	}
	// if($post->post_type == 'page' && count($ancestors) == 1) {
	// 	return;
	// }
	foreach($ancestors as $key=>$anc) {
		$currentID = ($key == count($ancestors) - 1) ? $anc : '0';
		$crumbObj = (object)[
			'currentid' => $currentID,
			'pageid' => $anc,
			'link' => get_the_permalink($anc),
			'name' => get_the_title($anc)
		];
		$children = false;
		if($term != false && $key == 0) { // Has an associated taxonomy. Get all other pages with that taxonomy
			echo get_tax_crumbs($term, false);
			$children = get_posts([
				'posts_per_page' => -1,
				'tax_query' => [
					[
						'taxonomy' => $term->taxonomy,
						'field' => 'slug',
						'terms' => $term->slug
					]
				]
			]);
		} else { // No associated taxonomy: get page children
			$parentID = wp_get_post_parent_id( $anc );
			$postType = get_post_type($anc);
			if($parentID != 0) {
				$args = array(
					'post_parent' => $parentID,
					'post_type'   => 'any',
					'numberposts' => -1,
					'post_status' => 'publish',
					'post__not_in' => [$anc]
				);
				if ($parentID == 12) {
					$args['orderby'] = 'menu_order';
					$args['order'] = 'ASC';
				}
				$children = get_children( $args );
			}
			if($parentID == 0 && $postType == 'data-library') {
				$children = get_data_library_parent_regions($anc);
			} else if ($parentID == 0 && $key == count($ancestors) - 1){
				$args = array(
					'post_parent' => $anc,
					'post_type'   => 'any',
					'numberposts' => -1,
					'post_status' => 'publish',
					'orderby'			=> 'menu_order',
					'order' 			=> 'ASC'
				);
				$children = get_children( $args );
			}

		}
		if($children && count($children) > 1) {
			$objchildren = [];
			foreach($children as $cPost) {
				array_push($objchildren, (object)[
					'link' => get_the_permalink($cPost->ID),
					'name' => $cPost->post_title
				]);
			}
			$crumbObj->children = $objchildren;
		}
		echo create_smart_crumb($crumbObj);

		if($children && count($children) == 1 && $key == count($ancestors) - 1) { // If only one child, show as a new breadcrumb.
			foreach($children as $cPost) {
				$crumbObj = (object)[
					'currentid' => $currentID,
					'pageid' => $cPost->ID,
					'link' => get_the_permalink($cPost->ID),
					'name' => get_the_title($cPost->ID)
				];
				echo create_smart_crumb($crumbObj);
			}
		}
		// if($key == count($ancestors) - 1) { // If last item, look for children??
		// 	$args = array(
		// 		'post_parent' => $anc,
		// 		'post_type'   => 'any',
		// 		'numberposts' => -1,
		// 		'post_status' => 'publish',
		// 	);
		// 	$children = get_children( $args );
		// 	if($children) {
		// 		$objchildren = [];
		// 		foreach($children as $cPost) {
		// 			array_push($objchildren, (object)[
		// 				'link' => get_the_permalink($cPost->ID),
		// 				'name' => $cPost->post_title
		// 			]);
		// 		}
		// 		$crumbObj = (object)[
		// 			'currentid' => $currentID,
		// 			'pageid' => $anc,
		// 			'link' => get_the_permalink($anc),
		// 			'name' => get_the_title($anc),
		// 			'children' => $objchildren,
		// 		];
		// 		echo create_smart_crumb($crumbObj);
		// 	}
		// }
	}
}

function get_tax_crumbs($term, $linklast = true) {
	$taxonomy = $term->taxonomy;
	$ancestors = get_ancestors($term->term_id, $taxonomy, 'taxonomy');
	$ancestors = array_reverse($ancestors);
	array_push($ancestors, $term->term_id);
	$heirarchy = _get_term_hierarchy($taxonomy);
	$taxParent = 0;
	// Get the post type thats associated with this taxonomy and make a crumb for that post types archive page.
	$assocPostType = wpse_172645_get_post_types_by_taxonomy( $taxonomy );
	$postType = get_post_type_object( $assocPostType[0] );
	$crumbObj = (object)[
		'currentid' => 0,
		'pageid' => 1,
		'link' => home_url($postType->has_archive),
		'name' => $postType->label
	];
	echo create_smart_crumb($crumbObj);

	// Cycle through tiers of taxonomies and stop when the current taxonomy is reached.
	for($a=0; $a<count($ancestors); $a++) {
		$tierTerms = get_terms([
			'taxonomy' => $taxonomy,
			'parent' => $taxParent,
			'exclude' => [$ancestors[$a]],
			'hide_empty' => false,
		]);
		$currentTerm = get_term_by('id', $ancestors[$a], $taxonomy);
		$taxParent = $ancestors[$a];
		// Create Crumb Obj
		$crumbObj = (object)[
			'currentid' => $currentTerm->term_id,
			'pageid' => $term->term_id,
			'link' => get_term_link($currentTerm),
			'name' => $currentTerm->name,
		];

		if(!$linklast) { // Last crumb shouldn't be a link because its the current page.
			$crumbObj->currentid = 0;
		}
		// Parent terms of technology type ie. Hardware, software should not be links and no children.
		if(($taxonomy == 'technology-type' && $currentTerm->parent == 0)) {
			$crumbObj->currentid = $term->term_id;
		} else { // Any other taxonomy (so far) can.
			if($tierTerms) {
				$children = [];
				foreach($tierTerms as $tTerm) {
					array_push($children, (object)[
						'link' => get_term_link($tTerm),
						'name' => $tTerm->name
					]);
				}
				$crumbObj->children = $children;
			}
		}

		// print_r($crumbObj);
		echo create_smart_crumb($crumbObj);
		// If the term has children and its the last one, show them with the plural of the taxonomy as the first item...
		if($a + 1 == count($ancestors) && $subTaxes = get_term_children($ancestors[$a], $taxonomy)) {
			$taxlabels = get_taxonomy($taxonomy);
			$crumbObj = (object)[
				'currentid' => 0, // make current and page id the same
				'pageid' => 0,
				'name' => $postType->label,
			];
			$children = [];
			foreach($subTaxes as $subTax) {
				$subTaxTerm = get_term_by('id', $subTax, $taxonomy);
				array_push($children, (object)[
					'link' => get_term_link($subTaxTerm),
					'name' => $subTaxTerm->name
				]);
			}
			$crumbObj->children = $children;
			echo create_smart_crumb($crumbObj);
		}
	}
}

function create_smart_crumb($crumb) {
	ob_start(); ?>
	<smartcrumb class="smart-crumb<?php echo isset($crumb->children) ? ' has-children' : ''; ?>">

			<?php
			if($crumb->currentid != $crumb->pageid) { ?>
				<span class="span-link">
					<a href="<?php echo $crumb->link; ?>">
			<?php
			} else { ?>
				<span class="span-no-link">
			<?php
			}
				$crumbTitle = character_limit($crumb->name, 24);
				echo $crumbTitle;
			if($crumb->currentid != $crumb->pageid) { ?>
				</a>
			<?php
			} ?>
		</span>
		<?php
		if(isset($crumb->children)) { ?>
			<div class="smart-crumb-list">
				<?php
				foreach($crumb->children as $child) { ?>
					<a href="<?php echo $child->link; ?>">
						<?php echo $child->name; ?>
					</a>
				<?php
				} ?>
			</div>
		<?php
		} ?>
	</smartcrumb>
<?php
	return ob_get_clean();
}

function character_limit($text, $max) {
	if (strlen($text) > $max) {
		$offset = ($max - 3) - strlen($text);
		$text = substr($text, 0, strrpos($text, ' ', $offset)) . '...';
	}
	return $text;
}

function get_data_library_parent_regions($id = false) {
	$args = array(
		'post_parent' => 0,
		'posts_per_page' => -1,
		'post_type' => 'data-library',
		'post_status' => 'publish',
	);
	if($id) {
		$args['exclude'] = [$id];
	}
	$children = get_posts( $args );
	return $children;
}

function wpse_172645_get_post_types_by_taxonomy( $tax = 'category' )
{
    global $wp_taxonomies;
    return ( isset( $wp_taxonomies[$tax] ) ) ? $wp_taxonomies[$tax]->object_type : array();
}
