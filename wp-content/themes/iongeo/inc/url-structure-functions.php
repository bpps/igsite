<?php
function register_product_rewrite_rules( $wp_rewrite ) {
  $new_rules = array(
    'markets/([^/]+)/?$' => 'index.php?offering-type=' . $wp_rewrite->preg_index( 1 ), // 'markets/any-character/'
    'markets/([^/]+)/([^/]+)/?$' => 'index.php?post_type=offering&offering-type=' . $wp_rewrite->preg_index( 1 ) . '&offering=' . $wp_rewrite->preg_index( 2 ), // 'markets/any-character/post-slug/'
    'markets/([^/]+)/([^/]+)/page/(\d{1,})/?$' => 'index.php?post_type=offering&offering-type=' . $wp_rewrite->preg_index( 1 ) . '&paged=' . $wp_rewrite->preg_index( 3 ), // match paginated results for a sub-category archive
    'markets/([^/]+)/([^/]+)/([^/]+)/?$' => 'index.php?post_type=offering&offering-type=' . $wp_rewrite->preg_index( 2 ) . '&offering=' . $wp_rewrite->preg_index( 3 ), // 'markets/any-character/sub-category/post-slug/'
    'markets/([^/]+)/([^/]+)/([^/]+)/([^/]+)/?$' => 'index.php?post_type=offering&offering-type=' . $wp_rewrite->preg_index( 3 ) . '&offering=' . $wp_rewrite->preg_index( 4 ), // 'markets/any-character/sub-category/sub-sub-category/post-slug/'
    'technologies/([^/]+)/?$' => 'index.php?technology-type=' . $wp_rewrite->preg_index( 1 ), // 'technologies/any-character/'
    'technologies/([^/]+)/([^/]+)/?$' => 'index.php?post_type=technology&technology-type=' . $wp_rewrite->preg_index( 1 ) . '&technology=' . $wp_rewrite->preg_index( 2 ), // 'technologies/any-character/post-slug/'
    'technologies/([^/]+)/([^/]+)/page/(\d{1,})/?$' => 'index.php?post_type=technology&technology-type=' . $wp_rewrite->preg_index( 1 ) . '&paged=' . $wp_rewrite->preg_index( 3 ), // match paginated results for a sub-category archive
    'technologies/([^/]+)/([^/]+)/([^/]+)/?$' => 'index.php?post_type=technology&technology-type=' . $wp_rewrite->preg_index( 2 ) . '&technology=' . $wp_rewrite->preg_index( 3 ), // 'technologies/any-character/sub-category/post-slug/'
    'technologies/([^/]+)/([^/]+)/([^/]+)/([^/]+)/?$' => 'index.php?post_type=technology&technology-type=' . $wp_rewrite->preg_index( 3 ) . '&technology=' . $wp_rewrite->preg_index( 4 ), // 'technologies/any-character/sub-category/sub-sub-category/post-slug/'
    'technologies/([^/]+)/([^/]+)/([^/]+)/([^/]+)/([^/]+)/?$' => 'index.php?post_type=technology&technology-type=' . $wp_rewrite->preg_index( 4 ) . '&technology=' . $wp_rewrite->preg_index( 5 ), // 'technologies/any-character/sub-category/sub-sub-category/post-slug/'
    'technologies/([^/]+)/([^/]+)/([^/]+)/([^/]+)/([^/]+)/?$' => 'index.php?post_type=technology&technology-type=' . $wp_rewrite->preg_index( 3 ) . '&technology=' . $wp_rewrite->preg_index( 4 ) . '/' . $wp_rewrite->preg_index( 5 ), // 'technologies/any-character/sub-category/sub-sub-category/post-slug/'
  );
  $wp_rewrite->rules = $new_rules + $wp_rewrite->rules;
}
add_action( 'generate_rewrite_rules', 'register_product_rewrite_rules' );

function amend_taxonomy_url_structure( $post_link, $post ){
  if ( is_object( $post ) && $post->post_type == 'offering' ){
    if( $terms = get_the_terms($post->ID, 'offering-type') ){
      usort($terms, 'sort_terms_by_id');
      $post_link = str_replace('%offering-type%', get_taxonomy_parents(array_pop($terms)->term_id, 'offering-type', false, '/', true), $post_link); // see custom function defined below\
      $post_link = str_replace('//', '/', $post_link);
      $post_link = str_replace('https:/', 'https://', $post_link);
      return $post_link;
    }
  }
  if ( is_object( $post ) && $post->post_type == 'technology' ){
    if( $terms = get_the_terms($post->ID, 'technology-type') ){
      usort($terms, 'sort_terms_by_id');
      $post_link = str_replace('%technology-type%', get_taxonomy_parents(array_pop($terms)->term_id, 'technology-type', false, '/', true), $post_link); // see custom function defined below\
      $post_link = str_replace('//', '/', $post_link);
      $post_link = str_replace('https:/', 'https://', $post_link);
      return $post_link;
    }
  }
  return $post_link;
}
add_filter( 'post_type_link', 'amend_taxonomy_url_structure', 1, 2 );

function sort_terms_by_id($object1, $object2) {
    return $object1->term_id > $object2->term_id;
}

function fix_sub_taxonomy_queries($query) {
  if ( isset( $query['post_type'] ) && 'offering' == $query['post_type'] ) {
    if ( isset( $query['offering'] ) && $query['offering'] && isset( $query['offering-type'] ) && $query['offering-type'] ) {
      $query_old = $query;
      // Check if this is a paginated result(like search results)
      if ( 'page' == $query['offering-type'] ) {
        $query['paged'] = $query['name'];
        unset( $query['offering-type'], $query['name'], $query['offering'] );
      }
      // Make it easier on the DB
      // $query['fields'] = 'ids';
      $query['posts_per_page'] = 1;
      // See if we have results or not
      $_query = new WP_Query( $query );
      if ( ! $_query->posts ) {
        $query = array( 'offering-type' => $query['offering'] );
        if ( isset( $query_old['offering-type'] ) && 'page' == $query_old['offering-type'] ) {
            $query['paged'] = $query_old['name'];
        }
      }
    }
  }
  if ( isset( $query['post_type'] ) && 'technology' == $query['post_type'] ) {
    if ( isset( $query['technology'] ) && $query['technology'] && isset( $query['technology-type'] ) && $query['technology-type'] ) {
      $query_old = $query;
      // Check if this is a paginated result(like search results)
      if ( 'page' == $query['technology-type'] ) {
        $query['paged'] = $query['name'];
        unset( $query['technology-type'], $query['name'], $query['technology'] );
      }
      // Make it easier on the DB
      // $query['fields'] = 'ids';
      $query['posts_per_page'] = 1;
      // See if we have results or not
      $_query = new WP_Query( $query );
      if ( ! $_query->posts ) {
        $query = array( 'technology-type' => $query['technology'] );
        if ( isset( $query_old['technology-type'] ) && 'page' == $query_old['technology-type'] ) {
            $query['paged'] = $query_old['name'];
        }
      }
    }
  }
  return $query;
}
add_filter( 'request', 'fix_sub_taxonomy_queries', 10 );

// function to do what get_category_parents does for other taxonomies
function get_taxonomy_parents($id, $taxonomy, $link = false, $separator = '/', $nicename = false, $visited = array()) {
  $chain = '';
  $parent = get_term($id, $taxonomy);

  if (is_wp_error($parent)) {
    return $parent;
  }

  if ($nicename) {
    $name = $parent -> slug;
	} else {
    $name = $parent -> name;

  }
  if ($parent -> parent && ($parent -> parent != $parent -> term_id) && !in_array($parent -> parent, $visited)) {
    $visited[] = $parent -> parent;
    $chain .= get_taxonomy_parents($parent -> parent, $taxonomy, $link, $separator, $nicename, $visited);
  }

  if ($link) {
      // nothing, can't get this working :(
  } else {
    $chain .= $name . $separator;
  }

  return $chain;
} ?>
