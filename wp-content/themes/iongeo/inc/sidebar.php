<?php
function custom_sidebar($sidebar, $image = false) {
	global $post;
	if ( !is_object( $post ) && !is_tax() ) {
		return;
	} ?>
	<div id="sidebar" class="col-4">
		<?php
		$image = get_field('sidebar_image', $post->ID) ? get_field('sidebar_image')['sizes']['small-medium'] : $image;
		if($image) { ?>
			<div id="sidebar-featured-image" style="background-image: url('<?php echo $image; ?>')">

			</div>
		<?php
		}
		if ( is_active_sidebar( $sidebar ) ) :
				dynamic_sidebar( $sidebar );
		endif; ?>
	</div>
<?php
}

function custom_sidebar_modules ($modules) {
  ob_start(); ?>
    <div id="sidebar" class="col-4">
      <?php
      foreach ( $modules as $module ) {
        switch ($module['acf_fc_layout']) {
          case 'related_post_type':
            sidebar_related_post_type( $module );
            break;
          case 'image':
            sidebar_image( $module );
          break;
          case 'html':
            sidebar_html( $module );
          break;
        } ?>
        <p>
          <?php
          // print_r($module); ?>
        </p>
      <?php
      } ?>
  	</div>
  <?php
  return ob_get_clean();
}

function sidebar_related_post_type ($module) {
  $p_type = $module['post_type'];
  $post_type_obj = get_post_type_object( $p_type );
  $title = 'Related '.$post_type_obj->label;
  $related_field = '';

  $args = [
    'post_type' => $p_type,
    'posts_per_page' => 5
  ];

  if ( is_tax() ) {
    $term = $id = get_queried_object();
    $post_type = $term->taxonomy;
  } else { // Post
    global $post;
    $post_type = $post->post_type;
    $id = $post->ID;
  }

  $fields = get_field_objects($id);

  foreach($fields as $key=>$field) {
    if(strpos($key, '_x_') !== false && strpos($key, $post_type) !== false && strpos($key, $p_type) !== false) {
      $related_field = $key;
    }
  }

  if ( $related_field == '' ) {
    $related_field = 'related_' . strtolower($post_type_obj->label);
  }

  $related_ids = [];
  if($related_posts = get_field($related_field, $id)) {
    foreach($related_posts as $related_post) {
      $related_post_id = is_object($related_post) ? $related_post->ID : $related_post;
      array_push($related_ids, $related_post_id);
    }
    $args['posts_per_page'] = count($related_ids);
    $args['orderby'] = 'post__in';
    $args['post__in'] = $related_ids;
  } else {
    return;
  }
  if(isset($instance['title']) && $instance['title'] != null) {
    $title = $instance['title'];
  }

  $related = new WP_Query($args);
  if($related->have_posts()) : ?>
    <div class="sidebar-ion-related" style="margin-bottom: 40px;">
      <h4><?php echo $title; ?></h4>
      <?php
      while($related->have_posts()): $related->the_post();
        $post_link = get_the_permalink();
        $link_class = 'post-link';
        $link_target = '_self';
        $file_type = false;
        $post_title = explode( ' – ', get_the_title() );
        $post_title = $post_title[0];
        // $post_title = get_the_title();
        if(get_post_type() == 'resource' && $filearray = get_field('upload_pdf')) {
          echo get_resource_link($filearray);
        } else { ?>
          <a class="<?php echo $link_class; ?>" target="<?php echo $link_target; ?>" href="<?php echo $post_link; ?>">
            <?php echo $post_title; ?>
          </a>
        <?php
        }
      endwhile;
      wp_reset_postdata(); ?>
    </div>
  <?php
  endif;
}

function sidebar_image ($module) { ?>
  <div class="sidebar-image" style="margin-bottom: 40px;">
    <?php
    if ( $module['title'] ) { ?>
      <h4 style="margin-bottom: 0;"><?php echo $module['title']; ?></h4>
    <?php
    }
    if ( $module['image'] ) { ?>
      <div>
        <img style="width: 100%; height: auto;" src="<?php echo $module['image']['sizes']['small-medium']; ?>"/>
			</div>
    <?php
    } ?>
  </div>
<?php
}

function sidebar_html ($module) { ?>
  <div class="sidebar-html" style="margin-bottom: 40px;">
    <?php
    if ( $module['title'] ) { ?>
      <h4 style="margin-bottom: 0;"><?php echo $module['title']; ?></h4>
    <?php
    }
    if ( $module['html'] ) { ?>
      <div>
        <?php echo $module['html']; ?>
			</div>
    <?php
    } ?>
  </div>
<?php
}
