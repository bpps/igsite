<?php
function ion_change_post_label() {
    global $menu;
    global $submenu;
    if($submenu['edit.php'][5][0] != 'Insights') {
      $menu[5][0] = 'Insights';
      $submenu['edit.php'][5][0] = 'Insights';
      $submenu['edit.php'][10][0] = 'Add Insight';
      $submenu['edit.php'][16][0] = 'Insight Tags';
    }

}
function ion_change_post_object() {
    global $wp_post_types;
    //print_r($wp_post_types['data-library']);
    //print_r($wp_post_types['technology']);

    $labels = &$wp_post_types['post']->labels;
    if($labels->name != 'Insights') {
      $wp_post_types['post']->hide_tax_crumbs = true;
      $wp_post_types['post']->archive_url = 'insights';
      $labels->name = 'Insights';
      $labels->singular_name = 'Insight';
      $labels->add_new = 'Add Insight';
      $labels->add_new_item = 'Add Insight';
      $labels->edit_item = 'Edit Insight';
      $labels->new_item = 'Insight';
      $labels->view_item = 'View Insight';
      $labels->search_items = 'Search Insights';
      $labels->not_found = 'No Insights found';
      $labels->not_found_in_trash = 'No Insights found in Trash';
      $labels->all_items = 'All Insights';
      $labels->menu_name = 'Insights';
      $labels->name_admin_bar = 'Insights';
    }

}

add_action( 'admin_menu', 'ion_change_post_label' );
add_action( 'init', 'ion_change_post_object' );

function display_insights( $atts = false, $content = false, $tag = false) {
  if($atts) {
    $a = shortcode_atts( array(), $atts );
  }
  ob_start();
  $featuredIds = [];
  if($featured = get_field('featured_insights', 'option')) { ?>
    <div id="featured-insights-container" class="featured-posts-slider-container">
      <h3>Featured</h3>
      <div id="featured-insights" class="featured-posts-slider">
        <?php
        foreach($featured as $feature) {
          array_push($featuredIds, $feature->ID); ?>
          <div class="featured-post">
            <div class="flex">
              <?php
              $image = get_template_directory_uri().'/images/post-placeholder.png';
              if(has_post_thumbnail($feature->ID)) {
                $image = get_the_post_thumbnail_url($feature->ID, 'small-medium');
              } ?>
              <div class="featured-post-image-container test-for-git col-5">
                <a href="<?php echo get_the_permalink($feature->ID); ?>" class="featured-post-image bg-centered ratio-3-2" style="background-image:url(<?php echo $image; ?>)">
                </a>
              </div>
              <div class="featured-post-content col-7">
                <h2>
                  <a href="<?php echo get_the_permalink($feature->ID); ?>">
                    <?php echo $feature->post_title; ?>
                  </a>
                </h2>
                <?php
                if(has_excerpt($feature->ID)) { ?>
                  <div class="event-desc">
                    <?php the_excerpt(); ?>
                  </div>
                <?php
                } ?>
              </div>
            </div>
          </div>
        <?php
        } ?>
      </div>
      <?php
      if(count($featuredIds) > 1) { ?>
        <div class="featured-posts-arrows">
          <a href="#" class="circle-arrow arrow-prev" data-func="prev">
          </a>
          <a href="#" class="circle-arrow arrow-next" data-max-pages="<?php echo $maxpages; ?>" data-func="next">
          </a>
        </div>
      <?php
      } ?>
    </div>
  <?php
  }
  $args = [
    'post_type' => 'post',
    'posts_per_page' => 6,
    'post__not_in' => $featuredIds,
    'paged' => 1,
  ];
  $topic = false;
  $all_posts_args = array(
    'posts_per_page'   => -1,
    'post_type'        => 'post',
  );
  $post_tag_query = new WP_Query( $all_posts_args );
  $post_tag_array = [];
  if($post_tag_query->have_posts()) :
    ob_start();
		while($post_tag_query->have_posts()): $post_tag_query->the_post();
      array_push($post_tag_array, get_the_ID());
    endwhile;
    wp_reset_postdata();
  endif;
  if(isset($_REQUEST['topic'])) {
    $topic = $_REQUEST['topic'];
    $args['tag_slug__in'] = [$topic];
  }
  $tags = wp_get_object_terms( $post_tag_array, 'post_tag' );
  ob_start(); ?>
  <div class="grid-filter insights">
    <span class="grid-filter-label">
      Topics:
    </span>
    <select class="filter-items">
      <option class="filter-item" value="all">all</option>
      <?php
      foreach($tags as $tag) { ?>
        <option class="filter-item" value="<?php echo $tag->slug; ?>"><?php echo $tag->name; ?></option>
      <?php
      } ?>
      <!--<img src="<?php echo get_template_directory_uri(); ?>/images/plus.png" class="more-insights-button">-->
    </select>
  </div>
  <?php
  $filterHTML = ob_get_clean();
  $filter = [
    'filterby' => 'tags',
    'filter' =>  $filterHTML,
  ];
  wp_localize_script( 'iongeo-js', 'filterObj', $filter );
  echo display_post_grid($args, true);
  return ob_get_clean();
}

add_shortcode( 'insights', 'display_insights' );

add_action('wp_ajax_nopriv_posts_ajax', 'posts_ajax');
add_action('wp_ajax_data_posts_ajax', 'posts_ajax');

function posts_ajax(){
  $paged = $_REQUEST['paged'];
  $posttype = $_REQUEST['posttype'];
  $postCount = $_REQUEST['count'];
  $exclude = $_REQUEST['exclude'];
  $args = [
    'post_type' => $posttype,
    'posts_per_page' => $postCount,
    'paged' => $paged,
    'post__not_in' => $exclude,
    'post_status' => 'publish'
  ];
  if($tag = $_REQUEST['filter']) {
    if($tag != 'false') {
      $args['tag'] = $tag;
    }
  }
  //echo json_encode($args);
  echo json_encode(get_grid_posts($args));
  //echo json_encode($args);
	die();
}

function display_post_grid($args, $filter) {
  ob_start();
  $posts = get_grid_posts($args); ?>
  <div class="ion-post-grid-container">
		<div class="ion-post-grid" has-filter=<?php echo $filter; ?>>
			<div class="grid-gutter"></div>
			<?php
      echo $posts->posts; ?>
		</div>
    <div class="show-more-wrapper">
      <a href="#" <?php echo !$posts->more ? ' style="display:none;"' : ''; ?> data-filter="" class="more-posts more-button" data-exclude="<?php echo json_encode($args['post__not_in']); ?>" data-paged="2" data-post-count="<?php echo $args['posts_per_page']; ?>" data-post-type="post">Show More</a>
    </div>
  </div>
  <?php
	$content = ob_get_clean();
  return $content;
}

function get_grid_posts($args) {
  $postsObj = (object)[
    'more' => false,
  ];
	$gridPosts = new WP_Query($args);
	if($gridPosts->have_posts()) :
    ob_start();
		while($gridPosts->have_posts()): $gridPosts->the_post(); ?>
			<div class="grid-post new-post">
				<a href="<?php echo get_the_permalink(); ?>">
          <?php
          $thumb = has_post_thumbnail()
            ? get_the_post_thumbnail_url(get_the_ID(), 'medium')
            : get_template_directory_uri().'/images/post-placeholder.png'; ?>
          <div class="post-thumbnail" style="background-image: url('<?php echo $thumb; ?>')"></div>
        </a>
        <a href="<?php echo get_the_permalink(); ?>">
					<h5><?php the_title(); ?></h5>
          <?php
          if(has_excerpt()) { ?>
            <div class="grid-post-desc with-arrow">
              <?php echo get_the_excerpt(); ?>
            </div>
          <?php
          } ?>
				</a>
			</div>
		<?php
		endwhile;
    $postsObj->posts = ob_get_clean();
		// $taxObj->maxpages = $gridPosts->max_num_pages;
		wp_reset_postdata();
    if($gridPosts->max_num_pages > $args['paged']) {
      $postsObj->more = true;
    }
	endif;
  return $postsObj;
}

function get_related_insights($insights) {
  ob_start(); ?>
  <div class="technology-x-insights content-inner">
    <?php
    foreach($insights as $insight) { ?>
      <div class="insight-item-container">
        <div class="insight-item flex">
          <?php
          $image = get_template_directory_uri().'/images/post-placeholder.png';
          if(has_post_thumbnail($insight)) {
            $image = get_the_post_thumbnail_url($insight, 'small-medium');
          } ?>
          <div class="insight-item-image-container col-3">
            <div class="insight-item-image bg-centered" style="background-image:url(<?php echo $image; ?>);">
            </div>
          </div>
          <div class="insight-item-content col-9">
            <h4><?php echo get_the_title($insight); ?></h4>
            <?php
            if(has_excerpt($insight)) { ?>
              <div class="insight-item-desc">
                <?php echo get_the_excerpt($insight); ?>
              </div>
            <?php
            } ?>
            <a class="ion-cta" href="<?php echo get_the_permalink($insight); ?>">
              Keep Reading
            </a>
          </div>
        </div>
      </div>
    <?php
    } ?>
  </div>
  <?php
  return ob_get_clean();
} ?>
