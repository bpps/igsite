<?php
function register_data_library_post_type() {
  $labels = array(
		'name'                       => _x( 'Data Types', 'taxonomy general name', 'iongeo' ),
		'singular_name'              => _x( 'Multi-client Product', 'taxonomy singular name', 'iongeo' ),
		'search_items'               => __( 'Search Multi-client Products', 'iongeo' ),
		'popular_items'              => __( 'Popular Multi-client Products', 'iongeo' ),
		'all_items'                  => __( 'All Multi-client Products', 'iongeo' ),
		'parent_item'                => null,
		'parent_item_colon'          => null,
		'edit_item'                  => __( 'Edit Multi-client Product', 'iongeo' ),
		'update_item'                => __( 'Update Multi-client Product', 'iongeo' ),
		'add_new_item'               => __( 'Add New Multi-client Product', 'iongeo' ),
		'new_item_name'              => __( 'New Multi-client Product Name', 'iongeo' ),
		'separate_items_with_commas' => __( 'Separate multi-client products with commas', 'iongeo' ),
		'add_or_remove_items'        => __( 'Add or remove multi-client products', 'iongeo' ),
		'choose_from_most_used'      => __( 'Choose from the most used multi-client products', 'iongeo' ),
		'not_found'                  => __( 'No multi-client products found.', 'iongeo' ),
		'menu_name'                  => __( 'Data Types', 'iongeo' ),
	);
	$args = array(
    'show_tagcloud'         => true,
		'hierarchical'          => true,
		'labels'                => $labels,
		'show_ui'               => true,
		'show_admin_column'     => true,
		'show_in_nav_menus'     => true,
    'show_in_rest'          => true,
		'public'								=> true,
		'update_count_callback' => '_update_post_term_count',
		'query_var'             => true,
		'rewrite'								=> true,
	);

	register_taxonomy( 'data-type', array('data-library', 'technology'), $args );
	register_taxonomy_for_object_type( 'data-type', array('data-library', 'technology') );

  $labels = array(
    'name' => _x('Data Library', 'post type general name'),
    'singular_name' => _x('Region Or Program', 'post type singular name'),
    'add_new' => _x('Add New Region Or Program', 'data-library'),
    'add_new_item' => __('Add New Region Or Program'),
    'edit_item' => __('Edit Region Or Program'),
    'new_item' => __('New Region Or Program'),
    'view_item' => __('View Region Or Program'),
    'search_items' => __('Search Regions Or Programs'),
    'not_found' =>  __('Nothing found'),
    'not_found_in_trash' => __('Nothing found in Trash'),
    'parent_item_colon' => __('Parent Region Or Program:'),
  );
  $args = [
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => true,
    'exclude_from_search' => false,
    'show_ui' => true,
    'query_var' => true,
    'show_in_rest' => true,
    'capability_type' => 'post',
    'rewrite' => true,
    'archive_url' => 'data-library',
    //'rest_controller_class' => 'WP_REST_Posts_Controller',
    'hierarchical' => true,
    'menu_position' => 1,
    'has_archive' => 'data-library',
    'menu_icon'   => 'dashicons-admin-site',
    'supports' => ['title', 'editor', 'thumbnail', 'excerpt', 'page-attributes', 'revisions'],
    'taxonomies' => ['data-type'],
    'hide_tax_crumbs' => true,
  ];

  register_post_type( 'data-library' , $args );

  $labels = array(
		'name'                       => _x( 'Authors', 'taxonomy general name', 'iongeo' ),
		'singular_name'              => _x( 'Author', 'taxonomy singular name', 'iongeo' ),
		'search_items'               => __( 'Search Authors', 'iongeo' ),
		'popular_items'              => __( 'Popular Authors', 'iongeo' ),
		'all_items'                  => __( 'All Authors', 'iongeo' ),
		'parent_item'                => null,
		'parent_item_colon'          => null,
		'edit_item'                  => __( 'Edit Author', 'iongeo' ),
		'update_item'                => __( 'Update Author', 'iongeo' ),
		'add_new_item'               => __( 'Add New Author', 'iongeo' ),
		'new_item_name'              => __( 'New Author Name', 'iongeo' ),
		'separate_items_with_commas' => __( 'Separate authors with commas', 'iongeo' ),
		'add_or_remove_items'        => __( 'Add or remove author', 'iongeo' ),
		'choose_from_most_used'      => __( 'Choose from the most common authors', 'iongeo' ),
		'not_found'                  => __( 'No authors found.', 'iongeo' ),
		'menu_name'                  => __( 'Author', 'iongeo' ),
	);

	$args = array(

    'show_tagcloud'         => true,
		'hierarchical'          => true,
		'labels'                => $labels,
		'show_ui'               => true,
		'show_admin_column'     => true,
    'show_in_rest'          => true,
		'show_in_nav_menus'     => true,
		'public'								=> true,
		'update_count_callback' => '_update_post_term_count',
		'query_var'             => true,
		'rewrite'               => true,
	);

	register_taxonomy( 'resource-author', array('resource', 'post'), $args );
	register_taxonomy_for_object_type( 'resource-author', array('resource', 'post') );

  $labels = array(
    'name'                       => _x( 'Interests', 'taxonomy general name', 'iongeo' ),
    'singular_name'              => _x( 'Interest', 'taxonomy singular name', 'iongeo' ),
    'search_items'               => __( 'Search Interests', 'iongeo' ),
    'popular_items'              => __( 'Popular Interests', 'iongeo' ),
    'all_items'                  => __( 'All Interests', 'iongeo' ),
    'parent_item'                => null,
    'parent_item_colon'          => null,
    'edit_item'                  => __( 'Edit Interest', 'iongeo' ),
    'update_item'                => __( 'Update Interest', 'iongeo' ),
    'add_new_item'               => __( 'Add New Interest', 'iongeo' ),
    'new_item_name'              => __( 'New Interest Name', 'iongeo' ),
    'separate_items_with_commas' => __( 'Separate interests with commas', 'iongeo' ),
    'add_or_remove_items'        => __( 'Add or remove interest', 'iongeo' ),
    'choose_from_most_used'      => __( 'Choose from the most common interests', 'iongeo' ),
    'not_found'                  => __( 'No interests found.', 'iongeo' ),
    'menu_name'                  => __( 'Interest', 'iongeo' ),
  );

  $args = array(

    'show_tagcloud'         => true,
    'hierarchical'          => true,
    'labels'                => $labels,
    'show_ui'               => true,
    'show_admin_column'     => true,
    'show_in_nav_menus'     => true,
    'show_in_rest'          => true,
    'public'								=> false,
    'update_count_callback' => '_update_post_term_count',
    'query_var'             => true,
    'rewrite'               => false,
  );

  register_taxonomy( 'resource-interest', array('resource'), $args );
  register_taxonomy_for_object_type( 'resource-interest', array('resource') );

  $labels = array(
    'name'                       => _x( 'Business units', 'taxonomy general name', 'iongeo' ),
    'singular_name'              => _x( 'Business unit', 'taxonomy singular name', 'iongeo' ),
    'search_items'               => __( 'Search Business units', 'iongeo' ),
    'popular_items'              => __( 'Popular Business units', 'iongeo' ),
    'all_items'                  => __( 'All Business units', 'iongeo' ),
    'parent_item'                => null,
    'parent_item_colon'          => null,
    'edit_item'                  => __( 'Edit Business unit', 'iongeo' ),
    'update_item'                => __( 'Update Business unit', 'iongeo' ),
    'add_new_item'               => __( 'Add New Business unit', 'iongeo' ),
    'new_item_name'              => __( 'New Business unit Name', 'iongeo' ),
    'separate_items_with_commas' => __( 'Separate business units with commas', 'iongeo' ),
    'add_or_remove_items'        => __( 'Add or remove business unit', 'iongeo' ),
    'choose_from_most_used'      => __( 'Choose from the most common business units', 'iongeo' ),
    'not_found'                  => __( 'No business units found.', 'iongeo' ),
    'menu_name'                  => __( 'Business unit', 'iongeo' ),
  );

  $args = array(

    'show_tagcloud'         => true,
    'hierarchical'          => true,
    'labels'                => $labels,
    'show_ui'               => true,
    'show_admin_column'     => true,
    'show_in_nav_menus'     => true,
    'show_in_rest'          => true,
    'public'								=> false,
    'update_count_callback' => '_update_post_term_count',
    'query_var'             => true,
    'rewrite'               => false,
  );

  register_taxonomy( 'business-unit', array('resource'), $args );
  register_taxonomy_for_object_type( 'business-unit', array('resource') );

  $labels = array(
    'name'                       => _x( 'Resource Categories', 'taxonomy general name', 'iongeo' ),
    'singular_name'              => _x( 'Resource Category', 'taxonomy singular name', 'iongeo' ),
    'search_items'               => __( 'Search Resource Categories', 'iongeo' ),
    'popular_items'              => __( 'Popular Resource Categories', 'iongeo' ),
    'all_items'                  => __( 'All Resource Categories', 'iongeo' ),
    'parent_item'                => null,
    'parent_item_colon'          => null,
    'edit_item'                  => __( 'Edit Resource Category', 'iongeo' ),
    'update_item'                => __( 'Update Resource Category', 'iongeo' ),
    'add_new_item'               => __( 'Add New Resource Category', 'iongeo' ),
    'new_item_name'              => __( 'New Resource Category Name', 'iongeo' ),
    'separate_items_with_commas' => __( 'Separate resource categories with commas', 'iongeo' ),
    'add_or_remove_items'        => __( 'Add or remove resource category', 'iongeo' ),
    'choose_from_most_used'      => __( 'Choose from the most common resource categories', 'iongeo' ),
    'not_found'                  => __( 'No resource categories found.', 'iongeo' ),
    'menu_name'                  => __( 'Resource Category', 'iongeo' ),
  );

  $args = array(

    'show_tagcloud'         => true,
    'hierarchical'          => true,
    'labels'                => $labels,
    'show_ui'               => true,
    'show_admin_column'     => true,
    'show_in_nav_menus'     => true,
    'show_in_rest'          => true,
    'public'								=> false,
    'update_count_callback' => '_update_post_term_count',
    'query_var'             => true,
    'rewrite'               => false,
  );

  register_taxonomy( 'resource-category', array('resource'), $args );
  register_taxonomy_for_object_type( 'resource-category', array('resource') );

  $labels = array(
    'name'                       => _x( 'Sources', 'taxonomy general name', 'iongeo' ),
    'singular_name'              => _x( 'Source', 'taxonomy singular name', 'iongeo' ),
    'search_items'               => __( 'Search Sources', 'iongeo' ),
    'popular_items'              => __( 'Popular Sources', 'iongeo' ),
    'all_items'                  => __( 'All Sources', 'iongeo' ),
    'parent_item'                => null,
    'parent_item_colon'          => null,
    'edit_item'                  => __( 'Edit Source', 'iongeo' ),
    'update_item'                => __( 'Update Source', 'iongeo' ),
    'add_new_item'               => __( 'Add New Source', 'iongeo' ),
    'new_item_name'              => __( 'New Source Name', 'iongeo' ),
    'separate_items_with_commas' => __( 'Separate sources with commas', 'iongeo' ),
    'add_or_remove_items'        => __( 'Add or remove source', 'iongeo' ),
    'choose_from_most_used'      => __( 'Choose from the most common sources', 'iongeo' ),
    'not_found'                  => __( 'No sources found.', 'iongeo' ),
    'menu_name'                  => __( 'Source', 'iongeo' ),
  );

  $args = array(

    'show_tagcloud'         => true,
    'hierarchical'          => true,
    'labels'                => $labels,
    'show_ui'               => true,
    'show_admin_column'     => true,
    'show_in_rest'          => true,
    'show_in_nav_menus'     => true,
    'public'								=> false,
    'update_count_callback' => '_update_post_term_count',
    'query_var'             => true,
    'rewrite'               => false,
  );

  register_taxonomy( 'resource-source', array('resource'), $args );
  register_taxonomy_for_object_type( 'resource-source', array('resource') );
}

add_action('init', 'register_data_library_post_type');

function display_data_library( $atts = false ) {
  $mapObj = (object)[];
  if($atts) {
    $a = shortcode_atts( array(), $atts );
    if($a['guid']) {
      $mapObj->token = $a['token'];
      //$mapSrc .= '&linkGuid='.$a['guid'];
    }
    if($a['slug']) {
      $mapObj->slug = $a['slug'];
      //$mapSrc .= '&selectGroup='.$a['slug'];
    }
  }
  ob_start();
    $mapObj->title = 'Data Library';
    echo get_data_library_map($mapObj);
  return ob_get_clean();
}
add_shortcode( 'data-library', 'display_data_library' );

function get_dataset_fields( $data ) {
	$APIposts = get_posts( array(
    'post_type' => 'data-library',
    'meta_key'		=> 'token',
	  'meta_value'	=> $data['token']
	) );

	if ( empty( $APIposts ) ) {
		return 'No dataset found';
	}

  $dataset = (object)[
    'title' => $APIposts[0]->post_title,
    'permalink' => get_the_permalink($APIposts[0]->ID)
  ];
  if(has_excerpt($APIposts[0]->ID)) {
    $dataset->excerpt = get_the_excerpt($APIposts[0]->ID);
  }
  if(has_post_thumbnail($APIposts[0]->ID)) {
    $dataset->image = get_the_post_thumbnail_url($APIposts[0]->ID, 'small');
  }
  if($datasheet = get_field('datasheet', $APIposts[0]->ID)){
    $dataset->datasheet = $datasheet['url'];
  }
	return json_encode($dataset);
}

add_action( 'rest_api_init', function () {
	register_rest_route( 'iongeo/v2', '/dataset/', array(
		'methods' => 'GET',
		'callback' => 'get_dataset_fields',
	) );
} );

add_action('wp_ajax_nopriv_data_library', 'data_library_ajax');
add_action('wp_ajax_data_library', 'data_library_ajax');

function data_library_ajax(){
  switch($_REQUEST['fn']){
    case 'get_dataset_data':
      $data = $_POST['data'];
      echo json_encode(get_dataset_data($data));
	  break;
    case 'onpagesearch':
      $search = $_POST['search'];
      echo get_locations_search($search);
    break;
		default:
			$output = 'nothing here';
			echo $output;
		break;
	}
	die();
}

function get_dataset_data($data) {
  $dataObj = (object)[];
  $args = array(
    'post_type' => 'data-library',
    'posts_per_page' => 1,
    'meta_query'=> [
      [
        'key' => 'token',
        'compare' => '=',
        'value' => $data['Token'],
      ]
    ],
   'meta_key' => 'token',
  );
  $dataPrograms = new WP_Query($args);
  if($dataPrograms->have_posts()) :
    while($dataPrograms->have_posts()): $dataPrograms->the_post();
      $dataObj->overview = get_the_excerpt();
      if($data['Type'] == 'dataset') {
        $dataObj->url = $data->Url.'?data-program='.basename(get_the_permalink());
      } else {
        $dataObj->url = get_the_permalink();
      }
      ob_start();
      echo get_page_crumbs(get_post(get_the_ID()));
      $dataObj->crumbs = ob_get_clean();
    endwhile;
    wp_reset_postdata();
  endif;
  return $dataObj;
}

function get_locations_search($search) {
  $args = [
		's' => $search,
		'post_type' => 'data-library',
    'meta_key' => 'post_type',
    'meta_value' => 'data-program',
		'posts_per_page' => 6
	];
	$posts = get_list_posts($args, 30);
	if($posts['posts']) {
		return $posts['posts'];
	} else {
		return 'Sorry, no results';
	}
}

function get_data_library_map($mapIDs = false) {
  $mapSrc = 'https://public.geopostenergy.com/CustomMap/Map?token=d3c8c4cb-3860-4bc2-ae84-0b0cec1e3f3f&betaTestAPI=true';
  //$mapSrc = 'http://localhost:8888/ion/datalibraryiframe';
  $token = $title = '';
  if(isset($mapIDs['token'])) {
    $token = $mapIDs['token'];
    $mapSrc .= '&integrationToken='.$token;
  } else {
    // $mapSrc .= '&integrationToken=726dd24d-100b-4873-a05b-10830d1e4037';
  }
  if(isset($mapIDs['slug'])) {
    $mapSrc .= '&selectGroup='.$mapIDs['slug'];
  }
  if(isset($mapIDs['title'])) {
    $title = $mapIDs['title'];
  } ?>
  <div id="data-library-container" data-token="<?php echo $token; ?>" data-title="<?php echo $title; ?>">
    <?php get_loader(true, true); ?>
    <iframe id="data-library-iframe" src="<?php echo $mapSrc; ?>" scrolling="no" frameborder="0"></iframe>
  </div>
  <?php
  if($overview = get_field('data_library_overview', 'option')){ ?>
    <div class="data-library-overview flex">
      <div class="overview-intro">
        <h2>Overview</h2>
        <p>What is the data library?</p>
      </div>
      <div class="overview-divider">
      </div>
      <div class="overview-content">
        <?php echo $overview; ?>
      </div>
    </div>
    <?php
    if($featuredPrograms = get_field('featured_programs', 'option')) { ?>
      <div id="featured-programs-post-row" class="post-row">
        <h2>Featured Programs</h2>
        <div class="post-row-content column-count-3 flex row">
          <?php
          foreach($featuredPrograms as $program) { ?>
            <div class="post-row-item col-4">
              <?php
              $image = has_post_thumbnail($program->ID) ? get_the_post_thumbnail_url($program->ID) : get_template_directory_uri().'/images/post-placeholder.png'; ?>
              <a href="<?php echo get_the_permalink($program->ID); ?>" class="bg-centered post-row-image-container ratio-image-container ratio-3-1">
                <img class="post-row-image" src="<?php echo $image; ?>"/>
              </a>
              <h6 class="post-row-title">
                <a href="<?php echo get_the_permalink($program->ID); ?>">
                  <?php echo $program->post_title; ?>
                </a>
              </h6>
              <?php
              if(has_excerpt($program->ID)) { ?>
                <div class="post-row-item-content">
                  <?php echo get_the_excerpt($program->ID); ?>
                </div>
              <?php
              } ?>
            </div>
          <?php
          } ?>
        </div>
      </div>
    <?php
    }
    get_multi_client_products();
  }
}

function get_multi_client_products() {
  $mcps = get_terms(['taxonomy' => 'data-type']); ?>
  <div id="mcps" class="post-row">
    <h2>Multi-Client Products</h2>
    <div class="post-row-content flex row">
      <?php
      foreach($mcps as $mcp) { ?>
        <div class="post-row-item col-3">
          <?php
          $image = '';
          if($taximage = get_field('featured_image', $mcp)) {
            $image = $taximage['sizes']['small-medium'];
          } else {
            $image = get_template_directory_uri().'/images/post-placeholder.png';
          } ?>
          <a href="<?php echo get_term_link($mcp); ?>" class="post-row-image-container ratio-image-container ratio-3-2">
            <div class="post-row-image bg-centered animated-image" style="background-image:url('<?php echo $image; ?>');">

            </div>
          </a>
          <h6 class="post-row-title">
            <a href="<?php echo get_term_link($mcp); ?>">
              <?php echo $mcp->name; ?>
            </a>
          </h6>
          <?php
          if($mcp->description) { ?>
            <div class="post-row-item-content">
              <?php echo character_limit($mcp->description, 100); ?>
            </div>
          <?php
          } ?>
        </div>
      <?php
      } ?>
    </div>
  </div>
<?php
}
