<?php
function register_resources_post_type() {

  $labels = array(
		'name'                       => _x( 'Resource Types', 'taxonomy general name', 'iongeo' ),
		'singular_name'              => _x( 'Resource Type', 'taxonomy singular name', 'iongeo' ),
		'search_items'               => __( 'Search Resource Types', 'iongeo' ),
		'popular_items'              => __( 'Popular Resource Types', 'iongeo' ),
		'all_items'                  => __( 'All Resource Types', 'iongeo' ),
		'parent_item'                => null,
		'parent_item_colon'          => null,
		'edit_item'                  => __( 'Edit Resource Type', 'iongeo' ),
		'update_item'                => __( 'Update Resource Type', 'iongeo' ),
		'add_new_item'               => __( 'Add New Resource Type', 'iongeo' ),
		'new_item_name'              => __( 'New Resource Type Name', 'iongeo' ),
		'separate_items_with_commas' => __( 'Separate resource types with commas', 'iongeo' ),
		'add_or_remove_items'        => __( 'Add or remove resource', 'iongeo' ),
		'choose_from_most_used'      => __( 'Choose from the most used resources', 'iongeo' ),
		'not_found'                  => __( 'No resources found.', 'iongeo' ),
		'menu_name'                  => __( 'Resource Type', 'iongeo' ),
	);

	$args = array(

    'show_tagcloud'         => true,
		'hierarchical'          => true,
		'labels'                => $labels,
		'show_ui'               => true,
		'show_admin_column'     => true,
    'show_in_rest'          => true,
		'show_in_nav_menus'     => true,
		'public'								=> true,
		'update_count_callback' => '_update_post_term_count',
		'query_var'             => true,
		'rewrite'               => array( 'slug' => 'resource-type' ),
	);

	register_taxonomy( 'resource-type', array('resource'), $args );
	register_taxonomy_for_object_type( 'resource-type', array('resource') );

  $labels = array(
    'name' => _x('Resources', 'post type general name'),
    'singular_name' => _x('Resource', 'post type singular name'),
    'add_new' => _x('Add New', 'resource'),
    'add_new_item' => __('Add New Resource'),
    'edit_item' => __('Edit Resource'),
    'new_item' => __('New Resource'),
    'view_item' => __('View Resource'),
    'search_items' => __('Search Resources'),
    'not_found' =>  __('Nothing found'),
    'not_found_in_trash' => __('Nothing found in Trash'),
    'parent_item_colon' => __('Parent Resources:')
  );

  $args = array(
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => true,
    'exclude_from_search' => false,
    'show_ui' => true,
    'show_in_rest' => true,
    'query_var' => true,
    'archive_url' => 'resources',
    'rewrite' => ['slug' => 'resource', 'with_front' => true],
    'capability_type' => 'post',
    'hierarchical' => false,
    'menu_position' => 5,
    'has_archive' => 'resources',
    'menu_icon'   => 'dashicons-media-spreadsheet',
    'supports' => ['title', 'editor', 'thumbnail', 'excerpt', 'revisions'],
    'taxonomies' => ['resource-type', 'post_tag'],
  );

  register_post_type( 'resource' , $args );
}

add_action('init', 'register_resources_post_type');
