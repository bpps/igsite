<?php
function register_technology_post_type() {
  // Register technology types taxonomy
  $labels = array(
		'name'                       => _x( 'Technology Type', 'taxonomy general name', 'iongeo' ),
		'singular_name'              => _x( 'Technology Type', 'taxonomy singular name', 'iongeo' ),
		'search_items'               => __( 'Search Technology Types', 'iongeo' ),
		'popular_items'              => __( 'Popular Technology Types', 'iongeo' ),
		'all_items'                  => __( 'All Technology Types', 'iongeo' ),
		'parent_item'                => null,
		'parent_item_colon'          => null,
		'edit_item'                  => __( 'Edit Technology Type', 'iongeo' ),
		'update_item'                => __( 'Update Technology Type', 'iongeo' ),
		'add_new_item'               => __( 'Add New Technology Type', 'iongeo' ),
		'new_item_name'              => __( 'New Technology Type Name', 'iongeo' ),
		'separate_items_with_commas' => __( 'Separate technology types with commas', 'iongeo' ),
		'add_or_remove_items'        => __( 'Add or remove technology types', 'iongeo' ),
		'choose_from_most_used'      => __( 'Choose from the most used technology types', 'iongeo' ),
		'not_found'                  => __( 'No technology types found.', 'iongeo' ),
		'menu_name'                  => __( 'Technology Type', 'iongeo' ),
	);

	$args = array(
    'show_tagcloud'         => true,
		'hierarchical'          => true,
		'labels'                => $labels,
		'show_ui'               => true,
    'show_in_rest'          => true,
		'show_admin_column'     => true,
		'show_in_nav_menus'     => true,
		'public'								=> true,
		'update_count_callback' => '_update_post_term_count',
		'query_var'             => true,
    'rewrite'               => [ 'slug' => 'technologies', 'hierarchical' => true, 'with_front' => false ],
	);

	register_taxonomy( 'technology-type', array('technology'), $args );
	register_taxonomy_for_object_type( 'technology-type', array('technology') );

	$labels = array(
    'name' => _x('Technologies', 'post type general name'),
    'singular_name' => _x('Technology', 'post type singular name'),
    'add_new' => _x('Add New Technology', 'technology'),
    'add_new_item' => __('Add New Technology'),
    'edit_item' => __('Edit Technology'),
    'new_item' => __('New Technology'),
    'view_item' => __('View Technology'),
    'search_items' => __('Search Technologies'),
    'not_found' =>  __('Nothing found'),
    'not_found_in_trash' => __('Nothing found in Trash'),
    'parent_item_colon' => __('Parent Technology:'),
  );
	//remove_rewrite_tag('%customtax%');
  $args = array(
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => true,
    'exclude_from_search' => false,
    'show_ui' => true,
    'query_var' => true,
    'show_in_rest' => true,
    'capability_type' => 'post',
    'rewrite' => true,
    'archive_url' => 'technologies',
    'primary_tax_type' => 'technology-type',
    'rewrite' => ['slug' => 'technologies/%technology-type%', 'with_front' => false],
    'hierarchical' => true,
    'menu_position' => 3,
    'has_archive' => 'technologies',
    'menu_icon'   => 'dashicons-laptop',
    'supports' => array('title', 'editor', 'thumbnail', 'excerpt', 'page-attributes', 'revisions'),
    'taxonomies' => array('technology-type', 'data-type'),
  );

  register_post_type( 'technology' , $args );
}

add_action('init', 'register_technology_post_type');

function get_child_technology_by_relationship($techs, $group = false, $additional_related_techs = false) {
  ob_start();
  if($group) { ?>
    <h5 class="technology-group-title"><?php echo $group; ?></h5>
  <?php
  } ?>
  <div class="tech-type-children flex">
    <?php
    foreach($techs as $tech) {
      $termObj = (object)[
        'title' => $tech->post_title,
        'link' => get_the_permalink($tech->ID)
      ];
      if(has_post_thumbnail($tech->ID)) {
        $termObj->image = get_the_post_thumbnail_url($tech->ID, 'small-medium');
      }
      if(has_excerpt($tech->ID)) {
        $termObj->desc = get_the_excerpt($tech->ID);
      }
      echo create_child_technology($termObj);
    }
    if ( $additional_related_techs ) {
      echo $additional_related_techs;
    } ?>
  </div>
  <?php
  return ob_get_clean();
}

function create_child_technology($obj) {
	ob_start(); ?>
		<div class="tech-child col-4">
			<div class="tech-child-content">
				<h5 class="tech-child-title">
          <a href="<?php echo $obj->link; ?>">
            <?php echo $obj->title; ?>
          </a>
        </h5>
        <?php
        $techImg = '';
        if(isset($obj->image)) {
          $techImg = $obj->image;
        } else {
          $techImg = get_template_directory_uri().'/images/post-placeholder.png';
        } ?>
        <a href="<?php echo $obj->link; ?>" class="tech-child-image bg-centered" style="background-image:url(<?php echo $techImg; ?>);">
  			</a>
				<?php
				if(isset($obj->desc)) { ?>
          <div class="tech-child-desc">
					  <?php echo $obj->desc; ?>
          </div>
				<?php
        } ?>
			</div>
		</div>
	<?php
	return ob_get_clean();
}

function add_featured_post_column( $columns ) {
    $columns['featured']  = 'Featured';
    return $columns;

}
add_filter('manage_technology_posts_columns', 'add_featured_post_column');

function featured_post_column( $column_name, $post_id ) {
    if( $column_name == 'featured' ) {
        $featured = get_field( 'featured', $post_id );
        if($featured == 1) {
            echo true;
        }
    }
}
add_action( 'manage_technology_posts_custom_column', 'featured_post_column', 10, 2 );

// This functiion repeats a lot from get_multi_client_products()
function get_related_technologies($args) {
  ob_start(); ?>
  <section id="related-technologies-container" class="content-inner">
    <div id="related-technologies" class="post-row">
      <h2>Related Technologies</h2>
      <?php
      $techPosts = new WP_Query($args);
      if($techPosts->have_posts()) : ?>
        <div class="post-row-content column-count-3 flex">
          <?php
          while($techPosts->have_posts()): $techPosts->the_post(); ?>
            <div class="post-row-item col-4">
              <?php
              $image = has_post_thumbnail() ? get_the_post_thumbnail_url(get_the_ID(), 'small-medium') : get_template_directory_uri().'/images/post-placeholder.png'; ?>
              <div class="post-row-image-container bg-centered ratio-image-container ratio-3-2">
                <a href="<?php echo get_the_permalink(); ?>">
                  <img src="<?php echo $image; ?>"/>
                </a>
              </div>
              <h6 class="post-row-title">
                <a href="<?php echo get_the_permalink(); ?>">
                  <?php the_title(); ?>
                </a>
              </h6>
              <?php
              if(has_excerpt()) { ?>
                <div class="post-row-item-content">
                  <?php the_excerpt(); ?>
                </div>
              <?php
              } ?>
            </div>
          <?php
          endwhile;
          wp_reset_postdata(); ?>
        </div>
      <?php
      endif; ?>
    </div>
  </section>
  <?php
  return ob_get_clean();
}
