<?php
function register_offering_post_type() {
  $labels = array(
		'name'                       => _x( 'Market Types', 'taxonomy general name', 'iongeo' ),
		'singular_name'              => _x( 'Market Type', 'taxonomy singular name', 'iongeo' ),
		'search_items'               => __( 'Search Market Types', 'iongeo' ),
		'popular_items'              => __( 'Popular Market Types', 'iongeo' ),
		'all_items'                  => __( 'All Market Types', 'iongeo' ),
		'parent_item'                => null,
		'parent_item_colon'          => null,
		'edit_item'                  => __( 'Edit Market Type', 'iongeo' ),
		'update_item'                => __( 'Update Market Type', 'iongeo' ),
		'add_new_item'               => __( 'Add New Market Type', 'iongeo' ),
		'new_item_name'              => __( 'New Market Type Name', 'iongeo' ),
		'separate_items_with_commas' => __( 'Separate offering types with commas', 'iongeo' ),
		'add_or_remove_items'        => __( 'Add or remove offering', 'iongeo' ),
		'choose_from_most_used'      => __( 'Choose from the most used offerings', 'iongeo' ),
		'not_found'                  => __( 'No offerings found.', 'iongeo' ),
		'menu_name'                  => __( 'Market Type', 'iongeo' ),
	);

	$args = array(
    'show_tagcloud'         => true,
  	'hierarchical'          => true,
  	'labels'                => $labels,
  	'show_ui'               => true,
  	'show_admin_column'     => true,
  	'show_in_nav_menus'     => true,
    'show_in_rest'          => true,
  	'public'								=> true,
  	'update_count_callback' => '_update_post_term_count',
  	'query_var'             => true,
    'hierarchical'          => true,
		'rewrite'               => [ 'slug' => 'markets', 'hierarchical' => true, 'with_front' => false ],
	);

	register_taxonomy( 'offering-type', ['offering'], $args );
	register_taxonomy_for_object_type( 'offering-type', ['offering'] );

  $labels = [
    'name' => _x('Markets', 'post type general name'),
    'singular_name' => _x('Market', 'post type singular name'),
    'add_new' => _x('Add New', 'offering'),
    'add_new_item' => __('Add New Market'),
    'edit_item' => __('Edit Market'),
    'new_item' => __('New Market'),
    'view_item' => __('View Market'),
    'search_items' => __('Search Markets'),
    'not_found' =>  __('Nothing found'),
    'not_found_in_trash' => __('Nothing found in Trash'),
    'parent_item_colon' => __('Parent Market'),
  ];
  $args = [
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => true,
    'exclude_from_search' => false,
    'show_ui' => true,
    'query_var' => true,
    'show_in_rest' => true,
    'hierarchical' => true,
    'capability_type' => 'post',
    'archive_url' => 'markets',
    'rewrite' => ['slug' => 'markets/%offering-type%', 'with_front' => false],
    'menu_position' => 2,
    'has_archive' => 'markets',
    'menu_icon'   => 'dashicons-carrot',
    'supports' => ['title', 'editor', 'thumbnail', 'excerpt', 'page-attributes', 'revisions'],
    'taxonomies' => ['offering-type'],
    'map_meta_cap' => 1,
  ];
  register_post_type( 'offering' , $args );

}
add_action('init', 'register_offering_post_type');

function create_child_offering($obj) {
	ob_start(); ?>
		<div class="offering-child side-scroll-item flex">
			<div class="offering-child-image col-7">
        <div class="offering-child-image-wrapper">
          <img src="<?php echo $obj['image']; ?>"/>
        </div>
			</div>
			<div class="offering-child-content col-5">
        <?php
        if( isset($obj['title']) ) { ?>
          <h5><?php echo $obj['title']; ?></h5>
        <?php
        }
				if(isset($obj['text'])) {
					echo "<p>" . $obj['text'] . "</p>";
				}
        if( isset($obj['link']) ) { ?>
  				<a class="ion-cta" target="<?php echo $obj['link']['target']; ?>" href="<?php echo $obj['link']['url']; ?>">
  					<?php echo $obj['link']['title']; ?>
  				</a>
        <?php
        } ?>
			</div>
		</div>
	<?php
	return ob_get_clean();
}

function acf_field_styles() {
  echo '<style>
    .acf-row .acf-row-handle {
      background: #e2cbbc;
    }
    .acf-row:nth-child(2n + 2) .acf-row-handle {
      background: #d2e2bc;
    }
  </style>';
}

add_action('admin_head', 'acf_field_styles');

function get_related_offerings($oArgs) {
	ob_start();
	$offerings = new WP_Query($oArgs);
	if($offerings->have_posts()) : ?>
		<section class="related-offerings-container content-inner">
      <div>
  			<h2>Related Offerings</h2>
  			<div id="related-offerings">
          <div class="flex row">
    				<?php
    				while($offerings->have_posts()): $offerings->the_post(); ?>
    					<a class="col-3" href="<?php echo get_the_permalink(); ?>">
    						<?php the_title(); ?>
    					</a>
    				<?php
    				endwhile;
    				wp_reset_postdata(); ?>
    			</div>
        </div>
      </div>
		</section>
	<?php
	endif;
	return ob_get_clean();
}

?>
