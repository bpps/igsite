<?php
function register_event_post_type() {

	$labels = array(
    'name' => _x('Events', 'post type general name'),
    'singular_name' => _x('Event', 'post type singular name'),
    'add_new' => _x('Add New', 'event'),
    'add_new_item' => __('Add New Event'),
    'edit_item' => __('Edit Event'),
    'new_item' => __('New Event'),
    'view_item' => __('View Event'),
    'search_items' => __('Search Events'),
    'not_found' =>  __('Nothing found'),
    'not_found_in_trash' => __('Nothing found in Trash'),
    'parent_item_colon' => ''
  );

  $args = array(
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => true,
    'exclude_from_search' => false,
    'show_ui' => true,
    'query_var' => true,
		'show_in_rest' => true,
    'rewrite' => [ 'slug' => 'events', 'with_front' => true ],
    'capability_type' => 'post',
    'hierarchical' => true,
    'menu_position' => 4,
    'has_archive' => 'events',
		'archive_url' => 'events',
		'menu_icon'   => 'dashicons-calendar-alt',
    'supports' => ['title', 'editor', 'thumbnail', 'excerpt', 'revisions'],
		// 'capabilities' => [
    //     'edit_post' => 'edit_event',
    //     'edit_posts' => 'edit_events',
    //     'edit_others_posts' => 'edit_others_events',
    //     'publish_posts' => 'publish_events',
    //     'read_post' => 'read_event',
    //     'read_private_posts' => 'read_private_events',
    //     'delete_post' => 'delete_event'
    // ],
		'map_meta_cap' => true
  );

  register_post_type( 'event' , $args );
}

add_action('init', 'register_event_post_type');

function display_events( $atts = false ) {
  if($atts) {
    $a = shortcode_atts( array(), $atts );
  }
  ob_start();
	$featuredIds = [];
	$date = date('Y-m-d H:i:s',strtotime("today"));
  if($featured = get_field('featured_events', 'option')) {
		foreach($featured as $feature) {
			array_push($featuredIds, $feature->ID);
		}
		$args = [
	    'post_type' => 'event',
	    'posts_per_page' => -1,
			'post__in' => $featuredIds,
			'meta_query' => [
				'relation' => 'OR',
				[
					'key'       => 'event_date',
					'value'     => $date,
					'compare'   => '>=',
					'type'      => 'DATETIME'
				],
				[
					'key'       => 'event_end_date',
					'value'     => $date,
					'compare'   => '>=',
					'type'      => 'DATETIME'
				]
			]
	  ];
		$events = new WP_Query($args);
	  if($events->have_posts()) : ?>
			<div id="featured-events-container" class="side-scroll-item has-notch featured-posts-slider-container">
				<div class="featured-events-wrapper">
					<h3>Featured</h3>
					<div id="featured-events" class="featured-posts-slider">
		        <?php
		        while($events->have_posts()): $events->the_post(); ?>
							<div class="featured-post">
								<div class="featured-posts-wrapper flex">
									<?php
									$eventID = get_the_ID();
									$image = has_post_thumbnail() ? get_the_post_thumbnail_url($eventID, 'small-medium') : get_template_directory_uri().'/images/post-placeholder.png'; ?>
									<div class="col-5">
										<a href="<?php the_permalink(); ?>" class="ratio-3-2 ratio-image-container featured-event-image">
											<div class="animated-image bg-centered" style="background-image:url(<?php echo $image; ?>)"></div>
										</a>
									</div>
									<div class="featured-post-content col-7">
										<div class="featured-event-date">
											<?php
											$date = get_field('event_date');
											$month = date("M", strtotime($date));
											$day = date("d", strtotime($date));
											echo $month; ?></br>
											<?php echo $day; ?>
										</div>
										<div class="featured-event-info">
											<h2>
												<a href="<?php the_permalink(); ?>">
													<?php the_title(); ?>
												</a>
											</h2>
											<?php
											if($location = get_field('event_location')) { ?>
												<h5 class="event-location"><?php echo $location; ?></h5>
											<?php
											}
											if(has_excerpt()) { ?>
												<div class="post-desc">
													<?php the_excerpt(); ?>
												</div>
											<?php
											} ?>
										</div>
									</div>
								</div>
							</div>
						<?php
						endwhile;
						wp_reset_postdata(); ?>
					</div>
				</div>
				<div class="featured-posts-arrows">
	        <a href="#" class="circle-arrow arrow-prev" data-func="prev">
	        </a>
	        <a href="#" class="circle-arrow arrow-next" data-max-pages="<?php echo $maxpages; ?>" data-func="next">
	        </a>
	      </div>
			</div>
		<?php
		endif;
  }

  // Upcoming Events
	$date = date('Y-m-d H:i:s', strtotime("today"));
  $args = [
    'post_type' => 'event',
    'posts_per_page' => -1,
		'order_by' => 'meta_value',
		'meta_key' => 'event_date',
		'order' => 'ASC',
		'meta_query' => [
			'relation' => 'OR',
			[
				'key'       => 'event_end_date',
				'value'     => $date,
				'compare'   => '>=',
				'type'      => 'DATETIME'
			],
			[
				'key'       => 'event_date',
				'value'     => $date,
				'compare'   => '>=',
				'type'      => 'DATETIME'
			]
		]
  ];
  if(count($featuredIds) > 0) {
    $args['post__not_in'] = $featuredIds;
  }
  echo events_group($args, 'Upcoming', 'upcoming');

  // Past Events
  $args = [
    'post_type' => 'event',
    'posts_per_page' => -1,
		'orderby' => 'meta_value',
		'meta_key' => 'event_date',
		'order' => 'DESC',
		'post_status' => 'publish',
		'meta_query' => [
			'relation' => 'AND',
			[
				'key'       => 'event_end_date',
				'value'     => $date,
				'compare'   => '<',
				'type'      => 'DATETIME'
			],
			[
				'key'       => 'event_date',
				'value'     => $date,
				'compare'   => '<',
				'type'      => 'DATETIME'
			]
		]
  ];
  echo events_group($args, 'Past', 'past');
	return ob_get_clean();
}
add_shortcode( 'events', 'display_events' );

function events_group($args, $label, $slug) {
	ob_start();
  $events = new WP_Query($args);
  if($events->have_posts()) : ?>
    <div id="events-<?php echo $slug; ?>-container" class="side-scroll-item has-notch">
      <div class="events-container">
				<h3><?php echo $label; ?></h3>
				<div class="events-wrapper flex row">
	        <?php
	        while($events->have_posts()): $events->the_post();
	          $eventID = get_the_ID();
	          $image = get_template_directory_uri().'/images/post-placeholder.png';
	          if(has_post_thumbnail($eventID)) {
	            $image = get_the_post_thumbnail_url($eventID, 'small-medium');
	          }
	          $event = (object)[
	            'title' => get_the_title(),
	            'image' => $image,
	            'date' => get_field('event_date'),
	            'link' => get_field('external_link', $eventID) ? get_field('external_link', $eventID) : get_the_permalink(),
							'target' => get_field('external_link', $eventID) ? '_blank' : '_self'
	          ];
	          if($location = get_field('event_location')) {
	            $event->location = $location;
	          }
	          if(has_excerpt()) {
	            $event->desc = get_the_excerpt();
	          }
	          echo create_event($event);
	        endwhile; ?>
				</div>
      </div>
    </div>
    <?php
    wp_reset_postdata();
  endif;
  return ob_get_clean();
}
function create_event($event) {
  ob_start(); ?>
    <div class="events-item col-4">
      <div class="event-header">
        <div class="event-date">
          <?php
          $month = date("M", strtotime($event->date));
          $day = date("d", strtotime($event->date));
					$year = date("Y", strtotime($event->date));
          echo $month; ?></br>
          <?php echo $day; ?></br>
					<?php echo $year; ?>
        </div>
				<div class="event-header-content">
	        <h4 class="event-title">
	          <a target="<?php echo $event->target; ?>" href="<?php echo $event->link; ?>">
	            <?php echo $event->title; ?>
	          </a>
	        </h4>
				</div>
      </div>
      <a
				target="<?php echo $event->target; ?>"
        href="<?php echo $event->link; ?>"
        class="event-image ratio-3-2 ratio-image-container">
				<div class="animated-image bg-centered" style="background-image:url(<?php echo $event->image; ?>);"></div>
      </a>
      <?php
      if(isset($event->location) || isset($event->desc)) { ?>
        <div class="event-description">
          <?php
          if(isset($event->location)) { ?>
            <span><?php echo $event->location; ?></span>
          <?php
          }
          if(isset($event->desc)) {
            echo $event->desc;
          } ?>
        </div>
      <?php
      } ?>
    </div>
  <?php
  return ob_get_clean();
}
