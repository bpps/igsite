<?php

function investor_menu_options() {
  add_menu_page(
    'Investor',
    'Investor',
    'manage_options',
    'investor-menu.php',
    'investor_menu_function',
    '',
    5
  );
  //add_submenu_page('investor-menu.php', 'Resource Type', 'Resource Type', 'manage_options', 'investor-menu.php?taxonomy=investor-material-type');
  // add_submenu_page() if you want subpages, but not necessary
}
//add_action('admin_menu', 'investor_menu_options');

function register_investor_materials_post_type() {

  $labels = array(
		'name'                       => _x( 'Categories', 'taxonomy general name', 'iongeo' ),
		'singular_name'              => _x( 'Investor Material Category', 'taxonomy singular name', 'iongeo' ),
		'search_items'               => __( 'Search Investor Material Categories', 'iongeo' ),
		'popular_items'              => __( 'Popular Investor Material Categories', 'iongeo' ),
		'all_items'                  => __( 'All Investor Material Categories', 'iongeo' ),
		'parent_item'                => null,
		'parent_item_colon'          => null,
		'edit_item'                  => __( 'Edit Investor Material Category', 'iongeo' ),
		'update_item'                => __( 'Update Investor Material Category', 'iongeo' ),
		'add_new_item'               => __( 'Add New Investor Material Category', 'iongeo' ),
		'new_item_name'              => __( 'New Investor Material Category Name', 'iongeo' ),
		'separate_items_with_commas' => __( 'Separate investor material categories with commas', 'iongeo' ),
		'add_or_remove_items'        => __( 'Add or remove investor material category', 'iongeo' ),
		'choose_from_most_used'      => __( 'Choose from the most used investor material categories', 'iongeo' ),
		'not_found'                  => __( 'No investor material categories found.', 'iongeo' ),
		'menu_name'                  => __( 'Material Categories', 'iongeo' ),
	);

	$args = array(
    'show_tagcloud'         => true,
  	'hierarchical'          => true,
  	'labels'                => $labels,
  	'show_ui'               => true,
  	'show_admin_column'     => true,
    'show_in_rest'          => true,
  	'show_in_nav_menus'     => true,
  	'public'								=> true,
  	'update_count_callback' => '_update_post_term_count',
  	'query_var'             => true,
		'rewrite'               => [ 'slug' => 'investor-material-category' ],
	);

	register_taxonomy( 'investor-material-category', array('investor-material'), $args );
	register_taxonomy_for_object_type( 'investor-material-category', array('investor-material') );

  $labels = array(
    'name' => _x('Investor Materials', 'post type general name'),
    'singular_name' => _x('Investor Material', 'post type singular name'),
    'add_new' => _x('Add New', 'investor-material'),
    'add_new_item' => __('Add New Investor Material'),
    'edit_item' => __('Edit Investor Material'),
    'new_item' => __('New Investor Material'),
    'view_item' => __('View Investor Material'),
    'search_items' => __('Search Investor Materials'),
    'not_found' =>  __('Nothing found'),
    'not_found_in_trash' => __('Nothing found in Trash'),
    'parent_item_colon' => __('Parent Investor Material'),
  );

  $args = array(
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => true,
    'exclude_from_search' => false,
    'show_ui' => true,
    'show_in_rest' => true,
    'query_var' => true,
    'rewrite' => [ 'slug' => 'investors/investor-materials', 'with_front' => true ],
    'capability_type' => 'post',
    'hierarchical' => true,
    'menu_position' => 6,
    'has_archive' => false,
    'archive_url' => 'investors/investor-materials',
    'menu_icon'   => 'dashicons-chart-line',
    //'show_in_menu' => 'investor-menu.php',
    'supports' => array('title', 'editor', 'thumbnail', 'excerpt', 'revisions'),
    'taxonomies' => array('investor-material-category'),
  );

  register_post_type( 'investor-material' , $args );

  $labels = array(
		'name'                       => _x( 'Investor News Types', 'taxonomy general name', 'iongeo' ),
		'singular_name'              => _x( 'Investor News Type', 'taxonomy singular name', 'iongeo' ),
		'search_items'               => __( 'Search Investor News Types', 'iongeo' ),
		'popular_items'              => __( 'Popular Investor News Types', 'iongeo' ),
		'all_items'                  => __( 'All Investor News Types', 'iongeo' ),
		'parent_item'                => null,
		'parent_item_colon'          => null,
		'edit_item'                  => __( 'Edit Investor News Type', 'iongeo' ),
		'update_item'                => __( 'Update Investor News Type', 'iongeo' ),
		'add_new_item'               => __( 'Add New Investor News Type', 'iongeo' ),
		'new_item_name'              => __( 'New Investor News Type Name', 'iongeo' ),
		'separate_items_with_commas' => __( 'Separate investor news types with commas', 'iongeo' ),
		'add_or_remove_items'        => __( 'Add or remove investor news types', 'iongeo' ),
		'choose_from_most_used'      => __( 'Choose from the most used investor news types', 'iongeo' ),
		'not_found'                  => __( 'No investor news types found.', 'iongeo' ),
		'menu_name'                  => __( 'News Types', 'iongeo' ),
	);

	$args = array(
    'show_tagcloud'         => true,
  	'hierarchical'          => true,
  	'labels'                => $labels,
  	'show_ui'               => true,
  	'show_admin_column'     => true,
    'show_in_rest'          => true,
  	'show_in_nav_menus'     => true,
    'archive_url'           => 'investor-relations/investor-news',
  	'public'								=> false,
  	'update_count_callback' => '_update_post_term_count',
  	'query_var'             => true,
		'rewrite'               => array( 'slug' => 'investor-news' ),
	);

	register_taxonomy( 'investor-news-type', array('investor-news'), $args );
	register_taxonomy_for_object_type( 'investor-news-type', array('investor-news') );

  $labels = array(
    'name' => _x('SEC Filings', 'post type general name'),
    'singular_name' => _x('SEC Filing', 'post type singular name'),
    'add_new' => _x('Add New', 'sec-filing'),
    'add_new_item' => __('Add New SEC Filing'),
    'edit_item' => __('Edit SEC Filing'),
    'new_item' => __('New SEC Filing'),
    'view_item' => __('View SEC Filing'),
    'search_items' => __('Search SEC Filings'),
    'not_found' =>  __('Nothing found'),
    'not_found_in_trash' => __('Nothing found in Trash'),
    'parent_item_colon' => __('Parent SEC Filing'),
  );

  $args = array(
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => false,
    'exclude_from_search' => false,
    'show_in_rest' => true,
    'show_ui' => true,
    'query_var' => true,
    'rewrite' => false,
    'capability_type' => 'post',
    'hierarchical' => true,
    'menu_position' => 7,
    'has_archive' => false,
    'archive_url' => 'investors/sec-filings',
    'filter_text' => 'Groupings',
    'show_in_menu' => 'edit.php?post_type=investor-material',
    'supports' => array('title', 'editor', 'thumbnail', 'excerpt', 'revisions'),
    'taxonomies' => array('sec-filing-type'),
  );

  register_post_type( 'sec-filing' , $args );

  $labels = array(
		'name'                       => _x( 'SEC Filing Types', 'taxonomy general name', 'iongeo' ),
		'singular_name'              => _x( 'SEC Filing Type', 'taxonomy singular name', 'iongeo' ),
		'search_items'               => __( 'Search SEC Filing Types', 'iongeo' ),
		'popular_items'              => __( 'Popular SEC Filing Types', 'iongeo' ),
		'all_items'                  => __( 'All SEC Filing Types', 'iongeo' ),
		'parent_item'                => null,
		'parent_item_colon'          => null,
		'edit_item'                  => __( 'Edit SEC Filing Type', 'iongeo' ),
		'update_item'                => __( 'Update SEC Filing Type', 'iongeo' ),
		'add_new_item'               => __( 'Add New SEC Filing Type', 'iongeo' ),
		'new_item_name'              => __( 'New SEC Filing Type Name', 'iongeo' ),
		'separate_items_with_commas' => __( 'Separate SEC filing types with commas', 'iongeo' ),
		'add_or_remove_items'        => __( 'Add or remove SEC filing types', 'iongeo' ),
		'choose_from_most_used'      => __( 'Choose from the most used SEC filing types', 'iongeo' ),
		'not_found'                  => __( 'No SEC filing types found.', 'iongeo' ),
		'menu_name'                  => __( 'SEC filing types', 'iongeo' ),
	);

	$args = array(
    'show_tagcloud'         => true,
  	'hierarchical'          => true,
  	'labels'                => $labels,
  	'show_ui'               => true,
  	'show_admin_column'     => true,
    'show_in_rest'          => true,
  	'show_in_nav_menus'     => true,
  	'public'								=> false,
  	'update_count_callback' => '_update_post_term_count',
  	'query_var'             => true,
		'rewrite'               => array( 'slug' => 'sec-filing' ),
	);

	register_taxonomy( 'sec-filing-type', array('sec-filing'), $args );
	register_taxonomy_for_object_type( 'sec-filing-type', array('sec-filing') );

  $labels = array(
    'name' => _x('Press Releases', 'post type general name'),
    'singular_name' => _x('Investor News', 'post type singular name'),
    'add_new' => _x('Add New', 'investor-news'),
    'add_new_item' => __('Add New Investor News Post'),
    'edit_item' => __('Edit Investor News Post'),
    'new_item' => __('New Investor News Post'),
    'view_item' => __('View Investor News'),
    'search_items' => __('Search Investor News Posts'),
    'not_found' =>  __('Nothing found'),
    'not_found_in_trash' => __('Nothing found in Trash'),
    'parent_item_colon' => __('Parent Investor News'),
  );

  $args = array(
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => true,
    'exclude_from_search' => false,
    'show_ui' => true,
    'show_in_rest' => true,
    'query_var' => true,
    'archive_url' => 'investors/investor-news',
    'rewrite' => [ 'slug' => 'investors/investor-news', 'with_front' => true ],
    'capability_type' => 'post',
    'hierarchical' => true,
    'menu_position' => 8,
    'filter_text' => 'Groupings',
    'has_archive' => false,
    'show_in_menu' => 'edit.php?post_type=investor-material',
    'supports' => ['title', 'editor', 'thumbnail', 'excerpt', 'revisions'],
    'taxonomies' => ['investor-news-type'],
    'hide_tax_crumbs' => true
  );

  register_post_type( 'investor-news' , $args );

  $labels = array(
		'name'                       => _x( 'Investor Event Types', 'taxonomy general name', 'iongeo' ),
		'singular_name'              => _x( 'Investor Event Type', 'taxonomy singular name', 'iongeo' ),
		'search_items'               => __( 'Search Investor Event Types', 'iongeo' ),
		'popular_items'              => __( 'Popular Investor Event Types', 'iongeo' ),
		'all_items'                  => __( 'All Investor Event Types', 'iongeo' ),
		'parent_item'                => null,
		'parent_item_colon'          => null,
		'edit_item'                  => __( 'Edit Investor Event Type', 'iongeo' ),
		'update_item'                => __( 'Update Investor Event Type', 'iongeo' ),
		'add_new_item'               => __( 'Add New Investor Event Type', 'iongeo' ),
		'new_item_name'              => __( 'New Investor Event Type Name', 'iongeo' ),
		'separate_items_with_commas' => __( 'Separate investor event types with commas', 'iongeo' ),
		'add_or_remove_items'        => __( 'Add or remove investor event type', 'iongeo' ),
		'choose_from_most_used'      => __( 'Choose from the most used investor event types', 'iongeo' ),
		'not_found'                  => __( 'No investor event types found.', 'iongeo' ),
		'menu_name'                  => __( 'Event Types', 'iongeo' ),
	);

	$args = array(
    'show_tagcloud'         => true,
  	'hierarchical'          => true,
  	'labels'                => $labels,
  	'show_ui'               => true,
  	'show_admin_column'     => true,
  	'show_in_nav_menus'     => true,
    'show_in_rest'          => true,
  	'public'								=> false,
  	'update_count_callback' => '_update_post_term_count',
  	'query_var'             => true,
		'rewrite'               => array( 'slug' => 'investor-event' ),
	);

	register_taxonomy( 'investor-event-type', array('investor-event'), $args );
	register_taxonomy_for_object_type( 'investor-event-type', array('investor-event') );

  $labels = array(
    'name' => _x('Investor Events', 'post type general name'),
    'singular_name' => _x('Investor Event', 'post type singular name'),
    'add_new' => _x('Add New', 'investor-event'),
    'add_new_item' => __('Add New Investor Event'),
    'edit_item' => __('Edit Investor Event'),
    'new_item' => __('New Investor Event'),
    'view_item' => __('View Investor Event'),
    'search_items' => __('Search Investor Events'),
    'not_found' =>  __('Nothing found'),
    'not_found_in_trash' => __('Nothing found in Trash'),
    'parent_item_colon' => __('Parent Investor Event'),
  );

  $args = array(
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => true,
    'exclude_from_search' => false,
    'show_ui' => true,
    'show_in_rest' => true,
    'query_var' => true,
    'rewrite' => false,
    'rewrite' => [ 'slug' => 'investor-relations/investor-events', 'with_front' => true ],
    'archive_url' => 'investor-relations/investor-events',
    'capability_type' => 'post',
    'hierarchical' => true,
    'menu_position' => 9,
    'has_archive' => false,
    'show_in_menu' => 'edit.php?post_type=investor-material',
    //'show_in_menu' => 'investor-menu.php',
    'supports' => array('title', 'editor', 'thumbnail', 'excerpt', 'revisions'),
    'taxonomies' => array('investor-event-type'),
  );

  register_post_type( 'investor-event' , $args );

  $labels = array(
    'name' => _x('Corporate Governances', 'post type general name'),
    'singular_name' => _x('Corporate Governance', 'post type singular name'),
    'add_new' => _x('Add New', 'corporate-governance'),
    'add_new_item' => __('Add New Corporate Governance'),
    'edit_item' => __('Edit Corporate Governance'),
    'new_item' => __('New Corporate Governance'),
    'view_item' => __('View Corporate Governance'),
    'search_items' => __('Search Corporate Governances'),
    'not_found' =>  __('Nothing found'),
    'not_found_in_trash' => __('Nothing found in Trash'),
    'parent_item_colon' => __('Parent Corporate Governance'),
  );

  $args = array(
    'labels' => $labels,
    'public' => false,
    'publicly_queryable' => false,
    'exclude_from_search' => false,
    'show_in_rest' => true,
    'show_ui' => true,
    'query_var' => true,
    'rewrite' => false,
    'capability_type' => 'post',
    'hierarchical' => true,
    'menu_position' => 7,
    'has_archive' => false,
    'archive_url' => 'investors/corporate-governance',
    'filter_text' => 'Groupings',
    'show_in_menu' => 'edit.php?post_type=investor-material',
    'supports' => array('title', 'editor', 'thumbnail', 'excerpt', 'revisions'),
    'taxonomies' => array('corporate-governance-type'),
  );

  register_post_type( 'corporate-governance' , $args );

  $labels = array(
		'name'                       => _x( 'Corporate Governance Types', 'taxonomy general name', 'iongeo' ),
		'singular_name'              => _x( 'Corporate Governance Type', 'taxonomy singular name', 'iongeo' ),
		'search_items'               => __( 'Search Corporate Governance Types', 'iongeo' ),
		'popular_items'              => __( 'Popular Corporate Governance Types', 'iongeo' ),
		'all_items'                  => __( 'All Corporate Governance Types', 'iongeo' ),
		'parent_item'                => null,
		'parent_item_colon'          => null,
		'edit_item'                  => __( 'Edit Corporate Governance Type', 'iongeo' ),
		'update_item'                => __( 'Update Corporate Governance Type', 'iongeo' ),
		'add_new_item'               => __( 'Add New Corporate Governance Type', 'iongeo' ),
		'new_item_name'              => __( 'New Corporate Governance Type Name', 'iongeo' ),
		'separate_items_with_commas' => __( 'Separate corporate governance types with commas', 'iongeo' ),
		'add_or_remove_items'        => __( 'Add or remove corporate governance type', 'iongeo' ),
		'choose_from_most_used'      => __( 'Choose from the most used corporate governance types', 'iongeo' ),
		'not_found'                  => __( 'No corporate governance types found.', 'iongeo' ),
		'menu_name'                  => __( 'Corporate Governance Types', 'iongeo' ),
	);

	$args = array(
    'show_tagcloud'         => true,
  	'hierarchical'          => true,
  	'labels'                => $labels,
  	'show_ui'               => true,
  	'show_admin_column'     => true,
  	'show_in_nav_menus'     => true,
    'show_in_rest'          => true,
  	'public'								=> false,
  	'update_count_callback' => '_update_post_term_count',
  	'query_var'             => true,
		'rewrite'               => array( 'slug' => 'corporate-governance' ),
	);

	register_taxonomy( 'corporate-governance-type', array('corporate-governance'), $args );
	register_taxonomy_for_object_type( 'corporate-governance-type', array('corporate-governance') );
}

add_action('init', 'register_investor_materials_post_type', 99);

add_action('wp_ajax_nopriv_investor_ajax', 'investor_ajax');
add_action('wp_ajax_investor_ajax', 'investor_ajax');

function investor_ajax(){
  switch($_REQUEST['fn']){
    case 'get_tax_list':
      $taxObj = [];
      $originalTax = false;
      $taxType = isset($_REQUEST['type']) ? $_REQUEST['type'] : false;
      $taxTerm = isset($_REQUEST['term']) ? $_REQUEST['term'] : false;
      $filter = isset($_REQUEST['filter']) ? $_REQUEST['filter'] : false;
      $postType = isset($_REQUEST['posttype']) ? $_REQUEST['posttype'] : false;
      $excluded = isset($_REQUEST['excluded']) ? $_REQUEST['excluded'] : false;
      $topics = isset($_REQUEST['topics']) ? $_REQUEST['topics'] : false;
      $date = isset($_REQUEST['date']) ? $_REQUEST['date'] : false;
      $search = isset($_REQUEST['search']) ? $_REQUEST['search'] : false;

      if ( $postTaxReq = $_REQUEST['tax'] ) {
        $taxType = $postTaxReq;
        $taxTerm = $filter;
      }
      if ( $taxType && $taxTerm ) {
        $originalTax = (object)['tax' => $taxType, 'term' => $taxTerm];
        array_push($taxObj, ['taxonomy' => $taxType, 'field' => 'slug', 'terms' => [$taxTerm]]);
      }
      if ( $topics ) {
        foreach ( $topics as $key=>$topic ) {
          array_push($taxObj, ['taxonomy' => $key, 'field' => 'slug', 'terms' => $topic]);
        }
      }

      $paged = $_REQUEST['paged'];

      echo json_encode(get_filter_list_posts_by_type($taxObj, $paged, $filter, $postType, json_decode($excluded), 'null', $date, $search));
	  break;
    case 'get_investor_events_by_year':
      $year = $_REQUEST['year'];
      $ids = $_REQUEST['ids'];
      $search = $_REQUEST['search'];
      // echo '1';
      echo get_investor_events_by_year($year, $ids, false, $search);
    break;
		default:
			$output = 'nothing here';
			echo $output;
		break;
	}
	die();
}

function get_stock_price() {
  return get_investor_quotes('stockprice');
}

function get_investor_quotes($type) {
  $quotes = wp_remote_request('https://clientapi.gcs-web.com/data/8cc48279-c570-4075-aa09-5c796c645901/Quotes', array(
    'method'     => 'GET'
  ));
  $quote = wp_remote_retrieve_body($quotes);
  $quote = json_decode($quote);
  switch($type) {
    case 'stockprice':
      return [
        'symbol' => $quote->data[0]->symbol,
        'changePercent' => $quote->data[0]->changePercent,
        'bid' => $quote->data[0]->lastTrade
      ];
    break;
    case 'performance':
      return $quote->data[0];
    break;
    default:
      return $quote;
    break;
  }
}

function my_cron_schedules($schedules){
  if(!isset($schedules["1min"])){
      $schedules["1min"] = array(
          'interval' => 60,
          'display' => __('Once every minute'));
  }
  return $schedules;
}

add_filter('cron_schedules','my_cron_schedules');

// Investor Materials post CRON

if ( ! wp_next_scheduled( 'check_investor_materials_posts' ) ) {
  wp_schedule_event( time(), '1min', 'check_investor_materials_posts');
}

function get_investor_materials() {
  query_ion_post_fetch('https://clientapi.gcs-web.com/data/8cc48279-c570-4075-aa09-5c796c645901/assets', 'investor-material', false);
}

add_action( 'check_investor_materials_posts', 'get_investor_materials' );

// SEC Filings post CRON

if ( ! wp_next_scheduled( 'check_sec_filing_posts' ) ) {
  wp_schedule_event( time(), '1min', 'check_sec_filing_posts');
}

function get_sec_filings() {
  query_ion_post_fetch('https://clientapi.gcs-web.com/data/8cc48279-c570-4075-aa09-5c796c645901/filings?size=50', 'sec-filing', 'sec-filing-type');
}

add_action( 'check_sec_filing_posts', 'get_sec_filings' );

// Investor events post CRON

if ( ! wp_next_scheduled( 'check_investor_event_posts' ) ) {
  wp_schedule_event( time(), 'hourly', 'check_investor_event_posts');
}

function get_investor_events() {
  query_ion_post_fetch('https://clientapi.gcs-web.com/data/8cc48279-c570-4075-aa09-5c796c645901/Events', 'investor-event', 'investor-event-type');
}

add_action( 'check_investor_event_posts', 'get_investor_events' );

// Investor news post CRON

if ( ! wp_next_scheduled( 'check_investor_news_posts' ) ) {
  wp_schedule_event( time(), '1min', 'check_investor_news_posts' );
}

function get_investor_news() {
  query_ion_post_fetch('https://clientapi.gcs-web.com/data/8cc48279-c570-4075-aa09-5c796c645901/News', 'investor-news', 'investor-news-type');
}

add_action( 'check_investor_news_posts', 'get_investor_news' );

// Function to fetch posts

function query_ion_post_fetch($url, $postType, $tax) {
  $fetchPosts = wp_remote_request($url, array(
    'method'     => 'GET'
  ));
  // pagination: links->next, links->prev, links->first, links->last
  $fetchJSON = wp_remote_retrieve_body($fetchPosts);
  $fetchJSON = json_decode($fetchJSON);
  upload_investor_posts($fetchJSON, $postType, $tax);
  if(isset($fetchJSON->links->next)) {
    query_ion_post_fetch($fetchJSON->links->next, $postType, $tax);
  }
}

function upload_investor_posts($posts_json, $postType, $taxonomy) {
  // ini_set('memory_limit', '1024M');
  // include realpath(__DIR__ . '/../..') . '/vendor/autoload.php';
  set_time_limit(60);
  $orig_post_type = $postType;
  foreach($posts_json->data as $item) {
    $unique_id = '';
    if(isset($item->id)) {
      $unique_id = $item->id;
    }
    if(isset($item->accessionNo)) {
      $unique_id = $item->accessionNo;
    }
    // If the post has these types associated, make them corporate governance post types
    $gov_type_ids = ['Governance Documents', 'Committee Charters', 'Conflict Minerals'];
    $postType = $orig_post_type;
    if( isset($item->categories) ) {
      if( isset($item->categories[0]->title) && in_array($item->categories[0]->title, $gov_type_ids) ) {
        $postType = 'corporate-governance';
        $taxonomy = 'corporate-governance-type';
      }
    }

    $args = [
      'post_type'  => $postType,
      'posts_per_page' => 1,
      'post_status' => ['publish', 'pending', 'draft', 'auto-draft', 'future', 'private', 'inherit'],
      'meta_query' => [
        [
          'key'     => 'api_id',
          'value'   => $unique_id,
          'compare' => '='
        ]
      ]
    ];
    $original_post_id = null;
    // Check for post and if post needs to be updated.
    if($invPost = get_posts($args)) {
      $original_post_id = $invPost[0]->ID;
      // Remove last two conditionals. Just using this to update the external link urls for these posts.
      if ( get_field('field_5de69aa0896b5', $original_post_id) == $item->lastUpdatedUTC && $invPost[0]->post_content ) {
        continue;
      }
    }
    // Check for investor news types and add if they don't exist
    $term_array = [];
    $sub_term_array = [];
    $tax_input_args = [];
    if( isset($item->type) && $taxonomy ) {
      $term_obj = create_term_object($item->type->title, $item->type->id, $taxonomy);
      array_push($term_array, $term_obj);
    }
    if( isset($item->formGroups) && $taxonomy ) {
      foreach($item->formGroups as $form_group) {
        $term_obj = create_term_object($form_group->title, $form_group->id, $taxonomy);
        array_push($term_array, $term_obj);
      }
    }
    if( isset($item->formType) && $taxonomy ) {
      $term_obj = create_term_object($item->formType, $item->formType, $taxonomy);
      if ( $postType == 'sec-filing' ) {
        array_push($sub_term_array, $term_obj);
      } else {
        array_push($term_array, $term_obj);
      }

    }
    if( isset($item->categories) && $postType == 'investor-material' ) {
      foreach( $item->categories as $cat ) {
        $term_obj = create_term_object($cat->title, $cat->id, 'investor-material-category');
        array_push($term_array, $term_obj);
      }
    }
    if( isset($item->categories) && $postType == 'corporate-governance' ) {
      foreach( $item->categories as $cat ) {
        $term_obj = create_term_object($cat->title, $cat->id, 'corporate-governance-type');
        array_push($term_array, $term_obj);
      }
    }
    if( count($term_array) > 0 ) {
      foreach( $term_array as $term ) {
        $args = [
          'taxonomy' => $term['tax'],
          'hide_empty' => false, // also retrieve terms which are not used yet
          'meta_query' => [
            [
             'key'       => 'api_id',
             'value'     => $term['id'],
             'compare'   => 'LIKE'
            ]
          ]
        ];
        if( !isset($tax_input_args[$term['tax']]) ) {
          $tax_input_args[$term['tax']] = [];
        }
        $item_term_id = null;
        if($terms = get_terms( $args )) {
          $item_term_id = $terms[0]->term_id;
        } else { // Create the term
          if( $inv_type = wp_insert_term($term['title'], $term['tax']) ) {
            if ( is_wp_error( $inv_type ) ) {
              // Error
            } else {
              $item_term_id = $inv_type['term_id'];
              update_field('field_5bfd699222a61', $term['id'], $term['tax'].'_'.$item_term_id);
            }
          }
        }
        array_push($tax_input_args[$term['tax']], $item_term_id);
        //process sub terms for sec filing formGroup
        if ( count($sub_term_array) > 0 ) {
          $tax_input_args[$term['tax']] = create_sub_terms($item_term_id, $sub_term_array, $tax_input_args[$term['tax']]);
        }
      }
    }
    $inv_post_args = [
      'post_type' => $postType,
      'post_status' => 'publish',
    ];

    if(isset($item->title)) {
      $inv_post_args['post_title'] = wp_strip_all_tags($item->title);
    }

    if(isset($item->description)) {
      $inv_post_args['post_title'] = wp_strip_all_tags($item->description);
    }
    // News release date
    if(isset($item->releaseDate->date)) {
      $inv_post_args['post_date'] = $item->releaseDate->date;
    }
    // Investor material date
    if(isset($item->date)) {
      $inv_post_args['post_date'] = $item->date;
    }
    // SEC Date Filed
    if(isset($item->dateFiled->date)) {
      $inv_post_args['post_date'] = $item->dateFiled->date;
    }
    // News excerpt
    if(isset($item->teaser)) {
      $inv_post_args['post_excerpt'] = $item->teaser;
    }
    // Event excerpt
    if(isset($item->summary)) {
      $inv_post_args['post_excerpt'] = $item->summary;
    }
    // if( count($tax_input_args) > 0 ) {
    //   $inv_post_args['tax_input'] = $tax_input_args;
    // }
    if($postType == 'investor-news') {
      if($news_html = wp_remote_request('https://clientapi.gcs-web.com/data/8cc48279-c570-4075-aa09-5c796c645901/news/'.$item->id.'/html', array(
        'method'     => 'GET'
      ))) {
        $news_html = wp_remote_retrieve_body($news_html);
        $news_html = json_decode($news_html);
        $inv_post_args['post_content'] = $news_html->data;
      }
    }
    $inv_post_id = $original_post_id;
    if ( $inv_post_id == null ) {
      $inv_post_id = wp_insert_post($inv_post_args);
    } else {
      $inv_post_args['ID'] = $original_post_id;
      wp_update_post($inv_post_args);
    }
    if ( $inv_post_id != null ) {
      // Set the terms
      if( count($tax_input_args) > 0 ) {
        foreach( $tax_input_args as $term_array_tax=>$terms ) {
          wp_set_object_terms( $inv_post_id, $terms, $term_array_tax );
        }
      }
      update_field('field_5bfd699222a61', $unique_id, $inv_post_id);

      // Update last updated field
      update_field('field_5de69aa0896b5', $item->lastUpdatedUTC, $inv_post_id);

      // Event Webcast
      if(isset($item->webCast)) {
        $link = [
          'title' => isset($item->webCast->title) ? $item->webCast->title : '',
          'url' => isset($item->webCast->url) ? $item->webCast->url : ''
        ];
        update_field('field_5bfed5a1a236f', $link, $inv_post_id);
      }
      // Event start date
      if(isset($item->startDate->date)) {
        $eventDate = date("Ymd h:i:s", strtotime($item->startDate->date));
        update_field('field_5cd2e8738342f', $eventDate, $inv_post_id);
      }
      if(isset($item->issuer)) {
        update_field('field_5c005b045b3f8', $item->issuer, $inv_post_id);
      }
      if(isset($item->formType)) {
        update_field('field_5c005be55b3fe', $item->formType, $inv_post_id);
      }
      if(isset($item->filer)) {
        update_field('field_5c005c265b3ff', $item->filer, $inv_post_id);
      }
      if(isset($item->documentDate)) {
        update_field('field_5c005b475b3f9', $item->documentDate->date, $inv_post_id);
      }
      // News hostedURL
      if(isset($item->link) && $postType == 'investor-news' ) {
        if ( $url = isset($item->link->hostedUrl) ) {
          update_field('field_5f3ffbc5b3bf0', $url, $inv_post_id);
        }
      }
      // Investor materials
      if( isset($item->link) && $postType == 'investor-material' ) {
        if ($url = $item->link->hostedUrl ) {
          update_field('field_5ca4ed81399e8', $url, $inv_post_id);
        }
      }
      if(isset($item->quarter)) {
        update_field('field_5ca4ee57399e9', $item->quarter, $inv_post_id);
      }
      if(isset($item->fiscalYear)) {
        update_field('field_5ca4ee79399ea', $item->fiscalYear, $inv_post_id);
      }
      if(isset($item->documents)) {
        delete_post_meta($inv_post_id, 'documents');
        foreach( $item->documents as $doc ) {
          $doc_row = array(
            'field_5ca4ee97399ec'	=> isset($doc->title) ? $doc->title : '',
            'field_5ca4eead399ed'	=> isset($doc->url) ? $doc->url : '',
            'field_5ca4eeb2399ee'	=> isset($doc->format) ? $doc->title : ''
          );
          add_row('field_5ca4ee8e399eb', $doc_row, $inv_post_id);

          // Parse PDF and use as content
          // if ( $doc->format == 'pdf' ) {
          //   $parser = new \Smalot\PdfParser\Parser();
          //   $pdf = $parser->parseFile($doc->url);
          //   $text = $pdf->getText();
          //   $inv_post_args['post_content'] = $text;
          // }
        }
      }

      if(isset($item->formats)) {
        $vars = get_object_vars($item->formats);
        delete_post_meta($inv_post_id, 'sec_formats');
        foreach($vars as $key=>$var) {
          if(!empty($var)) {
            $matRow = array(
              'field_5c005bb95b3fb' => $key,
              'field_5c005bcb5b3fc'	=> $var->title,
              'field_5c005bd05b3fd'	=> $var->url
            );
            add_row('field_5c005b695b3fa', $matRow, $inv_post_id);

            // Parse PDF and use as content
            // if ( $key == 'pdf' && isset($doc->url) ) {
            //   $parser = new \Smalot\PdfParser\Parser();
            //   $pdf = $parser->parseFile($doc->url);
            //   $text = $pdf->getText();
            //   $inv_post_args['post_content'] = $text;
            // }
          }
        }
      }

      if( isset($item->supportingMaterials) ) {
        delete_post_meta($inv_post_id, 'supporting_material');
        foreach( $item->supportingMaterials as $supMat ) {
          if( $supMat->source == 'api' ) {
            if( $supMatResource = wp_remote_request($supMat->url, array(
              'method'     => 'GET'
            )) ) {
              $supMatJSON = wp_remote_retrieve_body($supMatResource);
              $supMatJSON = json_decode($supMatJSON);
              $doc_cat = '';
              if ( isset($supMatJSON->data->categories) && count($supMatJSON->data->categories) > 0 ) {
                $doc_cat = strtolower($supMatJSON->data->categories[0]->title);
              }
              foreach( $supMatJSON->data->documents as $doc ) {
                $matRow = array(
                	'field_5bfed5cda2371'	=> $doc->title,
                	'field_5bfed5d2a2372'	=> $doc->url,
                  'field_5d65935dbcf31' => $doc->format,
                  'field_5d973fd62e879' => $doc_cat
                );

                add_row('field_5bfed5baa2370', $matRow, $inv_post_id);

                // Parse PDF and use as content
                // if ( $doc->format == 'pdf' ) {
                //   $parser = new \Smalot\PdfParser\Parser();
                //   $pdf = $parser->parseFile($doc->url);
                //   $text = $pdf->getText();
                //   $inv_post_args['post_content'] = $text;
                // }
              }
            }
          } else {
            $matRow = array(
              'field_5bfed5cda2371'	=> $supMat->title,
              'field_5bfed5d2a2372'	=> $supMat->url,
              'field_5d65935dbcf31' => $subMat->source
            );
            add_row('field_5bfed5baa2370', $matRow, $inv_post_id);
          }
        }
      }
    }
  }
}

function create_sub_terms($parent_id, $sub_terms, $tax_args) {
  foreach ( $sub_terms as $term ) {
    $args = [
      'taxonomy' => $term['tax'],
      'hide_empty' => false, // also retrieve terms which are not used yet
      'meta_query' => [
        [
         'key'       => 'api_id',
         'value'     => $term['id'],
         'compare'   => 'LIKE'
        ]
      ]
    ];
    $item_term_id = null;
    if($terms = get_terms( $args )) {
      $item_term_id = $terms[0]->term_id;
      if ( $terms[0]->parent == 0 || !isset($terms[0]->parent ) ) {
        wp_update_term( $item_term_id, $terms[0]->taxonomy, [ 'parent' => $parent_id ]);
      }
    } else { // Create the term
      if( $inv_type = wp_insert_term( $term['title'], $term['tax'], [ 'parent' => $parent_id ] ) ) {
        $item_term_id = $inv_type['term_id'];
        update_field('field_5bfd699222a61', $term['id'], $term['tax'].'_'.$item_term_id);
      }
    }
    array_push($tax_args, $item_term_id);
  }
  return $tax_args;
}

function create_term_object($name, $id, $tax = false) {
  return [
    'title' => $name,
    'id' => $id,
    'tax' => $tax
  ];
}

function display_investor_events( $atts = false, $content, $tag ) {
  ob_start();
  if($atts) {
    $a = shortcode_atts( array(), $atts );
  } ?>
  <div id="investor-events-container">
    <div id="investor-events-wrapper">
      <?php

      // Featured Investor Events
      $featuredIds = [];
      if($featured = get_field('featured_investor_events', 'option')) {
        foreach($featured as $feature) {
          array_push($featuredIds, $feature->ID);
        }
      }

      // Featured Investor Events
      echo featured_investor_events_group($featuredIds);

      // Upcoming Investor Events
      echo investor_events_groups($featuredIds); ?>
    </div>
  </div>
<?php
return ob_get_clean();
}
add_shortcode( 'investor-events', 'display_investor_events' );

function featured_investor_events_group($featuredIds) {
  ob_start();
  $date = date('Y-m-d H:i:s',strtotime("today"));
  $args = [
    'post_type' => 'investor-event',
    'posts_per_page' => -1,
    'post__in' => $featuredIds,
    'meta_query' => [
      [
        'key'       => 'event_date',
        'value'     => $date,
        'compare'   => '>=',
        'type'      => 'DATETIME'
      ]
    ]
  ];
  $events = new WP_Query($args);
  if($events->have_posts()) :
    $iEvents = create_investor_events($events, $args, 'featured-investor'); ?>
    <div id="events-featured-investor-container">
      <h3>Featured</h3>
      <div class="events-container flex row">
        <?php echo $iEvents->events; ?>
      </div>
    </div>
    <?php
    wp_reset_postdata();
  endif;
  return ob_get_clean();
}

function investor_events_groups($featuredIds) {
  ob_start();
  $args = [
    'post_type'       =>   'investor-event',
    'posts_per_page'  =>   -1,
    'meta_key'        =>   'event_date',
    'meta_compare'    =>   'EXISTS',
    'meta_type'       =>   'DATETIME',
    'order'           =>   'DESC',
  	'orderby'         =>   'meta_value_datetime',
  ];
  if(count($featuredIds) > 0) {
    $args['post__not_in'] = $featuredIds;
  }
  $events = new WP_Query($args);
  if( $events->have_posts() ) :
    $years = [];
    while( $events->have_posts() ) : $events->the_post();
      $date = get_field('event_date');
      $year = date("Y", strtotime($date));
      if ( ! isset( $years[ $year ] ) ) $years[ $year ] = array();
      $years[ $year ][] = get_the_ID();
    endwhile;
    wp_reset_postdata();
    if ( count($years) > 0 ) {
      krsort($years);
      reset($years);
      wp_localize_script( 'iongeo-js', 'investorYears', $years );
      $first_year = key($years);
      $selected_event = false;
      if ( isset($_GET['e']) ) {
        $url_event_args = array(
          'name'        =>  $_GET['e'],
          'post_type'   => 'investor-event',
          'numberposts' => 1
        );
        if ( $url_events = get_posts($url_event_args) ) {
          $url_event_date = get_field('event_date', $url_events[0]->ID);
          $url_event_year = date("Y", strtotime($url_event_date));
          $first_year = $url_event_year;
          $selected_event = $url_events[0]->ID;
        }
      } ?>
      <div id="investor-event-header">
        <div id="investor-event-years" class="flex">
          <?php
          $year_ind = 0;
          foreach ( $years as $key => $year ) {
            if ( $year_ind > 2 ) {
              continue;
            } ?>
            <a id="investor-year-<?php echo $key; ?>" class="investor-event-year<?php echo $key == $first_year ? ' active' : ''; ?>" href="#" data-year="<?php echo $key; ?>"><?php echo $key; ?></a>
            <?php
            $year_ind++;
          }
          if ( count($years) > 3 ) { ?>
            <select name="event-years" id="event-years">
              <option disabled selected>Choose a year</option>
              <?php
              $year_ind = 0;
              foreach ( $years as $key => $year ) {
                $year_ind++;
                if ( $year_ind < 4 ) {
                  continue;
                } ?>
                <option value="<?php echo $key; ?>"><?php echo $key; ?></option>
                <?php
              } ?>
            </select>
            <a href="#" class="more-event-years">
              <span>More</span>
              <img src="<?php echo get_template_directory_uri(); ?>/images/icons/plus.png"/>
            </a>
            <?php
          } ?>
        </div>
        <div class="filtered-list-search">
          <form id="filter-search-submit">
            <input type="text" placeholder="Search Investor Events" />
            <input type="submit" value="search" />
          </form>
        </div>
      </div>
      <div class="events-by-year-container">
        <div class="events-by-year">
          <?php echo get_investor_events_by_year($first_year, $years[$first_year], $selected_event); ?>
        </div>
        <div class="ion-loader-container fullheight low-white">
      		<div class="ion-loader">
      			<div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div>
      		</div>
      	</div>
      </div>
    <?php
    }
  endif;
  return ob_get_clean();
}

function get_investor_events_by_year($year, $ids, $selected = false, $search = false) {
  ob_start();
  $args = [
    'post_type'       =>   'investor-event',
    'posts_per_page'  =>   -1,
    'meta_key'        =>   'event_date',
    'order'           =>   'ASC',
    'meta_type'       =>   'DATETIME',
  	'orderby'         =>   'meta_value_datetime',
  ];
  if ( $ids ) {
    $args['post__in'] = $ids;
  }
  if ( $search ) {
    $args['s'] = $search;
  }
  $events_by_year = new WP_Query($args);
  if( $events_by_year->have_posts() ) : ?>
    <div class="event-list-items">
      <?php
      while( $events_by_year->have_posts() ) : $events_by_year->the_post();
        $webcast = get_field('investor_webcast');
        $materials = get_field('supporting_material');
        $event_open = false;
        if ( $selected ) {
          if ( $selected == get_the_ID() ) {
            $event_open = true;
          }
        } elseif ( $events_by_year->current_post == 0 ) {
          $event_open = true;
        } ?>
        <div class="event-list-item<?php echo $event_open ? ' open' : ''; echo !$webcast && !$materials ? ' disabled' : ''; ?>">
          <div class="event-list-item-header">
            <div class="content">
              <div class="row flex">
                <?php
                $date = get_field('event_date');
                $formatted_date = date("Y-m-d H:i", strtotime($date));
                $month = date("M", strtotime($date));
                $day = date("j", strtotime($date));
                $time = date("g:i", strtotime($date));
                $userTimezone = new DateTimeZone('America/Chicago');
                $gmtTimezone = new DateTimeZone('GMT');
                $myDateTime = new DateTime($formatted_date, $gmtTimezone);
                $offset = $userTimezone->getOffset($myDateTime);
                $myInterval=DateInterval::createFromDateString((string)$offset . 'seconds');
                $myDateTime->add($myInterval);
                $gmt_time = $myDateTime->format('g:i T');
                $full_date = date("Y-m-d", strtotime($date));
                $one_year_ago = date('Y-m-d', strtotime('-1 year')); ?>
                <div class="event-list-date col-1 col-2-md-down px-1">
                  <?php
                  echo $month; ?></br>
                  <?php echo $day; ?>
                </div>
                <span class="col-10 col-9-md-down px-1 event-list-title">
                  <?php echo get_the_title(); ?>
                </span>
                <?php
                if ( $webcast || $materials ) { ?>
                  <div class="col-1 col-1-md-down px-1 down-arrow-container">
                    <div class="down-arrow">
                      <?php echo file_get_contents( get_template_directory_uri() . '/images/icons/down-arrow.svg'); ?>
                    </div>
                  </div>
                <?php
                } ?>
              </div>
            </div>
          </div>

          <div class="event-list-item-content">
            <?php
            if ( $webcast && $one_year_ago < $full_date ) { ?>
              <a class="webcast-link align-center flex" target="_blank" href="<?php echo $webcast['url']; ?>">
                <?php echo file_get_contents( get_template_directory_uri() . '/images/icons/webcast.svg'); ?>
                <span class="ct-time">Webcast: <?php echo $time. ' CT'; ?></span>
                <span class="gmt-time"><?php echo $gmt_time; ?></span>
                <span class="join-webcast"></span>
              </a>
            <?php
            }
            if ( $materials ) { ?>
              <div class="row flex supporting-materials">
                <?php
                $mat_array = [];
                foreach ( $materials as $mat ) {
                  if ( in_array($mat['url'], $mat_array) ) {
                    continue;
                  }
                  array_push($mat_array, $mat['url']); ?>
                  <div class="supporting-material col-6 px-1 full-width">
                    <a target="_blank" href="<?php echo $mat['url']; ?>" class="flex align-center supporting-material-link">
                      <span class="icon"><img src="<?php echo get_template_directory_uri(); ?>/images/icons/icon-<?php echo $mat['category'] ? strtolower($mat['category']) : 'pdf'; ?>.svg"/></span>
                      <span class="title"><?php echo $mat['title']; ?></span>
                      <span class="download"><img src="<?php echo get_template_directory_uri(); ?>/images/icons/down-arrow.svg" alt="download icon"/></span>
                    </a>
                  </div>
                <?php
                } ?>
              </div>
            <?php
            } ?>
          </div>
        </div>
      <?php
      endwhile;
      wp_reset_postdata(); ?>
    </div>
  <?php
  endif;
  return ob_get_clean();
}

function create_investor_events($events, $args, $slug) {
  $eventsObj = (object)['moreevents' => false];
  ob_start();
  while($events->have_posts()): $events->the_post(); ?>
    <div class="events-item col-4">
      <div class="event-header">
        <div class="event-date">
          <?php
          $date = get_field('event_date');
          $month = date("M", strtotime($date));
          $day = date("d", strtotime($date));
          echo $month; ?></br>
          <?php echo $day; ?>
        </div>
        <div class="event-header-content">
          <h4 class="event-title">
            <?php the_title(); ?>
          </h4>
          <?php
          if($webcast = get_field('investor_webcast') && $slug != 'investors-tbd' && $slug != 'investors-past') { ?>
            <a class="event-linkout" target="_blank" href="<?php echo $webcast['url']; ?>">webcast</a>
          <?php
          }
          if($materials = get_field('supporting_material')) {
            foreach($materials as $mats) { ?>
              <a class="event-linkout" target="_blank" href="<?php echo $mats['url']; ?>"><!--<?php echo $mats['title']; ?>-->presentation</a>
            <?php
            }
          } ?>
        </div>
      </div>
    </div>
  <?php
  endwhile;
  $eventsObj->events = ob_get_clean();
  if($events->max_num_pages > $args['paged']) {
    $eventsObj->moreevents = true;
  }
  return $eventsObj;
}

function display_investors_landing() {
  ob_start(); ?>
  <div id="investor-modules-wrapper" class="has-side-scroll">
    <?php side_scroll_nav('investors-landing'); ?>
    <div id="investor-modules">
      <?php
      $modules = get_field('investor_modules');
      if($modules) { ?>
        <div id="investor-modules-section" class="flex">
          <!-- <div id="modules-gutter"></div> -->
          <?php
          foreach($modules as $module) {
            switch($module['acf_fc_layout']) {
              case 'post_list_by_type':
                $postType = get_post_type_object($module['post_type']);
                $title = ($module['title']) ? $module['title'] : $postType->labels->name;
                $archive_link = (isset($module['link']->url)) ? $module['link']->url : home_url($postType->archive_url);
                $linklabel = (isset($module['link']->title)) ? $module['link']->title : 'more '.$postType->labels->name;
                $number_of_posts = $module['number_of_posts'] ? $module['number_of_posts'] : 3;
                $args = [
                  'post_type' => $postType->name,
                  'posts_per_page' => $number_of_posts,
                  'orderby' => 'date'
                ];
                $ptWithDate = ['investor-event'];
                if(in_array($postType->name, $ptWithDate)) {
                  $args['meta_key'] = 'event_date';
                  $args['order'] = 'DESC';
                  $args['meta_type'] = 'DATETIME';
                	$args['orderby'] = 'meta_value_datetime';
                }
                if( $module['manual_selection'] ) {
                  $args['post__in'] = $module['manual_selection'];
                  $args['posts_per_page'] = count($module['manual_selection']);
                  $args['orderby'] = 'post__in';
                }
                $invPosts = new WP_Query($args);

                if( $invPosts->have_posts() ) : ?>
                  <div class="ion-module module-type-<?php echo $module['acf_fc_layout']; ?> side-scroll-item has-notch">
                    <h2><?php echo $title; ?></h2>
                    <div class="filtered-list">
                      <div class="filtered-list-container">
                        <?php
                        while($invPosts->have_posts()): $invPosts->the_post(); ?>
                          <div class="filtered-list-post" data-post-id="<?php echo get_the_ID(); ?>">
                            <?php
                            // if(in_array($postType->name, $ptWithDate)) {
                            // } ?>
                            <div class="filtered-post-date">
                              <?php
                              $date = get_field('event_date') ? get_field('event_date') : get_the_date();
                              $month = date("M", strtotime($date));
                              $day = date("d", strtotime($date));
                              $year = date("Y", strtotime($date));
                              echo $month . '</br>' . $day . '</br>' . $year; ?>
                            </div>
                            <?php
                            if ( $postType->name == 'investor-event' ) { ?>
                              <div class="filtered-post-title">
                                <a href="<?php echo home_url($postType->archive_url) . '?e=' . basename(get_the_permalink()); ?>"><?php echo get_the_title(); ?></a>
                              </div>
                            <?php
                            } else {
                              echo get_filtered_post_title(get_the_id());
                            } ?>
                          </div>
                        <?php
                        endwhile;
                        wp_reset_postdata(); ?>
                      </div>
                      <a class="more-posts" href="<?php echo $archive_link; ?>"><?php echo $linklabel; ?></a>
                    </div>
                  </div>
                <?php
                endif;
              break;
              case 'performance': ?>
                <div class="ion-module module-type-<?php echo $module['acf_fc_layout']; ?> has-notch side-scroll-item">
                  <h2>Performance</h2>
                </div>
              <?php
              break;
              case 'custom_content': ?>
                <div class="ion-module module-type-<?php echo $module['acf_fc_layout']; ?> has-notch side-scroll-item">
                    <?php
                    if($title = $module['title']) { ?>
                      <h2><?php echo $title; ?></h2>
                      <?php
                    }
                    if($content = $module['content']) { ?>
                      <div class="custom-module-content">
                        <?php echo $content; ?>
                      </div>
                    <?php
                  } ?>
                </div>
              <?php
              break;
              case 'single_post':
              case 'investor_presentation':
                $title = $featuredID = '';
                $args = $single_post = $recent_posts = $postType = $post_type_obj = false;
                // If the post type is selected, it should show recent posts
                if ( isset($module['post_type']) ) {
                  $postType = $module['post_type'];
                  $post_type_obj = get_post_type_object($postType);
                  $args = ['posts_per_page' => 1];
                  $args['post_type'] = $module['post_type'];
                  if($postType == 'event' || $postType == 'investor-event') {
                    $date = date('Y-m-d H:i:s',strtotime("today"));
                    $args['meta_key'] = 'event_date';
                    $args['order'] = 'DESC';
                    $args['meta_type'] = 'DATETIME';
                  	$args['orderby'] = 'meta_value_datetime';
                    $args['meta_query'] = [
                      [
                        'key'       => 'event_date',
                        'value'     => $date,
                        'compare'   => '>=',
                        'type'      => 'DATETIME',
                      ]
                    ];
                  }
                  if ( $investorPost = get_posts($args) ) {
                    $single_post = $investorPost[0];
                  } else { // If no posts and is event post type, get 3 most recent posts
                    if ( $postType == 'event' || $postType == 'investor-event' ) {
                      $date = date('Y-m-d H:i:s',strtotime("today"));
                      $args = [
                        'meta_key' => 'event_date',
                        'meta_type'       =>   'DATETIME',
                        'order'           =>   'DESC',
                      	'orderby'         =>   'meta_value_datetime',
                        'posts_per_page' => 3,
                        'post_type' => $postType,
                        'meta_query' => [
                          [
                            'key'       => 'event_date',
                            'value'     => $date,
                            'compare'   => '<',
                            'type'      => 'DATETIME',
                          ]
                        ]
                      ];
                      $recent_posts = get_posts($args);
                    }
                  }
                  $title = $post_type_obj->label;
                }
                // If featured post is selected, default to that post
                if ( isset($module['featured_post']) ) {
                  $single_post = $module['featured_post'];
                }
                if ( $module['title'] ) {
                  $title = $module['title'];
                } else if ( $single_post ){
                  $title = $single_post['post_title'];
                }
                if($content = $module['content']) {
                  if((isset($module['single_post_method']) && $module['single_post_method'] == 'custom') || $module['acf_fc_layout'] == 'investor_presentation') { ?>
                    <div class="ion-module module-type-<?php echo $module['acf_fc_layout']; ?> has-notch side-scroll-item">
                      <h2><?php echo $title; ?></h2>
                      <?php
                      echo $content; ?>
                    </div>
                  <?php
                  }
                } else if( $single_post ) { ?>
                  <div class="ion-module module-type-<?php echo $module['acf_fc_layout']; ?> has-notch side-scroll-item">
                    <h2><?php echo $title; ?></h2>
                    <?php
                    if(get_the_post_thumbnail($single_post->ID)) {
                      $fImg = get_the_post_thumbnail_url($single_post->ID, 'medium'); ?>
                      <a href="<?php echo get_the_permalink($single_post->ID); ?>">
                        <img src="<?php echo $fImg; ?>"/>
                      </a>
                    <?php
                    } ?>
                    <h3>
                      <a href="<?php echo get_the_permalink($single_post->ID); ?>">
                        <?php echo $single_post->post_title; ?>
                      </a>
                    </h3>
                    <?php
                    if($eventDate = get_field('event_date', $single_post->ID)) {
                      $format_in = 'd/m/Y g:i a'; // the format your value is saved in (set in the field options)
                      $format_out = 'M d, Y'; // the format you want to end up with
                      $eventDate = DateTime::createFromFormat($format_in, $eventDate); ?>
                      <p class="event-date"><?php echo $eventDate->format( $format_out ); ?></p>
                    <?php
                    }
                    if(has_excerpt($single_post->ID)) { ?>
                      <div class="single-post-desc">
                        <?php echo get_the_excerpt($single_post->ID); ?>
                      </div>
                    <?php
                    }
                    if($eventDate = get_field('event_date', $single_post->ID)) {
                      $format_in = 'd/m/Y g:i a'; // the format your value is saved in (set in the field options)
                      $format_out = 'g:i a'; // the format you want to end up with
                      $eventDate = DateTime::createFromFormat($format_in, $eventDate); ?>
                      <p class="event-time">
                        <?php echo $eventDate->format( $format_out );
                        $material = get_field('supporting_material', $single_post->ID);
                        if($material) { ?>
                          <span class="event-presentation-link">
                            <a href="<?php echo $material[0]['url']; ?>" target="_blank">
                              | Download Presentation
                            </a>
                          </span>
                        <?php
                        } ?>
                      </p>
                    <?php
                    }
                    if($post_type_obj) { ?>
                      <a class="see-more" href="<?php echo home_url($post_type_obj->archive_url); ?>">See More <?php echo $post_type_obj->label; ?></a>
                    <?php
                    } ?>
                  </div>
                <?php
                } else if ( $recent_posts ) {
                  // If recent posts, show 3 recent posts
                  $recent_posts_query = new WP_Query($args);
                  if( $recent_posts_query->have_posts() ) : ?>
                    <div class="ion-module module-type-<?php echo $module['acf_fc_layout']; ?> side-scroll-item has-notch">
                      <h2><?php echo $title; ?></h2>
                      <h6>PAST EVENTS</h6>
                      <div class="filtered-list">
                        <div class="filtered-list-container">
                          <?php
                          while($recent_posts_query->have_posts()): $recent_posts_query->the_post(); ?>
                            <div class="filtered-list-post">
                              <div class="filtered-post-date">
                                <?php
                                $date = get_field('event_date');
                                $month = date("M", strtotime($date));
                                $day = date("d", strtotime($date));
                                $year = date("Y", strtotime($date));
                                echo $month . '</br>' . $day . '</br>' . $year; ?>
                              </div>
                              <?php
                              echo get_filtered_post_title(get_the_id()); ?>
                            </div>
                          <?php
                          endwhile;
                          wp_reset_postdata(); ?>
                        </div>
                      </div>
                      <?php
                      if($post_type_obj) { ?>
                        <a class="see-more" href="<?php echo home_url($post_type_obj->archive_url); ?>">See More <?php echo $post_type_obj->label; ?></a>
                      <?php
                      } ?>
                    </div>
                  <?php
                  endif;
                }
              break;
              case 'list_slider': ?>
                <div class="ion-module module-type-<?php echo $module['acf_fc_layout']; ?> side-scroll-item has-notch">
                  <?php
                  if($title = $module['title']) { ?>
                    <h2><?php echo $title; ?></h2>
                    <?php
                  } ?>
                  <div class="list-slider-container">
                    <div class="list-slider">
                      <?php
                      foreach($module['list_items'] as $key=>$listItem) { ?>
                        <div class="list-slider-item">
                          <div class="list-slider-item-content">
                            <?php
                            if($listItemTitle = $listItem['title']) { ?>
                              <h6><?php echo $listItemTitle; ?></h6>
                            <?php
                            }
                            if($listItemContent = $listItem['content']) { ?>
                              <div class="list-item-content">
                                <?php echo $listItemContent; ?>
                              </div>
                            <?php
                            } ?>
                          </div>
                          <span class="list-slider-item-count"><?php echo $key+1; ?></span>
                        </div>
                      <?php
                      } ?>
                    </div>
                    <div class="list-slider-arrows">
                      <a href="#" class="list-slider-arrow circle-arrow arrow-prev">
                      </a>
                      <a href="#" class="list-slider-arrow circle-arrow arrow-next">
                      </a>
                    </div>
                  </div>
                </div>
              <?php
              break;
              default:
              break;
            }
          } ?>
        </div>
      <?php
    } ?>
    </div>
  </div>
<?php
return ob_get_clean();
}
add_shortcode( 'investors-page', 'display_investors_landing');

function get_filtered_post_title($id) {
  ob_start();
  $links = [];
  $link_target = '_self';
  if($formats = get_field('sec_formats', $id)) {
    $accForms = ['pdf', 'rtf', 'mp3'];
    foreach($formats as $format) {
      if(in_array($format['format'], $accForms)) {
        $link_obj = [
          'url' => $format['url'],
          'image' => 'download-'.strtolower($format['format']).'.png',
          'format' => $format['format']
        ];
        $link_target = '_blank';
        array_push($links, $link_obj);
      }
    }
  } else if($pdf = get_field('upload_pdf', $id)) {
    $link_obj = [
      'url' => $pdf['url'],
      'image' => 'icons/down-arrow.svg',
      'format' => 'pdf'
    ];
    array_push($links, $link_obj);
  } else if( $docs = get_field('documents', $id) ) {
    foreach( $docs as $doc ) {
      $link_obj = [
        'url' => $doc['url'],
        'image' => 'download-'.strtolower($doc['format']).'.png',
        'format' => $doc['format']
      ];
      $link_target = '_blank';
      array_push($links, $link_obj);
    }
  } else {
    $link_obj = [
      'url' => get_the_permalink($id),
      'image' => 'icons/down-arrow.svg',
      'format' => 'url'
    ];
    array_push($links, $link_obj);
  } ?>

  <div class="filtered-post-title">
    <?php
    if(count($links) < 2) { ?>
      <a target="<?php echo $link_target; ?>" href="<?php echo $links[0]['url']; ?>"<?php echo $links[0]['format'] != 'url' ? ' download' : ''; ?>><?php echo get_the_title($id); ?></a>
    <?php
    } else {
      echo get_the_title($id);
    } ?>
  </div>
  <?php
  if( count($links) > 0 ) {
    $links_array = [];
    // fix duplicate link arrays :(
    foreach($links as $link) {
      if ( in_array($link['url'], $links_array) ) {
        continue;
      }
      array_push($links_array, $link['url']); ?>
      <a class="filtered-post-link" style="margin-left: 4px;" target="<?php echo $link_target; ?>" href="<?php echo $link['url']; ?>"<?php echo $link['format'] != 'url' ? 'download' : ''; ?>>
        <span class="meta <?php echo $link['format'] == 'url' ? 'posturl' : 'download'; ?>">
          <img src="<?php echo get_template_directory_uri().'/images/'.$link['image']; ?>" alt="<?php echo $link['format']; ?>" class="posturl-icon" />
        </span>
      </a>
    <?php
    }
  }

  return ob_get_clean();
}

add_filter( 'manage_edit-investor-event_columns', 'edit_event_columns' ) ;
add_filter( 'manage_edit-event_columns', 'edit_event_columns' ) ;

function edit_event_columns( $columns ) {
  $columns['event_date'] = 'Event Date';
  return $columns;
}

function custom_events_column( $column_name, $post_id ) {
    if( $column_name == 'event_date' ) {
        $eventDate = get_field( 'event_date', $post_id );
        echo $eventDate;
    }
}
add_action( 'manage_investor-event_posts_custom_column', 'custom_events_column', 10, 2 );
add_action( 'manage_event_posts_custom_column', 'custom_events_column', 10, 2 );
