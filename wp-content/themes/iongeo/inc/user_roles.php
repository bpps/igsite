<?php
function wpse28782_remove_menu_items() {
  // Add all capabilities to admin
  // Admin capabilities
  if(current_user_can( 'administrator' ) ):
    $administrator = get_role('administrator');
    $addCaps = [
      'edit_event',
      'read_event',
      'edit_events',
      'edit_others_events',
      'delete_event',
      'read_private_events',
      'publish_events',
      'edit_offering',
      'edit_offerings',
      'edit_others_offerings',
      'publish_offerings',
      'read_offering',
      'read_private_offerings',
      'delete_offering',
      'manage_offering_cats'
    ];
    foreach($addCaps as $cap) {
      if(!isset($administrator->capabilities[ $cap ]) || $administrator->capabilities[ $cap ] == false) {
        //$administrator->add_cap($cap , true);
      }
    }

    $removeCaps = [
      'manage_offering_cats'
    ];
    foreach($removeCaps as $cap) {
      if(!isset($administrator->capabilities[ $cap ]) || $administrator->capabilities[ $cap ] == 1) {
        //$administrator->remove_cap( $cap );
      }
    }
    //print_r($administrator);

  endif;

  // Editor Capabilities

  if( current_user_can( 'editor' ) ):
    $editor = get_role('editor');
    $addCaps = [
      'edit_event',
      'read_event',
      'edit_events',
      'edit_others_events',
      'edit_offering',
      'edit_offerings',
      'edit_others_offerings',
      'edit_published_posts',
      'read_offerings',
      'manage_offering_cats'
    ];
    foreach($addCaps as $cap) {
      if(!isset($editor->capabilities[ $cap ]) || $editor->capabilities[ $cap ] == false) {
        //$editor->add_cap( $cap, true );
      }
    }

    // Remove Caps
    $removeCaps = [
      'publish_posts',
      'publish_pages',
      'publish_offering',
      'edit_post',
    ];
    foreach($removeCaps as $cap) {
      if(!isset($editor->capabilities[ $cap ]) || $editor->capabilities[ $cap ] == 1) {
        //$editor->remove_cap( $cap );
      }
    }

    remove_menu_page( 'edit.php?post_type=acf-field-group' );
    remove_menu_page( 'edit.php?post_type=acf' );
    remove_menu_page('link-manager.php');
    remove_menu_page('plugins.php');
    remove_menu_page('themes.php');
    remove_menu_page('users.php');
    remove_menu_page('tools.php');
    remove_menu_page('options-general.php');
    remove_menu_page('edit.php?post_type=acf');
    remove_menu_page('ion-settings');
    remove_menu_page('edit-comments.php');
  endif;
}
add_action( 'admin_init', 'wpse28782_remove_menu_items' );

function user_roles_css() {
  if( current_user_can( 'editor' ) ):
    echo '<style>
      .row-actions .edit, .post-type-event .row-actions .inline {
        display: none;
      }
      .iedit.level-0 .row-title {
        cursor: default;
        color: rgb(60,60,60);
        pointer-events: none;
      }
      #wpadminbar ul li#wp-admin-bar-edit {
        display: none;
      }
    </style>';
  endif;
}

add_action('admin_head', 'user_roles_css');

function notify_admin_for_pending( $post ) {
  $user_info = get_userdata($post->post_author);
  $strTo = array ('brandon@uncanny.studio', get_bloginfo('admin_email'));
  $strSubject = 'Editor: ' . $user_info->user_nicename . ' submitted a post';
  $strMessage = '"' . $post->post_title . '" by ' . $user_info->user_nicename . ' has submitted a post for review at ' . wp_get_shortlink ($post->ID) . '&preview=true. Please proof.';
  wp_mail( $strTo, $strSubject, $strMessage );
}

add_action( 'draft_to_pending', 'notify_admin_for_pending');
add_action( 'auto-draft_to_pending', 'notify_admin_for_pending' );
add_action( 'new_to_pending', 'notify_admin_for_pending');

?>
