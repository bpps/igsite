<?php

/*------------------------------*\
  Import from CSV
\*------------------------------*/

function import_ion_resources() {
	wp_add_dashboard_widget(
   'import_ion_resources_widget', // Widget slug.
   'Import ION resources', // Title.
   'import_ion_resources_widget_function' // Display function.
  );
}

add_action( 'wp_dashboard_setup', 'import_ion_resources' );

function import_ion_resources_widget_function() {
  if (isset($_POST['submit'])) {
    // $args = [
    //   'post_type' => 'post',
    //   'posts_per_page' => -1
    // ];
    // $all_posts_query = new WP_Query( $args );
    // if( $all_posts_query->have_posts() ): // If post with email exists
    //   while( $all_posts_query->have_posts() ) : $all_posts_query->the_post();
    //     update_post_meta( get_the_ID(), 'permalink', 'http://profile.haldiskin.com/' . basename( get_the_permalink() ) . '/' );
    //   endwhile;
    // endif;
    // wp_reset_query();

    $csv_file = $_FILES['csv_file'];
    $csv = array_map('str_getcsv', file($csv_file['tmp_name']));
    $createdResources = [];
    $updatedResources = [];
    $keys = array_shift($csv);
    foreach ($csv as $i=>$row) {
      $csv[$i] = array_combine($keys, $row);
    }
    // Create user profiles
    foreach ($csv as $key => $row) {
      // if( $key > 100 ) {
      //   continue;
      // }
      if ($key > 5) {
        continue;
      }
      $processResource = import_ion_resource($row);
      if($processResource == 'added') {
        array_push($createdResources, $row);
      } else if ( $processResource == 'update' ) {
        array_push($updatedResources, $row);
      }
    }

    echo count($createdResources) . ' resources imported</br>';
    echo count($updatedResources) . ' resources updated';
  } else {
    echo '<form action="" method="post" enctype="multipart/form-data">';
    echo '<input type="file" name="csv_file">';
    echo '<input type="submit" name="submit" value="submit">';
    echo '</form>';
  }
}

function import_ion_resource ($data) {

  if ($resource = get_page_by_title($data['Name'], OBJECT, 'resource') ) {
    // This resource exists. Update it.
    return 'update';
  } else {
    // No resource exists. Create it.
    $custom_taxes = [];
    $csvTaxes = [
      ['csv_name' => 'Type', 'wp_tax' => 'resource-type', 'meta' => 'type'],
      ['csv_name' => 'BusinessUnit', 'wp_tax' => 'business-unit', 'meta' => 'business unit'],
      ['csv_name' => 'Category', 'wp_tax' => 'resource-category', 'meta' => 'category'],
      ['csv_name' => 'Interest', 'wp_tax' => 'resource-interest', 'meta' => 'interest'],
      ['csv_name' => 'Source', 'wp_tax' => 'resource-source', 'meta' => 'source'],
      ['csv_name' => 'Author', 'wp_tax' => 'resource-author', 'meta' => 'author'],
    ];
    foreach($csvTaxes as $taxObj) {
      if ( $data[$taxObj['csv_name']] ) {
        $taxes = str_to_arr($data[$taxObj['csv_name']]);
        $tax_id_arr = [];
        foreach($taxes as $tax) {
          if ( $term = get_term_by( 'name', $tax, $taxObj['wp_tax'] ) ) {
            // If term exists, get its id and add it to the array
            array_push( $tax_id_arr, $term->term_id );
          } else {
            // If it doesn't exist, add it and add that id to the array
            if( $term = wp_insert_term( $tax, $taxObj['wp_tax'] ) ) {
							array_push( $tax_id_arr, $term['term_id'] );
						} else {
						}
          }
        }
        $custom_taxes[$taxObj['wp_tax']] = $tax_id_arr;
      }
    }

    // if ( $data['BusinessUnit'] ) {
    //   $taxes = str_to_arr($data['BusinessUnit']);
    //   array_push($custom_taxes, ['business-unit' => $taxes]);
    // }
    // if ( $data['Category'] ) {
    //   $taxes = str_to_arr($data['Category']);
    //   array_push($custom_taxes, ['resource-category' => $data['Category']]);
    // }
    // if ( $data['Interest'] ) {
    //   array_push($custom_taxes, ['interest' => $data['Interest']]);
    // }
    // if ( $data['Source'] ) {
    //   array_push($custom_taxes, ['source' => $data['Source']]);
    // }

    $args = [
      'post_title' => $data['Name'],
      'post_type' => 'resource',
      'post_status' => 'publish'
    ];

    // foreach($data as $key=>$row) {
		//
    // }

    // If there are custom taxonomies, add them to the arguments
    if ( count($custom_taxes) > 0 ) {
      $args['tax_input'] = $custom_taxes;
    }

    // If there are tags, add them to the arguments
    if ( $data['Tags'] ) {
      $args['tags_input'] = str_to_arr($data['Tags']);
    }

    // If there is a date, add it to the arguments
    if ( $data['Date'] ) {
      $date_stamp = strtotime($data['Date']);
      $postdate = date("Y-m-d H:i:s", $date_stamp);
      $args['post_date'] = $postdate;
    }

    // If there is a description, add it to the arguments
    if ( $data['Description'] ) {
      $args['post_content'] = $data['Description'];
    }

    if($resource = wp_insert_post( $args )) {
      // resource created.
      if ( $data['DocumentPath'] ) {
        $file = $data['DocumentPath'];
        $filename = basename($file);
				$file_contents = file_get_contents(rawurlencode($file));
        $upload_file = wp_upload_bits($filename, null, $file_contents);
        if (!$upload_file['error']) {
        	$wp_filetype = wp_check_filetype($filename, null );
        	$attachment = array(
        		'post_mime_type' => $wp_filetype['type'],
        		'post_parent' => $resource,
        		'post_title' => preg_replace('/\.[^.]+$/', '', $filename),
        		'post_content' => '',
        		'post_status' => 'inherit'
        	);
        	$attachment_id = wp_insert_attachment( $attachment, $upload_file['file'], $resource );
        	if (!is_wp_error($attachment_id)) {
        		require_once(ABSPATH . "wp-admin" . '/includes/image.php');
        		$attachment_data = wp_generate_attachment_metadata( $attachment_id, $upload_file['file'] );
        		wp_update_attachment_metadata( $attachment_id,  $attachment_data );

            foreach($csvTaxes as $taxObj) {
              if ( $data[$taxObj['csv_name']] ) {
                update_post_meta( $attachment_id, $taxObj['meta'], $data[$taxObj['csv_name']] );
              }
            }
            update_field('upload_pdf', $attachment_id, $resource);
        	}
        }
      }

      return 'added';
    }
  }
  return;
}

function str_to_arr ($str) {
  $new_arr = [];
  $arr = explode(',', $str);
  foreach($arr as $a) {
    array_push($new_arr, trim($a));
  }
  return $new_arr;
}
