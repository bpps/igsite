<?php
function display_ion_locations( $atts ) {
  $a = shortcode_atts( array(), $atts );
  ob_start();
  if($hq = get_field('hq_location_info', 'option')) {
    $map = $hq['map']['sizes']['small-medium']; ?>
    <div class="ion-locations flex" id="ion-locations-hq">
      <div class="location-image bg-centered col-5">
        <div class="ratio-image-container ratio-3-2">
          <?php
          if($hq['map_url']) { ?>
            <a target="_blank" href="<?php echo $hq['map_url']; ?>">
          <?php
          } ?>
            <img alt="map" src="<?php echo $map; ?>"/>
          <?php
          if($hq['map_url']) { ?>
            </a>
          <?php
          } ?>
        </div>
      </div>
      <div class="ion-location-content col-6">
        <?php
        if(isset($hq['name'])) { ?>
          <h2><?php echo $hq['name']; ?></h2>
        <?php
        }
        if(isset($hq['description'])) {
          echo $hq['description'];
        } ?>
        <div class="location-info">
          <div class="location-address">
            <?php
            if($hq['map_url']) { ?>
              <a target="_blank" href="<?php echo $hq['map_url']; ?>">
            <?php
            }
              echo $hq['address'];
            if($hq['map_url']) { ?>
              </a>
            </div>
          <?php
          }
          if(isset($hq['phone_numbers'])) { ?>
            <div class="location-phone">
              <?php
              foreach($hq['phone_numbers'] as $phone) { ?>
                <a href="tel:<?php echo $phone['phone_number']; ?>">
                  <span><?php echo $phone['label'].': '; ?></span>
                  <?php echo $phone['phone_number']; ?>
                </a>
              <?php
              } ?>
            </div>
          <?php
          } ?>
        </div>
      </div>
    </div>
  <?php
    $lTiers = ['secondary'];
    foreach($lTiers as $lTier) {
      if($locations = get_field($lTier.'_locations', 'option')) { ?>
        <div class="ion-locations flex" id="ion-locations-<?php echo $lTier; ?>">
          <?php
          foreach($locations as $loc) { ?>
            <div class="ion-location col-4">
              <?php
              if(isset($loc['name'])) { ?>
                <h4><?php echo $loc['name']; ?></h4>
              <?php
              }
              if(isset($loc['map'])) { ?>
                <div class="location-image bg-centered ratio-image-container ratio-3-1">
                  <?php
                  if($loc['map_url']) { ?>
                    <a target="_blank" href="<?php echo $loc['map_url']; ?>">
                  <?php
                  } ?>
                    <img src="<?php echo $loc['map']['sizes']['small-medium']; ?>"/>
                  <?php
                  if($loc['map_url']) { ?>
                    </a>
                  <?php
                  } ?>
                </div>
              <?php
              }
              if(isset($loc['address'])) { ?>
                <div class="location-address">
                  <?php
                  if($loc['map_url']) { ?>
                    <a target="_blank" href="<?php echo $loc['map_url']; ?>">
                  <?php
                  }
                    echo $loc['address'];
                  if($loc['map_url']) { ?>
                    </a>
                  <?php
                  } ?>
                </div>
              <?php
              }
              if($phones = $loc['phone_numbers']) { ?>
                <div class="location-phone">
                  <?php
                  foreach($phones as $phone) { ?>
                    <a href="tel:<?php echo $phone['phone_number']; ?>">
                      <span><?php echo $phone['label'].': '; ?></span>
                      <?php echo $phone['phone_number']; ?>
                    </a>
                  <?php
                  } ?>
                </div>
              <?php
              } ?>
            </div>
          <?php
          } ?>
        </div>
      <?php
      }
    }
    if($locations = get_field('tertiary_locations', 'option')) { ?>
      <div class="content-tabs">
				<div class="content-tabs-nav">
					<?php
					foreach($locations as $key=>$loc) { ?>
						<a href="#" class="content-tab-button<?php echo $key == 0 ? ' active' : ''; ?>" data-slug="<?php echo slugify($loc['location_country']); ?>">
							<?php echo $loc['location_country']; ?>
						</a>
					<?php
					} ?>
				</div>
				<?php
				foreach($locations as $key=>$loc) { ?>
					<div data-slug="<?php echo slugify($loc['location_country']); ?>" class="content-tab<?php echo $key == 0 ? ' active' : ''; ?>">
            <div class="country-locations flex">
  						<?php
              foreach($loc['locations'] as $location) { ?>
                <div class="country-location col-3">
                  <h4 class="country-name"><?php echo $location['name']; ?></h4>
                  <?php
                  if(isset($location['address'])) { ?>
                    <div class="country-address">
                      <?php
                      if($location['map_url']) { ?>
                        <a target="_blank" href="<?php echo $location['map_url']; ?>">
                      <?php
                      }
                          echo $location['address'];
                      if($location['map_url']) { ?>
                        </a>
                      <?php
                      } ?>
                    </div>
                  <?php
                  }
                  if(isset($location['phone_numbers'])) { ?>
                    <div class="location-phone">
                      <?php
                      foreach($location['phone_numbers'] as $phone) { ?>
                        <a href="tel:<?php echo $phone['phone_number']; ?>">
                          <span><?php echo $phone['label'].': '; ?></span>
                          <?php echo $phone['phone_number']; ?>
                        </a>
                      <?php
                      } ?>
                    </div>
                  <?php
                  } ?>
                </div>
              <?php
              } ?>
            </div>
					</div>
				<?php
				}	?>
	    </div>
    <?php
    }
  }
  return ob_get_clean();
}
add_shortcode( 'locations', 'display_ion_locations' );

function toggle_content( $atts = false, $content, $tag ) {
  $showmore = "show more";
  if($atts) {
    $a = shortcode_atts( array(), $atts );
    if(isset($a['button'])) {
      $showmore = $a['button'];
    }
  }
  ob_start(); ?>
  <div class="toggle-content">
    <?php
    echo $content; ?>
  </div>
  <a class="toggle-content-button more-button" href="#"><?php echo $showmore; ?></a>

  <?php
  return ob_get_clean();
}
add_shortcode( 'hide', 'toggle_content' );

function display_team( $atts = false, $content, $tag ) {
  if($atts) {
    $a = shortcode_atts( array(), $atts );
  }
  ob_start();
  global $post;
  if($team = get_field('team', $post->ID)) { ?>
    <section id="team-section">
      <?php
      foreach($team as $teamItem) {
        $image = get_template_directory_uri().'/images/post-placeholder.png';
        if($teamItem['team_image']) {
          $image = $teamItem['team_image']['sizes']['small-medium'];
        } ?>
        <div class="team-member flex">
          <div class="team-image-container col-3">
            <div class="team-image bg-centered" style="background-image:url(<?php echo $image; ?>);">

            </div>
          </div>
          <div class="team-content col-8">
            <?php
            if($name = $teamItem['team_name']) { ?>
              <h5><?php echo $name; ?></h5>
            <?php
            }
            if($title = $teamItem['title']) { ?>
              <h6><?php echo $title; ?></h6>
            <?php
            }
            if($content = $teamItem['team_bio']) { ?>
              <div class="team-bio">
                <?php echo $content; ?>
              </div>
            <?php
            } ?>
          </div>
        </div>
      <?php
      } ?>
    </section>
  <?php
  } ?>

  <?php
  return ob_get_clean();
}
add_shortcode( 'team', 'display_team' );

function filtered_tax_list( $atts = false ) {
  ob_start();
  $a = false;
  if($atts) {
    $a = $atts;
  }
  echo tax_list_by_type($a['type']);
  return ob_get_clean();
}

add_shortcode( 'filtered-tax-list', 'filtered_tax_list' );

function filtered_post_list( $atts = false, $content, $tag ) {
  $a = false;
  if($atts) {
    $a = $atts;
  }
  $featured = isset($a['featured']) ? $a['featured'] : false;
  ob_start();
    echo post_list_by_post_type($a['type'], $featured);
  return ob_get_clean();
}

add_shortcode( 'filtered-post-list', 'filtered_post_list' );

function post_filter_by_year( $atts = false ) {
  ob_start();
  $post_type = 'post';
  if($atts) {
    $post_type = $atts['type'];
  }
  $years = get_posts_years_array($post_type);
  echo create_list_by_year($post_type, $years);
  return ob_get_clean();
}

add_shortcode( 'post-filter-by-year', 'post_filter_by_year' );

function get_posts_years_array($post_type) {
  global $wpdb;
  $result = [];
  $placeholder = ['post_type' => '%s'];
  $years = $wpdb->get_results(
    $wpdb->prepare(
      "SELECT YEAR(post_date) FROM {$wpdb->posts} WHERE wp_posts.post_type = %s GROUP BY YEAR(post_date) DESC", $post_type
    ),
    ARRAY_N
  );
  if ( is_array( $years ) && count( $years ) > 0 ) {
    foreach ( $years as $year ) {
      $result[] = $year[0];
    }
  }
  return $result;
}

function tax_filtered_post_list( $atts = false ) {
  ob_start();
  $a = false;
  if($atts) {
    $a = $atts;
  }
  $taxes = get_object_taxonomies( $a['posttype'] );
  $filter = [];
  foreach($taxes as $tax) {
    if(isset($_GET[$tax])) {
      array_push($filter, ['taxonomy' => $tax, 'field' => 'slug', 'terms' => explode(",",$_GET[$tax])]);
    }
  }
  $filter_props = [
    'type' => 'shortcode',
    'tType' => false,
    'pType' => $a['posttype'],
    'filters' => $filter,
    'paged' => 1,
    'year' => isset($a['year']) ? $a['year'] : false,
    'presentation' => isset($a['presentation'])
  ];
  echo create_list_by_term($filter_props);
  return ob_get_clean();
}

add_shortcode( 'tax-filtered-post-list', 'tax_filtered_post_list' );

function tax_list_by_type($type) {
  $tTypes = get_terms([
    'taxonomy' => $type,
    'hide_empty' => true
  ]);
  ob_start();
  foreach($tTypes as $tType) {
    $filter_props = [
      'type' => $type,
      'tType' => $tType
    ];
    echo create_list_by_term($filter_props);
  }
  return ob_get_clean();
}

function post_list_by_post_type($type, $featured = false) {
  ob_start();
  $taxonomies = get_object_taxonomies( $type );
  $taxonomy = $taxonomies[0];
  $post_type_obj = get_post_type_object( $type );
  $terms = get_terms(['taxonomy' => $taxonomy]);
  $featuredIds = false;
  if($featured) {
    if($featuredPosts = get_field($featured, 'option')) {
      $featuredIds = []; ?>
      <div class="featured-posts-container flex">
        <div class="featured-posts-wrapper">
          <h2>Featured</h2>
          <div class="featured-posts">
            <?php
            foreach($featuredPosts as $fPost) {
              array_push($featuredIds, $fPost->ID); ?>
              <div class="featured-post">
                <div class="filtered-post-date">
                  <?php
                  $date = get_the_date($fPost->ID);
                  $month = date("M", strtotime($date));
                  $day = date("d", strtotime($date));
                  $year = date("Y", strtotime($date));
                  echo $year.'</br>'.$month.' '.$day; ?>
                </div>
                <div class="filtered-post-content">
                  <h3><?php echo get_the_title($fPost->ID); ?></h3>
                  <?php
                  if(has_excerpt($fPost->ID)) { ?>
                    <div class="filtered-post-desc">
                      <?php echo character_limit(get_the_excerpt($fPost->ID), 120); ?>
                    </div>
                  <?php
                  } ?>
                  <a href="<?php echo get_the_permalink($fPost->ID); ?>" class="ion-cta">
                    Read more
                  </a>
                </div>
              </div>
            <?php
            } ?>
          </div>
        </div>
      </div>
    <?php
    }
  }
  $posts_list = get_filter_list_posts_by_type(false, 1, false, $type, $featuredIds); ?>
  <div class="filtered-list-wrapper flex">
    <div class="filtered-list-tax-filters">

    </div>
    <section
      class="filtered-list filtered-post-type-list"
      data-excluded="<?php echo $featuredIds ? htmlspecialchars(json_encode($featuredIds), ENT_QUOTES, 'UTF-8') : ''; ?>"
      data-paged="1" data-post-type-title="<?php echo $post_type_obj->label; ?>" data-post-type="<?php echo $type; ?>" id="filtered-list-type-<?php echo $type; ?>">
      <div class="filtered-list-content">
        <div class="filtered-list-header">
          <div class="filtered-list-filter">
            <?php
            if(isset($post_type_obj->filter_text)) { ?>
              <span><?php echo $post_type_obj->filter_text; ?>:</span>
            <?php
            } ?>
            <a class="post-filter active" data-func="filter" data-tax-type="<?php echo $taxonomy; ?>" href="http://">All</a>
            <?php
            foreach($terms as $term) { ?>
              <a class="post-filter" data-func="filter" data-tax-type="<?php echo $taxonomy; ?>" data-filter="<?php echo $term->slug; ?>" href="#"><?php echo $term->name; ?></a>
            <?php
            } ?>
          </div>
        </div>
        <div class="filtered-list-posts">
          <div class="filtered-list-container">
            <?php
            echo $posts_list->posts; ?>
          </div>
        </div>
        <div style="<?php echo $posts_list->maxpages ? '' : 'display:none;'; ?>" class="filtered-list-arrows">
          <a href="#" class="circle-arrow arrow-prev inactive" data-direction="-1" data-func="prev">
          </a>
          <a href="#" class="circle-arrow arrow-next" data-direction="1" data-max-pages="<?php echo $maxpages; ?>" data-func="next">
          </a>
        </div>
      </div>
    </section>
  </div>
<?php
  return ob_get_clean();
}

function create_list_by_year($p_type, $years) {
  ob_start();
    $posts_list = get_filter_list_posts_by_type(false, 1, 'date', $p_type, false, false, $years[0]); ?>
    <div class="filtered-list-wrapper">
      <div class="filtered-list-tax-filters">

      </div>
      <section
        class="filtered-list filtered-post-type-list"
        data-paged="1"
        data-post-type="<?php echo $p_type; ?>"
        data-post-date="<?php echo $years[0]; ?>" id="filtered-list-type-<?php echo $p_type; ?>">
        <div class="filtered-list-content">
          <div class="filtered-list-posts">
            <div class="filtered-list-header">
              <div class="filtered-list-filter">
                <p>Year:</p>
                <select class="post-filter-select filter-by-year">
                  <?php
                  foreach($years as $year) { ?>
                    <option class="post-filter" value="<?php echo $year; ?>"><?php echo $year; ?></option>
                  <?php
                  } ?>
                </select>
              </div>
            </div>
            <div class="filtered-list-container">
              <?php
              echo $posts_list->posts; ?>
            </div>
            <?php echo get_loader(false, true); ?>
            <div style="<?php echo $posts_list->maxpages ? '' : 'display:none;'; ?>" class="filtered-list-arrows">
              <a href="#" class="circle-arrow arrow-prev inactive" data-direction="-1" data-func="prev">
              </a>
              <a href="#" class="circle-arrow arrow-next" data-direction="1" data-max-pages="<?php echo $maxpages; ?>" data-func="next">
              </a>
            </div>
          </div>
        </div>
      </section>
    </div>
  <?php
  return ob_get_clean();
}

function create_list_by_term($props) {
  ob_start();
  $taxes = false;
  $type = $props['type'] ?: false;
  $tType = $props['tType'] ?: false;
  $pType = $props['pType'] ?: false;
  $filters = $props['filters'] ?: false;
  $paged = $props['paged'] ?: 1;
  $year = $props['year'] ?: false;
  if($tType && $type) {
    $taxes = ['taxonomy' => $type, 'field' => 'slug', 'terms' => $tType->slug];
  }
  $filterSlugs = [];
  if($filters && !empty($filters)) { // Add filters from URL
    if($taxes == false) {
      $taxes = [];
    }
    foreach($filters as $filt) {
      array_push($taxes, $filt);
      foreach($filt['terms'] as $filtTerm) {
        array_push($filterSlugs, $filtTerm);
      }
    }
  }
  $tTypeSlug = isset($tType->slug) ? $tType->slug : false;
  $tTypeName = isset($tType->name) ? $tType->name : false;
  $taxposts = get_filter_list_posts_by_type($taxes, $paged, 'date', $pType, false, (object)['tax' => $type, 'term' => $tTypeSlug]);
  if(!empty($taxposts->posts)) { ?>
    <section
      class="filtered-list filtered-tax-list tax-list-<?php echo $type; ?>"
      data-post-type="<?php echo $pType; ?>"
      data-paged="1"
      data-tax-type="<?php echo $type; ?>"
      data-tax-term="<?php echo isset($tType->slug) ? $tType->slug : ''; ?>"
      id="filtered-list-type-<?php echo isset($tType->slug) ? $tType->slug : ''; ?>">
      <div class="filtered-list-content">
        <?php
        // START: GET TAX FILTER IF EXISTS
        $associated_filter_posts = get_associated_tax_terms($type, $tTypeSlug, $pType, $year);
        $filter_array = $associated_filter_posts['filters'];
        $years_array = $associated_filter_posts['years'];
        if(count($filter_array) > 0 || count($years_array) > 0) { // If there are associated terms or years create the filters ?>
          <div class="filtered-list-by-taxes">
            <h4>Show me</h4>
            <a href="#" class="clear-filter post-filter">all</a>
            <div class="filtered-list-by-tax-wrapper">
              <?php
              if ( count($filter_array) > 0 ) {
                foreach($filter_array as $filter) { ?>
                  <div class="filtered-list-by-tax-container">
                    <h5><?php echo $filter->tax; ?></h5>
                    <<?php echo $pType == 'sec-filing' ? 'select' : 'div'; ?> class="filtered-list-by-tax-terms post-filter-select filter-by-<?php echo $filter->slug; ?>">
                      <?php
                      // Logic to put sec filing taxes into a select instead of in regular filter style
                      if ( $pType == 'sec-filing' ) { ?>
                        <option disabled selected value>Select SEC Filing type</option>
                      <?php
                      }
                      foreach($filter->terms as $key => $term) {
                        if ( $key == 10 && $pType != 'sec-filing') { ?>
                          <div class="more-filter-items-container">
                            <div class="more-filter-items">
                        <?php
                        }
                        if ( $pType != 'sec-filing') { // If SEC FILING create select options ?>
                          <a
                            class="filter-item-link filter-item<?php echo (isset($taxposts->availableterms) && !in_array($term->slug, $taxposts->availableterms)) ? ' inactive' : ''; echo in_array($term->slug, $filterSlugs) ? ' active': ''; ?>"
                            href="#"
                            data-term="<?php echo $term->slug; ?>"
                            data-tax="<?php echo $filter->slug; ?>">
                            <?php echo $term->name; ?>
                          </a>
                        <?php
                        } else { ?>
                          <option
                            class="filter-item-link"
                            <?php echo (isset($taxposts->availableterms) && !in_array($term->slug, $taxposts->availableterms)) ? ' diabled ' : ''; ?>
                            value="<?php echo $term->slug; ?>"
                            data-tax="<?php echo $filter->slug; ?>">
                            <?php echo $term->name; ?>
                          </option>
                        <?php
                        }
                        if ( count($filter->terms) > 10 && $key == count($filter->terms) -  1 && $pType != 'sec-filing') { ?>
                            </div>
                            <a href="#" class="show-more-filter-items"></a>
                          </div>
                        <?php
                        }
                      } ?>
                    </<?php echo $pType == 'sec-filing' ? 'select' : 'div'; ?>>
                  </div>
                <?php
                }
              }
              if ( count($years_array) > 0 ) { ?>
                <div class="filtered-list-by-tax-container">
                  <h5>Year</h5>
                  <select name="filter_by_year" id="" class="filter-by-year post-filter-select">
                    <option disabled selected value>Select</option>
                    <?php
                    foreach ( $years_array as $year ) { ?>
                      <option value="<?php echo $year; ?>"><?php echo $year; ?></option>
                    <?php
                    } ?>
                  </select>
                </div>
              <?php
              } ?>
            </div>
            <?php
            if ( $props['presentation'] ) {
              echo display_investor_presentation();
            } ?>
          </div>
        <?php
        }
        // END: GET TAX FILTER IF EXISTS ?>
        <div class="filtered-list-posts-wrapper">
          <div class="filtered-list-header">
            <?php
            $listTitle = $type == 'resource-type' ? 'Resources' : $tTypeName;
            $listTitle = $type == 'investor-material-type' ? 'Materials' : $tTypeName; ?>
            <h2><?php echo $listTitle; ?></h2>
            <div class="filtered-list-filter">
              <span>Sort by:</span>
              <a class="post-filter active" data-func="filter" data-filter="date" href="http://">Latest</a>
              <a class="post-filter" data-func="filter" data-filter="title" href="http://">Alphabet</a>
            </div>
          </div>
          <div class="filtered-list-posts">
            <div class="filtered-list-container">
              <?php
              echo $taxposts->posts; ?>
            </div>
          </div>
          <div style="<?php echo $taxposts->maxpages > 1 ? '' : 'display:none;'; ?>" class="filtered-list-arrows">
            <a href="#" class="circle-arrow arrow-prev inactive" data-direction="-1" data-func="prev">
            </a>
            <a href="#" class="circle-arrow arrow-next" data-direction="1" data-max-pages="<?php echo $taxposts->maxpages; ?>" data-func="next">
            </a>
          </div>
        </div>
      </div>
    </section>
  <?php
  }
  return ob_get_clean();
}

function get_associated_tax_terms($type, $tType = false, $pType, $years = false) {
  $associatedTaxes = get_object_taxonomies( $pType );
  $objectArgs = [
    'numberposts' => -1,
    'post_type' => $pType
  ];
  if($tType) {
    $objectArgs['tax_query'] = [
      [
        'taxonomy' => $type,
        'field' => 'slug',
        'terms' => [$tType],
        'operator' => 'IN'
      ]
    ];
  }
  $objects = get_posts($objectArgs);
  $objectIds = [];
  $years_array = [];
  foreach ($objects as $object) {
    if ( $years ) {
      $year = date('Y', strtotime($object->post_date));
      if ( !in_array($year, $years_array) ) {
        array_push($years_array, $year);
      }
    }
    array_push($objectIds, $object->ID);
  }
  $filter_array = [];
  foreach($associatedTaxes as $tax) { // Get all custom taxes associated with the post type
    if($tax == $type) { continue; } // if its not the current post type, get all terms that have posts associated with the current tax type
    if($taxFilters = wp_get_object_terms( $objectIds, $tax, ['orderby' => 'name', 'order' => 'ASC', 'parent' => 0] )) {
      $taxType = get_taxonomy($tax);
      array_push($filter_array, (object)[
        'tax' => $taxType->label,
        'slug' => $tax,
        'terms' => $taxFilters
      ]);
    }
  }
  return ['filters' => $filter_array, 'years' => $years_array];
}

function get_filter_list_by_year($paged, $year = '2019') {
  $news_obj = (object)[];
  $args = array(
    'post_type' => 'investor-news',
    'order_by' => 'date',
    'order' => 'DESC',
    'paged' => $paged,
    'date_query' => [
      [
        'year' => 2016
      ]
    ]
  );
  $investor_news_query = new WP_Query($args);
  if($investor_news_query->have_posts()) :
    while($investor_news_query->have_posts()): $investor_news_query->the_post(); ?>
      <div class="filtered-list-post new-post">
        <div class="filtered-post-date">
          <?php
          $date = get_the_date();
          $month = date("M", strtotime($date));
          $day = date("d", strtotime($date));
          $year = date("Y", strtotime($date));
          echo $year.'</br><span>'.$month.' '.$day.'</span>'; ?>
        </div>
        <?php
        echo get_filtered_post_title(get_the_id()); ?>
      </div>
    <?php
    endwhile;
    $news_obj->maxpages = $investor_news_query->max_num_pages;
    wp_reset_postdata();
  endif;
}

function get_filter_list_posts_by_type($tax_array, $paged, $filter = false, $post_type = false, $excluded = false, $originalTax = false, $date = false) {
  //return false;
  ob_start();
  $tax_obj = (object)[];
  $args = array(
    'post_type' => 'any',
    'posts_per_page' => 6,
    'paged' => $paged,
  );
  if(!empty($tax_array)) {
    $args['tax_query'] = [
      'relation' => 'AND',
      $tax_array
    ];
  }
  if($date != false) {
    $args['date_query'] = [['year' => $date]];
  }
  if($excluded != false) {
    $args['post__not_in'] = $excluded;
  }
  if($post_type != false) {
    $args['post_type'] = $post_type;
  }
  if($filter) {
    $args['orderby'] = $filter;
    $args['order'] = $filter == 'date' ? 'DESC' : 'ASC';
  }
  $tax_posts = new WP_Query($args);
  if($tax_posts->have_posts()) :
    while($tax_posts->have_posts()): $tax_posts->the_post();
      $post_type_obj = get_post_type_object(get_post_type()); ?>
      <div class="filtered-list-post new-post">
        <div class="filtered-post-date">
          <?php
          $date = get_the_date();
          $month = date("M", strtotime($date));
          $day = date("d", strtotime($date));
          $year = date("Y", strtotime($date));
          echo $year.'</br><span>'.$month.' '.$day.'</span>'; ?>
        </div>
        <?php
        if($form = get_the_terms(get_the_id(), 'sec-filing-type')) { ?>
          <div class="filtered-post-form<?php echo strlen($form[0]->name) > 7 ? ' auto-width' : ''; ?>">
            <div class="form-content">
              <p>FORM</p>
              <?php echo $form[0]->name; ?>
            </div>
          </div>
        <?php
        }
        echo get_filtered_post_title(get_the_id()); ?>
      </div>
    <?php
    endwhile;
    $tax_obj->maxpages = $tax_posts->max_num_pages;
    wp_reset_postdata();
  endif;
  if(!empty($tax_array) && $originalTax && $post_type) {
    // return a list of terms that no longer apply to filter
    $associatedTaxes = get_object_taxonomies( $post_type );
    $args['posts_per_page'] = -1;
    $objects = get_posts($args);
    $objectIds = [];
    foreach ($objects as $object) {
      array_push($objectIds, $object->ID);
    }
    $filter_array = [];
    foreach($associatedTaxes as $tax) { // Get all custom taxes associated with the post type
      if($tax == $originalTax->tax) { continue; } // if its not the current post type, get all terms that have posts associated with the current tax type
      if($taxFilters = wp_get_object_terms( $objectIds, $tax )) {
        foreach($taxFilters as $tFilter) {
          array_push($filter_array, $tFilter->slug);
        }
      }
    }
    if(count($filter_array) > 0) {
      $tax_obj->availableterms = $filter_array;
    }
  }
  $tax_obj->posts = ob_get_clean();
  return $tax_obj;
}

function display_history( $atts = false ) {
  global $post;
  $a = false;
  if($atts) {
    $a = $atts;
  }
  ob_start();
  if($history = get_field('history_year', $post->ID)) { ?>
    <div id="history-container">
      <div class="history-slider-arrows">
        <a href="#" class="circle-arrow history-slider-arrow arrow-prev">
        </a>
        <a href="#" class="circle-arrow history-slider-arrow arrow-next">
        </a>
      </div>
      <div id="history-year-images">
        <?php
        foreach($history as $key=>$year) {
          if($year['has_image']) { ?>
            <div class="history-year-image-container year-slide-<?php echo $key; ?>">
              <div class="flex">
                <div class="year-image-content">
                  <h5><?php echo $year['image_block']['year_image_title']; ?></h5>
                  <div class="year-image-text">
                    <?php print_r($year['image_block']['year_image_content']); ?>
                  </div>
                </div>
                <div class="history-year-image bg-centered ratio-3-1" style="background-image:url(<?php echo $year['image_block']['year_image']['sizes']['large']; ?>);">
                </div>
              </div>
            </div>
          <?php
          }
        } ?>
      </div>
      <div id="history-years">
        <?php
        foreach($history as $key=>$year) {
          $hasImage = $year['has_image'] ? true : false; ?>
          <div class="history-year history-year-<?php echo $key; ?>" data-has-image="<?php echo $hasImage; ?>">
            <div class="history-year-content">
              <h6><?php echo $year['title']; ?></h6>
              <div class="history-year-text">
                <?php echo $year['content']; ?>
              </div>
            </div>
            <span class="history-year-date"><h1><?php echo $year['year']; ?></h1></span>
          </div>
        <?php
        } ?>
        <div class="history-year placeholder-year">
        </div>
      </div>
    </div>
  <?php
  }
  return ob_get_clean();
}

function display_investor_presentation () {
  global $post;
  $parent_id = wp_get_post_parent_id(get_the_ID());
  if ( $modules = get_field('investor_modules', $parent_id) ) {
    ob_start();
    foreach( $modules as $module ) {
      if ( $module['acf_fc_layout'] == 'investor_presentation' ) { ?>
        <div id="investor-presentation">
          <?php
          if ( $title = $module['title'] ) { ?>
            <h5><?php echo $title; ?></h5>
          <?php
          }
          if($content = $module['content']) {
            echo $content;
          } ?>
        </div>
      <?php
      }
    }
    return ob_get_clean();
  }
}

function display_stock_performance( $atts = false ) {
  global $post;
  $a = false;
  if($atts) {
    $a = $atts;
  }
  ob_start();
  if($performance = get_investor_quotes('performance')) {
    $change = 'positive';
    if($performance->changePercent == 0) {
      $change = 'neutral';
    }
    if($performance->changePercent < 0) {
      $change = 'negative';
    }
    $performance_time_stamp = $performance->date;
    $performance_time_stamp = date_create($performance_time_stamp); ?>
    <h2><?php echo $performance->symbol; ?> (Common Stock)</h2>
    <div class="stock-performance-header">
      <p>Data as of <?php echo date_format($performance_time_stamp,"Y/m/d H:iA"); ?>ET. *Minimum 15 minute delay</p>
      <a href="<?php echo get_permalink($post->ID); ?>">Refresh Quote</a>
    </div>
    <table class="stock-performance">
      <tr><td>Exchange</td><td><p><?php echo $performance->exchange; ?> (US Dollar)</p></td></tr>
      <tr><td>Price</td><td><p><strong>$<?php echo number_format((float)$performance->lastTrade, 2, '.', ''); ?></strong></p></td></tr>
      <tr><td>Change (%)</td><td><p class="<?php echo $change; ?>"><span class="stock-change <?php echo $change; ?>"></span><?php echo $performance->changeNumber; ?> (<?php echo $performance->changePercent; ?>%)</p></td></tr>
      <tr><td>Volume</td><td><p><?php echo number_format($performance->volume); ?></p></td></tr>
      <tr><td>Today's Open</td><td><p>$<?php echo number_format((float)$performance->open, 2, '.', ''); ?></p></td></tr>
      <tr><td>Previous Close</td><td><p>$<?php echo number_format((float)$performance->previousClose, 2, '.', ''); ?></p></td></tr>
      <!-- <tr><td>Intraday High</td><td><p>$<?php echo number_format((float)$performance->dayHigh, 2, '.', ''); ?></p></td></tr>
      <tr><td>Intraday Low</td><td><p>$<?php echo number_format((float)$performance->dayLow, 2, '.', ''); ?></p></td></tr>
      <tr><td>52 Week High</td><td><p>$<?php echo number_format((float)$performance->{'52WeekHigh'}, 2, '.', ''); ?></p></td></tr>
      <tr><td>52 Week Low</td><td><p>$<?php echo number_format((float)$performance->{'52WeekLow'}, 2, '.', ''); ?></p></td></tr> -->
    </table>
    <!-- TradingView Widget BEGIN -->
    <div class="tradingview-widget-container">
      <div id="tradingview_a4df6"></div>
      <div class="tradingview-widget-copyright"><a href="https://www.tradingview.com/symbols/NYSE- IO/" rel="noopener" target="_blank"><span class="blue-text"> IO Chart</span></a> by TradingView</div>
      <script type="text/javascript" src="https://s3.tradingview.com/tv.js"></script>
      <script type="text/javascript">
        new TradingView.widget(
          {
            "width": 'auto',
            "height": 610,
            "symbol": "NYSE:IO",
            "interval": "D",
            "timezone": "Etc/UTC",
            "theme": "Light",
            "style": "1",
            "locale": "en",
            "toolbar_bg": "#f1f3f6",
            "enable_publishing": false,
            "save_image": false,
            "container_id": "tradingview_a4df6"
          }
        );
      </script>
    </div>
    <!-- TradingView Widget END -->
  <?php
  } else {
    echo 'There was a problem accessing stock performance. Please check again later';
  }
  return ob_get_clean();
}

add_shortcode( 'history', 'display_history' );

add_shortcode( 'stock-performance', 'display_stock_performance' );
