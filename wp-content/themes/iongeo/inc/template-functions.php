<?php
/**
 * Functions which enhance the theme by hooking into WordPress
 *
 * @package Iongeo
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function iongeo_body_classes( $classes ) {
	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	// Adds a class of no-sidebar when there is no sidebar present.
	if ( ! is_active_sidebar( 'sidebar-1' ) ) {
		$classes[] = 'no-sidebar';
	}

	$classes[] = 'no-js';

	return $classes;
}
add_filter( 'body_class', 'iongeo_body_classes' );

/**
 * Add a pingback url auto-discovery header for single posts, pages, or attachments.
 */
function iongeo_pingback_header() {
	if ( is_singular() && pings_open() ) {
		echo '<link rel="pingback" href="', esc_url( get_bloginfo( 'pingback_url' ) ), '">';
	}
}
add_action( 'wp_head', 'iongeo_pingback_header' );

function ion_page_header($args) {
	// $args = [
	// 	'fimg' => isset($header_args['fimg']) ? $header_args || false,
	// 	'excerpt' => $header_args['excerpt'] || false,
	// 	'title' => $header_args['title'] || false,
	// 	'frompage' => $header_args['frompage'] || false
	// ];
	global $post;
	// $fImg = false, $excerpt = false, $title = false, $frompage = false
	ob_start();
	$headerObj = (object)['image' => false];
	$postID = isset($post->ID) ? $post->ID : null;
	if(is_home()) { // Get post id of posts page
		$postID = get_option('page_for_posts');
	}
	if( !isset($args['fimg']) && has_post_thumbnail($postID) && get_post_type($postID) != 'post' ) {
		$args['fimg'] = ['large' => get_the_post_thumbnail_url($postID), 'small' => get_the_post_thumbnail_url($postID, 'small')];
	}
	$headerStyle = 'standard';
	$headerImg = false;
	if(isset($args['fimg']) && $args['fimg'] != '') {
		$headerStyle = 'full-width-image bg-centered';
		$headerImg = true;
		$headerObj->image = true;
	}
	if(!isset($args['title']) && get_the_title($postID)) {
		$args['title'] = get_the_title($postID);
	}
	if(!isset($args['excerpt']) && has_excerpt($postID)) {
		$args['excerpt'] = get_the_excerpt($postID);
	} ?>
	<header id="page-header" class="<?php echo $headerStyle; ?>"<?php echo isset($args['fimg']) ? ' style="background-image:url('.$args['fimg']['large'].');"' : ''; ?>>
		<?php
		if($headerImg) { ?>
			<div class="temp-header-image bg-centered" style="background-image:url(<?php echo $args['fimg']['small']; ?>);">
			</div>
			<div class="header-image-cover"></div>
		<?php
		} ?>
		<div id="page-header-title" class="content-inner">
			<div class="page-header-title-content">
				<h1><?php echo $args['title']; ?></h1>
				<?php
				if(get_post_type($postID) == 'data-library' && get_field('post_type', $postID) == 'data-program' ) { ?>
					<div class="on-page-search-container" data-posttype="data-library">
						<div class="on-page-search-icon">
						</div>
						<input class="on-page-search" placeholder="Search Locations"/>
						<div class="on-page-search-results-container">
						</div>
					</div>
				<?php
				} ?>
			</div>
			<?php
			if(isset($args['excerpt'])) { ?>
				<div class="header-description">
					<?php echo $args['excerpt']; ?>
				</div>
			<?php
			}
			if($authors = get_the_terms($postID, 'resource-author')) {
				$authorArray = [];
				foreach($authors as $author) {
					array_push($authorArray, $author->name);
				}
				$authorString = implode(',', $authorArray); ?>
				<div class="post-authors">
					<?php echo $authorString; ?>
				</div>
			<?php
			} ?>
		</div>
		<div class="page-header-content">
		</div>
	</header>
<?php
	$headerObj->header = ob_get_clean();
	return $headerObj;
}
function ion_custom_sidebar() {

  register_sidebar(
    array (
      'name' => __( 'Location', 'iongeo' ),
      'id' => 'location-sidebar',
      'description' => __( 'Sidebar for location single pages', 'iongeo' ),
      'before_widget' => '<div class="widget-content">',
      'after_widget' => "</div>",
      'before_title' => '<h3 class="widget-title">',
      'after_title' => '</h3>',
    )
  );
	register_sidebar(
    array (
      'name' => __( 'Investor News', 'iongeo' ),
      'id' => 'investor-news-sidebar',
      'description' => __( 'Sidebar for investor news single pages', 'iongeo' ),
      'before_widget' => '<div class="widget-content">',
      'after_widget' => "</div>",
      'before_title' => '<h3 class="widget-title">',
      'after_title' => '</h3>',
    )
  );
	register_sidebar(
    array (
      'name' => __( 'Data Type', 'iongeo' ),
      'id' => 'data-type-sidebar',
      'description' => __( 'Sidebar for data type pages', 'iongeo' ),
      'before_widget' => '<div class="widget-content">',
      'after_widget' => "</div>",
      'before_title' => '<h3 class="widget-title">',
      'after_title' => '</h3>',
    )
  );
	register_sidebar(
    array (
      'name' => __( 'Data Library', 'iongeo' ),
      'id' => 'data-library-sidebar',
      'description' => __( 'Sidebar for data library pages', 'iongeo' ),
      'before_widget' => '<div class="widget-content">',
      'after_widget' => "</div>",
      'before_title' => '<h3 class="widget-title">',
      'after_title' => '</h3>',
    )
  );
	register_sidebar(
    array (
      'name' => __( 'Technology Type', 'iongeo' ),
      'id' => 'technology-type-sidebar',
      'description' => __( 'Sidebar for technology type pages', 'iongeo' ),
      'before_widget' => '<div class="widget-content">',
      'after_widget' => "</div>",
      'before_title' => '<h3 class="widget-title">',
      'after_title' => '</h3>',
    )
  );
	register_sidebar(
    array (
      'name' => __( 'Technology', 'iongeo' ),
      'id' => 'technology-sidebar',
      'description' => __( 'Sidebar for technology single pages', 'iongeo' ),
      'before_widget' => '<div class="widget-content">',
      'after_widget' => "</div>",
      'before_title' => '<h3 class="widget-title">',
      'after_title' => '</h3>',
    )
  );
	register_sidebar(
    array (
      'name' => __( 'Insights', 'iongeo' ),
      'id' => 'insights-sidebar',
      'description' => __( 'Sidebar for insight single pages', 'iongeo' ),
      'before_widget' => '<div class="widget-content">',
      'after_widget' => "</div>",
      'before_title' => '<h3 class="widget-title">',
      'after_title' => '</h3>',
    )
  );
	register_sidebar(
    array (
      'name' => __( 'Offerings', 'iongeo' ),
      'id' => 'offerings-sidebar',
      'description' => __( 'Sidebar for offering single pages', 'iongeo' ),
      'before_widget' => '<div class="widget-content">',
      'after_widget' => "</div>",
      'before_title' => '<h3 class="widget-title">',
      'after_title' => '</h3>',
    )
  );
	register_sidebar(
    array (
      'name' => __( 'Offering Types', 'iongeo' ),
      'id' => 'offering-type-sidebar',
      'description' => __( 'Sidebar for offering type pages', 'iongeo' ),
      'before_widget' => '<div class="widget-content">',
      'after_widget' => "</div>",
      'before_title' => '<h3 class="widget-title">',
      'after_title' => '</h3>',
    )
  );
	register_sidebar(
    array (
      'name' => __( 'Events', 'iongeo' ),
      'id' => 'event-sidebar',
      'description' => __( 'Sidebar for events pages', 'iongeo' ),
      'before_widget' => '<div class="widget-content">',
      'after_widget' => "</div>",
      'before_title' => '<h3 class="widget-title">',
      'after_title' => '</h3>',
    )
  );
}
add_action( 'widgets_init', 'ion_custom_sidebar' );

function save_post_switch($post_id){
  $post = get_post( $post_id );
  if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE || $post->post_status == 'auto-draft' )
    return;
  $post_type = get_post_type( $post_id );
  switch($post_type){
		case 'post':
    // $pdfID = get_field('upload_pdf', $post_id);
		// $pdfAttachment = wp_get_attachment_metadata( $pdfID );
		// $pdfURL = wp_get_attachment_url( $pdfID );
		//
		// $pdf = pdf_new();
		//
		// PDF_begin_document($pdf, wp_upload_dir().$pdfURL, []);
		//
		// // set PDF properties
		// pdf_set_info($pdf, "Author", "Martín Gonzalez");
		// pdf_set_info($pdf, "Title", "Test PDF");
		// pdf_set_info($pdf, "Creator", "Martín Gonzalez");
		// pdf_set_info($pdf, "Subject", "Test  PDF");
		// PDF_end_document($pdf, []);
		// $pdfdata = pdf_get_buffer($pdf);
		// file_put_contents(wp_upload_dir().$pdfURL, $pdfdata);
		// $attachment = array(
		// 	'guid'           => $pdfAttachment['file'],
		// 	'post_mime_type' => get_post_mime_type( $pdfID ),
		// 	'post_title'     => preg_replace( '/\.[^.]+$/', '', basename( wp_upload_dir().$pdfURL ) ),
		// 	'post_content'   => sys_get_temp_dir(),
		// 	'post_status'    => 'inherit'
		// );


		//wp_insert_attachment( $attachment, wp_upload_dir().$pdfURL, $post_id );
		break;
		case 'resource':
		break;
		default:
		break;
  }
}
//add_action( 'save_post', 'save_post_switch');

function populate_data_tour_form ($form) {
	// print_r($form);
	global $post;
	if ( empty($post) ) {
		return $form;
	}
	if ( have_rows('tours', $post->ID) ):
		while ( have_rows('tours') ) : the_row();
			print 'has a row';
				$tour_obj = get_sub_field_object('tour_datetimes');
				$tour_id = $tour_obj['name'];
				$tour_name = get_sub_field('data_tour_name');
				$choices = [];
				if ( $tour_obj['value'] ) {
					foreach( $tour_obj['value'] as $datetime ) {
						$choices[] = array( 'text' => $datetime['datetime'], 'value' => $datetime['datetime'] );
					}
					$has_field = false;
					foreach( $form['fields'] as &$field ) {
						if ( $field['id'] == $tour_id ) {
							$field->choices = $choices;
							$has_field = true;
						}
					}
					if ( !$has_field ) {
						$props = array(
					    'id' => $tour_id,
					    'label' => $tour_name,
					    'type' => 'radio',
							'choices' => $choices
						);
						$field = GF_Fields::create( $props );
						array_push( $form['fields'], $field );
					}
				}
		endwhile;
	endif;

	return $form;
}

//NOTE: update the '221' to the ID of your form
// add_filter( 'gform_pre_render_1', 'populate_data_tour_form' );
// add_filter( 'gform_pre_validation_1', 'populate_data_tour_form' );
// add_filter( 'gform_pre_submission_filter_1', 'populate_data_tour_form' );
// add_filter( 'gform_admin_pre_render_1', 'populate_data_tour_form' );

add_filter( 'caldera_forms_summary_magic_fields', function( $fields, $form ) {
	//change your field ID
	log_it($fields);

	return $fields;

}, 10, 2 );

function update_technology_type_with_resources($value, $post_id, $field) {
	if($field['name'] == 'resource_x_technology-type') {
		$term = get_term(9, 'technology-type');
		$field = get_field_object('resource_x_technology-type');
		update_field($field['key'], [328], 'technology-type_9');

		// $rfield = get_field_object('resources_x_technology_types');
		// $origArr = get_post_meta($post_id, $rfield['key'], true);
		// foreach($origArr as $origVal) { // Find deleted terms
		// 	if(!in_array($origVal, $value)) { // Resource was deleted
		// 		$term = get_term($origVal, 'technology-type');
		// 		$field = get_field_object('resources_x_taxonomies');
		// 		$termRecs = get_term_meta($term->term_id, $field['key'], true);
		// 		if (($key = array_search($post_id, $termRecs)) !== false) {
		// 		   unset($termRecs[$key]); // remove deleted resource
		// 		}
		// 		update_field($field['key'], $termRecs, 'technology-type_'.$term->term_id);
		// 	}
		// }
		// foreach($value as $val) { // Find added terms
		// 	if(!in_array($val, $origArr)) {
		// 		$term = get_term($val, 'technology-type');
		// 		$field = get_field_object('resources_x_taxonomies');
		// 		$termRecs = get_term_meta($term->term_id, $field['key'], true);
		// 		array_push($termRecs, $post_id); // Add new resource
		// 		update_field($field['key'], $termRecs, 'technology-type_'.$term->term_id);
		// 	}
		// }
	}
	return $value;
}

add_filter( 'acf/update_value', 'update_technology_type_with_resources', 10, 3);

function content_tabs( $atts = false ) {
  if($atts) {
		$a = shortcode_atts( array(), $atts );
	}
	global $post;
  ob_start();
		if($tabs = get_field('content_tabs', $post->ID)) { ?>
	    <div class="content-tabs">
				<div class="content-tabs-nav">
					<?php
					foreach($tabs as $key=>$tab) { ?>
						<a href="#" class="content-tab-button<?php echo $key == 0 ? ' active' : ''; ?>" data-slug="<?php echo slugify($tab['title']); ?>">
							<?php echo $tab['title']; ?>
						</a>
					<?php
					} ?>
				</div>
				<?php
				foreach($tabs as $key=>$tab) { ?>
					<div data-slug="<?php echo slugify($tab['title']); ?>" class="content-tab<?php echo $key == 0 ? ' active' : ''; ?>">
						<?php
						if ($tab['content']) {
							echo $tab['content'];
						}
						if( $tab['resources']) { ?>
							<div class="filtered-list-wrapper flex">
								<div class="filtered-list filtered-post-type-list">
									<div class="filtered-list-content">
										<div class="filtered-list-posts">
											<div class="filtered-list-container">
												<?php
												foreach($tab['resources'] as $resource) { ?>
													<div class="filtered-list-post">
														<?php echo get_filtered_post_title($resource); ?>
													</div>
												<?php
												} ?>
											</div>
										</div>
									</div>
								</div>
							</div>
						<?php
						}
						?>
					</div>
				<?php
				}	?>
	    </div>
		<?php
		}
  $content = ob_get_clean();
  return $content;
}
add_shortcode( 'content-tabs', 'content_tabs' );

function get_loader($active = false, $fullheight = false) { ?>
	<div class="ion-loader-container<?php echo $active ? ' active' : ''; echo $fullheight ? ' fullheight' : ''; ?>">
		<div class="ion-loader">
			<div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div>
		</div>
	</div>
<?php
}

function side_scroll_nav($id) { ?>
	<div class="side-scroll-nav" scroll-id="<?php echo $id; ?>">
		<div class="side-scroll-nav-content">
			<div class="nav-arrows">
				<div class="nav-arrow nav-arrow-up inactive">
				</div>
				<div class="nav-arrow nav-arrow-down">
				</div>
			</div>
		</div>
	</div>
<?php
}

function get_resource_link($fileurl) {
	$filetype = explode('.', $fileurl['url']);
	$filetype = $filetype[count($filetype) - 1];
	// $post_title = str_replace('–', '-', get_the_title());
	$wptexturize = remove_filter( 'the_title', 'wptexturize' );
	$post_title = get_the_title();

	if ( $wptexturize ) {
    add_filter( 'the_title', 'wptexturize' );
	}
	$post_title = explode(' - ', $post_title);
	$post_title = $post_title[0];
	ob_start(); ?>
		<div class="download-link">
			<a class="download-link-title test-class" target="_blank" href="<?php echo $fileurl['url']; ?>">
				<?php echo $post_title; ?>
			</a>
			<a class="download-link-type" target="_blank" href="<?php echo $fileurl['url']; ?>">
				<?php echo $filetype; ?>
				<span>↓</span>
			</a>
		</div>
	<?php
	return ob_get_clean();
}

function remove_revisionize_for_admins()
{
    global $current_user;
    if (in_array('administrator', $current_user->roles)) {
        deactivate_plugins( // deactivate for media_manager
            array(
                '/revisionize/revisionize.php'
            ),
            true, // silent mode (no deactivation hooks fired)
            false // network wide
        );
    } else { // activate for those than can use it
        activate_plugins(
            array(
                '/revisionize/revisionize.php'
            ),
            '', // redirect url, does not matter (default is '')
            false, // network wise
            true // silent mode (no activation hooks fired)
        );
    }
}

//add_action('admin_init', 'remove_revisionize_for_admins');

function by_the_numbers($term) {
	ob_start();
	if($bythenumbers = get_field('by_the_numbers', $term)) { ?>
		<div id="by_the_numbers_container" class="flex">
			<div id="by_the_numbers">
				<?php foreach($bythenumbers as $btn) { ?>
					<div class="by_the_number">
						<?php
						if(isset($btn['number'])) { ?>
							<div class="by_the_number_number">
								<?php echo $btn['number']; ?>
							</div>
						<?php
						}
						if(isset($btn['description'])) { ?>
							<div class="by_the_number_desc">
								<?php echo $btn['description']; ?>
							</div>
						<?php
						} ?>
					</div>
				<?php
				} ?>
			</div>
		</div>
	<?php
	}
	return ob_get_clean();
}

if(!function_exists('log_it')){
 function log_it( $message ) {
   if( WP_DEBUG === true ){
     if( is_array( $message ) || is_object( $message ) ){
       error_log( print_r( $message, true ) );
     } else {
       error_log( $message );
     }
   }
 }
}
