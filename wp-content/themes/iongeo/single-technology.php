<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Iongeo
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

			<?php
			while ( have_posts() ) :
				the_post();
				$sidebar_image = get_field('sidebar_image') ? get_field('sidebar_image') : false;
				$header = ion_page_header(['frompage' => 'tech single']);
				$content = get_the_content();
				echo $header->header;
				echo get_share_link('ION Geo | '.get_the_title(), get_the_permalink()); ?>
				<section class="page-content-wrapper content-inner<?php echo is_active_sidebar( 'technology-sidebar' ) ? ' has-sidebar' : ''; ?>">
					<div class="page-content-container reverse-container <?php echo $header->image ? 'has-header-image ' : ''; ?>flex">
						<?php
						if ( is_active_sidebar( 'technology-sidebar' ) ) :
							custom_sidebar( 'technology-sidebar', $sidebar_image['sizes']['medium'] );
						endif; ?>
						<div class="page-content col-8">
							<?php
							the_content();
							if ( !has_shortcode( $content, 'content-tabs' ) ) {
								echo content_tabs();
							} ?>
						</div>
					</div>
				</section>
				<?php
				if( $techDescs = get_field('technology_descriptors') ) { ?>
					<div id="info-graphic-list-container" class="has-side-scroll">
						<div class="content-inner">
							<?php side_scroll_nav('info-graphic-list-container'); ?>
							<ol id="info-graphic-list">
								<?php
								foreach($techDescs as $techDesc) { ?>
									<li class="info-graphic-list-item side-scroll-item">
										<div class="info-graphic-list-item-container">
											<?php
											if($header = $techDesc['header']) { ?>
												<h2><?php echo $header; ?></h2>
											<?php
											} ?>
											<div class="info-graphic-content">
												<div class="flex row">
													<?php
													if($image = $techDesc['image']) { ?>
														<div class="info-graphic-image-container col-4">
															<div class="info-graphic-image bg-centered" style="background-image:url(<?php echo $image['sizes']['small-medium']; ?>);">
															</div>
														</div>
														<div class="info-graphic-divider col-2"></div>
													<?php
													}
													if($content = $techDesc['content']) { ?>
														<div class="info-graphic-text col-6">
															<?php echo $content; ?>
														</div>
													<?php
													} ?>
												</div>
											</div>
										</div>
									</li>
								<?php
								} ?>
							</ol>
						</div>
					</div>
				<?php
				}
				if($insights = get_field('post_x_technology')) {
					echo get_related_insights($insights);
				}
				if($offerings = get_field('offering_x_technology')) {
					$offArr = [];
					foreach($offerings as $offering) {
						array_push($offArr, $offering);
					}
					$oArgs = [
						'post_type' => 'offering',
						'posts_per_page' => 8,
						'post__in' => $offArr,
					];
					echo get_related_offerings($oArgs);
				}
			// $terms = get_terms(['taxonomy' => 'software']);
			// echo get_term_link($terms[0], 'software');

			endwhile; // End of the loop.
			?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
