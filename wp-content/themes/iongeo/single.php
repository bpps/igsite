<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Iongeo
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

		<?php
		while ( have_posts() ) : the_post();
			$content = get_the_content();
			$headerImage = false;
			$sidebar_image = get_field('sidebar_image') ? get_field('sidebar_image') : false;
			if(get_post_type() == 'investor-news') {
				if($investorHeader = get_field('global_investors_header_image', 'option')) {
					$headerImage = ['large' => $investorHeader['sizes']['large'], 'small' => $investorHeader['sizes']['small']];
				}
			}
			$header = ion_page_header([ 'fimg' => $headerImage, 'frompage' => 'single']);
			echo $header->header;
			if(has_post_thumbnail()) { ?>
				<div class="full-width-page">
					<div class="content-inner">
						<div class="ratio-3-1 bg-centered" style="background-image:url(<?php echo get_the_post_thumbnail_url(get_the_ID(), 'large'); ?>);">
						</div>
					</div>
				</div>
			<?php
			} ?>
			<?php echo get_share_link(get_the_title(), get_the_permalink()); ?>
			<section class="page-content-wrapper content-inner<?php echo is_active_sidebar( 'insights-sidebar' ) ? ' has-sidebar' : ''; ?>">
				<div class="page-content-container reverse-container flex">
					<?php
					custom_sidebar('insights-sidebar', $sidebar_image['sizes']['medium']); ?>
					<div class="page-content col-8">
						<?php
						echo apply_filters('the_content', $content);
						if ( !has_shortcode( $content, 'content-tabs' ) ) {
							echo content_tabs();
						}
						if($techs = get_field('post_x_technology')) { ?>
							<div id="related-technologies" class="post-row">
								<h4>Related Technologies</h4>
								<div class="post-row-content column-count-2 flex row">
									<?php
									foreach($techs as $tech) { ?>
										<div class="post-row-item col-6">
											<?php
											$image = has_post_thumbnail($tech) ? get_the_post_thumbnail_url($tech, 'small-medium') : get_template_directory_uri().'/images/post-placeholder.png'; ?>
											<div class="post-row-image-container bg-centered ratio-image-container ratio-3-2">
				                <a href="<?php echo get_the_permalink($tech); ?>">
				                  <img src="<?php echo $image; ?>"/>
				                </a>
				              </div>
					            <h6 class="post-row-title with-arrow">
					              <a href="<?php echo get_the_permalink($tech); ?>">
					                <?php echo get_the_title($tech); ?>
					              </a>
					            </h6>
										</div>
									<?php
									} ?>
							</div>
						<?php
						} ?>
					</div>
				</div>
			</section>
			<?php
			echo get_footer_cta('default');
		endwhile; // End of the loop.
		?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
