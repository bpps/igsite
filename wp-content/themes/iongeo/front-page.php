<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Iongeo
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

		<?php
		if ( have_posts() ) :
			/* Start the Loop */
			while ( have_posts() ) :
				the_post();
				if($slides = get_field('header_slides', get_option('page_on_front'))) { ?>
					<header id="page-header">
						<div id="page-header-slides-wrapper">
							<div class="page-header-slides-wrapper-wrapper">
								<div id="slide-scrubber-wrapper">
									<div id="slide-scrubber" class="content-inner">
										<?php
										foreach($slides as $slide) {
											$slidePost = $slide['slide_post'];
											$slideTitle = '';
											if($linkTitle = $slide['slider_link_title']) {
											  $slideTitle = $linkTitle;
											}
											else if($altTitle = $slide['slide_alt_title']) {
												$slideTitle = $altTitle;
											} else if($slidePost){
												$slideTitle = $slidePost->post_title;
											}
											?>
											<a class="slide-scrub-title">
												<?php echo $slideTitle; ?>
											</a>
										<?php
										} ?>
									</div>
								</div>
								<div id="page-header-slides" data-autoplay="<?php echo get_field('auto_rotate_slides', get_option('page_on_front')); ?>" class="content-inner">
									<?php
									foreach($slides as $slide) {
										$slideObj = (object)[];
										$slidePost = $slide['slide_post'];
										if($slidePost) {
											$slideObj->ID = $slidePost->ID;
										}
										$slideObj->title = ($slide['slide_alt_title']) ? $slide['slide_alt_title'] : $slidePost->post_title;
										// Slide Excerpt (Alt or post)
										if($altExcerpt = $slide['slide_alt_excerpt']) {
											$slideObj->excerpt = $altExcerpt;
										} else if($slidePost && has_excerpt($slideObj->ID)) {
											$slideObj->excerpt = get_the_excerpt($slideObj->ID);
										}
										// Slide Link (Alt or post)
										$slideObj->link = ($slide['slide_alt_link']['url']) ? $slide['slide_alt_link']['url'] : get_the_permalink($slideObj->ID);
										// Slide CTA
										$slideObj->cta = ($slide['slide_alt_link']['call_to_action']) ? $slide['slide_alt_link']['call_to_action'] : 'Keep Reading';
										// Slide Image (Alt or post)
										$slideObj->image = ($slide['slide_alt_image']['sizes']['medium']) ? $slide['slide_alt_image']['sizes']['medium'] : get_the_post_thumbnail_url($slideObj->ID, 'medium'); ?>
										<div class="header-slide-container">
											<div class="header-slide">
												<div class="header-slide-content">
													<div class="header-slide-content-wrapper">
														<div class="header-slide-spacer">
														</div>
														<?php
														if($slideObj->title) { ?>
															<h2 class="header-slide-title"><?php echo $slideObj->title; ?></h2>
														<?php
														}
														if($slideObj->excerpt) { ?>
															<div class="header-slide-excerpt">
																<?php echo $slideObj->excerpt; ?>
															</div>
														<?php
														}
														if($slideObj->link) { ?>
															<div class="ion-cta-container">
																<a class="ion-cta" href="<?php echo $slideObj->link; ?>">
																	<?php echo $slideObj->cta; ?>
																</a>
															</div>
														<?php
														} ?>
													</div>
												</div>
												<?php if($slideObj->image) { ?>
													<div class="header-slide-image">
														<div style="background-image:url(<?php echo $slideObj->image; ?>);" class="bg-centered ratio-3-2" style="">
														</div>
													</div>
												<?php } ?>
											</div>
										</div>
									<?php
									} ?>
								</div>
								<?php
								// Display arrows if more than one post
								if(count($slides) > 1) { ?>
									<div class="content-inner">
										<div class="page-header-arrows">
					            <a href="#" class="circle-arrow arrow-prev" data-func="prev">
					            </a>
					            <a href="#" class="circle-arrow arrow-next" data-func="next">
					            </a>
					          </div>
									</div>

								<?php
								} ?>
						</div>
						</div>
						<div class="notch-wrapper">
							<div class="content-inner">
								<div class="flex">
									<div class="notch-wrapper-notch">
									</div>
								</div>
							</div>
						</div>
					</header>
				<?php
				} ?>
				<div id="ion-modules-wrapper" class="content-inner">
					<div id="ion-modules" class="flex">
						<?php
						$modules = get_field('modules_column_1', get_option('page_on_front'));
						if($modules) { ?>
							<div class="ion-modules-section col-5" id="ion-modules-1">
								<?php
								foreach($modules as $module) { ?>
									<div class="ion-module">
										<?php
										switch($module['acf_fc_layout']) {
											case 'post_preview':
												$p_type = $module['post_type'];
												$p_type_obj = get_post_type_object($p_type);
												$p_type_link = get_post_type_archive_link( $p_type );
												$p_type_label = get_post_type_labels($p_type_obj);
												$modTitle = isset($module['title']) ? $module['title'] : $p_type_label->name; ?>
												<header class="module-header">
													<h3><?php echo $modTitle; ?></h3>
													<a class="view-all" href="<?php echo $p_type_link; ?>">See All</a>
												</header>
												<?php
												$args = array(
											    'post_type' => $p_type,
											    'posts_per_page' => 2,
											  );
											  $postslides = new WP_Query($args);
											  if($postslides->have_posts()) : ?>
													<div class="post-previews-wrapper">
														<div class="post-previews">
												      <?php
												      while($postslides->have_posts()): $postslides->the_post();
																$image = get_the_post_thumbnail_url(get_the_id(), 'medium')
																	? get_the_post_thumbnail_url(get_the_id(), 'medium')
																	: get_template_directory_uri().'/images/post-placeholder.png';
																$link = get_field('upload_pdf') ? get_field('upload_pdf')['url'] : get_the_permalink(); ?>
																<div class="post-preview">
																	<div class="post-preview-image bg-centered">
																		<a href="<?php echo $link; ?>">
																			<img src="<?php echo $image; ?>"/>
																		</a>
																	</div>
																	<div class="post-preview-content with-arrow">
																		<a href="<?php echo $link; ?>">
																			<h4><?php the_title(); ?></h4>
																			<?php
																			if(has_excerpt()) { ?>
																				<div class="post-preview-excerpt">
																					<?php echo character_limit(get_the_excerpt(), 65); ?>
																				</div>
																			<?php
																			} ?>
																		</a>
																	</div>
																</div>
															<?php
															endwhile;
															wp_reset_postdata(); ?>
														</div>
													</div>
												<?php
												endif;
											break;
											case 'events_modules':
											break;
											default:
											break;
										} ?>
									</div>
								<?php
								} ?>
							</div>
							<?php
						}
						$modules = get_field('modules_column_2', get_option('page_on_front'));
						if($modules) { ?>
							<div class="ion-modules-section col-7" id="ion-modules-2">
								<?php
								foreach($modules as $module) { ?>
									<div class="ion-module">
										<?php
										switch($module['acf_fc_layout']) {
											case 'post_slides':
												$p_type = $module['post_type'];
												$p_type_obj = get_post_type_object($p_type);
												$p_type_link = get_post_type_archive_link( $p_type );
												$p_type_label = get_post_type_labels($p_type_obj); ?>
												<header class="module-header">
													<h3><?php echo $p_type_label->name; ?></h3>
													<a class="view-all" href="<?php echo $p_type_link; ?>">See All <!--<?php echo $p_type_label->name; ?>--></a>
												</header>
												<?php
												$args = array(
											    'post_type' => $p_type,
											    'posts_per_page' => 6,
											  );
											  $postslides = new WP_Query($args);
											  if($postslides->have_posts()) : ?>
													<div class="post-slideshow-wrapper">
														<div class="post-slideshow">
												      <?php
												      while($postslides->have_posts()): $postslides->the_post();
																$image = get_the_post_thumbnail_url(get_the_id(), 'medium')
																	? get_the_post_thumbnail_url(get_the_id(), 'medium')
																	: get_template_directory_uri().'/images/post-placeholder.png';
																	$link = get_field('upload_pdf') ? get_field('upload_pdf')['url'] : get_the_permalink(); ?>
																<div class="post-slide">
																	<div class="post-slide-image bg-centered">
																		<a href="<?php echo $link; ?>">
																			<img src="<?php echo $image; ?>"/>
																		</a>
																	</div>
																	<h4><a href="<?php echo $link; ?>"><?php the_title(); ?></a></h4>
																</div>
															<?php
															endwhile;
															wp_reset_postdata(); ?>
														</div>
														<div class="post-slideshow-arrows">
															<div class="post-slideshow-arrow arrow-next">
																<div class="circle-arrow">
																</div>
															</div>
															<div class="post-slideshow-arrow arrow-prev">
																<div class="circle-arrow">
																</div>
															</div>
														</div>
													</div>
												<?php
												endif;
											break;
											case 'post_list':
												$p_type = $module['post_type'];
												$post_type = get_post_type_object($p_type);
												$p_type_obj = get_post_type_object($p_type);
												$p_type_link = $p_type == 'investor-news' ? 'https://ir.iongeo.com/investor-news' : get_post_type_archive_link( $p_type );
												$p_type_label = get_post_type_labels($p_type_obj); ?>
												<header class="module-header">
													<h3><?php echo $p_type_label->name; ?></h3>
													<a class="view-all" href="<?php echo $p_type_link; ?>">See All <!--<?php echo $p_type_label->name; ?>--></a>
												</header>
												<?php
												if ( $p_type == 'investor-news' ) {
													$ir_news_posts = get_recent_investor_news();
													if ( count($ir_news_posts->data) > 0 ) { ?>
														<div class="ion-module">
					                    <div class="filtered-list">
																<div class="filtered-list-container">
																	<?php
																	foreach( $ir_news_posts->data as $news_post ) { ?>
																		<div class="filtered-list-post">
																			<div class="filtered-post-date">
																				<?php
																				$date = $news_post->releaseDate->date;
																				$month = date("M", strtotime($date));
																				$day = date("d", strtotime($date));
																				$year = date("Y", strtotime($date));
																				echo $month.'</br>'.$day; ?>
																			</div>
																			<a class="filtered-post-title" href="<?php echo $news_post->link->hostedUrl; ?>"><?php echo $news_post->title; ?></a>
																			<a class="filtered-post-link" href="<?php echo $news_post->link->hostedUrl; ?>">
																				<span class="meta posturl">
																					<img src="<?php echo get_template_directory_uri(); ?>/images/icons/down-arrow.svg" alt="posturl" class="posturl-icon" />
																				</span>
																			</a>
																		</div>
																	<?php
																	} ?>
																</div>
															</div>
														</div>
													<?php
													}
													break;
												}
												$args = array(
											    'post_type' => $p_type,
											    'posts_per_page' => 3,
											  );
											  $postslides = new WP_Query($args);
											  if($postslides->have_posts()) :
													$ptWithDate = ['investor-news', 'sec-filing', 'investor-material']; ?>
													<div class="ion-module">
				                    <div class="filtered-list">
															<div class="filtered-list-container">
																<?php
																while($postslides->have_posts()): $postslides->the_post();
																	$link = get_field('upload_pdf', get_the_id()) ? get_field('upload_pdf', get_the_id())['url'] : get_the_permalink(); ?>
																	<div class="filtered-list-post">
																		<?php
																		if(in_array($post_type->name, $ptWithDate)) { ?>
																			<div class="filtered-post-date">
																				<?php
																				$date = get_the_date();
																				$month = date("M", strtotime($date));
																				$day = date("d", strtotime($date));
																				$year = date("Y", strtotime($date));
																				echo $month.'</br>'.$day; ?>
																			</div>
																		<?php
																		} ?>
																		<a class="filtered-post-title" href="<?php echo $link; ?>"><?php the_title(); ?></a>
																		<?php
																		if($formats = get_field('sec_formats')) {
														          $accForms = ['pdf', 'rtf'];
														          foreach($formats as $format) {
														            if(in_array($format['format'], $accForms)) { ?>
														              <a href="<?php echo $format['url']; ?>" class="filtered-post-format format-<?php echo $format['format']; ?>">
														                <img src="<?php echo get_template_directory_uri().'/images/download-'.strtolower($format['format']).'.png'; ?>" alt="<?php echo $format['format']; ?>"/>
														              </a>
														            <?php
														            }
														          }
														        } else if($pdf = get_field('upload_pdf')) { ?>
														          <a class="filtered-post-link" href="<?php echo $pdf['url']; ?>">
														            <span class="meta download">
														              <img src="<?php echo get_template_directory_uri(); ?>/images/icons/down-arrow.svg" alt="download" class="download-icon" />
														            </span>
														          </a>
														        <?php
														        } else { ?>
														          <a class="filtered-post-link" href="<?php echo get_the_permalink(); ?>">
														            <span class="meta posturl">
														              <img src="<?php echo get_template_directory_uri(); ?>/images/icons/down-arrow.svg" alt="posturl" class="posturl-icon" />
														            </span>
														          </a>
														        <?php
																		} ?>
																	</div>
																<?php
																endwhile;
																wp_reset_postdata(); ?>
															</div>
														</div>
													</div>
												<?php
												endif;
											break;
											case 'events_module':
												$date = date('Y-m-d H:i:s',strtotime("today"));
												$eventargs = [
										      'post_type' => 'event',
										      'posts_per_page' => -1,
													'order_by' => 'meta_value',
													'meta_key' => 'event_date',
													'order' => 'ASC',
													'meta_query' => [
														'relation' => 'OR',
										        [
										          'key'       => 'event_end_date',
										          'value'     => $date,
										          'compare'   => '>=',
										          'type'      => 'DATETIME'
										        ],
														[
										          'key'       => 'event_date',
										          'value'     => $date,
										          'compare'   => '>=',
										          'type'      => 'DATETIME'
										        ]
										      ]
										    ];
												$eventPosts = new WP_Query($eventargs);
												if($eventPosts->have_posts()) : ?>
													<header class="module-header">
														<h3>Upcoming Events</h3>
														<a class="view-all" href="<?php echo home_url('events'); ?>">See All <!--Events--></a>
													</header>
													<div class="upcoming-events-module<?php echo $eventPosts->post_count < 4 ? ' hide-dates' : ''; ?>" data-event-count="<?php echo $eventPosts->post_count; ?>">
														<div class="upcoming-event-dates-container">
															<div class="upcoming-event-dates">
																<?php
																$events_obj = (object)[];
																$currentDate = $currentMonth = $currentYmd = '';
																while($eventPosts->have_posts()): $eventPosts->the_post();
																	$date = date('Y-m-d H:i:s', strtotime(get_field('event_date')));
																	$ymd = date('Y-m-d', strtotime(get_field('event_date')));
																	$event_obj = (object)[
																		'title' => get_the_title(),
																		'date' => get_field('event_date'),
																		'link' => get_field('external_link') ? get_field('external_link') : get_the_permalink(),
																		'target' => get_field('external_link') ? '_blank' : '_self'
																	];
																	$event_obj->imagelarge = get_template_directory_uri().'/images/post-placeholder.png';
																	$event_obj->imagesmall = get_template_directory_uri().'/images/post-placeholder.png';
																	if(has_post_thumbnail()) {
																		$event_obj->imagelarge = get_the_post_thumbnail_url(get_the_id(), 'small-medium');
																		$event_obj->imagesmall = get_the_post_thumbnail_url(get_the_id(), 'small');
																	}
																	if($ymd != $currentDate) {
																		// First post in this date.
																		$event_obj->events = [];
																		if(has_excerpt()) {
																			$event_obj->excerpt = get_the_excerpt();
																		}
																		if($location = get_field('event_location')) {
																			$event_obj->location = $location;
																		}
																		// Add event object to events object
																		$events_obj->$ymd = $event_obj;
																		$currentYmd = $ymd;
													          $month = date("M", strtotime($date));
													          $day = date("j", strtotime($date)); ?>
																		<div class="event-module-date<?php echo $currentMonth != $month ? ' first-of-month': ''; ?>"
																			<?php echo $currentMonth != $month ? ' month="'.$month.'"' : ''; ?>>
																			<?php
																			if($currentMonth != $month) { ?>
																				<div class="event-module-month">
																					<span>
																						<?php echo $month; ?>
																					</span>
																				</div>
																				<?php
																				$currentMonth = $month;
																			} ?>
																			<div class="event-module-date-content">
																				<span><?php echo $day; ?></span>
																			</div>
																		</div>
													        <?php
																	} else { // Another event on the same day.
																		array_push($events_obj->$ymd->events, $event_obj);
																	}
																	$currentDate = $ymd;
																endwhile; ?>
															</div>
															<div class="event-module-arrow arrow-prev">
															</div>
															<div class="event-module-arrow arrow-next">
															</div>
														</div>
														<div class="upcoming-event-posts">
															<?php
															foreach($events_obj as $key=>$event) { ?>
																<div class="upcoming-event-post event-post-<?php echo $key; ?>">
																	<div class="upcoming-featured flex">
																		<?php
																		if(isset($event->imagelarge)) { ?>
																			<div class="upcoming-featured-image-container">
																				<a target="<?php echo $event->target; ?>" href="<?php echo $event->link; ?>" class="upcoming-featured-image ratio-1-1" style="background-image: url('<?php echo $event->imagelarge; ?>')">
																					<!--<img src="<?php echo $event->imagelarge; ?>"/>-->
																				</a>
																			</div>
																		<?php
																		} ?>
																		<div class="upcoming-featured-content">
																			<a target="<?php echo $event->target; ?>" class="upcoming-featured-title" href="<?php echo $event->link; ?>">
																				<?php echo $event->title; ?>
																			</a>
																			<h6>
																				<?php
																				$year = date("Y", strtotime($event->date));
																				$month = date("M", strtotime($event->date));
																				$day = date("d", strtotime($event->date));
																				echo $year . ' ' . $month . ' ' .$day;
																				if(isset($event->location)) { ?>
																					<span> | <?php echo $event->location; ?></span>
																				<?php
																				} ?>
																			</h6>
																			<?php
																			if(isset($event->excerpt)) { ?>
																				<div class="upcoming-featured-desc">
																					<?php echo $event->excerpt; ?>
																				</div>
																			<?php
																			} ?>
																			<a class="upcoming-featured-link" target="<?php echo $event->target; ?>" href="<?php echo $event->link; ?>">
																				See Event Details
																			</a>
																		</div>
																	</div>
																	<?php
																	if(count($event->events)) {
																		// has more events ?>
																		<div class="upcoming-more-events">
																			<?php
																			foreach($event->events as $key=>$event) {
																				if($key == 3) {
																					return;
																				} ?>
																				<div class="upcoming-more-events-post flex row">
																					<?php
																					$image = get_template_directory_uri().'/images/post-placeholder.png';
																					if(isset($event->imagesmall)) {
																						$image = $event->imagesmall;
																					} ?>
																					<a class="upcoming-more-events-image">
																						<img src="<?php echo $image; ?>"/>
																					</a>
																					<div class="upcoming-more-events-date">
																						<?php
																						$year = date("Y", strtotime($event->date));
																						$month = date("M", strtotime($event->date));
																	          $day = date("d", strtotime($event->date));
																						echo $year . '</br>' . $month .' '.$day; ?>
																					</div>
																					<div class="upcoming-more-events-post-content with-arrow">
																						<a target="<?php echo $event->target; ?>" href="<?php echo $event->link; ?>" class="upcoming-more-events-title">
																							<?php echo $event->title; ?>
																						</a>
																						<?php
																						if(isset($event->location)) { ?>
																							<div class="upcoming-more-events-location">
																								<?php echo $event->location; ?>
																							</div>
																						<?php
																						}?>
																					</div>
																				</div>
																			<?php
																			} ?>
																		</div>
																	<?php
																	} ?>
																</div>
															<?php
															} ?>
														</div>
													</div>
													<?php
												wp_reset_postdata();
												endif;
											break;
											default:
											break;
										} ?>
									</div>
								<?php
								} ?>
							</div>
							<?php
						} ?>
					</div>
				</div>
			<?php
			endwhile;

		endif;
		?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
