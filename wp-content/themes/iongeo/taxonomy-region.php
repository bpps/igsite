<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Iongeo
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<?php
			if ( have_posts() ) :
				/* Start the Loop */
				while ( have_posts() ) :
					the_post();
					$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
					$guid = get_field('guid', $term); ?>

	        <header id="page-header" class="full-width-image bg-centered">
	  				<div id="page-header-title" class="content-inner">
							<div class="page-header-title-content">
		  					<h1><?php echo $term->name; ?></h1>
							</div>
	  				</div>
	  			</header>
					<?php
					echo get_share_link('ION Geo Data Library | '.$term->name, get_term_link($term)); ?>
	  			<section class="page-content-wrapper content-inner">
						<div class="page-content-container">
		  				<div class="page-content">
		  					<?php echo get_data_library_map(['guid' => $guid, 'slug' => $term->slug]); ?>
								<?php echo get_multi_client_products(); ?>
		  				</div>
						</div>
	  			</section>
	  			<?php

	  			// $terms = get_terms(['taxonomy' => 'software']);
	  			// echo get_term_link($terms[0], 'software');

	  		endwhile; // End of the loop.

			else :

				get_template_part( 'template-parts/content', 'none' );

			endif;
			?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
