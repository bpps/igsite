<?php
/**
 * The template for displaying resource taxonomy pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Iongeo
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<?php
			$term = get_queried_object();
			?>

			<header id="page-header" class="standard-header">
 				<div id="page-header-title" class="content-inner">
					<div class="page-header-title-content">
	 					<h1><?php echo $term->name; ?></h1>
					</div>
 				</div>
 			</header>
			<?php echo get_share_link('ION Geo Resources | '.$term->name, get_term_link($term)); ?>
			<section class="page-content-wrapper content-inner">
				<?php
				$paged = 1;
				if(isset($_GET['page'])) {
					$paged = $_GET['page'];
				}
				$taxes = get_object_taxonomies( 'resource' );
				$filter = [];
				foreach($taxes as $tax) {
					if(isset($_GET[$tax])) {
						array_push($filter, ['taxonomy' => $tax, 'field' => 'slug', 'terms' => explode(",",$_GET[$tax])]);
					}
				}
				$filter_props = [
					'type' => 'resource-type',
					'tType' => $term,
					'pType' => 'resource',
					'filters' => $filter,
					'paged' => $paged
				];
				echo create_list_by_term($filter_props); ?>
			</section>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
