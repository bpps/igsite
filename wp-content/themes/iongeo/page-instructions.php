<?php
/**
 * The template for displaying instructions page
 *
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Iongeo
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

		<?php
		while ( have_posts() ) : the_post();
			$pw = post_password_required(); ?>
			<header id="page-header" class="standard-header">
				<div id="page-header-title" class="content-inner">
					<div class="page-header-title-content">
						<h1><?php the_title(); ?></h1>
					</div>
				</div>
			</header>
			<section class="page-content-wrapper<?php echo $pw ? ' password-protected' : ''; ?>">
				<div class="page-content-container has-sidebar">
					<div class="page-content">
						<?php
						the_content();
						if(!post_password_required()) {
							if($instructions = get_field('instructions')) { ?>
								<div id="instructions-container">
									<div id="table-of-contents" class="col-4">
										<ul>
											<?php
											foreach($instructions as $inst) { ?>
												<li>
													<a class="anchor-link anchor-link-<?php echo slugify($inst['title']); ?>" href="#" data-anchor="<?php echo slugify($inst['title']); ?>">
														<?php echo $inst['title']; ?>
													</a>
													<?php
													if($inst['sub_instructions']) { ?>
														<ul>
															<?php
															foreach($inst['sub_instructions'] as $subInst) { ?>
																<li>
																	<a class="anchor-link anchor-link-<?php echo slugify($subInst['title']); ?>" href="#" data-anchor="<?php echo slugify($subInst['title']); ?>">
																		<?php echo $subInst['title']; ?>
																	</a>
																</li>
															<?php
															} ?>
														</ul>
														<?php
													} ?>
												</li>
											<?php
											} ?>
										</ul>
									</div>
									<div id="instructions-content" class="col-8">
										<?php
										foreach($instructions as $inst) { ?>
											<section data-inst="<?php echo slugify($inst['title']); ?>" class="instruction inst-<?php echo slugify($inst['title']); ?>">
												<h2><?php echo $inst['title']; ?></h2>
												<div class="instruction-content">
													<?php echo $inst['instructions']; ?>
												</div>
											</section>
											<?php
											if($inst['sub_instructions']) {
												foreach($inst['sub_instructions'] as $subInst) { ?>
													<section data-inst="<?php echo slugify($subInst['title']); ?>" class="instruction inst-<?php echo slugify($subInst['title']); ?>">
														<h2><?php echo $subInst['title']; ?></h2>
														<div class="instruction-content">
															<?php echo $subInst['instructions']; ?>
														</div>
													</section>
												<?php
												}
											}
										} ?>
									</div>
								</div>
							<?php
							}
						} ?>
					</div>
				</div>
			</section>
		<?php
		endwhile; // End of the loop.
		?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
