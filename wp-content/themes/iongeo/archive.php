<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Iongeo
 */

get_header();
$archive = get_queried_object();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<header id="page-header" class="standard-header">
 				<div id="page-header-title" class="content-inner">
					<div class="page-header-title-content">
						<h1><?php echo $archive->label; ?></h1>
					</div>
 				</div>
 			</header>
			<?php
      $postType = get_queried_object();
      echo get_share_link('ION Geo | '.$postType->label, get_post_type_archive_link( $postType->name )); ?>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
