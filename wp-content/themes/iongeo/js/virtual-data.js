import $ from 'jquery';
var tourSelections = {}

$(document).on('cf.form.init', function (event, data) {
  var state = data.state;

  function removeTourDate (id, val) {
    state.mutateState(id, false);
    console.log('id', id,'val', val)
    $(`#${id}-wrap input[value="${val}"]`).prop('checked', false)
    delete tourSelections[id]
    console.log(tourSelections)
    showSelectedTours()
  }

  function showSelectedTours () {
    if ( Object.keys(tourSelections).length ) {
      $('.caldera-grid input[type="submit"]').attr('disabled', false)
      let selections = '';
      Object.keys(tourSelections).forEach( id => {
        selections += `<li class="data-tour-selected" data-tour-date="${tourSelections[id].date}" data-tour-id="${id}">${tourSelections[id].tour}: ${tourSelections[id].date}</li>`
      })
      $('.data-tour-summary').html('<h5>Selected Tours</h5><ul>' + selections + '</ul>')
      $('.data-tour-selected').off().on('click', function() {
        removeTourDate( $(this).data('tour-id'), $(this).data('tour-date') )
      })

    } else {
      // $('.caldera-grid input[type="submit"]').attr('disabled', true)
      $('.data-tour-summary').html('<h5>Selected Tours</h5><p><span>No tours selected</span></p>')
    }
  }
  //create callback function to record field as being focused, by form and field ID
  var callback = function(fieldId, value){
      console.log('id', fieldId, 'value', value.toString());
      if ( $('.radio', `#${fieldId}-wrap`).length ) {
        tourSelections[fieldId] = { tour: $(`label[for="${fieldId}"]`).text(), date: value }
      }
      console.log(tourSelections)



      showSelectedTours()
      //report field ID and (if you want value) to Google Analytics
  };
  console.log(data)
  $('.data-tour-summary').html('<h5>Selected Tours</h5><p><span>No tours selected</span></p>')
  data.fieldIds.forEach( function(fieldId){
      state.events().subscribe(fieldId,callback)
  });
  // $('.caldera-grid input[type="submit"]').attr('disabled', true)
});

if ( $('#full-width-slider').length ) {
  $('#full-width-slider').slick({
    arrows: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    fade: true,
    autoplaySpeed: 3500,
    infinite: true,
    adaptiveHeight: true,
    prevArrow : $('.arrow-prev'),
		nextArrow : $('.arrow-next'),
    rows: 0,
    pauseOnHover: false
  });
}
