import $ from 'jquery';
require('accordion')

if ( $('.investor-event-year').length ) {

  $(document).ready(function() {
    if ( getUrlParameter('e') ) {
      setTimeout(function() {
        $('html,body').animate({
          scrollTop: $('.event-list-item.open').offset().top - 200
        });
      }, 3000)
    }
  })

  var el = document.querySelector('.event-list-items');
  var acc = new Accordion(el, {
    heightOffset: 8,
    modal: true
  });
  $('.investor-event-year').on('click', function(e) {
    e.preventDefault()
    let year = $(this).data('year');
    showEventsForYear( year );
  })

  $('.more-event-years').on('click', function(e) {
    e.preventDefault()
    $('.more-event-years').hide()
    $('#event-years').show()
  })

  $('#event-years').on('change', function(e) {
    let year = this.value;
    console.log(year)
    showEventsForYear( year );
  })

  function showEventsForYear( year ) {
    let ids = investorYears[year]
    console.log(ids, year)
    $('.investor-event-year').removeClass('active')
    $('#investor-year-' + year).addClass('active')
    $('.events-by-year-container .ion-loader-container').addClass('active')
    $.ajax({
      url: ajaxurl,
      data: {
        action: 'investor_ajax',
        fn: 'get_investor_events_by_year',
        year: year,
        ids: ids
      }
    }).done( function (response) {
      $('.events-by-year').html(response);
      $('.events-by-year-container .ion-loader-container').removeClass('active')
      var el = document.querySelector('.event-list-items');
      var acc = new Accordion(el, {
        heightOffset: 8,
        modal: true
      });
    });
  }

}

var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
        }
    }
};
