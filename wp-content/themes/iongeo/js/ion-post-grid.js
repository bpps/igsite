import $ from 'jquery';
var $postGrid;
if($('.ion-post-grid').length) {
  $postGrid = $('.ion-post-grid').masonry({
    itemSelector: '.grid-post',
    percentPosition: true,
    gutter: '.grid-gutter',
    horizontalOrder: 'true',
    columnWidth : '.grid-post',
    fitWidth: 'true'
  });
  $postGrid.imagesLoaded( {}, function() {
    $postGrid.masonry('layout');
    $('.grid-post').removeClass('new-post');
  });

  if(filterObj) {
    $('#page-header-title').addClass('has-filter').append(filterObj.filter);
    $('.filter-items').on('change', function(e){
      e.preventDefault();
      var termslug = $(this).children("option:selected").val();
      $postGrid.masonry( 'remove', $postGrid.find('.grid-post') );
      if(termslug === 'all') {
        termslug = false;
      }
      console.log(termslug);
      $('.more-posts').data('filter', termslug).data('paged', 1);
      add_grid_posts();
    });
  }
  $('.more-posts').on('click', function(e) {
    e.preventDefault();
    add_grid_posts();
  });
}

function add_grid_posts() {
  var moreButton = $('.more-posts');
  var paged = $(moreButton).data('paged');
  var url = document.location.href.substring(0, document.location.href.indexOf('?'));
  var filter = $(moreButton).data('filter');
  const data = {
    'action': 'posts_ajax',
    'paged' : paged,
    'posttype' : $(moreButton).data('post-type'),
    'count' : $(moreButton).data('post-count'),
    'exclude' : $(moreButton).data('exclude'),
    'filter' : $(moreButton).data('filter')
  }
  console.log(data);
  $.ajax({
    url: ajaxurl,
    method: 'post',
    type: 'json',
    data: data
  }).done( function (response) {
    console.log(response);
    let data = $.parseJSON(response);
    if(!data.more) {
      $('.more-posts').hide();
    } else {
      $('.more-posts').show();
    }
    url = filter != '' ? url+"?topic="+filter : url;
    if(paged != 1) {
      url += url.indexOf("?") >= 0 ? "&page="+paged : "?page="+paged;
    }
    window.history.pushState({}, $(document).find("title").text(), url);
    let $newPosts = $($.parseHTML(data.posts)).filter('*');
    $postGrid.append($newPosts).masonry( 'appended', $newPosts );
    $('.new-post').imagesLoaded( {}, function() {
      $postGrid.masonry('layout');
    });
    paged++;
    $('.more-posts').data('paged', paged);
  }).error(function (xhr, ajaxOptions, thrownError) {
    alert(xhr.status);
    alert(thrownError);
  });
}
