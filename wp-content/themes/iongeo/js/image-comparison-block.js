(function (blocks, editor, components, i18n, element) {
  var el = wp.element.createElement
  var registerBlockType = wp.blocks.registerBlockType
  var RichText = wp.editor.RichText
  var MediaUpload = wp.editor.MediaUpload
  var InspectorControls = wp.editor.InspectorControls
  var TextControl = components.TextControl

  registerBlockType('image-comparison-block/image-comparison', { // The name of our block. Must be a string with prefix. Example: my-plugin/my-custom-block.
    title: i18n.__('Image Comparison'), // The title of our block.
    description: i18n.__('A custom block for displaying image comparisons.'), // The description of our block.
    icon: 'image-flip-horizontal', // Dashicon icon for our block. Custom icons can be added using inline SVGs.
    category: 'widgets', // The category of the block.
    attributes: { // Necessary for saving block content.
      subtitle: {
        type: 'array',
        source: 'children',
        selector: 'h5'
      },
      mediaID: {
        type: 'number'
      },
      img1URL: {
        type: 'string',
        source: 'attribute',
        selector: 'img',
        attribute: 'src'
      },
      img1PreviewURL: {
        type: 'string',
        source: 'attribute',
        selector: 'img',
        attribute: 'src'
      },
      img2URL: {
        type: 'string',
        source: 'attribute',
        selector: 'img',
        attribute: 'src'
      },
      img2PreviewURL: {
        type: 'string',
        source: 'attribute',
        selector: 'img',
        attribute: 'src'
      }
    },

    edit: function (props) {
      var attributes = props.attributes

      var onSelectImage1 = function (media) {
        return props.setAttributes({
          img1URL: media.sizes['extra-large'] ? media.sizes['extra-large'].url : media.url,
          img1PreviewURL: media.sizes.medium_large ? media.sizes.medium_large.url : media.url,
          img1mediaID: media.id,
          mediaRatio: (media.height / media.width) * 100
        })
      }

      var onSelectImage2 = function (media) {
        return props.setAttributes({
          img2URL: media.sizes['extra-large'] ? media.sizes['extra-large'].url : media.url,
          img2PreviewURL: media.sizes.medium_large ? media.sizes.medium_large.url : media.url,
          img2mediaID: media.id
        })
      }

      return [
        el('div', { className: props.className, style: {} },
          el('p', {}, i18n.__('Bottom image.')),
          el('div', {
            className: attributes.img1mediaID ? 'comparison-image-1 image-active' : 'comparison-image-1 image-inactive',
            style: attributes.image1mediaID ? { backgroundImage: 'url(' + attributes.img1PreviewURL + ')' } : {}
          },
            el(MediaUpload, {
              onSelect: onSelectImage1,
              type: 'image',
              value: attributes.img1mediaID,
              render: function (obj) {
                return el(components.Button, {
                  className: attributes.img1mediaID ? 'image-button' : 'button button-large',
                  onClick: obj.open
                },
                !attributes.img1mediaID ? i18n.__('Upload Image') : el('img', { src: attributes.img1PreviewURL })
                )
              }
            })
          ),
          el('p', {}, i18n.__('Top image.')),
          el('div', {
            className: attributes.img2mediaID ? 'comparison-image-2 image-active' : 'comparison-image-2 image-inactive',
            style: attributes.img2mediaID ? { backgroundImage: 'url(' + attributes.img2PreviewURL + ')' } : {}
          },
            el(MediaUpload, {
              onSelect: onSelectImage2,
              type: 'image',
              value: attributes.mediaID,
              render: function (obj) {
                return el(components.Button, {
                  className: attributes.img2mediaID ? 'image-button' : 'button button-large',
                  onClick: obj.open
                },
                !attributes.img2mediaID ? i18n.__('Upload Image') : el('img', { src: attributes.img2PreviewURL })
                )
              }
            })
          ),
          el('div', { className: 'image-comparison-content', style: { marginTop: '20px' } },
            el(RichText, {
              tagName: 'p',
              placeholder: i18n.__('Subtitle'),
              keepPlaceholderOnFocus: true,
              value: attributes.subtitle,
              onChange: function (newSubtitle) {
                props.setAttributes({ subtitle: newSubtitle })
              }
            })
          )
        )
      ]
    },

    save: function (props) {
      var attributes = props.attributes
      return (
        el('div', { className: props.className },
          el('div', { className: 'comparison-images-container', style: {paddingBottom : attributes.mediaRatio + '%'}},
            el('div', { className: 'comparison-images'},
              el('div', {
                className: 'comparison-image-1 comparison-image',
                'data-url' : attributes.img1mediaID ? attributes.img1URL : '',
              },
                el('img', { src: attributes.img1PreviewURL })
              ),
              el('div', {
                className: 'comparison-image-2 comparison-image',
                'data-url' : attributes.img2mediaID ? attributes.img2URL : '',
              },
                el('img', { src: attributes.img2PreviewURL })
              )
            )
          ),
          el('p', {className: 'comparison-image-instruction'}, i18n.__('Drag the slider to compare data')),
          el('div', { className: 'comparison-image-content', style: {  } },
            el(RichText.Content, {
              tagName: 'p',
              value: attributes.subtitle
            })
          )
          // el('div', { className: 'pan-zoom-controls' },
          //   el('button', { className: 'zoom-in', text : 'Zoom In' }, 'Zoom In'),
          //   el('button', { className: 'zoom-out' }, 'Zoom Out'),
          //   el('input', { className: 'zoom-range', type: 'range' }),
          //   el('button', { className: 'reset' }, 'Reset')
          // )
        )
      )
    }
  })

})(
  window.wp.blocks,
  window.wp.editor,
  window.wp.components,
  window.wp.i18n,
  window.wp.element
)
