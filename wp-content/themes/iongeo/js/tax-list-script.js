import $ from 'jquery';
if ($('.filtered-list').length) {
  if($('.filtered-list').data('post-type-title')) {
    var postTypeTitle = $('.filtered-list').data('post-type-title');
    var postTypeSlug = $('.filtered-list').data('post-type');
    $('.page-header-title-content').append(`
      <div class="on-page-search-bg">
      </div>
      <div class="on-page-search-container" data-posttype="${postTypeSlug}">
        <div class="on-page-search-icon">
        </div>
        <input class="on-page-search" placeholder="Search In ${postTypeTitle}"/>
        <div class="on-page-search-results-container">
        </div>
      </div>
    `);
    $('.on-page-search').on('input', function() {
      let searchVal = $('.on-page-search').val();
      if(searchVal.length > 2) {
        //$('#searchform .ion-loader-container').addClass('active');
        $.ajax({
          url: ajaxurl,
          method: 'post',
          type: 'json',
          data: {
            'action': 'do_ajax',
            'fn' : 'onpagesearch',
            'search' : searchVal,
            'posttype' : postTypeSlug
          }
        }).done( function (response) {
          //$('#searchform .ion-loader-container').removeClass('active');
          if(response) {
            $('.on-page-search-results-container').html(`<div class="on-page-search-results">${response}</div>`);
            // $('.on-page-search-results a').on('click', function(e) {
            //   e.preventDefault();
            //   var resultlink = $(this).attr('href');
            //   window.location.href = resultlink;
            // });
          }
        });
      }
    });
    $('.on-page-search').on('focus', function() {
      $('.on-page-search-bg').fadeIn();
      $('.on-page-search-container').addClass('active');
    });
    $('.on-page-search').on('focusout', function() {
      //closeOnPageSearch();
    });
    $('.on-page-search-bg').on('click', function() {
      closeOnPageSearch();
    });
    $(window).on('scroll', function() {
      if($('.on-page-search-bg').is(':visible')) {
        $('.on-page-search').blur();
        closeOnPageSearch();
      }
    });
    function closeOnPageSearch() {
      //$('.on-page-search').blur();
      $('.on-page-search-bg').fadeOut();
      $('.on-page-search-results-container').html('');
      $('.on-page-search').val('');
      $('.on-page-search-container').removeClass('active');
    }
  }

  $('.filtered-list').each(function (l, list) {
    $('.filtered-list-post', list).removeClass('new-post');
    var listtax = $(list).data('tax-type');
    var listterm = $(list).data('tax-term');
    var postType = $(list).data('post-type');
    var excluded = $(list).data('excluded');
    var date = $(list).data('post-date');
    var maxpages = $('.arrow-next', list).data('max-pages');
    var filter = $('.post-filter.active', list).data('filter');
    var listData = {
      'action': 'investor_ajax',
      'fn': 'get_tax_list',
      'type': listtax,
      'term': listterm,
      'filter': filter,
      'posttype': postType,
      'excluded': excluded,
      'date': date
    };
    // Clear filter button
    $('.clear-filter').on('click', function(e) {
      e.preventDefault();
      $('.filter-item').removeClass('active').removeClass('inactive');
      $('select.filtered-list-by-tax-terms option:first-of-type').prop("selected", true)
      $('select.filter-by-year option:first-of-type').prop("selected", true)
      listData.paged = 1;
      getFilteredPosts(listData, list);
    });
    // Side Filter Items
    $('.filter-item').on('click', function(e) {
      e.preventDefault();
      $(this).toggleClass('active');
      listData.paged = 1;
      getFilteredPosts(listData, list);
    });
    // Side filter select
    $('select.filtered-list-by-tax-terms').on('change', function() {
      listData.paged = 1;
      getFilteredPosts(listData, list);
    });
    // Top Filter Buttons
    $('select.filter-by-year').on( 'change', function(e) {
      listData.paged = 1
      listData.transitionType = 'filter-posts'
      getFilteredPosts(listData, list)
    })
    $('.post-filter', list).on('click', function (e) {
      e.preventDefault();
      if($(this).hasClass('active')) {
        return;
      }
      $('.post-filter').removeClass('active');
      $(this).addClass('active');
      listData.paged = 1;
      listData.transitionType = 'filter-posts';
      getFilteredPosts(listData, list);
    });
    // Next/Prev Buttons
    $('.circle-arrow', list).on('click', function (e) {
      e.preventDefault();
      if ($(this).hasClass('inactive')) {
        return
      }
      var listpage = $(list).data('paged');
      var arrowDirection = parseInt($(this).data('direction'));
      listData.paged = listpage + arrowDirection;
      listData.transitionType = $(this).data('func');
      $('.circle-arrow', list).addClass('inactive');
      getFilteredPosts(listData, list);
    });
  });
}
function getFilteredPosts(listData, list) {
  listData.filter = $('.post-filter.active').data('filter');

  var url = document.location.href.substring(0, document.location.href.indexOf('?'));
  if ( $('.post-filter.active').data('tax-type') ) {
    listData.tax = $(this).data('tax-type'); // REMOVE THIS???
    listData.type = $('.post-filter.active').data('tax-type');
    listData.term = $('.post-filter.active').data('filter');
  }
  if ( $( 'select.filter-by-year' ).length ) {
    var selectedYear = $('select.filter-by-year option:selected').val()
    listData.date = selectedYear ? $('select.filter-by-year option:selected').val() : false
  }
  if(listData.paged == 1) { // If this is a new filter, reset arrows
    $('.circle-arrow').removeClass('inactive');
    $('.arrow-prev').addClass('inactive');
  }
  if($('.filtered-list-by-taxes').length) {
    var filterObj = {};
    $('.filter-item.active').each(function() {
      var filterTax = $(this).data('tax');
      var filterTerm = $(this).data('term');
      if(filterObj.hasOwnProperty(filterTax)) { // if no key, add array or add to array
        filterObj[filterTax].push(filterTerm);
      } else {
        filterObj[filterTax] = [filterTerm];
      }
    });
    // Get active items from filter selects
    $('select.filtered-list-by-tax-terms').each(function() {
      var selectedOption = $(this).find(":selected")
      if ( !$(selectedOption).val() ) {
        return
      }
      var filterTax = $(selectedOption).data('tax')
      var filterTerm = $(selectedOption).val()
      if(filterObj.hasOwnProperty(filterTax)) { // if no key, add array or add to array
        filterObj[filterTax].push(filterTerm);
      } else {
        filterObj[filterTax] = [filterTerm];
      }
    })
    $.each(filterObj, function(index, val){
      url += url.indexOf("?") >= 0 ? "&"+index+"="+val : "?"+index+"="+val
    })
    if(listData.paged != 1) {
      url += url.indexOf("?") >= 0 ? "&page="+listData.paged : "?page="+listData.paged;
    }
    window.history.pushState({}, $(document).find("title").text(), url);
    listData.topics = filterObj;
  }
  $(list).data('paged', listData.paged);
  if (listData.topics && Object.keys(listData.topics).length !== 0) {
    $('.clear-filter').removeClass('active')
  }
  $('.filtered-list-posts .ion-loader-container').addClass('active');
  $.ajax({
    url: ajaxurl,
    method: 'post',
    type: 'json',
    data: listData
  }).done(function (response) {
    response = $.parseJSON(response);
    if(response.availableterms) {
      $('.filter-item.inactive').removeClass('inactive');
      $('.filter-item').each(function() {
        if($.inArray( $(this).data('term'), response.availableterms ) == -1) {
          $(this).addClass('inactive');
        }
      });
    }
    if (response.posts) {
      $('.filtered-list-container', list).addClass(listData.transitionType + '-posts');
      setTimeout(function () {
        $('.filtered-list-container').html(response.posts);
        //$('.filtered-list-container').removeClass('prev-posts next-posts');
        setTimeout(function () {
          $('.new-post').removeClass('new-post');
          $('.filtered-list-container').removeClass('prev-posts next-posts filter-posts');
          $('.filtered-list-posts .ion-loader-container').removeClass('active');
        }, 100);
      }, 300);
    } else {
      $('.filtered-list-container').html('<p>Nothing available</p>');
    }
    $('.filtered-list-arrows').show();
    if ( response.maxpages === undefined || response.maxpages === 1 ) {
      $('.filtered-list-arrows').hide();
    }
    console.log(response.maxpages)
    if (response.maxpages != $(list).data('paged')) {
      // make next button inactive
      $('.arrow-next', list).removeClass('inactive');
    }
    // if (response.maxpages == $(list).data('paged')) {
    //   $('.filtered-list-arrows').hide();
    // } else {
    //   $('.filtered-list-arrows').show();
    // }
    if ($(list).data('paged') != 1) {
      $('.arrow-prev', list).removeClass('inactive');
    }
  });
}
