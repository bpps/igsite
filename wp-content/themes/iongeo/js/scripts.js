import $ from 'jquery';
import 'slick-carousel';
import imagesLoaded from 'imagesloaded';

document.body.className = document.body.className.replace("no-js","js");

if ( $('body').hasClass('ie') ) {
$('.bg-centered').each(function () {
  var $container = $(this),
      imgUrl = $container.find('img').prop('src');
  if (imgUrl) {
    $container
      .css('backgroundImage', 'url(' + imgUrl + ')')
      .addClass('compat-object-fit');
  }
});
}

// More code using $ as alias to jQuery
$.fn.isInViewport = function(minusHeight = 0) {
var elementTop = $(this).offset().top + minusHeight;
var elementBottom = elementTop + $(this).outerHeight();
var viewportTop = $(window).scrollTop();
var viewportBottom = viewportTop + $(window).height();
//console.log('el bottom: '+elementBottom+' viewport top: '+viewportTop+' el top: '+elementTop + ' view bottom: '+ viewportBottom);
return elementBottom > viewportTop && elementTop < viewportBottom;
}

if ( $('#data-library-container').length ) {
window.addEventListener("message", receiveMessage, false);
}

// Receive messages from Pardot or Data Library iframe
function receiveMessage(event) {
switch (event.origin) {
  case 'https://public.geopostenergy.com':
    get_dataset_data(event.data, true);
  break;
  case 'https://info.iongeo.com':
    resize_iframe(event.data);
  break;
  default:
}
}

// Resize iframe from Pardot
function resize_iframe(data) {
$('iframe[src$="'+data.url+'"]').css('height', data.height+'px')
}

// For resources page, when someone selects an option
if($('.select-to-link').length) {
$('.select-to-link').on('change', function (e) {
    e.preventDefault();
    var url = $('option:selected', this).attr('href'); // get selected value
    if (url) { // require a URL
        window.location = url; // redirect
    }
});
}

// On iframe load remove loader
if($('#data-library-container iframe').length) {
$('#data-library-container iframe').load(function(){
  $('#data-library-container .ion-loader-container').removeClass('active');
});
}

// Data Library search
if($('.data-library-search').length) {
$('.on-page-search').on('input', function() {
  let searchVal = $('.on-page-search').val();
  if(searchVal.length > 2) {
    //$('#searchform .ion-loader-container').addClass('active');
    $.ajax({
      url: ajaxurl,
      method: 'post',
      type: 'json',
      data: {
        'action': 'data_library',
        'fn' : 'onpagesearch',
        'search' : searchVal
      }
    }).done( function (response) {
      //$('#searchform .ion-loader-container').removeClass('active');
      if(response) {
        $('.on-page-search-results-container').html(`<div class="on-page-search-results">${response}</div>`);
      }
    });
  }
});
$('.on-page-search').on('focus', function() {
  $('.on-page-search-bg').fadeIn();
  $('.on-page-search-container').addClass('active');
});
$('.on-page-search').on('focusout', function() {
  //closeOnPageSearch();
});
$('.on-page-search-bg').on('click', function() {
  closeOnPageSearch();
});
$(window).on('scroll', function() {
  if($('.on-page-search-bg').is(':visible')) {
    $('.on-page-search').blur();
    //closeOnPageSearch();
  }
});
function closeOnPageSearch() {
  $('.on-page-search-container').removeClass('active');
  //$('.on-page-search').blur();
  $('.on-page-search-bg').fadeOut();
  $('.on-page-search-results-container').html('');
  $('.on-page-search').val('');
}
}

// Change URLs when navigating the data library
function get_dataset_data(data, switchurl) {
if(data != undefined) {
  data.Token = data.Token.toString();
  data.Url = window.location.href;
  $.ajax({
    url: ajaxurl,
    method: 'post',
    type: 'json',
    data: {
      'action': 'data_library',
      'fn' : 'get_dataset_data',
      'data' : data
    }
  }).done( function (response) {
    response = $.parseJSON(response);
    if(response.crumbs) {
      $('#ion-smart-crumbs .smart-crumb').not('.smart-crumb:first').remove();
      $('#ion-smart-crumbs').html(response.crumbs);
    }
    if(response.url && switchurl) {
      window.history.pushState({'Type':data.Type,'Token':data.Token, 'prevTitle':$('#page-header-title h1').html(), 'Title':data.Title}, data.Title, response.url);
    }
    if(!switchurl) {
      window.addEventListener("message", receiveMessage, false);
    }
    if(data.Title) {
      $('#page-header-title h1').html(data.Title);
    } else {
      $('#page-header-title h1').html($('#data-library-container').data('title'));
    }

  });
}

}

window.onpopstate = function(e){
window.removeEventListener("message", receiveMessage);
var returnObj;
if(e.state){
  var $mapiframe = document.getElementById('data-library-iframe');
  if(e.state.Title) {
    $('#page-header-title h1').html(e.state.Title);
  }
  $mapiframe.contentWindow.postMessage(e.state.Token, '*');
  returnObj = e.state;
} else {
  if($('#data-library-container').data('title')) {
    returnObj = {
      'Title': $('#data-library-container').data('title'),
      'Token': $('#data-library-container').data('token'),
      'Type': $('#data-library-container').data('type')
    }

  } else {
    $('#data-library-container').html('Data Library');
  }

}
get_dataset_data(returnObj, false);
}

// Page content tabs
if($('.content-tabs').length) {
$('.content-tab-button').on('click', function(e) {
  e.preventDefault();
  $('.content-tab-button').removeClass('active');
  $(this).addClass('active');
  var tabSlug = $(this).data('slug');
  $('.content-tab').hide().removeClass('active');
  $('.content-tab[data-slug="'+tabSlug+'"]').show().addClass('active');
});
}

// Post slideshow
if($('.post-slideshow-wrapper').length) {
$('.post-slideshow-wrapper').each(function(s, slideshow) {
  $('.post-slideshow', slideshow).slick({
    'arrows': true,
    'slidesToShow': 2,
    'slidesToScroll': 2,
    'autoplay': false,
    'fade': false,
    'infinite': true,
    'prevArrow' : $('.arrow-prev', slideshow),
		'nextArrow' : $('.arrow-next', slideshow),
    'rows': 0,
    'responsive': [
      {
        'breakpoint': 668,
        'settings': {
          'slidesToShow': 1,
          'slidesToScroll': 1
        }
      }
    ]
  });
});
}

// Featured events slick
if($('.featured-posts-slider').length) {
$('.featured-posts-slider').slick({
  'arrows': true,
  'slidesToShow': 1,
  'slidesToScroll': 1,
  'autoplay': false,
  'fade': true,
  'infinite': true,
  'adaptiveHeight': true,
  'prevArrow' : $('.arrow-prev'),
	'nextArrow' : $('.arrow-next'),
  'rows': 0
});
}

// Investor modules masonry

// if($('#investor-modules').length) {
//   let $modules = $('#investor-modules-section').isotope({
//     itemSelector: '.ion-module',
//     percentPosition: true,
//     layoutMode: 'packery',
//     packery: {
//       gutter: '#modules-gutter',
//       columnWidth: $('#investor-modules').width() * .3
//     }
//   });
//   $modules.imagesLoaded( {}, function() {
//     $modules.isotope('layout');
//   });
//   $modules.on( 'layoutComplete', function( event, items ) {
//     console.log('ok');
//     $('#investor-modules-section').addClass('masonry-active');
//   });
// }

// Hide/show content toggle

if($('.toggle-content').length) {
$('.toggle-content-button').each(function(){
  $(this).on('click', function(e, toggle) {
    e.preventDefault();
    if($(this).prev('.toggle-content').is(':visible')) {
      $(this).prev('.toggle-content').slideUp();
      $(this).html('show more');
    } else {
      $(this).prev('.toggle-content').slideDown();
      $(this).html('show less');
    }
  });
});
}

// Image loading on headers
if ( $('header.full-width-image').length ) {
  imagesLoaded( $('.temp-header-image'), { background: true }, function() {
    //$('.temp-header-image').css('opacity', 1);
    $('.header-image-cover').fadeOut(600);
  });
  imagesLoaded( $('#page-header'), { background: true }, function() {
    $('.temp-header-image').fadeOut();
  });
}

// Homepage header slides

if($('#page-header-slides').length) {
$('#page-header-slides').slick({
  'arrows': false,
  'slidesToShow': 1,
  'slidesToScroll': 1,
  'autoplay': $('#page-header-slides').data('autoplay'),
  'fade': true,
  'infinite': true,
  'speed' : 500,
  'autoplaySpeed': 5000,
  'rows': 0,
  'asNavFor': '#slide-scrubber'
  // 'prevArrow' : $('.page-header-arrows .arrow-prev'),
	// 'nextArrow' : $('.page-header-arrows .arrow-next'),
});
$('#slide-scrubber').on('init', function(slick) {
  setTimeout(function() {
    $('#slide-scrubber-wrapper').addClass('active');
    $('.page-header-arrows').addClass('active');
  }, 600);
}).slick({
  'slidesToShow': 1,
  'slidesToScroll': 1,
  'asNavFor': '#page-header-slides',
  'dots': false,
  'infinite': false,
  'arrows': true,
  'centerMode': false,
  'focusOnSelect': true,
  'prevArrow' : $('.page-header-arrows .arrow-prev'),
	'nextArrow' : $('.page-header-arrows .arrow-next'),
  'rows': 0
});
}

if($('.upcoming-event-dates').length) {
if($('.upcoming-events-module').data('event-count') > 3) {
  $('.upcoming-event-posts').on('afterChange', function(event, slick, currentSlide){
    $('.event-module-date').removeClass('slick-current');
    $('.upcoming-event-dates').find("[data-slick-index='" + currentSlide + "']").addClass('slick-current');
  }).slick({
    'slidesToShow': 1,
    'slidesToScroll': 1,
    'variableWidth': false,
    'infinite': false,
    'arrows': false,
    'adaptiveHeight': true,
    'rows': 0
  });
  $('.upcoming-event-dates').slick({
    'slidesToShow': 2,
    'slidesToScroll': 2,
    'centerMode': false,
    'variableWidth': true,
    'infinite': false,
    'arrows': true,
    'focusOnSelect': true,
    'asNavFor': '.upcoming-event-posts',
    'prevArrow' : $('.event-module-arrow.arrow-prev'),
		'nextArrow' : $('.event-module-arrow.arrow-next'),
    'rows': 0
  });
}
}

if($('.share-post-link').length) {
$('.share-post-link').on('click', function(e) {
  e.preventDefault();
  $('#share-post-screen').toggleClass('active');
  $('.share-post-link').toggleClass('active');
});
}

if($('.list-slider').length) {
$('.list-slider-container').each(function() {
  $('.list-slider', this).slick({
    'slidesToShow': 2,
    'slidesToScroll': 2,
    'centerMode': false,
    'variableWidth': false,
    'infinite': false,
    'arrows': true,
    'prevArrow' : $('.list-slider-arrow.arrow-prev', this),
		'nextArrow' : $('.list-slider-arrow.arrow-next', this),
    'rows': 0
  });
})
}

if($('#history-year-images').length) {
$('body').addClass('history-slider');
let $yearImages = $('#history-year-images').slick({
  'slidesToShow': 1,
  'slidesToScroll': 1,
  'infinite': false,
  'arrows': false,
  'rows': 0,
  'swipe': false,
  'touchMove' : false
});

$('#history-years').on('beforeChange', function(event, slick, currentSlide, nextSlide){
  if($('.history-year-'+nextSlide).data('has-image') === 1) {
    var yearIndex = $('.year-slide-'+nextSlide).data('slick-index');
    $yearImages.slick('slickGoTo', yearIndex);
  }
}).slick({
  'slidesToShow': 2,
  'slidesToScroll': 1,
  'infinite': false,
  'arrows': true,
  'prevArrow' : $('.history-slider-arrow.arrow-prev'),
  'nextArrow' : $('.history-slider-arrow.arrow-next'),
  'rows': 0,
  'responsive': [
    {
      'breakpoint': 600,
      'settings': {
        'slidesToShow': 1,
        'slidesToScroll': 1
      }
    }
  ]
});
}

if($('#table-of-contents').length) {
$(window).on('resize scroll load', function() {
  $('.instruction').each(function() {
    if($(this).isInViewport()) {
      var anchorlink = $(this).data('inst');
      $('.anchor-link.active').removeClass('active');
      if(!$('.anchor-link-'+anchorlink).hasClass('active')) {
        $('.anchor-link-'+anchorlink).addClass('active');
      }
    }
  });
});
$('.anchor-link').on('click', function(e) {
  e.preventDefault();
  var anchorlink = $(this).data('anchor');
  $('html,body').animate({
    scrollTop: $('.inst-'+anchorlink).offset().top - 100
  }, 1000);
});
}

if($('#ion-modules').length) {
$('.ion-module').each(function(i, ionmodule) {
  showVisibleBlock(ionmodule);
});
$(window).on('scroll', function() {
  $('.ion-module').each(function(i, ionmodule) {
    showVisibleBlock(ionmodule);
  });
});
}

function showVisibleBlock($item) {
if($($item).isInViewport()) {
  $($item).addClass('active');
} else {
}
}



if($('.wp-block-gallery').length) {
$('.page-content .wp-block-gallery').each(function(index, gallery) {
  $(gallery).append('<div class="gallery-arrows"><a href="#" class="circle-arrow arrow-prev"></a><a href="#" class="circle-arrow arrow-next"></a></div>');
  $(gallery).slick({
    'slide' : '.blocks-gallery-item',
    'arrows': true,
    'slidesToShow': 1,
    'slidesToScroll': 1,
    'adaptiveHeight': true,
    'fade': true,
    'autoplay': false,
    'infinite': true,
    'prevArrow' : $('.arrow-prev', gallery),
		'nextArrow' : $('.arrow-next', gallery),
    'rows': 0
  });
});
}

if($('.wp-block-image-comparison-block-image-comparison').length) {
$('.wp-block-image-comparison-block-image-comparison').each(function() {
  $('.comparison-image-2', this).each(function() {
    compareImages(this);
  });
});
}

// var x, i;
// /* Find all elements with an "overlay" class: */
// x = document.getElementsByClassName("img-comp-overlay");
// for (i = 0; i < x.length; i++) {
//   /* Once for each "overlay" element:
//   pass the "overlay" element as a parameter when executing the compareImages function: */
//   compareImages(x[i]);
// }
function compareImages(img) {
var slider, img, clicked = 0, w, h;
/* Get the width and height of the img element */
w = img.offsetWidth;
h = img.offsetHeight;
/* Set the width of the img element to 50%: */
img.style.width = (w / 2) + "px";
/* Create slider: */
slider = document.createElement("DIV");
slider.setAttribute("class", "img-comp-slider");
/* Insert slider */
img.parentElement.insertBefore(slider, img);
/* Position the slider in the middle: */
slider.style.top = (h / 2) - (slider.offsetHeight / 2) + "px";
slider.style.left = (w / 2) - (slider.offsetWidth / 2) + "px";
/* Execute a function when the mouse button is pressed: */
slider.addEventListener("mousedown", slideReady);
/* And another function when the mouse button is released: */
window.addEventListener("mouseup", slideFinish);
/* Or touched (for touch screens: */
slider.addEventListener("touchstart", slideReady);
 /* And released (for touch screens: */
window.addEventListener("touchstop", slideFinish);
function slideReady(e) {
  /* Prevent any other actions that may occur when moving over the image: */
  e.preventDefault();
  /* The slider is now clicked and ready to move: */
  clicked = 1;
  /* Execute a function when the slider is moved: */
  window.addEventListener("mousemove", slideMove);
  window.addEventListener("touchmove", slideMove);
}
function slideFinish() {
  /* The slider is no longer clicked: */
  clicked = 0;
}
function slideMove(e) {
  var pos;
  /* If the slider is no longer clicked, exit this function: */
  if (clicked == 0) return false;
  /* Get the cursor's x position: */
  pos = getCursorPos(e)
  /* Prevent the slider from being positioned outside the image: */
  if (pos < 0) pos = 0;
  if (pos > w) pos = w;
  /* Execute a function that will resize the overlay image according to the cursor: */
  slide(pos);
}
function getCursorPos(e) {
  var a, x = 0;
  e = e || window.event;
  /* Get the x positions of the image: */
  a = img.getBoundingClientRect();
  /* Calculate the cursor's x coordinate, relative to the image: */
  x = e.pageX - a.left;
  /* Consider any page scrolling: */
  x = x - window.pageXOffset;
  return x;
}
function slide(x) {
  /* Resize the image: */
  img.style.width = x + "px";
  /* Position the slider: */
  slider.style.left = img.offsetWidth - (slider.offsetWidth / 2) + "px";
}
}

// Cookies alert

if($('#cookie-alert-cta').length) {
if (document.cookie.length > 0) {
  if(document.cookie.indexOf("cookie_alert=") == -1) {
    $('#cookie-alert').slideDown();
  }
}
$('#cookie-alert-cta').on('click', function(e) {
  e.preventDefault();
  $('#cookie-alert').slideUp();
  var date = new Date();
  date.setTime(date.getTime()+365*24*60*60*1000); // ) removed
  var expires = '; expires=' + date.toGMTString(); // + added
  document.cookie = 'cookie_alert=1' +expires + ';path=/';
  //$.cookie('cookie_alert', 1, { expires: 365}, '/')
  $.ajax({
    url: ajaxurl,
    method: 'post',
    type: 'json',
    data: {
      'action': 'do_ajax',
      'fn' : 'set_cookie',
      'name' : 'cookie_alert'
    }
  }).done( function (response) {

  });
});
}

// Sidebar Scroll
if($('.side-scroll-nav').length) {
  $(window).on('scroll', function(e) {
    var scroll = $(window).scrollTop();
    var arrowsScroll = $('.nav-arrows').position().top;
    //var scrollHeight = $('.side-scroll-nav-content').height();
    //var arrowsHeight = $('.nav-arrows').height();
    var lastItemTop = $('.side-scroll-item:last-of-type').position().top;
    //var lastItemHeight = $('.side-scroll-item:last-of-type').height();
    if(arrowsScroll <= 0 && !$('.nav-arrow-up').hasClass('inactive')) {
      $('.nav-arrow-up').addClass('inactive');
    }
    if(arrowsScroll >= lastItemTop && !$('.nav-arrow-down').hasClass('inactive')) {
      $('.nav-arrow-down').addClass('inactive');
    }
    if(arrowsScroll > 0 && arrowsScroll < lastItemTop) {
      $('.nav-arrow.inactive').removeClass('inactive');

    }
  });

  $('.nav-arrow-up').on('click', function() {
    $($('.side-scroll-item').get().reverse()).each(function(i) {
      console.log($(this).offset().top - $('.site-header').height(), $('.nav-arrows').position().top)
      if($(this).position().top - $('.site-header').height() < $('.nav-arrows').position().top) {
        console.log('clicked')
        let scrollTo = i == ($('.side-scroll-item').length - 1) ? $(this).position().top - 200 : $(this).position().top - $('.site-header').height() - 100
        $('html,body').animate({
          scrollTop: scrollTo
        }, 400);
        return false;
      }
    });
  });

  $('.nav-arrow-down').on('click', function() {
    $('.side-scroll-item').each(function() {
      if($(this).position().top > $('.nav-arrows').position().top) {
        $('html,body').animate({
          scrollTop: $(this).offset().top - $('.site-header').height()
        }, 400);
        return false;
      }
    });
  });
}

if ( $('.show-more-filter-items').length ) {
  $('.show-more-filter-items').on('click', function() {
    $('.more-filter-items', $(this).parent()).slideToggle('fast')
    $(this).toggleClass('active')
    return false
  })
}
