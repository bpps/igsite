import $ from 'jquery';
document.body.className = document.body.className.replace("no-js","js");

if($('.page-template-collateral').length) {
  $('#ion-smart-crumbs').append(`
    <smartcrumb>
      <div class="collateral-toolbar">
        <div class="collateral-count">
          Documents in my list <span class="doc-count">(0)</span>
        </div>
        <a class="view-my-collateral">
          View My List
        </a>
        <a class="email-my-collateral">
          <img src=""> Email my list
        </a>
      </div>
    </smartcrumb>
  `);
  var myList = [];
  $('.email-my-collateral').on('click', function(e) {
    e.preventDefault();
    $('.col-resources-email-popup').show();
  });
  $('.col-resources-submit').on('click', function(e) {
    e.preventDefault();
    var validForm = true;
    $('.col-resources-name.error').removeClass('error');
    $('.col-resources-email.error').removeClass('error');
    if($('.col-resources-name').val().length == 0) {
      validForm = false;
      $('.col-resources-name').addClass('error');
    }
    if($('.col-resources-email').val().length === 0 || !isEmail($('.col-resources-email').val())) {
      validForm = false;
    }
    if(!validForm) {
      return;
    }
    var collateral = [];
    $.each(myList, function(i, item) {
      collateral.push($(item).data('url'));
    });
    $.ajax({
      url: ajaxurl,
      method: 'post',
      type: 'json',
      data: {
        'action': 'do_ajax',
        'fn' : 'send_collateral',
        'collateral' : collateral,
        'name' : $('.col-resources-name').val(),
        'email' : $('.col-resources-email').val()
      }
    }).done( function (response) {
      //$('#searchform .ion-loader-container').removeClass('active');
      if(response) {
        // Clear the email and remove all selections etc.
        collateral = [];
        $('.col-resource.selected').removeClass('selected');
        $('.col-resources-name').val('');
        $('.col-resources-email').val('');
        $('.col-resources-email-popup').hide();
        $('.doc-count').html('0');
      }
    });
  });
  $('.col-resource').on('click', function(e) {
    e.preventDefault();
    $(this).toggleClass('selected');
    let resourceID = $(this).data('colid');
    if($(this).hasClass('selected')) {
      myList.push($(this));
      $('.add-remove-col', this).html('X Remove from my list');
    } else {
      $('.add-remove-col', this).html('+ Add to my list');
      myList = $.grep(myList, function(value) {
        return $(value).data('colid') != resourceID;
      });
    }
    $('.doc-count').html(`(${myList.length})`);
    if(myList.length > 0 && !$('.view-my-collateral').hasClass('active')) {
      $('.view-my-collateral, .email-my-collateral').addClass('active');
    }
    if(myList.length == 0) {
      $('.view-my-collateral, .email-my-collateral').removeClass('active');
    }
  });
}

function isEmail(email) {
  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  return regex.test(email);
}
