import { PERFORM_SEARCH } from '../actions/search'

export const searchQuery = (state = null, action) => {
  switch(action.type) {
    case PERFORM_SEARCH:
      return action.search
    default:
      return state
  }
}
