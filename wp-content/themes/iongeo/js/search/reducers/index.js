import { combineReducers } from 'redux'
import { searchQuery } from './search'

export default combineReducers({
  searchQuery
})
