export const PERFORM_SEARCH = 'PERFORM_SEARCH'

export const performSearch = search => {
  return {
    type: SELECT_EVENT,
    search
  }
}
