import React from 'react'
import SearchBar from './components/SearchBar'

function App(props) {
  return (
    <div className="App">
      <SearchBar/>
    </div>
  );
}

export default App
