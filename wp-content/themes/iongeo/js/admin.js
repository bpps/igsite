import $ from 'jquery';

$(document).on('change', '.acf-field-taxonomy input[type="radio"]', function(){
  var taxID = $(this).val();
  var $parent = $(this).closest('.acf-field-taxonomy');
  var $relationshipField = $($parent).nextAll('div.acf-field-relationship').not(".hidden-by-conditional-logic");
  $.ajax({
    url: ajaxurl,
    data: {
      action: 'do_ajax',
      fn: 'get_acf_tax',
      tax: taxID
    }
  }).done( function (response) {
    response = $.parseJSON(response);
    if(response) {
      $(`.filter option[value="${response.taxonomy}:${response.slug}"]`, $relationshipField[0]).prop('selected', true);
    }
  });
});
