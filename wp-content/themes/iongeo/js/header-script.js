import $ from 'jquery';
var menuWidthLimit = 900;
var hovers = [];
get_all_menu_slides();
var allMenuSlides = false;
function get_all_menu_slides() {
  $.ajax({
    url: ajaxurl,
    method: 'post',
    type: 'json',
    data: {
      'action': 'do_ajax',
      'fn' : 'get_all_menu_slides'
    }
  }).done( function (response) {
    allMenuSlides = $.parseJSON(response);
    $('#navigation-container .ion-loader-container').removeClass('active');
  });
}

$('body').click(function(evt){
   if(evt.target.id == "menu" || evt.target.id == "slide")
      return;
   //For descendants of menu_content being clicked, remove this check if you do not want to put constraint on descendants.
   if($(evt.target).closest('#menu').length || $(evt.target).closest('#slide').length)
      return;
  //Do processing of click event here for every element except with id menu_content
  if($('#menu-content').hasClass('active')) {
    $('#menu-content').removeClass('active');
  }
});
var currentMenuId = false;

$('#site-navigation, #content').on('mouseenter', function(evt) {
  if(hovers.length == 0) {
    hovers.push('menu');
  }
});

$('.menu-item.has-modules').on('click', function(e) {
  if($(window).width() < menuWidthLimit) {
    e.preventDefault();
    openMenuDrawer(this);
  }
});

$('.menu-item.has-modules').on('mouseenter', function(evt) {
  if($(window).width() < menuWidthLimit) {
    return;
  }
  if(hovers.length == 0) {
    return;
  }
  openMenuDrawer(this);
});

$('.menu-item').on('mouseleave', function(evt) {
  if($(window).width() < menuWidthLimit) {
    return;
  }
  if($("#navigation-container").hasClass('mobile-active')) {
    return;
  }
  if(evt.toElement && evt.toElement.className != 'has-modules'
    && evt.toElement.id != 'navigation-slide'
    ) {
    if($('#navigation-container').hasClass('active')) {
      $('#navigation-container').removeClass('active');
    }
  }
});

$('#navigation-container').on('mouseleave', function(evt) {
  if($("#navigation-container").hasClass('mobile-active')) {
    return;
  }
  if($('#navigation-container').hasClass('active')) {
    $('#navigation-container').removeClass('active');
  }
});

$('.menu-item.has-modules').on('click', function(evt) {
  if($("#navigation-container").hasClass('mobile-active')) {
    if(hovers.length == 0) {
      return;
    }
    return false;
    currentMenuId = $(this).data('menuid');
    let menuItem = menuItems.find(function (obj) { return obj.id === currentMenuId.toString(); });
    if(allMenuSlides[currentMenuId]) {
      $('#navigation-slide').html(allMenuSlides[currentMenuId]);
    }
    if(!$('#navigation-container').hasClass('active')) {
      $('#navigation-container').addClass('active');
    }
  }
});
let menuObjs = {}
function openMenuDrawer(item) {
  currentMenuId = $(item).data('menuid');
  let menuColor = '#60604b';
  if($(item).data('menucol')) {
    menuColor = $(item).data('menucol');
  }
  let menuItem = menuItems.find(function (obj) { return obj.id === currentMenuId.toString(); });
  $('#menu-color').css('background-color', menuColor);
  if(allMenuSlides[currentMenuId]) {
    $('#navigation-slide').html(allMenuSlides[currentMenuId]);
  }
  if(!$('#navigation-container').hasClass('active')) {
    $('#navigation-container').addClass('active');
  }
}

$('#main-menu-button').on('click', function(evt) {
  if($('#navigation-container').hasClass('active')) {
    $('#navigation-container').removeClass('active');
  }
});

$(' .hamburger').click(function() {
  $(this).toggleClass("is-active");
  $("#navigation-container").toggleClass("mobile-active").removeClass("active");
  $("header").toggleClass("mobile-active");
  $("body").toggleClass('menu-active');
  hovers.push('menu');
});

if (!Array.prototype.find) {
  Array.prototype.find = function(predicate) {
    if (this == null) {
      throw new TypeError('Array.prototype.find called on null or undefined');
    }
    if (typeof predicate !== 'function') {
      throw new TypeError('predicate must be a function');
    }
    var list = Object(this);
    var length = list.length >>> 0;
    var thisArg = arguments[1];
    var value;

    for (var i = 0; i < length; i++) {
      value = list[i];
      if (predicate.call(thisArg, value, i, list)) {
        return value;
      }
    }
    return undefined;
  };
}
