import $ from 'jquery';
// More code using $ as alias to jQuery
$('.search-overlay-background').on('click', function() {
  closeSearchOverlay();
});
$('.search-icon').on('click', function(e){
  e.preventDefault();
  $('header.site-header').addClass('search-active');
  $('.search-field').focus();
  $('.search-overlay-background').addClass('active');
  setTimeout(function() {
    $('body').bind('click', function(evt){
      // console.log(evt);
      // if(evt.target.id != "s" && evt.target.id != "search-container") {
      //   closeSearchOverlay();
      // }
    });
  }, 500);
});
$('.search-close').on('click', function(e){
  e.preventDefault();
  closeSearchOverlay();
});
function closeSearchOverlay() {
  $('header.site-header').removeClass('search-active');
  $('.search-overlay-background').removeClass('active');
  setTimeout(function() {
    $('.site-header .search-results-container').html('');
    $('.search-field').val('')
  }, 100)
  $('body').unbind('click');
}
$('#searchform').on('submit', function(e) {
  e.preventDefault();
});
$('.search-field').on('input', function() {
  let searchVal = $('.search-field').val();
  if(searchVal.length > 2) {
    $('#searchform .ion-loader-container').addClass('active');
    $.ajax({
      url: ajaxurl,
      method: 'post',
      type: 'json',
      data: {
        'action': 'do_ajax',
        'fn' : 'autosearch',
        'search' : searchVal
      }
    }).done( function (response) {
      $('#searchform .ion-loader-container').removeClass('active');
      let searchResponse = response ? response : '<p class="no-results">Sorry, no results. Try searching again.</p>';
        $('header.site-header .search-results-container').html(searchResponse);
        $('.search-list-arrows .circle-arrow').on('click', function(e) {
          e.preventDefault();
          var paged = $('.search-list-arrows').data('paged');
          var arrowDirection = parseInt($(this).data('direction'));
          var listpage = paged + arrowDirection;
          var searchVal = $('.search-list-arrows').data('search');
          $('.search-list-arrows').data('paged', listpage);
          $('.search-list-arrows .circle-arrow').addClass('inactive');
          getMoreSearch(listpage, searchVal)
        });
    });
  }
});

function getMoreSearch(paged, search) {
  $.ajax({
    url: ajaxurl,
    method: 'post',
    type: 'json',
    data: {
      'action': 'do_ajax',
      'fn': 'getsearch',
      'paged': paged,
      'search': search
    }
  }).done(function (response) {
    response = $.parseJSON(response);
    if(response) {
      $('header.site-header .search-results-inner').html(response.posts);
      if (response.maxpages != paged) {
        // make next button inactive
        $('header.site-header .search-list-arrows .arrow-next').removeClass('inactive');
      }
      if (paged != 1) {
        $('header.site-header .search-list-arrows .arrow-prev').removeClass('inactive');
      }
    }
  });
}
