<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package Iongeo
 */

get_header();
$actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$query404 = explode('/', $actual_link);
$query404 = $query404[count($query404) - 1];
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<?php
			if($searchResults = get_autosearch_results($query404, false)) { // Attempt to search for a page
				$image = false;
				$title = get_field('404_results_title', 'option');
				$excerpt = get_field('404_results_excerpt', 'option');
				$image = get_field('404_results_image', 'option');
				$image = [
					'large' => $image['sizes']['large'],
					'small' => $image['sizes']['small']
				];
				$header_args =
				$header = ion_page_header([ 'fimg' => $image, 'excerpt' => $excerpt, 'title' => $title, 'frompage' => '404']);
				echo $header->header; ?>
				<div class="content-inner error404-container">
					<div class="search-results-container error404-content">
						<?php echo $searchResults; ?>
					</div>
				</div>
			<?php
			} else { // if nothing turns up. ?>
				<section class="error-404 not-found">
					<?php
					$image = false;
					$title = get_field('404_title', 'option');
					$excerpt = get_field('404_excerpt', 'option');
					$image = get_field('404_image', 'option');
					$image = [
						'large' => $image['sizes']['large'],
						'small' => $image['sizes']['small']
					];
					$header = ion_page_header([ 'fimg' => $image, 'excerpt' => $excerpt, 'title' => $title, 'frompage' => '404 no results']);
					echo $header->header; ?>
				</section><!-- .error-404 -->
			<?php
			} ?>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
