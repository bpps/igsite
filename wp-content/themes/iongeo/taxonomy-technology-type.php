<?php
/**
 * The template for displaying technology taxonomy pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Iongeo
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<?php
			$termslug = get_query_var( 'term' );
			$term = get_term_by( 'slug', $termslug, 'technology-type');
			$fImg = get_field('featured_image', $term);
			$headerStyle = 'standard';
			$headerImg = false;
			$sidebar_image = get_field('sidebar_image', $term) ? get_field('sidebar_image', $term) : false;
			if($fImg) {
				$headerStyle = 'full-width-image bg-centered';
				$headerImg = true;
			}
			?>

      <header id="page-header" class="<?php echo $headerStyle; ?>"<?php echo $fImg ? ' style="background-image:url('.$fImg['sizes']['large'].');"' : ''; ?>>
				<?php
				if($headerImg) { ?>
					<div class="temp-header-image bg-centered" style="background-image:url(<?php echo $fImg['sizes']['small']; ?>);">
					</div>
					<div class="header-image-cover"></div>
				<?php
				} ?>
				<div id="page-header-title" class="content-inner">
					<div class="page-header-title-content">
						<h1><?php echo $term->name; ?></h1>
					</div>
					<?php
					if($termDesc = $term->description) { ?>
						<div class="header-description">
					<?php echo apply_filters('the_content', $termDesc); ?>
						</div>
					<?php
					} ?>
				</div>
			</header>
			<?php echo get_share_link('ION Geo Technologies | '.$term->name, get_term_link($term)); ?>
			<section class="page-content-wrapper content-inner<?php echo is_active_sidebar( 'technology-type-sidebar' ) ? ' has-sidebar' : ''; ?>">
				<?php
				// If there's content for the taxonomy
				if($termContent = get_field('content', $term)) { ?>
					<div class="page-content-container flex">
						<div class="page-content col-8">
							<?php echo $termContent; ?>
						</div>
						<?php
						if ( is_active_sidebar( 'technology-type-sidebar' ) ) :
							custom_sidebar( 'technology-type-sidebar', $sidebar_image['sizes']['medium']);
						endif; ?>
					</div>
				<?php
				} ?>
				<?php echo by_the_numbers($term); ?>
				<section id="tech-type-children-container">
					<?php
					$additional_techs = null;
					if ( $custom_techs = get_field('custom_related_technologies', $term ) ) {
						ob_start();
						foreach($custom_techs as $tech) {
							$termObj = (object)[
								'title' => $tech['link']['title'],
								'link' => $tech['link']['url']
							];
							if( $tech['image'] ) {
								$termObj->image = $tech['image']['sizes']['small-medium'];
							}
							echo create_child_technology($termObj);
						}
						$additional_techs = ob_get_clean();
					}
					if($subTerms = get_terms(['taxonomy' => 'technology-type', 'parent' => $term->term_id, 'hide_empty' => false])) { ?>
						<h2>Technologies</h2>
						<div class="tech-type-children flex row">
							<?php
							foreach($subTerms as $sTerm) {
								$termObj = (object)[
									'title' => $sTerm->name,
									'link' => get_term_link($sTerm, 'technology-type')
								];
								if($image = get_field('featured_image', $sTerm)) {
									$termObj->image = $image['sizes']['small-medium'];
								}
								echo create_child_technology($termObj);
							}
							if ( $selected_techs = get_field('related_technologies', $term) ) {
								foreach($selected_techs as $tech) {
						      $termObj = (object)[
						        'title' => $tech->post_title,
						        'link' => get_the_permalink($tech->ID)
						      ];
						      if(has_post_thumbnail($tech->ID)) {
						        $termObj->image = get_the_post_thumbnail_url($tech->ID, 'small-medium');
						      }
						      if(has_excerpt($tech->ID)) {
						        $termObj->desc = get_the_excerpt($tech->ID);
						      }
						      echo create_child_technology($termObj);
						    }
							}
							if ( $additional_related_techs ) {
					      echo $additional_related_techs;
					    } ?>
						</div>
					<?php
					} else {
						// If no child taxonomy this is the last lowest 'depth' of tech type,
						// look for pages with the current taxonomy

						// First look for manual override of tech types
						$techs = get_field('related_technologies', $term);
						$techgroups = get_field('related_technology_group', $term);
						if( $techs != null || $techgroups != null ) { ?>
							<h3>Technologies</h3>
							<?php
							// Manual override exists. Display these related technologies and technology groups
							// if first level of related technologies
							if($techs) {
								echo get_child_technology_by_relationship($techs, false, $additional_techs);
							}
							if($techgroups) {
								foreach($techgroups as $techgroup) {
									echo get_child_technology_by_relationship($techgroup['related_technologies'], $techgroup['technology_group_name'], $additional_techs);
								}
							}
						} else {
							// No manual override, get all related technologies
							$args = array(
						    'post_type' => 'technology',
						    'posts_per_page' => -1,
								'orderby' => 'menu_order',
								'order' => 'ASC',
						    'tax_query' => [
						      [
						        'taxonomy' => 'technology-type',
						        'field' => 'slug',
						        'terms' => $termslug
						      ]
						    ]
						  );
						  $taxPosts = new WP_Query($args);
						  if($taxPosts->have_posts()) : ?>
								<h2>Technologies</h2>
								<div class="tech-type-children flex row">
							    <?php
									while($taxPosts->have_posts()): $taxPosts->the_post();
										$termObj = (object)[
											'title' => get_the_title(),
											'link' => get_the_permalink()
										];
										if(has_post_thumbnail()) {
											$termObj->image = get_the_post_thumbnail_url(get_the_id(), 'small-medium');
										}
										if(has_excerpt()) {
											$termObj->desc = get_the_excerpt();
										}
										echo create_child_technology($termObj);
							    endwhile;
									if ( $additional_related_techs ) {
							      echo $additional_related_techs;
							    }
							    wp_reset_postdata(); ?>
								</div>
							<?php
						  endif;
						}
					} ?>
				</section>
			</section>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
