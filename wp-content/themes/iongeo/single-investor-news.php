<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Iongeo
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

		<?php
		while ( have_posts() ) : the_post();
			$content = get_the_content();
			$headerImage = false;
			$sidebar_image = get_field('sidebar_image') ? get_field('sidebar_image') : false;
			if(get_post_type() == 'investor-news') {
				if($investorHeader = get_field('global_investors_header_image', 'option')) {
					$headerImage = ['large' => $investorHeader['sizes']['large'], 'small' => $investorHeader['sizes']['small']];
				}
			}
			$header = ion_page_header([ 'fimg' => $headerImage, 'frompage' => 'single', 'excerpt' => false ]);
			echo $header->header;
			if(has_post_thumbnail()) { ?>
				<div class="content-inner full-width-page">
					<div class="ratio-3-1 bg-centered" style="background-image:url(<?php echo get_the_post_thumbnail_url(get_the_ID(), 'large'); ?>);">
					</div>
				</div>
			<?php
			} ?>
			<?php echo get_share_link(get_the_title(), get_the_permalink()); ?>
			<section class="page-content-wrapper content-inner<?php echo is_active_sidebar( 'investor-news-sidebar' ) ? ' has-sidebar' : ''; ?>">
				<div class="page-content-container reverse-container flex">
					<?php
					custom_sidebar('investor-news-sidebar', $sidebar_image['sizes']['medium']); ?>
					<div class="page-content col-8">
						<?php
						echo apply_filters('the_content', $content);
						if ( !has_shortcode( $content, 'content-tabs' ) ) {
							echo content_tabs();
						}
						if($techs = get_field('post_x_technology')) { ?>
							<div id="related-technologies" class="post-row">
								<h4>Related Technologies</h4>
								<div class="post-row-content column-count-2 flex row">
									<?php
									foreach($techs as $tech) { ?>
										<div class="post-row-item col-6">
											<?php
											$image = has_post_thumbnail($tech->ID) ? get_the_post_thumbnail_url($tech->ID, 'small-medium') : get_template_directory_uri().'/images/post-placeholder.png'; ?>
					            <a href="<?php echo get_the_permalink($tech->ID); ?>" class="post-row-image-container ratio-image-container ratio-3-2">
					              <div class="post-row-image bg-centered" style="background-image:url('<?php echo $image; ?>');">

					              </div>
					            </a>
					            <h6 class="post-row-title with-arrow">
					              <a href="<?php echo get_the_permalink($tech->ID); ?>">
					                <?php echo get_the_title($tech->ID); ?>
					              </a>
					            </h6>
										</div>
									<?php
									} ?>
							</div>
						<?php
						} ?>
					</div>
				</div>
			</section>
			<?php
			echo get_footer_cta('default');
		endwhile; // End of the loop.
		?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
