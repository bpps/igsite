<?php
/**
 * The template for displaying Data Library page
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Iongeo
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

		<?php
		while ( have_posts() ) : the_post();
			$pageLayout = get_field('page_layout');
			$header = ion_page_header(['frompage' => 'data library']);
			echo $header->header;
      echo get_share_link('ION Geo | '.get_the_title(), get_the_permalink()); ?>
			<section class="page-content-wrapper">
				<?php echo get_share_link(get_the_title(), get_the_permalink(), true); ?>
				<div class="page-content-container full-width-page <?php echo $header->image ? 'has-header-image ' : ''; ?>">
					<div class="content-inner">
						<div class="page-content-container">
							<div class="page-content">
								<?php
								$mapObj = (object)['title' => 'Data Library'];
								echo get_data_library_map($mapObj); ?>
							</div>
						</div>
					</div>
				</div>
			</section>
			<?php
			echo get_footer_cta('default');
		endwhile; // End of the loop.
		?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
