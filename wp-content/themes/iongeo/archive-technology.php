<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Iongeo
 */

 get_header();
 ?>

 	<div id="primary" class="content-area">
 		<main id="main" class="site-main">
 			<header id="page-header">
 				<div id="page-header-title" class="content-inner">
          <div class="page-header-title-content">
 					  <h1>Technologies</h1>
          </div>
 				</div>
 			</header>
      <?php
      $postType = get_queried_object();
      echo get_share_link('ION Geo | '.$postType->label, get_post_type_archive_link( $postType->name )); ?>
 			<section class="page-content-wrapper">
 				<div class="page-content-container has-sidebar full-width-page">
          <div class="content-inner">
 						<div id="tech-landing" class="has-side-scroll">
 							<?php
 							if($technologies = get_terms([
 								'taxonomy' => 'technology-type',
 								'hide_empty' => false,
 								'parent' => 0
 							])) { ?>
 								<?php side_scroll_nav('technologies'); ?>
 							<?php
 							$featuredTechs = get_field('featured_technologies', 'option');
 							foreach($technologies as $tech) {
 								$techParent = $tech->term_id;
 								$techCount = 0; ?>
 								<div class="tech-type side-scroll-item has-notch">
 									<div class="tech-items">
 										<h2><?php echo $tech->name; ?></h2>
                    <div class="tech-items-wrapper flex">
   										<?php
   										$args = [
   											'posts_per_page' => 1,
   											'post_type' => 'technology',
   											'post__in' => $featuredTechs,
   											'tax_query' => [
   												[
   													'taxonomy' => 'technology-type',
   													'field' => 'id',
   													'terms' => $tech->term_id
   												]
   											]
   										];
   										// Get featured post if it exists
   										$techPosts = new WP_Query($args);
   									  if($techPosts->have_posts()) :
   									    while($techPosts->have_posts()): $techPosts->the_post(); ?>
   									      <div class="tech-item featured-post col-5">
   													<h5>
   										        <a href="<?php echo get_the_permalink(); ?>">
   										          <?php the_title(); ?>
   										        </a>
   													</h5>
   													<?php
   													if(has_post_thumbnail()) {
   														$image = get_the_post_thumbnail_url(get_the_id(), 'small-medium'); ?>
   														<a class="tech-image-container" href="<?php echo get_the_permalink(); ?>">
   															<div class="tech-image bg-centered" style="background-image:url(<?php echo $image; ?>);">
   															</div>
   														</a>
   													<?php
   													} ?>
   													<h6 class="featured-post-label">Featured</h6>
   									      </div>
   									    <?php
   											$techCount++;
   									    endwhile;
   									    wp_reset_postdata();
   									  endif;
   										// Get sub technologies
   										if($subtechs = get_terms([
   											'taxonomy' => 'technology-type',
   											'hide_empty' => false,
   											'parent' => $techParent,
   											'number' => 3 - $techCount
   										])) {
   											foreach($subtechs as $tech) {
   												$termLink = get_term_link($tech); ?>
   												<div class="tech-item col-3">
   													<h5>
   										        <a href="<?php echo $termLink; ?>">
   										          <?php echo $tech->name; ?>
   										        </a>
   													</h5>
   													<?php
   													if($image = get_field('featured_image', $tech)) {
   														$image = $image['sizes']['small-medium']; ?>
   														<a class="tech-image-container" href="<?php echo $termLink; ?>">
   															<div class="tech-image bg-centered" style="background-image:url(<?php echo $image; ?>);">
   															</div>
   														</a>
   													<?php
   													}
   													if($description = $tech->description) { ?>
   														<div class="tech-description">
   															<?php echo $description; ?>
   														</div>
   													<?php
   													} ?>
   									      </div>
   											<?php
   											}
   										} ?>
   									</div>
                  </div>
 								</div>
 							<?php
 							} ?>
 						</div>
          </div>
 					<?php
 					} ?>

 				</div>
 			</section>

 		</main><!-- #main -->
 	</div><!-- #primary -->

 <?php
 get_footer();
