<?php
/**
 * The template for displaying  Resource Archive
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Iongeo
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<header id="page-header">
				<div id="page-header-title" class="content-inner">
					<div class="page-header-title-content">
						<h1>Resources</h1>
					</div>
				</div>
			</header>
			<?php
      $postType = get_queried_object();
      echo get_share_link('ION Geo | '.$postType->label, get_post_type_archive_link( $postType->name )); ?>
			<section class="page-content-wrapper">
				<div class="page-content-container full-width-page">
					<div class="content-inner">
						<div class="page-content-container">
							<div class="resources-grid has-side-scroll">
								<?php side_scroll_nav('resources'); ?>
								<?php
								$featuredResources = get_field('featured_resources', 'option');
								if($featuredResources) { ?>
									<div class="featured-resources-container has-notch side-scroll-item">
										<h3>Featured</h3>
										<div class="featured-posts featured-posts-slider">
											<?php
											foreach($featuredResources as $resource) {
												$pdf = get_field('upload_pdf', $resource);
												$link = $pdf ? $pdf['url'] : get_the_permalink();
												$target = $pdf ? '_blank' : '_self';
												$thumb = $pdf ? wp_get_attachment_image_src($pdf['ID'], 'medium')[0] : get_the_post_thumbnail_url($resource, 'small-medium');
												if(!$thumb) {
													$thumb = get_template_directory_uri().'/images/post-placeholder.png';
												} ?>
					              <div class="featured-post">
													<div class="flex">
														<div class="filtered-post-thumb-container col-6">
															<div class="filtered-post-thumb ratio-3-2" style="background-image: url('<?php echo $thumb; ?>')">
															</div>
														</div>
						                <div class="filtered-post-content col-6">
															<div class="filtered-post-date">
																<?php
																$date = get_the_date();
																$month = date("M", strtotime($date));
																$day = date("d", strtotime($date));
																$year = date("Y", strtotime($date));
																echo $month.'</br>'.$day;
																?>
							                </div>
															<div class="resource-meta">
							                  <h4>
																	<a href="<?php echo $link; ?>" target="<?php echo $target; ?>">
																		<?php echo get_the_title($resource); ?>
																	</a>
																</h4>
																<?php
																if( get_the_excerpt($resource) ) { ?>
																	<div class="resource-content" style="margin-top: 1em;">
																		<?php echo get_the_excerpt($resource); ?>
																	</div>
																<?php
																}
																if($authors = get_the_terms(get_the_id(), 'resource-author')) { ?>
																	<span style="margin-top: 1em; display: block;">By
																		<?php
																		foreach( $authors as $key=>$author ) { ?>
																			<a class="resource-author" href="<?php echo home_url('resources/resources/?resource-author='.$author->slug); ?>">
																				<?php echo $author->name; echo ($key != count($authors) - 1) ? ', ' : ''; ?>
																			</a>
																		<?php
																		} ?>
																	</span>
																<?php
																} ?>
						                	</div>
						              	</div>
													</div>
												</div>
											<?php
											} ?>
										</div>
										<div class="featured-posts-arrows">
							        <a href="#" class="circle-arrow arrow-prev" data-func="prev">
							        </a>
							        <a href="#" class="circle-arrow arrow-next" data-max-pages="" data-func="next">
							        </a>
							      </div>
									</div>
								<?php
								} ?>
								<div class="filtered-resource-types-container has-notch side-scroll-item">
									<h3>Latest Uploads</h3>
									<div class="filtered-resource-types flex justify-space-between">
										<?php
										$resourceTypes = get_terms(['taxonomy' => 'resource-type']);
										foreach($resourceTypes as $rType) {
											$args = [
												'post_type' => 'resource',
												'posts_per_page' => 4,
												'post__not_in' => $featuredResources,
												'tax_query' => [
													[
														'taxonomy' => 'resource-type',
														'field' => 'slug',
														'terms' => $rType->slug
													]
												]
											];
											$resourcePosts = new WP_Query($args);
											if($resourcePosts->have_posts()) : ?>
												<div class="filtered-list-wrapper col-6">
													<div class="filtered-list" data-post-type="resource" data-post-type-title="Resources">
														<div class="filtered-list-header">
															<h4><?php echo $rType->name; ?></h4>
															<a class="more-posts" href="<?php echo get_term_link($rType); ?>">See All</a>
														</div>
														<div class="filtered-list-container">
															<?php
															while($resourcePosts->have_posts()): $resourcePosts->the_post();
																$link = get_field('upload_pdf', get_the_id()) ? get_field('upload_pdf', get_the_id())['url'] : get_the_permalink();
																$target = get_field('upload_pdf', get_the_id()) ? '_blank' : '_self'; ?>
																<div class="filtered-list-post">
																	<div class="filtered-post-date">
																		<?php
																		$date = get_the_date();
																		$month = date("M", strtotime($date));
																		$day = date("d", strtotime($date));
																		$year = date("Y", strtotime($date));
																		echo $month.'</br>'.$day; ?>
																	</div>
																	<a class="filtered-post-title" target="<?php echo $target; ?>" href="<?php echo $link; ?>"><?php the_title(); ?></a>
																	<?php
																	if($authors = get_the_terms(get_the_id(), 'resource-author')) { ?>
																		<a style="white-space: nowrap; flex-shrink: 0;" href="<?php echo home_url('resources/resources/?resource-author='.$authors[0]->slug); ?>">
																			<?php echo $authors[0]->name; echo count($authors) > 0 ? ' +' : ''; ?>
																		</a>
																	<?php
																	} ?>
																</div>
															<?php
															endwhile; ?>
														</div>
													</div>
												</div>
											<?php
											endif;
										} ?>
									</div>
								</div>
								<?php
								if($topics = get_field('featured_resource_topics', 'option')) { ?>
									<div class="featured-topics-container resources-landing-topics has-notch side-scroll-item">
										<h3>Featured Topics</h3>
										<div class="featured-topics">
											<?php
											foreach($topics as $topic) { ?>
												<a class="featured-topic filter-item-link" href="<?php echo home_url('resources/resources/?resource-interest='.$topic->slug); ?>">
													<?php echo $topic->name; ?>
												</a>
											<?php
											} ?>
										</div>
									</div>
								<?php
								} ?>
								<div class="resources-landing-authors has-notch side-scroll-item">
									<h3>Resource by Author</h3>
									<?php
									$objectArgs = [
								    'numberposts' => -1,
								    'post_type' => 'resource',
								  ];
									$objects = get_posts($objectArgs);
								  $objectIds = [];
								  foreach ($objects as $object) {
								    array_push($objectIds, $object->ID);
								  }

									$maxCount = 10;
									if($taxFilters = wp_get_object_terms( $objectIds, 'resource-author', ['orderby' => 'name'] )) {
										$containerType = count($taxFilters) > $maxCount ? 'select' : 'div';
										$filterType = count($taxFilters) > $maxCount ? 'option' : 'a'; ?>
										<<?php echo $containerType; ?> class="resources-authors-container select-to-link">
											<?php
											if(count($taxFilters) > $maxCount) { ?>
												<option disabled selected value> -- Author -- </option>
											<?php
											}
											foreach( $taxFilters as $tax ) { ?>
												<<?php echo $filterType; ?> href="<?php echo home_url('resources/resources/?resource-author='.$tax->slug); ?>" class="filter-item-link filter-item">
													<?php echo $tax->name; ?>
												</<?php echo $filterType; ?>>
											<?php
											} ?>
										</<?php echo $containerType; ?>>
									<?php
									} ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<?php
			echo get_footer_cta('default'); ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
