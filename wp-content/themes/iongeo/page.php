<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Iongeo
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

		<?php
		while ( have_posts() ) : the_post();
			$id = get_the_id();
			$pw = post_password_required();
			$pageLayout = get_field('page_layout');
			$headerImage = false;
			$sidebar_image = get_field('sidebar_image') ? get_field('sidebar_image') : false;
			if(get_the_id() == 12 || wp_get_post_parent_id(get_the_id()) == 12) {
				if($investorHeader = get_field('global_investors_header_image', 'option')) {
					$headerImage = ['large' => $investorHeader['sizes']['large'], 'small' => $investorHeader['sizes']['small']];
				}
			} else {
				if(has_post_thumbnail()) {
					$headerImage = ['large' => get_the_post_thumbnail_url(get_the_ID(), 'large'), 'small' => get_the_post_thumbnail_url('small')];
				}
			}
			$header = ion_page_header([ 'fimg' => $headerImage, 'frompage' => 'page']);
			$content = get_the_content();
			echo $header->header;
      echo get_share_link('ION Geo | '.get_the_title(), get_the_permalink()); ?>
			<section class="page-content-wrapper<?php echo $pw ? ' password-protected' : ''; ?>">
				<div class="page-content-container reverse-container <?php echo $pageLayout.'-page'; echo $header->image ? ' has-header-image ' : ''; echo $pageLayout == 'standard' ? ' has-sidebar' : ''; echo has_shortcode( $content, 'filtered-post-list' ) ? ' filtered-list-page' : ' vpo'; ?>">
					<div class="content-inner">
						<div class="page-content-container-inner flex">
							<?php
							switch($pageLayout) {
								case 'standard': ?>
									<?php custom_sidebar('sidebar-1', $sidebar_image['sizes']['medium']);  ?>
									<div class="page-content col-8">
										<?php the_content(); ?>
									</div>
								<?php
								break;
								case 'no-sidebar': ?>
									<div class="page-content col-10 full-width">
										<?php the_content(); ?>
									</div>
								<?php
								break;
								case 'full-width': ?>
									<div class="page-content col-10 full-width">
										<?php the_content(); ?>
									</div>
								<?php
								break;
							} ?>
						</div>
						<?php
						if ( !has_shortcode( $content, 'content-tabs' ) ) {
							echo content_tabs();
						} ?>
					</div>
				</div>
			</section>
			<?php

			if(get_the_id() == 12 || wp_get_post_parent_id(get_the_id()) == 12) {
				echo get_footer_cta('investor');
			} else if($footerCTA = get_field('footer_cta')) {
				echo get_footer_cta($footerCTA);
			}
		endwhile; // End of the loop.
		?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
