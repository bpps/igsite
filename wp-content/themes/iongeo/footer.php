<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Iongeo
 */

?>

	</div><!-- #content -->
	<?php
	if(!isset($_COOKIE['cookie_alert'])) { ?>
		<div id="cookie-alert" class="content-inner">
			<div id="cookie-alert-content">
				<div id="cookie-alert-text">
					<?php the_field('cookies_popup_text', 'option'); ?>
				</div>
				<a href="#" id="cookie-alert-cta">
					OK
				</a>
			</div>
		</div>
	<?php
	} ?>
	<footer id="colophon" class="site-footer">
		<div id="primary-footer">
			<div class="content-inner">
				<div class="flex row">
					<!-- <div id="newsletter-wrapper">
						<h6>Subscribe</h6>
						<iframe src="https://info.iongeo.com/l/50562/2019-01-11/bhl3wq" width="100%" height="108" type="text/html" scrolling="no" frameborder="0" allowTransparency="true" style="border: 0"></iframe>
					</div> -->
					<?php
					if($quicklinks = wp_get_nav_menu_items( 'quick-links' )) { ?>
						<div id="quick-links-wrapper">
							<h6>Quicklinks</h6>
							<div id="quick-links">
								<ul>
									<?php
									foreach($quicklinks as $ql) { ?>
										<li class="with-arrow">
											<a href="<?php echo $ql->url; ?>">
												<?php echo $ql->title; ?>
											</a>
										</li>
									<?php
									} ?>
								</ul>
							</div>
						</div>
					<?php
					} ?>
				</div>
			</div>
		</div>
		<div id="secondary-footer">
			<!-- .site-info -->
				<div id="footer-menu">
						<div class="content-inner">
								<div class="site-info">
									<img src="<?php echo get_template_directory_uri(); ?>/images/Logo_White.png"/>
									<div class="address">
										<p>2105 CityWest Blvd.,</p>
										<p>Suite 100,</p>
										<p>Houston, TX 77042-2855</p>
										<p>P: <a href="tel:+1 (281) 933-3339">+1 (281) 933-3339</a></p>
									</div>
								</div>
						<?php
						wp_nav_menu(array('menu' => 'footer-menu', 'container' => 'menu-footer-menu'));
						?>
			</div>
		</div>
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
