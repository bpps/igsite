<?php
/**
 * Iongeo functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Iongeo
 */

if ( ! function_exists( 'iongeo_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function iongeo_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on Iongeo, use a find and replace
		 * to change 'iongeo' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'iongeo', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		add_image_size( 'small', 200 );
		add_image_size( 'small-medium', 600 );
		add_image_size( 'medium', 800 );
		add_image_size( 'large', 1400 );
		add_image_size( 'extra-large', 1900 );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'iongeo' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'iongeo_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'iongeo_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function iongeo_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'iongeo_content_width', 640 );
}
add_action( 'after_setup_theme', 'iongeo_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function iongeo_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'iongeo' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'iongeo' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'iongeo_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function iongeo_scripts() {
	$manifest = json_decode(file_get_contents('dist/assets.json', true));
	$main = $manifest->main;

// 	wp_enqueue_script( 'jquery' );
// wp_enqueue_script( 'jquery-ui-core' );
// wp_enqueue_script('jquery-ui-accordion');

	wp_enqueue_style( 'iongeo-style', get_template_directory_uri() . $main->css, false, null );

	wp_enqueue_script('iongeo-js', get_template_directory_uri() . $main->js, [ 'jquery'  ], null, true);

	//wp_enqueue_script('masonry-js', 'https://unpkg.com/masonry-layout@4/dist/masonry.pkgd.min.js', null, true);
	wp_enqueue_script('cookie-js', get_template_directory_uri().'/js/jquery.cookie.js', null, true);

	wp_enqueue_script('masonry-js', get_template_directory_uri().'/js/masonry.pkgd.min.js', null, true);

	wp_enqueue_script('isotope-js', get_template_directory_uri().'/js/isotope.pkgd.min.js', null, true);

	wp_enqueue_script('packery-layout', get_template_directory_uri().'/js/packery-mode.pkgd.min.js', null, true);

	//wp_enqueue_style( 'slick-style', '//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css' );

	wp_enqueue_style( 'slick-style', get_template_directory_uri().'/js/slick.css' );

	//wp_enqueue_script( 'slick', '//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js' );

	// wp_enqueue_script( 'slick', get_template_directory_uri().'/js/slick.min.js' );

	//wp_enqueue_script( 'iongeo-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'iongeo-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	wp_enqueue_script( 'iongeo-responsive-tabs', get_template_directory_uri() . '/js/responsive-tabs.js', array(), false, true );


	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'iongeo_scripts' );

function enqueue_admin_after_plugins() {
  wp_enqueue_script('admin_script', get_template_directory_uri().'/js/admin.js', array('jquery'));
}

add_action( 'admin_enqueue_scripts', 'enqueue_admin_after_plugins' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Custom import for resources.
 */
require get_template_directory() . '/inc/custom-import.php';

/**
 * Custom sidebar.
 */
require get_template_directory() . '/inc/sidebar.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

/**
	* Ion Shortcodes.
	*/
require get_template_directory() . '/inc/ion-shortcodes.php';

if( function_exists('acf_add_options_page') ) {

	acf_add_options_page(array(
		'page_title' 	=> 'Settings',
		'menu_title'	=> 'Ion Settings',
		'menu_slug' 	=> 'ion-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false,
		'icon_url'		=> get_template_directory_uri().'/images/icons/info.png'
	));

	acf_add_options_sub_page(array(
		'page_title' 	=> 'Locations',
		'menu_title'	=> 'Locations',
		'parent_slug'	=> 'ion-settings',
	));

	acf_add_options_sub_page(array(
		'page_title' 	=> 'Social Media',
		'menu_title'	=> 'Social Media',
		'parent_slug'	=> 'ion-settings',
	));

	acf_add_options_sub_page(array(
		'title'      => 'Investor Settings',
		'parent'     => 'edit.php?post_type=investor-material',
		'capability' => 'manage_options'
	));

	acf_add_options_sub_page(array(
		'title'      => 'Data Library Settings',
		'parent'     => 'edit.php?post_type=data-library',
		'capability' => 'manage_options'
	));

	acf_add_options_sub_page(array(
		'title'      => 'Resource Settings',
		'parent'     => 'edit.php?post_type=resource',
		'capability' => 'manage_options'
	));

	acf_add_options_sub_page(array(
		'title'      => 'Events Settings',
		'parent'     => 'edit.php?post_type=event',
		'capability' => 'manage_options'
	));

	acf_add_options_sub_page(array(
		'title'      => 'Technology Settings',
		'parent'     => 'edit.php?post_type=technology',
		'capability' => 'manage_options'
	));

	acf_add_options_sub_page(array(
		'title'      => 'Insights Settings',
		'parent'     => 'edit.php',
		'capability' => 'manage_options'
	));

}

function slugify($text)
{
  $text = preg_replace('~[^\pL\d]+~u', '-', $text);
  $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
  $text = preg_replace('~[^-\w]+~', '', $text);
  $text = trim($text, '-');
  $text = preg_replace('~-+~', '-', $text);
  $text = strtolower($text);

  if (empty($text)) {
    return 'n-a';
  }

  return $text;
}

require get_template_directory() . '/inc/post-types/investor-functions.php';

require get_template_directory() . '/inc/post-types/data-library-functions.php';

require get_template_directory() . '/inc/post-types/resources-functions.php';

require get_template_directory() . '/inc/post-types/offerings-functions.php';

require get_template_directory() . '/inc/post-types/technology-functions.php';

require get_template_directory() . '/inc/post-types/events-functions.php';

require get_template_directory() . '/inc/post-types/insights-functions.php';

require get_template_directory() . '/inc/smart-crumbs.php';

require get_template_directory() . '/inc/menu-modules.php';

require get_template_directory() . '/inc/user_roles.php';

/**
	* URL Structure Functions.
	*/
require get_template_directory() . '/inc/url-structure-functions.php';

function term_link_filter( $url, $term, $taxonomy ) {
  switch($taxonomy) {
		case 'region':
			if($term->parent != 0) {
				$ancestors = get_ancestors($term->term_id, $taxonomy, 'taxonomy');
				$ancestors = array_reverse($ancestors);
				$termSlugs = [];
				foreach($ancestors as $anc) {
					$currentTerm = get_term_by('id', $anc, $taxonomy);
					array_push($termSlugs, $currentTerm->slug);
				}
				return str_replace( '%regions%' , 'data-library/'.implode('/', $termSlugs) , $url );
			} else {
				$url = str_replace( '%regions%' , 'data-library' , $url );
				return $url;
			}
		break;
		default:
			return $url;
		break;
	}
	return $url;
}
//add_filter('term_link', 'term_link_filter', 10, 3);

add_action('wp_head','pluginname_ajaxurl');
function pluginname_ajaxurl() { ?>
	<script type="text/javascript">
		var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
		var templateurl = '<?php echo get_template_directory_uri(); ?>';
		var homeurl = '<?php echo get_home_url(); ?>';
		var siteurl = '<?php echo site_url(); ?>';
	</script>
<?php
}

add_action('wp_ajax_nopriv_do_ajax', 'ajax_function');
add_action('wp_ajax_do_ajax', 'ajax_function');

function ajax_function(){
  switch($_REQUEST['fn']){
    case 'get_acf_tax':
      $taxID = $_REQUEST['tax'];
			$tax = get_term($taxID);
			if($tax) {
				echo json_encode(get_acf_tax($tax));
			}
			//json_encode(get_more_posts($paged));
	  break;
		case 'set_cookie':
			setcookie($_REQUEST['name'], '1', time()+60*60*24*365, "/");
			echo 1;
		break;
		case 'get_all_menu_slides':
			$menuObjs = (object)[];
			// Main menu
			$menus = ['main-menu', 'secondary-menu'];
			foreach($menus as $menu) {
				$menuItems = wp_get_nav_menu_items( $menu );
				foreach($menuItems as $menuItem) {
					if($menuContent = get_menu_modules($menuItem, $menuItems)) {
						$menuItemID = $menuItem->object_id;
						$menuObjs->$menuItemID = $menuContent;
					}
				}
			}
			echo json_encode($menuObjs);
		break;
		case 'get_menu_content':
			$menuItemID = $_REQUEST['menu_item'];
			$menu = $_REQUEST['menu'];
			$menuItem = false;
			$menuItems = wp_get_nav_menu_items( $menu );
			foreach($menuItems as $item) {
				if($item->ID == $menuItemID) {
					$menuItem = $item;
				}
			}
			echo get_menu_modules($menuItem, $menuItems);
			//echo json_encode($menuItem);
		break;
		case 'autosearch':
			$searchVal = $_REQUEST['search'];
			echo get_autosearch_results($searchVal);
		break;
		case 'onpagesearch':
			$searchVal = $_REQUEST['search'];
			$postType = $_REQUEST['posttype'];
			echo get_on_page_search_results($searchVal, $postType);
		break;
		case 'getsearch':
			$searchVal = $_REQUEST['search'];
			$paged = $_REQUEST['paged'];
			$args = array(
		    's' => $searchVal,
				'paged' => $paged,
		    'posts_per_page' => 6,
		    'post_type' => ['any'],
				'post_status' => 'publish'
		  );
			echo json_encode(get_list_posts($args));
		break;
		case 'send_collateral':
			$collateral = $_REQUEST['collateral'];
			$name = htmlspecialchars($_REQUEST['name']);
			$email = htmlspecialchars($_REQUEST['email']);
			echo json_encode(send_collateral($collateral, $name, $email));
		break;
		default:
			$output = 'nothing here';
			echo $output;
		break;
	}
	die();
}

function send_collateral($collateral, $name, $email) {
	$zip = new ZipArchive;
	$nameslug = slugify(urlencode($name));
	mkdir($nameslug);
	$zip->open($nameslug.'/ion-resources.zip', ZipArchive::CREATE);
	foreach($collateral as $file) {
		$filename = explode('/', $file);
		$filename = $filename[count($filename)-1];
		$zip->addFile($file, $filename);
	}
	$zipfilename = $zip->filename;
	$zip->close();
	$strTo = array ($email);
	$strSubject = 'ION Geo PDF Resources';
	$headers  = "From: Brandon <brandon@uncanny.studio> \n";
	$headers .= "Reply-To: Brandon <brandon@uncanny.studio> \n";
	$headers .= "MIME-Version: 1.0 \n";
	$headers .= "Content-type: text/html; charset=utf8 \n";
	$headers .= "Return-Path:brandon@uncanny.studio \n";
	$headers .= "To: ".$email." \n";
	$strMessage = $name.', </br> Attached you\'ll find the collateral you requested from ION Geo.</br>';
	if(wp_mail( $strTo, $strSubject, $strMessage, $headers, $zipfilename )) {
		// Remove tmp directory
		$it = new RecursiveDirectoryIterator($nameslug, RecursiveDirectoryIterator::SKIP_DOTS);
		$files = new RecursiveIteratorIterator($it,
		             RecursiveIteratorIterator::CHILD_FIRST);
		foreach($files as $file) {
		    if ($file->isDir()){
		        rmdir($file->getRealPath());
		    } else {
		        unlink($file->getRealPath());
		    }
		}
		rmdir($nameslug);
		return 1;
	} else {
		return 0;
	}
}

function get_acf_tax($tax) {

	return $tax;
}

function get_autosearch_results($val, $showArrows = true) {
	$args = array(
    's' => $val,
		'paged' => 1,
    'posts_per_page' => 6,
    'post_type' => ['any'],
		'post_status' => 'publish'
  );
	$posts = get_list_posts($args);
	if($posts['posts']) {
		ob_start(); ?>
		<div class="search-results-inner flex">
			<?php echo $posts['posts']; ?>
		</div>
		<?php
		if($posts['maxpages'] > 1 && $showArrows) { ?>
			<div class="search-list-arrows" data-search="<?php echo $val; ?>" data-paged="1">
				<a href="#" class="circle-arrow arrow-prev inactive" data-direction="-1" data-func="prev">
				</a>
				<a href="#" class="circle-arrow arrow-next" data-direction="1" data-max-pages="" data-func="next">
				</a>
			</div>
		<?php
		}
		return ob_get_clean();
	} else {
		return false;
	}


}
//remove_filter( 'the_content', 'wpautop' );
function get_on_page_search_results($search, $postType) {
	$args = [
		's' => $search,
		'post_type' => $postType,
		'posts_per_page' => 6,
		'post_status' => 'publish'
	];
	$posts = get_list_posts($args, 30);
	if($posts['posts']) {
		return $posts['posts'];
	} else {
		return 'Sorry, no results';
	}

}

// function atom_search_where($where){
// 	log_it($where);
//   global $wpdb;
//   if (is_search())
//     $where .= "OR (t.name LIKE '%".get_search_query()."%' AND {$wpdb->posts}.post_status = 'publish')";
//   return $where;
// }
//
// function atom_search_join($join){
//   global $wpdb;
//   if (is_search())
//     $join .= "LEFT JOIN {$wpdb->term_relationships} tr ON {$wpdb->posts}.ID = tr.object_id INNER JOIN {$wpdb->term_taxonomy} tt ON tt.term_taxonomy_id=tr.term_taxonomy_id INNER JOIN {$wpdb->terms} t ON t.term_id = tt.term_id";
//   return $join;
// }
//
// function atom_search_groupby($groupby){
//   global $wpdb;
//
//   // we need to group on post ID
//   $groupby_id = "{$wpdb->posts}.ID";
//   if(!is_search() || strpos($groupby, $groupby_id) !== false) return $groupby;
//
//   // groupby was empty, use ours
//   if(!strlen(trim($groupby))) return $groupby_id;
//
//   // wasn't empty, append ours
//   return $groupby.", ".$groupby_id;
// }
//
// add_filter('posts_where','atom_search_where');
// add_filter('posts_join', 'atom_search_join');
// add_filter('posts_groupby', 'atom_search_groupby');

function get_list_posts($args, $maxChars = 65) {
	$listPosts = new WP_Query($args);
	if($listPosts->have_posts()) :
		ob_start();
    while($listPosts->have_posts()): $listPosts->the_post(); ?>
			<div class="search-item col-4">
				<div class="search-item-content">
					<?php
					$postType = get_post_type();
					$postTypeObj = get_post_type_object($postType);
					//echo get_post_type();
					$label = $postType == 'page' ? get_the_title(wp_get_post_parent_id( get_the_ID() )) : $postTypeObj->labels->singular_name;
					switch(get_post_type()):
						case 'investor-material':
							$url = '';
							$urlType = '';
							if($downloadurl = get_field('upload_pdf')) {
								$url = $downloadurl['url'];
								$urlType = 'download';
							} else {
								$url = get_field('link');
								$urlType = 'posturl';
							} ?>
							<a class="search-item-flex" href="<?php echo $url; ?>">
								<span class="meta label">
									<?php
									$date = get_field('event_date') ? get_field('event_date') : get_the_date();
									$year = date("Y", strtotime($date));
									$month = date("M", strtotime($date));
									$day = date("d", strtotime($date));
									echo $year . "</br><span>" . $month.' '.$day.'</span>'; ?>
								</span>
								<p><?php echo character_limit(get_the_title(), $maxChars); ?></p>
								<span class="meta <?php echo $urlType; ?>">
									<img src="<?php echo get_template_directory_uri(); ?>/images/icons/down-arrow.svg" alt="download" class="download-icon" />
								</span>
							</a>
						<?php
						break;
						case 'investor-news':
							$url = '';
							if( $hosted_url = get_field('link') ) {
								$url = $hosted_url;
							} ?>
							<a class="search-item-flex i-news" href="<?php echo $url; ?>">
								<span class="meta label">
									<?php
									$date = get_field('last_updated') ? get_field('last_updated') : get_the_date();
									$year = date("Y", strtotime($date));
									$month = date("M", strtotime($date));
									$day = date("d", strtotime($date));
									echo $year . "</br><span>" . $month.' '.$day.'</span>'; ?>
								</span>
								<p><?php echo character_limit(get_the_title(), $maxChars); ?></p>
								<span class="meta posturl">
									<img src="<?php echo get_template_directory_uri(); ?>/images/icons/down-arrow.svg" alt="download" class="download-icon" />
								</span>
							</a>
						<?php
						break;
						case 'investor-event':
							$material = get_field('supporting_material'); ?>
							<div class="search-item-flex">
								<span class="meta label">
									<?php
									$date = get_field('event_date', get_the_ID());
									$format_in = 'd/m/Y g:i a'; // the format your value is saved in (set in the field options)
									$format_out = 'Y </br><span> M, d </span>'; // the format you want to end up with
									$eventDate = DateTime::createFromFormat($format_in, $date);
									$year = $eventDate->format( 'Y' );
									$month = $eventDate->format( 'M' );
									$day = $eventDate->format( 'd' );
									echo $year . "</br><span>" . $month.' '.$day.'</span>'; ?>
								</span>
								<p>
									<?php
									if($material) { ?>
										<a href="<?php echo $material[0]['url']; ?>" target="_blank">
									<?php
									}
											echo character_limit(get_the_title(), $maxChars);
									if($material) { ?>
										</a>
									<?php
									} ?>
								</p>
								<?php
								if($webcast = get_field('investor_webcast')) { ?>
									<span class="meta download">
										<a href="<?php echo $webcast['url']; ?>" target="_blank">Webcast</a>
									</span>
								<?php
								} else if ($material) { ?>
									<span class="meta download">
										<a href="<?php echo $material[0]['url']; ?>" target="_blank"></a>
									</span>
								<?php
								} ?>
							</div>
						<?php
						break;
						case 'sec-filing': ?>
							<div class="search-item-flex">
								<?php
								if($formType = get_field('sec_form_type')) { ?>
									<span class="meta label">
										form </br> <?php echo $formType; ?>
									</span>
								<?php
								} ?>
								<span class="meta label">
									<?php
									$date = get_the_date();
									$year = date("Y", strtotime($date));
									$month = date("M", strtotime($date));
									$day = date("d", strtotime($date));
									echo $year . "</br><span>" . $month.' '.$day.'</span>'; ?>
								</span>
								<p><?php echo character_limit(get_the_title(), $maxChars); ?></p>
								<?php
								if($secFormats = get_field('sec_formats')) {
									foreach($secFormats as $format) {
										if($format['format'] == 'html') {
											continue;
										} ?>
										<span class="meta <?php echo $format['format']; ?>">
											<a href="<?php echo $format['url']; ?>" target="_blank">
												<img alt="download link" src="<?php echo get_template_directory_uri().'/images/download-'.strtolower($format['format']).'.png'; ?>"/>
											</a>
										</span>
									<?php
									}
								} ?>
							</div>
						<?php
						break;
						case 'event': ?>
							<a class="search-item-flex" href="<?php the_permalink(); ?>">
								<span class="meta label">
									<?php
									$date = get_field('event_date', get_the_ID());
									$year = date("Y", strtotime($date));
									$month = date("M", strtotime($date));
									$day = date("d", strtotime($date));
									echo $year . "</br><span>" . $month.' '.$day.'</span>'; ?>
								</span>
								<p><?php echo character_limit(get_the_title(), $maxChars); ?></p>
								<span class="meta posturl">
									<img src="<?php echo get_template_directory_uri(); ?>/images/icons/down-arrow.svg" alt="download" class="download-icon" />
								</span>
							</a>
						<?php
						break;
						case 'resource':
							$url = '';
							$urlType = '';
							if($downloadurl = get_field('upload_pdf')) {
								$url = $downloadurl['url'];
								$urlType = 'download';
							} else {
								$url = get_the_permalink();
								$urlType = 'posturl';
							} ?>
							<a class="search-item-flex" href="<?php echo $url; ?>">
								<span class="meta label">
									<?php
									$date = get_the_date();
									$year = date("Y", strtotime($date));
									$month = date("M", strtotime($date));
									$day = date("d", strtotime($date));
									echo $year . "</br><span>" . $month.' '.$day.'</span>'; ?>
								</span>
								<p><?php echo character_limit(get_the_title(), $maxChars); ?></p>
								<span class="meta <?php echo $urlType; ?>">
									<img src="<?php echo get_template_directory_uri(); ?>/images/icons/down-arrow.svg" alt="download" class="download-icon" />
								</span>
							</a>
						<?php
						break;
						default: ?>
							<a class="search-item-flex" href="<?php the_permalink(); ?>">
								<span class="meta label"><?php echo $label; ?></span>
								<p><?php echo character_limit(get_the_title(), $maxChars); ?></p>
								<span class="meta posturl">
									<img src="<?php echo get_template_directory_uri(); ?>/images/icons/down-arrow.svg" alt="download" class="download-icon" />
								</span>
							</a>
						<?php
						break;
					endswitch; ?>
				</div>
			</div>
    <?php
    endwhile;
		$posts = [
			'maxpages' => $listPosts->max_num_pages,
			'posts' => ob_get_clean()
		];
    wp_reset_postdata();
		return $posts;
  endif;
	return;
}
function wpb_image_editor_default_to_gd( $editors ) {
    $gd_editor = 'WP_Image_Editor_GD';
    $editors = array_diff( $editors, array( $gd_editor ) );
    array_unshift( $editors, $gd_editor );
    return $editors;
}
add_filter( 'wp_image_editors', 'wpb_image_editor_default_to_gd' );

// Make Taxonomy description WYSIWYG

function add_form_fields_example($term, $taxonomy){ ?>
  <tr valign="top">
      <th scope="row">Description</th>
      <td>
          <?php wp_editor(html_entity_decode($term->description), 'description', array('media_buttons' => false)); ?>
          <script>
              jQuery(window).ready(function(){
                  jQuery('label[for=description]').parent().parent().remove();
              });
          </script>
      </td>
  </tr>
<?php
}
//add_action("data-type_edit_form_fields", 'add_form_fields_example', 10, 2);
//add_action("offering-type_edit_form_fields", 'add_form_fields_example', 10, 2);

// Remove Description from taxonomy
function remove_taxonomy_description($columns){
	unset($columns['description']);
 	return $columns;
}
add_filter('manage_edit-data-type_columns','remove_taxonomy_description');
add_filter('manage_edit-offering-type_columns','remove_taxonomy_description');

function acf_load_cta_options( $field ) {
  // reset choices
  $field['choices'] = array();
  // if has rows
  if( have_rows('learn_more', 'option') ) {
    // while has rows
    while( have_rows('learn_more', 'option') ) {
      // instantiate row
      the_row();
      // vars
      $value = slugify(get_sub_field('cta_type'));
      $label = get_sub_field('cta_type');
      // append to choices
      $field['choices'][ $value ] = $label;
    }
  }
  // return the field
  return $field;
}

add_filter('acf/load_field/name=footer_cta', 'acf_load_cta_options');

function get_footer_cta($cta) {
	$footerCTAs = get_field('learn_more', 'option');
	foreach($footerCTAs as $footerCTA) {
		if(slugify($footerCTA['cta_type']) == slugify($cta)) {
			ob_start();
			$image = $footerCTA['image']['sizes']['large']; ?>
				<section id="learn-more-footer" class="bg-centered" style="background-image:url(<?php echo $image; ?>);">
					<div id="learn-more-content" class="content-inner<?php echo !isset($footerCTA['link']) ? ' no-link' : ''; ?>">
						<div class="flex">
							<?php
							if(isset($footerCTA['text'])) { ?>
								<div class="learn-more-text col-6 stretch">
									<?php echo $footerCTA['text']; ?>
								</div>
							<?php
							}
							if($footerCTA['link']) { ?>
								<div class="learn-more-link col-6">
									<a class="ion-cta" href="<?php echo $footerCTA['link']['url']; ?>"><?php echo $footerCTA['link']['title']; ?></a>
								</div>
							<?php
							}
							if($links = $footerCTA['links']) { ?>
								<div class="learn-more-links-container col-6">
									<div class="learn-more-links">
										<?php foreach( $links as $link ) { ?>
											<a class="ion-cta" href="<?php echo $link['link']['url']; ?>"><?php echo $link['link']['title']; ?></a>
										<?php } ?>
									</div>
								</div>
							<?php
							} ?>
						</div>
					</div>
				</section>
			<?php
			return ob_get_clean();
		}
	}
	return;
}

function get_share_link($title, $link, $login = false) {
	ob_start(); ?>
	<section id="share-post-screen">
		<div id="share-post-container">
			<h2>Share this page with someone.</h2>
			<div id="share-post-links">
				<a href="http://www.facebook.com/sharer.php?u=https://iongeo.com" target="_blank">
		    	<img src="<?php echo get_template_directory_uri().'/images/icons/facebook.png'; ?>" alt="Facebook" />
		    </a>
				<a href="https://twitter.com/share?url=<?php echo $link; ?>&amp;text=<?php echo $title; ?>&amp;hashtags=simplesharebuttons" target="_blank">
		    	<img src="<?php echo get_template_directory_uri().'/images/icons/twitter.png'; ?>" alt="Twitter" />
		    </a>
				<a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=<?php echo $link; ?>" target="_blank">
		    	<img src="<?php echo get_template_directory_uri().'/images/icons/linkedin.png'; ?>" alt="LinkedIn" />
		    </a>
				<a href="javascript:;" onclick="window.print()">
		    	<img src="<?php echo get_template_directory_uri().'/images/icons/printer.png'; ?>" alt="Print" />
		    </a>
				<?php
				$title = rawurlencode($title);
				$link = urlencode($link);
				$email_link = htmlspecialchars('mailto:?Subject=' . $title . '&Body=' . $title . ' -- ' . $link, ENT_COMPAT,'ISO-8859-1', true); ?>
				<a href="<?php echo $email_link; ?>">
		    	<img src="<?php echo get_template_directory_uri().'/images/icons/email.png'; ?>" alt="Email" />
		    </a>
				<button onclick="copyPermalink()">
		    	<img src="<?php echo get_template_directory_uri().'/images/icons/link_round.png'; ?>" alt="Copy To Clipboard" />
					<input id="share-link-url" type="text" hidden="true" value="<?php echo $link; ?>"/>
		    </button>
			</div>
		</div>
	</section>
	<div class="share-post-link-wrapper">
		<div class="share-post-link-container">
			<a href="#" class="share-post-link">
				<span>Share</span>
				<div class="share-post-x">
					<img alt="close share" src="<?php echo get_template_directory_uri(); ?>/images/icons/x_white.png;"/>
				</div>
			</a>
			<?php
			if($login) { ?>
				<a class="ion-login-link" target="_blank" href="https://www.ionspanviewer.com">Login</a>
			<?php
			} ?>
		</div>
	</div>
	<script>
		function copyPermalink() {
			jQuery('#share-link-url').attr('hidden', false);
			var copyText = document.getElementById("share-link-url");
			copyText.select();
			document.execCommand("copy");
			jQuery('#share-link-url').attr('hidden', true);
			alert("Copied to clipboard: " + copyText.value);
		}
	</script>
	<?php
	return ob_get_clean();
}

add_action( 'template_redirect', 'se219663_template_redirect' );

function se219663_template_redirect()
{
  global $wp_rewrite;

  if ( is_search() && ! empty ( $_GET['s'] )  )
  {
    $s         = sanitize_text_field( $_GET['s'] ); // or get_query_var( 's' )
    $location  = home_url().'/?search='.$s;
    wp_safe_redirect( $location, 301 );
    exit;
  }
}

function remove_protected_title_prefix() {
	return '%s';
}

add_filter( 'protected_title_format', 'remove_protected_title_prefix', 10, 3);

function custom_password_form($post) {
  $post = get_post( $post );
  $label = 'pwbox-' . ( empty($post->ID) ? rand() : $post->ID );
  $output = '<div class="content-inner"><form action="' . esc_url( site_url( 'wp-login.php?action=postpass', 'login_post' ) ) . '" class="post-password-form" method="post">
  <p>' . __( 'This content is password protected. To view it please enter your password below' ) . '</p>
  <div id="password-form-fields"><input name="post_password" id="' . $label . '" placeholder="password" type="password" size="20" /> <input type="submit" name="Submit" value="' . esc_attr_x( 'Enter', 'post password form' ) . '" /></div></form></div>
  ';
  return $output;
}

add_filter( 'the_password_form', 'custom_password_form', 10, 3);

function add_slug_body_class( $classes ) {
global $post;
if ( isset( $post ) ) {
$classes[] = $post->post_type . '-' . $post->post_name;
}
if ( !wp_is_mobile() ) {
    array_push($classes, 'is_desktop');
  } else {
    array_push($classes, 'is_mobile');
  }
	global $is_lynx, $is_gecko, $is_IE, $is_edge, $is_opera, $is_NS4, $is_safari, $is_chrome, $is_iphone;
  if($is_lynx) $classes[] = 'lynx';
  elseif($is_gecko) $classes[] = 'gecko';
  elseif($is_opera) $classes[] = 'opera';
  elseif($is_NS4) $classes[] = 'ns4';
  elseif($is_safari) $classes[] = 'safari';
  elseif($is_chrome) $classes[] = 'chrome';
  elseif($is_IE || $is_edge) {
          $classes[] = 'ie';
					if (strpos($_SERVER['HTTP_USER_AGENT'], 'Trident/7.0; rv:11.0') !== false) {
						$classes[] = 'ie11';
					}
					if (strpos($_SERVER['HTTP_USER_AGENT'], 'Trident/7.0; Touch; rv:11.0') !== false) {
						$classes[] = 'ie11';
					}
  } else $classes[] = 'unknown';
  if($is_iphone) $classes[] = 'iphone';
  if ( stristr( $_SERVER['HTTP_USER_AGENT'],"mac") ) {
           $classes[] = 'osx';
     } elseif ( stristr( $_SERVER['HTTP_USER_AGENT'],"linux") ) {
           $classes[] = 'linux';
     } elseif ( stristr( $_SERVER['HTTP_USER_AGENT'],"windows") ) {
           $classes[] = 'windows';
     }
	return $classes;
}
add_filter( 'body_class', 'add_slug_body_class' );

function pan_zoom_data_block() {
    wp_register_script(
        'pan-zoom-data-script',
        get_template_directory_uri().'/js/pan-zoom-block.js',
        array( 'wp-blocks', 'wp-components', 'wp-element', 'wp-i18n', 'wp-editor' )
    );

    register_block_type( 'pan-zoom-data-block/pan-zoom-data', array(
        'editor_script' => 'pan-zoom-data-script',
    ) );
}
add_action( 'init', 'pan_zoom_data_block' );

function image_comparison_block() {
    wp_register_script(
        'image-comparison-script',
        get_template_directory_uri().'/js/image-comparison-block.js',
        array( 'wp-blocks', 'wp-components', 'wp-element', 'wp-i18n', 'wp-editor' )
    );

    register_block_type( 'image-comparison-block/image-comparison', array(
        'editor_script' => 'image-comparison-script',
    ) );
}
add_action( 'init', 'image_comparison_block' );
