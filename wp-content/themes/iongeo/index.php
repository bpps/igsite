<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Iongeo
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<?php
			$pageLayout = get_field('page_layout');
			$header = ion_page_header(['frompage' => 'index']);
			echo $header->header; ?>
			<?php echo get_share_link('ION Geo | Insights', get_the_permalink()); ?>
			<section class="page-content-wrapper">
				<div class="page-content-container full-width-page">
					<div class="content-inner">
						<div class="page-content-container">
							<div class="page-content">
								<?php echo display_insights(); ?>
							</div>
						</div>
					</div>
				</div>
			</section>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
