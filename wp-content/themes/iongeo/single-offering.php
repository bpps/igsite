<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Iongeo
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

			<?php
			while ( have_posts() ) :
				the_post();
				$header = ion_page_header(['frompage' => 'single offering']);
				$content = get_the_content();
				$sidebar_image = get_field('sidebar_image') ? get_field('sidebar_image') : false;
				echo $header->header;
				echo get_share_link('ION Geo | '.get_the_title(), get_the_permalink()); ?>
				<section class="page-content-wrapper content-inner<?php echo is_active_sidebar( 'location-sidebar' ) ? ' has-sidebar' : ''; ?>">
					<div class="page-content-container <?php echo $header->image ? 'has-header-image ' : ''; ?>flex">
						<div class="page-content col-8">
							<?php the_content(); ?>
						</div>
						<?php
						if ( is_active_sidebar( 'offerings-sidebar' ) ) :
							custom_sidebar('offerings-sidebar', $sidebar_image['sizes']['medium']);
						endif; ?>
					</div>
					<?php
					if ( !has_shortcode( $content, 'content-tabs' ) ) {
						echo content_tabs();
					} ?>
				</section>
				<?php
				$cArgs = [
					'post_parent' => get_the_ID(),
					'post_type' => 'offering',
					'numberposts' => -1,
					'post_status' => 'publish'
				];
				if($children = get_children($cArgs)) { ?>
					<div id="offering-type-children">
						<div class="content-inner">
							<?php
							foreach($children as $child) {
								$termObj = (object)[
									'title' => $child->post_title,
									'link' => get_permalink($child->ID)
								];
								$termObj->image = get_template_directory_uri().'/images/post-placeholder.png';
								if(has_post_thumbnail($child->ID)) {
									$termObj->image = get_the_post_thumbnail_url($child->ID, 'small-medium');
								}
								if(has_excerpt($child->ID)) {
									$termObj->desc = get_the_excerpt($child->ID);
								}
								echo create_child_offering($termObj);
					 		} ?>
						</div>
					</div>
				<?php
				}
				if($insights = get_field('post_x_offering')) {
					echo get_related_insights($insights);
				}
				if($oTypes = get_the_terms(get_the_ID(), 'offering-type')) {
					$oTypeArr = [];
					foreach($oTypes as $oType) {
						array_push($oTypeArr, $oType->term_id);
					}
					$oArgs = [
						'post_type' => 'offering',
						'posts_per_page' => 8,
						'post__not_in' => [get_the_ID()],
						'tax_query' => [
							[
								'taxonomy' => 'offering-type',
								'field' => 'term_id',
								'terms' => $oTypeArr
							]
						]
					];
					echo get_related_offerings($oArgs);
					$techArgs = [
						'post_type' => 'technology',
						'posts_per_page' => 3,

					];
					if($manualTechs = get_field('offering_x_technology', get_the_id())) {
						$techArray = [];
						foreach($manualTechs as $tech) {
							array_push($techArray, $tech);
						}
						$techArgs['post__in'] = $techArray;
						echo get_related_technologies($techArgs);
					}
				}
				// $terms = get_terms(['taxonomy' => 'software']);
				// echo get_term_link($terms[0], 'software');
				echo get_footer_cta('default');
			endwhile; // End of the loop. ?>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
