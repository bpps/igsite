<?php
/**
 * The template for displaying Event archive page
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Iongeo
 */

 get_header();
 ?>

 	<div id="primary" class="content-area">
 		<main id="main" class="site-main">
 			<header id="page-header">
 				<div id="page-header-title" class="content-inner">
          <div class="page-header-title-content">
   					<h1>Events</h1>
          </div>
 				</div>
 			</header>
      <?php
      $postType = get_queried_object();
      echo get_share_link('ION Geo | '.$postType->label, get_post_type_archive_link( $postType->name )); ?>
 			<section class="page-content-wrapper">
 				<div class="page-content-container has-sidebar full-width-page">
          <div class="content-inner">
            <div id="events-page-container" class="has-side-scroll">
              <?php side_scroll_nav('events'); ?>
              <?php echo display_events(); ?>
            </div>
          </div>
 				</div>
 			</section>

 		</main><!-- #main -->
 	</div><!-- #primary -->

 <?php
 get_footer();
