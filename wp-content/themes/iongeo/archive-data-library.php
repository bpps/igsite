<?php
/**
 * The template for displaying Data Library page
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Iongeo
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<header id="page-header">
				<div id="page-header-title" class="content-inner">
					<div class="page-header-title-content">
						<h1>Data Library</h1>
						<div class="data-library-search on-page-search-bg">
	          </div>
	          <div class="on-page-search-container" data-posttype="data-library">
	            <div class="on-page-search-icon">
	            </div>
	            <input class="on-page-search" placeholder="Search Locations"/>
	            <div class="on-page-search-results-container">
	            </div>
	          </div>
					</div>
				</div>
			</header>
			<?php
      $postType = get_queried_object();
      echo get_share_link('ION Geo | '.$postType->label, get_post_type_archive_link( $postType->name ), true); ?>
			<section class="page-content-wrapper">
				<div class="page-content-container full-width-page">
					<div class="content-inner">
						<div class="page-content-container">
							<div class="page-content">
								<?php
								$mapObj = (object)[
									'token' => '',
									'slug' => ''
								];
								echo get_data_library_map(); ?>
							</div>
						</div>
					</div>
				</div>
			</section>
			<?php
			echo get_footer_cta('default'); ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
